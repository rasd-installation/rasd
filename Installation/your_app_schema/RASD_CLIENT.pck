create or replace package RASD_CLIENT is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASD_CLIENT generated on 07.01.23 by user RASDCLI.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*-----RASD_CLIENT SETTINGS--------------------------------------------------------------------------*/
/*-----THIS CHANGES EFFECTS ALL RASD PROGRAMS USING THIS CLIENT!!!!!---------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  
  
  e_finished exception;

  type dt is varray(2) of varchar2(30);
  type dttab is table of dt;
  c_date_transform constant dttab := dttab(
      -- PL/SQL type , jQuery type : format is case sensitive
      dt('yyyy-mm-dd','yy-mm-dd') ,
      dt('yyyy/mm/dd','yy/mm/dd') ,
      dt('dd.mm.yyyy','dd.mm.yy') ,
      dt('dd/mm/yyyy', 'dd/mm/yy') ,
      dt('dd-mm-yyyy', 'dd-mm-yy') ,
      dt('mm/dd/yyyy', 'mm/dd/yy') ,
      -- default type NULL must always be the last element
      dt('NULL','mm/dd/yy')
  );
  type dtt is varray(2) of varchar2(30);
  type dtttab is table of dtt;
  c_time_transform constant dtttab := dtttab(
      -- PL/SQL type , jQuery type : ime format is case sensitive
      dtt('hh24:mi','hh:mm') ,
      dtt('hh24:mi:ss','hh:mm:ss'),
      -- default type NULL must always be the last element
      dtt('NULL','hh:mm')
  );

  C_DATE_FORMAT constant varchar2(10) := 'mm/dd/yyyy';
  C_TIMESTAMP_FORMAT constant varchar2(30) := 'mm/dd/yyyy hh24:mi:ss.ff';  
  C_NUMBER_DECIMAL constant varchar2(1) := '.';
  C_NUMBER_THOUSAND constant varchar2(1) := ',';
  C_NLS_DATE_LANGUAGE constant varchar2(20) := 'AMERICAN';
  C_NLS_LANGUAGE constant varchar2(20) := 'AMERICAN'; 

  c_HtmlJSLibraryFile varchar2(30) := 'rasd/rasd_jslib_blue.html';
  --c_HtmlJSLibraryFile varchar2(30) := 'rasd/rasd_jslib.html';
  c_HtmlDataTableFile varchar2(30) := 'rasd/rasd_datatable.html';
  c_HtmlHtmlDatePickerFile varchar2(30) := 'rasd/rasd_datepicker.html';
  c_HtmlFooterFile varchar2(30) := 'rasd/rasd_footer.html';
  c_HtmlDataTable2File varchar2(30) := 'rasd/rasd_datatable2.html';
  c_HtmlHeaderFile  varchar2(30) := 'rasd/rasd_header.html';

  c_DOC_ACCESS_PATH varchar2(30) := '/rasdlib/docs'; --

  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2;
  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2;
  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2;

  function varchr2number(p_value varchar2) return number;

  procedure sessionStart;
  procedure sessionSetValue(pname varchar2, pvalue varchar2);
  function  sessionGetValue(pname varchar2) return varchar2;
  procedure sessionClose;

  function getHtmlMenuList(p_formname varchar2) return varchar2;

  /*Security functions*/
  function secGetUsername return varchar2;
  function secGetLOB return varchar2;
  function secGet_HTTP_ACCEPT return varchar2;
  function secGet_HTTP_ACCEPT_ENCODING return varchar2;
  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2;
  function secGet_HTTP_ACCEPT_CHARSET return varchar2;
  function secGet_HTTP_HOST return varchar2;
  function secGet_HTTP_PORT return varchar2;
  function secGet_HTTP_USER_AGENT return varchar2;
  function secGet_PATH_INFO return varchar2;
  function secGet_PATH_ALIAS return varchar2;
  function secGet_REMOTE_ADDR return varchar2;
  function secGet_REQUEST_CHARSET return varchar2;
  function secGet_SCRIPT_NAME return varchar2;      

  procedure secCheckPermission(p_form varchar2, p_action varchar2);
  procedure secCheckCredentials(p_username varchar2, p_password varchar2, p_other varchar2 default null);
  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr);
  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 );

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  

/*IF you generated RASD_CLIENT make sure that your enviorment setting's for NLS settings are correct or add belowe code in BODY execution part*/

/*

BEGIN
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS= '''||C_NUMBER_DECIMAL||''||C_NUMBER_THOUSAND||''' ';
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_LANGUAGE = '''||C_NLS_DATE_LANGUAGE||''' ';
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LANGUAGE = '''||C_NLS_LANGUAGE||''' ';   
end RASD_CLIENT

*/

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  

end;
/

create or replace package body RASD_CLIENT is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASD_CLIENT generated on 07.01.23 by user RASDCLI.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  PAGE                          varchar2(4000);
  WARNING                       varchar2(4000);
  B10OUT                        ctab;
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*-----RASD_CLIENT SETTINGS--------------------------------------------------------------------------*/
/*-----THIS CHANGES EFFECTS ALL RASD PROGRAMS USING THIS CLIENT!!!!!---------------------------------*/
/*---------------------------------------------------------------------------------------------------*/


  function Blob2Clob(B BLOB) 
return clob is 
c clob;
n number;
v varchar2(32767 CHAR); 
begin 
if (b is null) then 
return null;
end if;
if (length(b)=0) then
return empty_clob(); 
end if;
dbms_lob.createtemporary(c,true);
n:=1;

while (n+32767<=length(b)) loop
dbms_lob.writeappend(c,32767,utl_raw.cast_to_varchar2(dbms_lob.substr(b,32767,n)));
n:=n+32767;
end loop;
v := utl_raw.cast_to_varchar2(dbms_lob.substr(b,length(b)-n+1,n));

dbms_lob.writeappend(c,length(v),v);
return c;
end;


  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2 is
    v_t varchar(32000);
    v_root varchar2(200);
    x blob;
  begin
    v_root := OWA_UTIL.get_cgi_env('DOC_ACCESS_PATH');
  
    select blob_content into x
    from documents where name = c_HtmlJSLibraryFile;
    v_t := Blob2Clob(x);
    
    if v_root is null then
       v_t := replace(v_t,'docs/',c_DOC_ACCESS_PATH||'/');
    end if;    
    
    return replace(replace(v_t, ':NAME', name),':VALUE',value);
    exception when others then return '';
  end;

  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from documents where name = c_HtmlHeaderFile;
    v_t := Blob2Clob(x);
    select blob_content into x
    from documents where name = c_HtmlDataTable2File;
    v_t := v_t || Blob2Clob(x);

    v_t := replace(v_t , ':VALUE' , value);
    return replace(v_t, ':NAME', name);
    exception when others then return '';
  end;

  function getHtmlDataTable (name varchar2, value varchar2 default '') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from documents where name = c_HtmlDataTableFile;
    v_t := Blob2Clob(x);
    v_t := replace(v_t, ':NAME', name);
    v_t := replace(v_t, ':VALUE', value);
    return v_t;
    exception when others then return '';
  end;

  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2 is
     v_t varchar(32000);
    v_fd varchar2(100);
    v_ft varchar2(100);
    v_p varchar2(1000);
    v_format varchar2(1000) := format;
    x blob;
  begin
    if v_format is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';
    else
      FOR i IN c_date_transform.FIRST .. c_date_transform.LAST
      LOOP
        if instr( nvl(v_format,'NULL') , c_date_transform(i)(1)) > 0 then
           v_fd := c_date_transform(i)(2);
           v_format := replace(v_format, c_date_transform(i)(1) , '');
           exit;
        end if;   
      END LOOP;
      if v_fd is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
      end if;
      if v_format is not null then
      FOR i IN c_time_transform.FIRST .. c_time_transform.LAST
      LOOP
        if instr( nvl(v_format,'NULL') , c_time_transform(i)(1)) > 0 then
           v_ft := c_time_transform(i)(2);
           v_format := replace(v_format, c_time_transform(i)(1) , '');
           exit;
        end if;   
      END LOOP;
      if v_ft is null then
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';        
      end if;
      if trim(v_format) is not null then 
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';           
      end if;  
      else
        v_p := ', showTimepicker: false';                   
      end if;    
    end if;
       
    select blob_content into x 
    from documents where name = c_HtmlHtmlDatePickerFile;
        v_t := Blob2Clob(x);
    return replace(replace(replace(replace(v_t, ':NAME', name), ':DFORMAT', v_fd),':TFORMAT', v_ft),':PROPERTIES', v_p);
  exception when others then return '';
  end;  

  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2 is
    v_t varchar(32000);
    x blob;
  begin
    select blob_content into x 
    from documents where name = c_HtmlFooterFile;
        v_t := Blob2Clob(x);
    return replace(replace(replace(v_t, ':VERSION', version), ':PROGRAM', program),':USER', user);
    exception when others then return '';
  end; 

 function varchr2number(p_value varchar2) return number is
    v_dot   number;
    v_comma number;
    v_value varchar2(30) := p_value;
    error exception;
  begin
    if p_value is not null then
      v_dot := instr(v_value, C_NUMBER_THOUSAND);
      if v_dot = 0 then
        return to_number(v_value);
      else
        while v_dot <> 0 Loop
          v_value := substr(v_value, v_dot + 1);
          v_dot   := instr(v_value, C_NUMBER_THOUSAND);
          v_comma := instr(v_value, C_NUMBER_DECIMAL);
          if v_dot <> 0 then
            if v_dot <> 4 then
              raise error;
            end if;
          else

            if v_comma <> 0 then
              if v_comma <> 4 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '')));
              end if;
            else
              if length(v_value) <> 3 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '')));
              end if;
            end if;
          end if;
        end loop;
      end if;
    else
      return(to_number(null));
    end if;
  end;

  procedure sessionStart is
  begin
    owa_util.mime_header('text/html', FALSE);
  end;

  procedure sessionSetValue(pname varchar2, pvalue varchar2) is
  begin
    OWA_COOKIE.SEND('rasd$'||pname, UTL_URL.ESCAPE(pvalue, false, 'UTF8') , null,null);
  end;

  function sessionGetValue(pname varchar2) return varchar2 is
     vc OWA_COOKIE.cookie;
  begin
     vc := owa_cookie.get('rasd$'||pname);
     return UTL_URL.UNESCAPE(vc.vals(1),'UTF8');
  exception when others then
    return '';
  end;

  procedure sessionclose is
  begin
     owa_util.http_header_close;
  end;

  function getHtmlMenuList(p_formname varchar2) return varchar2 is
    v_menu varchar2(32000) := '';
    v_js_menu varchar2(32000) := '';
    v__ varchar2(32000) := '';
    v_lobid varchar2(100);
  begin


select max(substr(s.text,instr(s.text,'<lobid>')+7, instr(s.text,'</lobid>')-instr(s.text,'<lobid>')-7 )) into v_lobid
from user_source s 
where s.name = replace(p_formname,'_MENU')
 and s.text like '%<lobid>%'
;
  
for r in (
select distinct nvl(application,'NoN') application, vr from (
select '' application, 2 vr from dual
union
select substr(s.text, instr(s.text,'</compileyn><application>')+25 , instr(s.text,'</application>') - instr(s.text,'</compileyn><application>')-25 ) application , 1 vr
from user_source s where s.text like '%<compiledInfo><info>%' and s.text like '%</compileyn><application>%'
and type = 'PACKAGE BODY'
and  substr(s.text, instr(s.text,'</compileyn><application>')+25 , instr(s.text,'</application>') - instr(s.text,'</compileyn><application>')-25 ) is not null
and  substr(s.text,instr(s.text,'<lobid>')+7, instr(s.text,'</lobid>')-instr(s.text,'<lobid>')-7 ) = v_lobid
)
order by vr , application
  )  loop


      v__ := '';
    for r1 in (
select '!'||lower(object_name)||'.webclient' url, object_name  , y.application , z.label
from user_objects x,
(
select s.name , substr(s.text, instr(s.text,'</compileyn><application>')+25 , instr(s.text,'</application>') - instr(s.text,'</compileyn><application>')-25 ) application 
from user_source s where s.text like '%<compiledInfo><info>%' and s.text like '%</compileyn><application>%'
and type = 'PACKAGE BODY'
and (name <> 'RASD_CLIENT' or name = 'RASD_CLIENT' and substr(s.text, instr(s.text,'</compileyn><application>')+25 , instr(s.text,'</application>') - instr(s.text,'</compileyn><application>')-25 ) is not null )  
and substr(s.text,instr(s.text,'<lobid>')+7, instr(s.text,'</lobid>')-instr(s.text,'<lobid>')-7 ) = v_lobid
) y,
(
select s.name , replace(substr(s.text, instr(s.text,'</user><label><![CDATA[')+23 , instr(s.text,']]></label>') - instr(s.text,'</user><label><![CDATA[')-23 ),'''''''''','''') label 
from user_source s where s.text like '%</user><label><![CDATA[%' 
and type = 'PACKAGE BODY' 
and (name <> 'RASD_CLIENT' or name = 'RASD_CLIENT' and replace(substr(s.text, instr(s.text,'</user>
<label><![CDATA[')+23 , instr(s.text,']]></label>') - instr(s.text,'</user><label><![CDATA[')-23 ),'''''''''','''') like '%RASD_CLIENT%' )  
) z
where 
x.object_type = 'PACKAGE BODY'
and x.status = 'VALID'
and x.object_name = y.name(+)
and x.object_name = z.name(+)
and nvl(y.application,'NoN') = r.application
and exists (
select 1 from user_source s where s.text like '%procedure webclient%'
and type = 'PACKAGE BODY'
and x.object_name = s.name
)
order by object_name    
) loop
      v__ := v__ || '<li><a href="'||r1.url||'">'||r1.object_name||' - '||r1.label||'</a></li>
';
  end loop;
  
  
    v_menu := v_menu || '
      <li>
	  <a href="#">'||replace(r.application,'NoN','Unallocated forms ')||'</a>
      <ul >
         '||v__||'
      </ul>
      </li>
';  
  v_js_menu :=  v_js_menu ||'$( "#menu_'||replace(replace(replace(replace(replace(r.application,' ',''),'''',''''''),'&',''),';',''),'.','dot')||'" ).menu();';

end loop;  
  
    v_menu := '
<ul id="menu">
<il><a href="#">Menu</a>
<ul>
         '||v_menu||'
</ul>		 
</il>		 
</ul>
  <script>
  $( function() {
    $( "#menu" ).menu();
  } );  
  </script>
  <style>
    .ui-menu { padding: 0px 10px 0px 10px; white-space: nowrap;} 
	.ui-menu-item  {  background-color: #f5f8f9;}
  </style>
';

    return v_menu;

  end;
  /*Security functions*/

  function secGetUsername return varchar2 is
  begin
    /*Your code. Username is from cgi enviorment and it is based on you DADs configutration.*/

     return '';
    /*---------*/
  end;

  function secGetLOB return varchar2 is
  begin
    /*Your code. LineOfBusiness is from cgi enviorment and it is based on you DADs configutration.*/

     return '';
    /*---------*/
  end;

  function secGet_HTTP_ACCEPT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT'); --HTTP_ACCEPT = text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
  end;

  function secGet_HTTP_ACCEPT_ENCODING return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_ENCODING'); --HTTP_ACCEPT_ENCODING = gzip, deflate
  end;

  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_LANGUAGE'); --HTTP_ACCEPT_LANGUAGE = en,sl;q=0.9,en-GB;q=0.8
  end;

  function secGet_HTTP_ACCEPT_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_CHARSET'); --HTTP_ACCEPT_CHARSET =
  end;

  function secGet_HTTP_HOST return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_HOST'); --HTTP_HOST = was-test.zpiz.si:9081
  end;

  function secGet_HTTP_PORT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_PORT'); --HTTP_PORT = 9081
  end;

  function secGet_HTTP_USER_AGENT return varchar2 is
  begin
     return  OWA_UTIL.get_cgi_env('HTTP_USER_AGENT'); --HTTP_USER_AGENT = Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36
  end;

  function secGet_PATH_INFO return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('PATH_INFO'); --
  end;

  function secGet_PATH_ALIAS return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('PATH_ALIAS'); --
  end;
  
  function secGet_REMOTE_ADDR return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('REMOTE_ADDR'); --
  end;
  
  function secGet_REQUEST_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('REQUEST_CHARSET'); --
  end;
  
  function secGet_SCRIPT_NAME return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('SCRIPT_NAME'); --
  end;      



  procedure secCheckPermission(p_form varchar2, p_action varchar2) is
  begin
    /*Your code.*/

    if false then raise_application_error('-20000', 'No privileges'); end if;
    /*---------*/
  end;

  procedure secCheckCredentials(p_username varchar2, p_password varchar2, p_other varchar2 default null) is
  begin
    /*Your code.*/
    if false is null then -- check credentials in you user db
      raise_application_error('-20000', 'Invalid user logon credentials!');
    end if;
    /*---------*/
  end;

  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr ) is
    /*Your code.*/
    vu varchar2(111);
    vp varchar2(222);
    vo varchar2(333);
    vl varchar2(32000);
    vl1 varchar2(32000);
    vcv varchar2(444);
  begin
    declare
      vc OWA_COOKIE.cookie;
    begin
      vc := owa_cookie.get('SECCLIENTUSER');
      vcv := vc.vals(1);
    exception when others then
      vcv := '';
    end;

    if vcv is null then

    for i__ in 1 .. nvl(name_array.count, 0) loop
      if    upper(name_array(i__)) = upper('SECCLIENTUSER') then
        vu := value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTPWD') then
        vp := value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTOTHER') then
        vo:= value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTURL') then
        vl:= value_array(i__);
      end if;
      vl1 := vl1||'&'||name_array(i__)||'='||value_array(i__);
    end loop;

    vl1:= substr(owa_util.get_cgi_env('PATH_INFO'),2)||'?rasd=cool'||vl1;


    secCheckCredentials(vu, vp, vo);
    begin
    owa_util.mime_header('text/html', FALSE);
    OWA_COOKIE.SEND('SECCLIENTUSER', nvl(vu,'set values in RASD_CLIENT package'), null);
    OWA_COOKIE.SEND('SECCLIENTOTHER', nvl(vo,'set values in RASD_CLIENT package'), null);


    if vl is not null then
       OWA_UTIL.REDIRECT_URL(vl);
       owa_util.http_header_close;
    else
       OWA_UTIL.REDIRECT_URL(vl1);
       owa_util.http_header_close;
    end if;

    exception when others then
      null;
    end ;

    end if;
  exception when others then
       OWA_UTIL.REDIRECT_URL('your redirection to login page');
       owa_util.http_header_close;
  /*---------*/
  end;

  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 ) is
   begin
    /*Your code for remote logging.*/

    /*---------*/
    null;
   end;
   

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASD_CLIENT',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
       begin
        return  '
$(function() {

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
 });
        ';
       end; 
     function form_css return clob is
       begin
        return '

        ';
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.1.20230107205112'; 
  end;
  function this_form return varchar2 is
  begin
   return 'RASD_CLIENT';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version ); 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
 rasd_client.sessionStart;
declare vc varchar2(2000); begin
null;
exception when others then  null; end;    rasd_client.sessionClose;  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE varchar2(4000) PATH '$.page'
)) jt ) loop
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10OUT varchar2(4000) PATH '$.b10out'
)) jt ) loop
 if instr(RESTREQUEST,'b10out') > 0 then B10OUT(i__) := r__.B10OUT; end if;
i__ := i__ + 1;
end loop;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := value_array(i__);
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10OUT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10OUT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10OUT') and B10OUT.count = 0 and value_array(i__) is not null then
        B10OUT(1) := value_array(i__);
      end if;
    end loop;
-- organize records
-- init fields
    v_max := 0;
    if B10OUT.count > v_max then v_max := B10OUT.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10OUT.exists(i__) then
        B10OUT(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin

    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10OUT(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    ERROR := null;
    MESSAGE := null;
    PAGE := null;
    WARNING := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_B10(0);

  null;
  end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      pclear_B10(B10OUT.count);
  null; end;
  procedure pselect is
  begin


  null;
 end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10OUT.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit is
  begin


  null; 
  end;
  procedure formgen_js is
  begin
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
  function ShowFieldERROR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then  
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption>
<table border="0" id="B10_TABLE"><tr id="B10_BLOCK"><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10OUT"><span id="B10OUT_LAB" class="label"></span></td><td class="rasdTxB10OUT rasdTxTypeC" id="rasdTxB10OUT_1"><span id="B10OUT_1_RASD">');  htp.p('
<p>
RASD_CLIENT (your dev. schema) is PLSQL API to integrate your environment with generated programs	  
</p>
	  
<p>	  
<a href="https://sourceforge.net/p/rasd/wiki/Customization/" target="_blank">More about setting RASD_CLIENT is described on this link.</a>
</p>	  
<p>	  
If you change RASD_CLIENT here check your NLS settings in yor environment or add next code in package BODY execution part:
</br>
</br>	  
BEGIN</br>
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_NUMERIC_CHARACTERS= ''''''||C_NUMBER_DECIMAL||''''||C_NUMBER_THOUSAND||'''''' '';</br>
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_DATE_LANGUAGE = ''''''||C_NLS_DATE_LANGUAGE||'''''' '';</br>
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_LANGUAGE = ''''''||C_NLS_LANGUAGE||'''''' '';   </br>
end RASD_CLIENT</br>
</p>	  
	  
');  
htp.prn('</span></td></tr><tr></tr></table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','RASD_CLIENT Library')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>'); 
 
htp.prn('</head>
<body><div id="RASD_CLIENT_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASD_CLIENT_LAB','RASD_CLIENT Library') ||'     </div><div id="RASD_CLIENT_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASD_CLIENT_MENU') ||'     </div>
<form name="RASD_CLIENT" method="post" action="!rasd_client.webclient"><div id="RASD_CLIENT_DIV" class="rasdForm"><div id="RASD_CLIENT_HEAD" class="rasdFormHead"><input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||PAGE||'"/>
</div><div id="RASD_CLIENT_BODY" class="rasdFormBody">'); output_B10_DIV; htp.p('</div><div id="RASD_CLIENT_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASD_CLIENT_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASD_CLIENT_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div><div id="RASD_CLIENT_FOOTER" class="rasdFormFooter">'|| rasd_client.getHtmlFooter(version , substr('RASD_CLIENT_FOOTER',1,instr('RASD_CLIENT_FOOTER', '_',-1)-1) , '') ||'</div></div></form></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is 
  begin 
    return true;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="RASD_CLIENT" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<page><![CDATA['||PAGE||']]></page>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('</formfields>'); 
    if ShowBlockb10_DIV then 
    htpp('<b10>'); 
    htpp('<element>'); 
    htpp('<b10out><![CDATA['||B10OUT(1)||']]></b10out>'); 
    htpp('</element>'); 
  htpp('</b10>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"RASD_CLIENT","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"page":"'||escapeRest(PAGE)||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp('},'); 
    if ShowBlockb10_DIV then 
    htpp('"b10":['); 
     htpp('{'); 
    htpp('"b10out":"'||escapeRest(B10OUT(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp('"b10":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASD_CLIENT',ACTION);  
  if ACTION is null then null;
    pselect;
    poutput;
  end if;

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','RASD_CLIENT Library')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head><body><div id="RASD_CLIENT_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASD_CLIENT_LAB','RASD_CLIENT Library') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASD_CLIENT' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASD_CLIENT_FOOTER',1,instr('RASD_CLIENT_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASD_CLIENT',ACTION);  

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
    poutput;
  end if;

-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASD_CLIENT',ACTION);  
  if ACTION is null then null;
    pselect;
    poutputrest;
  end if;

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASD_CLIENT" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"RASD_CLIENT","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>25</formid><form>RASD_CLIENT</form><version>1</version><change>07.01.2023 08/51/12</change><user>RASDCLI</user><label><![CDATA[RASD_CLIENT Library]]></label><lobid>RASD</lobid><program>!rasd_client.webclient</program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>19.12.2017 12/45/45</change><compileyn>Y</compileyn><application>RASD lib&apos;s</application><owner>rasd</owner><editor>rasd</editor></info></compiledInfo><blocks><block><blockid>B10</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>OUT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>1</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10OUT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><i';
 v_vc(2) := 'nsertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields><links></links><pages></pages><triggers><trigger><blockid></blockid><triggerid>ComponentName</triggerid><plsql><![CDATA[/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*-----RASD_CLIENT SETTINGS-------------------------';
 v_vc(3) := '-------------------------------------------------*/
/*-----THIS CHANGES EFFECTS ALL RASD PROGRAMS USING THIS CLIENT!!!!!---------------------------------*/
/*---------------------------------------------------------------------------------------------------*/


  function Blob2Clob(B BLOB) 
return clob is 
c clob;
n number;
v varchar2(32767 CHAR); 
begin 
if (b is null) then 
return null;
end if;
if (length(b)=0) then
return empty_clob(); 
end if;
dbms_lob.createtemporary(c,true);
n:=1;

while (n+32767<=length(b)) loop
dbms_lob.writeappend(c,32767,utl_raw.cast_to_varchar2(dbms_lob.substr(b,32767,n)));
n:=n+32767;
end loop;
v := utl_raw.cast_to_varchar2(dbms_lob.substr(b,length(b)-n+1,n));

dbms_lob.writeappend(c,length(v),v);
return c;
end;


  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2 is
    v_t varchar(32000);
    v_root varchar2(200);
    x blob;
  begin
    v_root := OWA_UTIL.get_cgi_env(''DOC_ACCESS_PATH'');
  
    select blob_content into x
    from documents where name = c_HtmlJSLibraryFile;
    v_t := Blob2Clob(x);
    
    if v_root is null then
       v_t := replace(v_t,''docs/'',c_DOC_ACCESS_PATH||''/'');
    end if;    
    
    return replace(replace(v_t, '':NAME'', name),'':VALUE'',value);
    exception when others then return '''';
  end;

  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '''') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from documents where name = c_HtmlHeaderFile;
    v_t := Blob2Clob(x);
    select blob_content into x
    from documents where name = c_HtmlDataTable2File;
    v_t := v_t || Blob2Clob(x);

    v_t := replace(v_t , '':VALUE'' , value);
    return replace(v_t, '':NAME'', name);
    exception when others then return '''';
  end;

  function getHtmlDataTable (name varchar2, value varchar2 default '''') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from documents where name = c_HtmlDataTableFile;
    v_t := Blob2Clob(x);
    v_t := replace(v_t, '':NAME'', name);
    v_t := replace(v_t, '':VALUE'', value);
    return v_t;
    exception when others then return '''';
  end;

  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2 is
     v_t varchar(32000);
    v_fd varchar2(100);
    v_';
 v_vc(4) := 'ft varchar2(100);
    v_p varchar2(1000);
    v_format varchar2(1000) := format;
    x blob;
  begin
    if v_format is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := '', showTimepicker: false'';
    else
      FOR i IN c_date_transform.FIRST .. c_date_transform.LAST
      LOOP
        if instr( nvl(v_format,''NULL'') , c_date_transform(i)(1)) > 0 then
           v_fd := c_date_transform(i)(2);
           v_format := replace(v_format, c_date_transform(i)(1) , '''');
           exit;
        end if;   
      END LOOP;
      if v_fd is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
      end if;
      if v_format is not null then
      FOR i IN c_time_transform.FIRST .. c_time_transform.LAST
      LOOP
        if instr( nvl(v_format,''NULL'') , c_time_transform(i)(1)) > 0 then
           v_ft := c_time_transform(i)(2);
           v_format := replace(v_format, c_time_transform(i)(1) , '''');
           exit;
        end if;   
      END LOOP;
      if v_ft is null then
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := '', showTimepicker: false'';        
      end if;
      if trim(v_format) is not null then 
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := '', showTimepicker: false'';           
      end if;  
      else
        v_p := '', showTimepicker: false'';                   
      end if;    
    end if;
       
    select blob_content into x 
    from documents where name = c_HtmlHtmlDatePickerFile;
        v_t := Blob2Clob(x);
    return replace(replace(replace(replace(v_t, '':NAME'', name), '':DFORMAT'', v_fd),'':TFORMAT'', v_ft),'':PROPERTIES'', v_p);
  exception when others then return '''';
  end;  

  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2 is
    v_t varchar(32000);
    x blob;
  begin
    select blob_content into x 
    from documents where name = c_HtmlFooterFile;
        v_t := Blob2Clob(x);
    return replace(replace(replace(v_t, '':VERSION'', version), '':PROGRAM'', program),'':USER'', user);
    exception when others then return '''';
  end; 

 function varchr2number(p_value varchar2) return number is
    v_dot   number;
    v_co';
 v_vc(5) := 'mma number;
    v_value varchar2(30) := p_value;
    error exception;
  begin
    if p_value is not null then
      v_dot := instr(v_value, C_NUMBER_THOUSAND);
      if v_dot = 0 then
        return to_number(v_value);
      else
        while v_dot <> 0 Loop
          v_value := substr(v_value, v_dot + 1);
          v_dot   := instr(v_value, C_NUMBER_THOUSAND);
          v_comma := instr(v_value, C_NUMBER_DECIMAL);
          if v_dot <> 0 then
            if v_dot <> 4 then
              raise error;
            end if;
          else

            if v_comma <> 0 then
              if v_comma <> 4 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '''')));
              end if;
            else
              if length(v_value) <> 3 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '''')));
              end if;
            end if;
          end if;
        end loop;
      end if;
    else
      return(to_number(null));
    end if;
  end;

  procedure sessionStart is
  begin
    owa_util.mime_header(''text/html'', FALSE);
  end;

  procedure sessionSetValue(pname varchar2, pvalue varchar2) is
  begin
    OWA_COOKIE.SEND(''rasd$''||pname, UTL_URL.ESCAPE(pvalue, false, ''UTF8'') , null,null);
  end;

  function sessionGetValue(pname varchar2) return varchar2 is
     vc OWA_COOKIE.cookie;
  begin
     vc := owa_cookie.get(''rasd$''||pname);
     return UTL_URL.UNESCAPE(vc.vals(1),''UTF8'');
  exception when others then
    return '''';
  end;

  procedure sessionclose is
  begin
     owa_util.http_header_close;
  end;

  function getHtmlMenuList(p_formname varchar2) return varchar2 is
    v_menu varchar2(32000) := '''';
    v_js_menu varchar2(32000) := '''';
    v__ varchar2(32000) := '''';
    v_lobid varchar2(100);
  begin


select max(substr(s.text,instr(s.text,''<lobid>'')+7, instr(s.text,''</lobid>'')-instr(s.text,''<lobid>'')-7 )) into v_lobid
from user_source s 
where s.name = replace(p_formname,''_MENU'')
 and s.text like ''%<lobid>%''
;
  
for r in (
select distinct nvl(application,''NoN'') application, vr from (
select '''' application, 2 vr from dual
union
select substr(s.text, instr(s.text,''</compileyn><application>'')+25 , instr(s.text,''</application>'') - instr(s.';
 v_vc(6) := 'text,''</compileyn><application>'')-25 ) application , 1 vr
from user_source s where s.text like ''%<compiledInfo><info>%'' and s.text like ''%</compileyn><application>%''
and type = ''PACKAGE BODY''
and  substr(s.text, instr(s.text,''</compileyn><application>'')+25 , instr(s.text,''</application>'') - instr(s.text,''</compileyn><application>'')-25 ) is not null
and  substr(s.text,instr(s.text,''<lobid>'')+7, instr(s.text,''</lobid>'')-instr(s.text,''<lobid>'')-7 ) = v_lobid
)
order by vr , application
  )  loop


      v__ := '''';
    for r1 in (
select ''!''||lower(object_name)||''.webclient'' url, object_name  , y.application , z.label
from user_objects x,
(
select s.name , substr(s.text, instr(s.text,''</compileyn><application>'')+25 , instr(s.text,''</application>'') - instr(s.text,''</compileyn><application>'')-25 ) application 
from user_source s where s.text like ''%<compiledInfo><info>%'' and s.text like ''%</compileyn><application>%''
and type = ''PACKAGE BODY''
and (name <> ''RASD_CLIENT'' or name = ''RASD_CLIENT'' and substr(s.text, instr(s.text,''</compileyn><application>'')+25 , instr(s.text,''</application>'') - instr(s.text,''</compileyn><application>'')-25 ) is not null )  
and substr(s.text,instr(s.text,''<lobid>'')+7, instr(s.text,''</lobid>'')-instr(s.text,''<lobid>'')-7 ) = v_lobid
) y,
(
select s.name , replace(substr(s.text, instr(s.text,''</user><label><![CDATA['')+23 , instr(s.text,''&apos;]]&gt;&apos;</label>'') - instr(s.text,''</user><label><![CDATA['')-23 ),'''''''''''''''''''','''''''') label 
from user_source s where s.text like ''%</user><label><![CDATA[%'' 
and type = ''PACKAGE BODY'' 
and (name <> ''RASD_CLIENT'' or name = ''RASD_CLIENT'' and replace(substr(s.text, instr(s.text,''</user><label><![CDATA['')+23 , instr(s.text,''&apos;]]&gt;&apos;</label>'') - instr(s.text,''</user><label><![CDATA['')-23 ),'''''''''''''''''''','''''''') like ''%RASD_CLIENT%'' )  
) z
where 
x.object_type = ''PACKAGE BODY''
and x.status = ''VALID''
and x.object_name = y.name(+)
and x.object_name = z.name(+)
and nvl(y.application,''NoN'') = r.application
and exists (
select 1 from user_source s where s.text like ''%procedure webclient%''
and type = ''PACKAGE BODY''
and x.object_name = s.name
)
order by object_name    
) loop
      v__ := v__ || ''<li><a href="''||r1.url||''">''||r1.object_name||'' - ''||r1.label||''</a></li>
'';
  end loop;
  
  
    v_menu := v_menu || ''
      <li>
	  <a href="#">''||replace(r.applicat';
 v_vc(7) := 'ion,''NoN'',''Unallocated forms '')||''</a>
      <ul >
         ''||v__||''
      </ul>
      </li>
'';  
  v_js_menu :=  v_js_menu ||''$( "#menu_''||replace(replace(replace(replace(replace(r.application,'' '',''''),'''''''',''''''''''''),''&'',''''),'';'',''''),''.'',''dot'')||''" ).menu();'';

end loop;  
  
    v_menu := ''
<ul id="menu">
<il><a href="#">Menu</a>
<ul>
         ''||v_menu||''
</ul>		 
</il>		 
</ul>
  <script>
  $( function() {
    $( "#menu" ).menu();
  } );  
  </script>
  <style>
    .ui-menu { padding: 0px 10px 0px 10px; white-space: nowrap;} 
	.ui-menu-item  {  background-color: #f5f8f9;}
  </style>
'';

    return v_menu;

  end;
  /*Security functions*/

  function secGetUsername return varchar2 is
  begin
    /*Your code. Username is from cgi enviorment and it is based on you DADs configutration.*/

     return '''';
    /*---------*/
  end;

  function secGetLOB return varchar2 is
  begin
    /*Your code. LineOfBusiness is from cgi enviorment and it is based on you DADs configutration.*/

     return '''';
    /*---------*/
  end;

  function secGet_HTTP_ACCEPT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_ACCEPT''); --HTTP_ACCEPT = text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
  end;

  function secGet_HTTP_ACCEPT_ENCODING return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_ACCEPT_ENCODING''); --HTTP_ACCEPT_ENCODING = gzip, deflate
  end;

  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_ACCEPT_LANGUAGE''); --HTTP_ACCEPT_LANGUAGE = en,sl;q=0.9,en-GB;q=0.8
  end;

  function secGet_HTTP_ACCEPT_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_ACCEPT_CHARSET''); --HTTP_ACCEPT_CHARSET =
  end;

  function secGet_HTTP_HOST return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_HOST''); --HTTP_HOST = was-test.zpiz.si:9081
  end;

  function secGet_HTTP_PORT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''HTTP_PORT''); --HTTP_PORT = 9081
  end;

  function secGet_HTTP_USER_AGENT return varchar2 is
  begin
     return  OWA_UTIL.get_cgi_env(''HTTP_USER_AGENT''); --HTTP_USER_AGENT = Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36
  end;

  function secGet_PATH_INFO ';
 v_vc(8) := 'return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''PATH_INFO''); --
  end;

  function secGet_PATH_ALIAS return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''PATH_ALIAS''); --
  end;
  
  function secGet_REMOTE_ADDR return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''REMOTE_ADDR''); --
  end;
  
  function secGet_REQUEST_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''REQUEST_CHARSET''); --
  end;
  
  function secGet_SCRIPT_NAME return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env(''SCRIPT_NAME''); --
  end;      



  procedure secCheckPermission(p_form varchar2, p_action varchar2) is
  begin
    /*Your code.*/

    if false then raise_application_error(''-20000'', ''No privileges''); end if;
    /*---------*/
  end;

  procedure secCheckCredentials(p_username varchar2, p_password varchar2, p_other varchar2 default null) is
  begin
    /*Your code.*/
    if false is null then -- check credentials in you user db
      raise_application_error(''-20000'', ''Invalid user logon credentials!'');
    end if;
    /*---------*/
  end;

  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr ) is
    /*Your code.*/
    vu varchar2(111);
    vp varchar2(222);
    vo varchar2(333);
    vl varchar2(32000);
    vl1 varchar2(32000);
    vcv varchar2(444);
  begin
    declare
      vc OWA_COOKIE.cookie;
    begin
      vc := owa_cookie.get(''SECCLIENTUSER'');
      vcv := vc.vals(1);
    exception when others then
      vcv := '''';
    end;

    if vcv is null then

    for i__ in 1 .. nvl(name_array.count, 0) loop
      if    upper(name_array(i__)) = upper(''SECCLIENTUSER'') then
        vu := value_array(i__);
      elsif upper(name_array(i__)) = upper(''SECCLIENTPWD'') then
        vp := value_array(i__);
      elsif upper(name_array(i__)) = upper(''SECCLIENTOTHER'') then
        vo:= value_array(i__);
      elsif upper(name_array(i__)) = upper(''SECCLIENTURL'') then
        vl:= value_array(i__);
      end if;
      vl1 := vl1||''&''||name_array(i__)||''=''||value_array(i__);
    end loop;

    vl1:= substr(owa_util.get_cgi_env(''PATH_INFO''),2)||''?rasd=cool''||vl1;


    secCheckCredentials(vu, vp, vo);
    begin
    owa_util.mime_header(''text/html'', FALSE);
    OWA_COOKIE.SEND(''SECCLIENTUSER'', nvl(vu,''set values in RASD_CLIENT pack';
 v_vc(9) := 'age''), null);
    OWA_COOKIE.SEND(''SECCLIENTOTHER'', nvl(vo,''set values in RASD_CLIENT package''), null);


    if vl is not null then
       OWA_UTIL.REDIRECT_URL(vl);
       owa_util.http_header_close;
    else
       OWA_UTIL.REDIRECT_URL(vl1);
       owa_util.http_header_close;
    end if;

    exception when others then
      null;
    end ;

    end if;
  exception when others then
       OWA_UTIL.REDIRECT_URL(''your redirection to login page'');
       owa_util.http_header_close;
  /*---------*/
  end;

  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 ) is
   begin
    /*Your code for remote logging.*/

    /*---------*/
    null;
   end;
   

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

]]></plsql><plsqlspec><![CDATA[/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*-----RASD_CLIENT SETTINGS--------------------------------------------------------------------------*/
/*-----THIS CHANGES EFFECTS ALL RASD PROGRAMS USING THIS CLIENT!!!!!---------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  
  
  e_finished exception;

  type dt is varray(2) of varchar2(30);
  type dttab is table of dt;
  c_date_transform constant dttab := dttab(
      -- PL/SQL type , jQuery type : format is case sensitive
      dt(''yyyy-mm-dd'',''yy-mm-dd'') ,
      dt(''yyyy/mm/dd'',''yy/mm/dd'') ,
      dt(''dd.mm.yyyy'',''dd.mm.yy'') ,
      dt(''dd/mm/yyyy'', ''dd/mm/yy'') ,
      dt(''dd-mm-yyyy'', ''dd-mm-yy'') ,
      dt(''mm/dd/yyyy'', ''mm/dd/yy'') ,
      -- default type NULL must always be the last element
      dt(''NULL'',''mm/dd/yy'')
  );
  type dtt is varray(2) of varchar2(30);
  type dtttab ';
 v_vc(10) := 'is table of dtt;
  c_time_transform constant dtttab := dtttab(
      -- PL/SQL type , jQuery type : ime format is case sensitive
      dtt(''hh24:mi'',''hh:mm'') ,
      dtt(''hh24:mi:ss'',''hh:mm:ss''),
      -- default type NULL must always be the last element
      dtt(''NULL'',''hh:mm'')
  );

  C_DATE_FORMAT constant varchar2(10) := ''mm/dd/yyyy'';
  C_TIMESTAMP_FORMAT constant varchar2(30) := ''mm/dd/yyyy hh24:mi:ss.ff'';  
  C_NUMBER_DECIMAL constant varchar2(1) := ''.'';
  C_NUMBER_THOUSAND constant varchar2(1) := '','';
  C_NLS_DATE_LANGUAGE constant varchar2(20) := ''AMERICAN'';
  C_NLS_LANGUAGE constant varchar2(20) := ''AMERICAN''; 

  c_HtmlJSLibraryFile varchar2(30) := ''rasd/rasd_jslib_blue.html'';
  --c_HtmlJSLibraryFile varchar2(30) := ''rasd/rasd_jslib.html'';
  c_HtmlDataTableFile varchar2(30) := ''rasd/rasd_datatable.html'';
  c_HtmlHtmlDatePickerFile varchar2(30) := ''rasd/rasd_datepicker.html'';
  c_HtmlFooterFile varchar2(30) := ''rasd/rasd_footer.html'';
  c_HtmlDataTable2File varchar2(30) := ''rasd/rasd_datatable2.html'';
  c_HtmlHeaderFile  varchar2(30) := ''rasd/rasd_header.html'';

  c_DOC_ACCESS_PATH varchar2(30) := ''/rasdlib/docs''; --

  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2;
  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '''') return varchar2;
  function getHtmlDataTable (name varchar2, value varchar2 default '''') return varchar2;
  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2;
  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2;

  function varchr2number(p_value varchar2) return number;

  procedure sessionStart;
  procedure sessionSetValue(pname varchar2, pvalue varchar2);
  function  sessionGetValue(pname varchar2) return varchar2;
  procedure sessionClose;

  function getHtmlMenuList(p_formname varchar2) return varchar2;

  /*Security functions*/
  function secGetUsername return varchar2;
  function secGetLOB return varchar2;
  function secGet_HTTP_ACCEPT return varchar2;
  function secGet_HTTP_ACCEPT_ENCODING return varchar2;
  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2;
  function secGet_HTTP_ACCEPT_CHARSET return varchar2;
  function secGet_HTTP_HOST return varchar2;
  function secGet_HTTP_PORT return varchar2;
  function secGet_HTTP_USER_AGENT return varchar2;
  funct';
 v_vc(11) := 'ion secGet_PATH_INFO return varchar2;
  function secGet_PATH_ALIAS return varchar2;
  function secGet_REMOTE_ADDR return varchar2;
  function secGet_REQUEST_CHARSET return varchar2;
  function secGet_SCRIPT_NAME return varchar2;      

  procedure secCheckPermission(p_form varchar2, p_action varchar2);
  procedure secCheckCredentials(p_username varchar2, p_password varchar2, p_other varchar2 default null);
  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr);
  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 );

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  

/*IF you generated RASD_CLIENT make sure that your enviorment setting''s for NLS settings are correct or add belowe code in BODY execution part*/

/*

BEGIN
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_NUMERIC_CHARACTERS= ''''''||C_NUMBER_DECIMAL||''''||C_NUMBER_THOUSAND||'''''' '';
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_DATE_LANGUAGE = ''''''||C_NLS_DATE_LANGUAGE||'''''' '';
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_LANGUAGE = ''''''||C_NLS_LANGUAGE||'''''' '';   
end RASD_CLIENT

*/

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/  

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>ON_INITIALIZATION</triggerid><plsql><![CDATA[   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_NUMERIC_';
 v_vc(12) := 'CHARACTERS= ''''''||C_NUMBER_DECIMAL||''''||C_NUMBER_THOUSAND||'''''' '';
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_DATE_LANGUAGE = ''''''||C_NLS_DATE_LANGUAGE||'''''' '';
   EXECUTE IMMEDIATE ''ALTER SESSION SET NLS_LANGUAGE = ''''''||C_NLS_LANGUAGE||'''''' '';
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10OUT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p(''
<p>
RASD_CLIENT (your dev. schema) is PLSQL API to integrate your environment with generated programs	  
</p>
	  
<p>	  
<a href="https://sourceforge.net/p/rasd/wiki/Customization/" target="_blank">More about setting RASD_CLIENT is described on this link.</a>
</p>	  
<p>	  
If you change RASD_CLIENT here check your NLS settings in yor environment or add next code in package BODY execution part:
</br>
</br>	  
BEGIN</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_NUMERIC_CHARACTERS= ''''''''''''||C_NUMBER_DECIMAL||''''''''||C_NUMBER_THOUSAND||'''''''''''' '''';</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_DATE_LANGUAGE = ''''''''''''||C_NLS_DATE_LANGUAGE||'''''''''''' '''';</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_LANGUAGE = ''''''''''''||C_NLS_LANGUAGE||'''''''''''' '''';   </br>
end RASD_CLIENT</br>
</p>	  
	  
'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></value';
 v_vc(13) := 'id><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% 
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''RASD_CLIENT Library'')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
%>]]></value><valuecode><![CDATA['');  
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''RASD_CLIENT Library'')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
 
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagel';
 v_vc(14) := 'ementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASD_CLIENT</id><nameid>RASD_CLIENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[!rasd_client.webclient]]></value><valuecode><![CDATA[="!rasd_client.webclient"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASD_CLIENT]]></value><valuecode><![CDATA[="RASD_CLIENT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><r';
 v_vc(15) := 'id></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASD_CLIENT_LAB</id><nameid>RASD_CLIENT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_LAB]]></value><valuecode><![CDATA[="RASD_CLIENT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></na';
 v_vc(16) := 'me><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASD_CLIENT_LAB'',''RASD_CLIENT Library'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASD_CLIENT_LAB'',''RASD_CLIENT Library'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASD_CLIENT_MENU</id><nameid>RASD_CLIENT_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_MENU]]></value><valuecode><![CDATA[="RASD_CLIENT_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]';
 v_vc(17) := '></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASD_CLIENT_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASD_CLIENT_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASD_CLIENT_DIV</id><nameid>RASD_CLIENT_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_DIV]]></value><valuecode><![CDATA[="RASD_CLIENT_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></va';
 v_vc(18) := 'luecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>19</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASD_CLIENT_HEAD</id><nameid>RASD_CLIENT_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_HEAD]]></value><valuecode><![CDATA[="RASD_CLIENT_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>19</pelementid><orderby>2</orderby><element>FORM_BODY</element><type>F</type><id>RASD_CLIENT_BODY</id><nameid>RASD_CLIENT_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid';
 v_vc(19) := '><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_BODY]]></value><valuecode><![CDATA[="RASD_CLIENT_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>19</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASD_CLIENT_MESSAGE</id><nameid>RASD_CLIENT_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A';
 v_vc(20) := '</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_MESSAGE]]></value><valuecode><![CDATA[="RASD_CLIENT_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>19</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASD_CLIENT_ERROR</id><nameid>RASD_CLIENT_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_ERROR]]></value><valuecode><![CDATA[="RASD_CLIENT_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode>';
 v_vc(21) := '<![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>19</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASD_CLIENT_WARNING</id><nameid>RASD_CLIENT_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_WARNING]]></value><valuecode><![CDATA[="RASD_CLIENT_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid><';
 v_vc(22) := '/valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>19</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASD_CLIENT_FOOTER</id><nameid>RASD_CLIENT_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASD_CLIENT_FOOTER]]></value><valuecode><![CDATA[="RASD_CLIENT_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASD_CLIENT_FOOTER'',1,instr(''RASD_CLIENT_FOOTER'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASD_CLIENT_FOOTER'',1,instr(''RASD_CLIENT_FOOTER'', ''_'',-1)-1) , '''') ||'']]></valuecode><forl';
 v_vc(23) := 'oop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>21</pelementid><orderby>102</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valu';
 v_vc(24) := 'eid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>26</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>27</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CD';
 v_vc(25) := 'ATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>26</pelementid><orderby>103</orderby><element>TABLE_</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4';
 v_vc(26) := '</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_RASD]]></value><valuecode><![CDATA[="B10_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>20</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid>';
 v_vc(27) := '</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</att';
 v_vc(28) := 'ribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>20</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></te';
 v_vc(29) := 'xtid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||PAGE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</typ';
 v_vc(30) := 'e><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>22</pelementid><orderby>9998</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform';
 v_vc(31) := '></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><h';
 v_vc(32) := 'iddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[OND';
 v_vc(33) := 'BLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>23</pelementid><orderby>9996</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</or';
 v_vc(34) := 'derby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><texti';
 v_vc(35) := 'd></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>30</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10OUT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]>';
 v_vc(36) := '</name><value><![CDATA[rasdTxLabB10OUT]]></value><valuecode><![CDATA[="rasdTxLabB10OUT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>36</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10OUT_LAB</id><nameid>B10OUT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10OUT_LAB]]></value><valuecode><![CDATA[="B10OUT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valu';
 v_vc(37) := 'eid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>29</pelementid><orderby>4</orderby><element>TR_</element><type>B</type><id></id><nameid>B10_BLOCK4</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>30</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>B10OUT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10OUT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB10OUT rasdTxTypeC"]]></value';
 v_vc(38) := 'code><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10OUT_NAME]]></value><valuecode><![CDATA[="rasdTxB10OUT_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>B10OUT</id><nameid>B10OUT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10OUT_NAME_RASD]]></value><valuecode><![CDATA[="B10OUT_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid>';
 v_vc(39) := '</rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% B10OUT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''
<p>
RASD_CLIENT (your dev. schema) is PLSQL API to integrate your environment with generated programs	  
</p>
	  
<p>	  
<a href="https://sourceforge.net/p/rasd/wiki/Customization/" target="_blank">More about setting RASD_CLIENT is described on this link.</a>
</p>	  
<p>	  
If you change RASD_CLIENT here check your NLS settings in yor environment or add next code in package BODY execution part:
</br>
</br>	  
BEGIN</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_NUMERIC_CHARACTERS= ''''''''''''||C_NUMBER_DECIMAL||''''''''||C_NUMBER_THOUSAND||'''''''''''' '''';</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_DATE_LANGUAGE = ''''''''''''||C_NLS_DATE_LANGUAGE||'''''''''''' '''';</br>
   EXECUTE IMMEDIATE ''''ALTER SESSION SET NLS_LANGUAGE = ''''''''''''||C_NLS_LANGUAGE||'''''''''''' '''';   </br>
end RASD_CLIENT</br>
</p>	  
	  
'');  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASD_CLIENT_v.1.1.20230107205112.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
--<ON_INITIALIZATION formid="25" blockid="">
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS= '''||C_NUMBER_DECIMAL||''||C_NUMBER_THOUSAND||''' ';
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_LANGUAGE = '''||C_NLS_DATE_LANGUAGE||''' ';
   EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_LANGUAGE = '''||C_NLS_LANGUAGE||''' ';
--</ON_INITIALIZATION>
end RASD_CLIENT;
/

