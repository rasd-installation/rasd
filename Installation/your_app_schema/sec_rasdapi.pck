create or replace package SEC_RASDAPI is

  function getVersion return varchar2;

  function pwd_encode(p_pwd varchar2) return varchar2;

  function login(pusername varchar2, ppassword varchar2, pdevice varchar2, pip varchar2)
    return varchar2;

  function logout(guid varchar2) return varchar2;

  function checkPrivileges(guid     varchar2,
                           pprogram varchar2,
                           paction  varchar2) return varchar2;

  function userHasRole(guid varchar2, prole varchar2) return varchar2;

  function saveCheckData(guid      varchar2,
                         pprogram  varchar2,
                         psql      varchar2,
                         pporpouse varchar2) return varchar2;

  function saveGUIResult(guid       varchar2,
                         pprogram   varchar2,
                         paction    varchar2,
                         pguiresult clob) return varchar2;

  function getGuidFromGUI return varchar2;

  function getGuidFromSession(psession varchar2) return varchar2;

  function loggedUser(guid varchar2) return varchar2;

  function getObjectURL(pprogram varchar2) return varchar2;

end;
/

create or replace package body SEC_RASDAPI is

  c_guid_val varchar2(50) := 'guid';

  function getVersion return varchar2 is
  begin
    return 'v0.0.1';
    /*
     -- Author  : DOMEN.DOLAR
    
    202208081100 - first version 
    */
  end;

  procedure saveToSecLoginWork(guid       varchar2,
                               pprogram   varchar2,
                               paction    varchar2,
                               puser      varchar2,
                               pporpouse  varchar2,
                               psql       clob,
                               pguiresult clob) is
  begin
  
    insert into sec_login_work
      (login_work_id,
       guid,
       program_id,
       action_id,
       user_id,
       work_time,
       sql_request,
       gui_result,
       porpouse,
       sy_user,
       sy_change,
       sy_from,
       sy_type)
    values
      (sec_login_work_seq.nextval,
       guid,
       (SELECT program_id
          FROM SEC_PROGRAMS
         WHERE program_code = pprogram
           AND SY_Type <> 'D'),
       (SELECT action_id
          FROM SEC_ACTIONS
         WHERE ACTION_CODE = paction
           AND SY_Type <> 'B'),
       (SELECT user_id
          FROM SEC_USERS
         WHERE USER_CODE = puser
           AND SY_Type <> 'B'),
       systimestamp,
       psql,
       pguiresult,
       pporpouse,
       puser,
       systimestamp,
       systimestamp,
       'I');
  
    commit;
  end;

  function loggedUser(guid varchar2) return varchar2 is
    vu varchar2(100);
    vt timestamp;
  begin
    select x.user_code, x.sy_to
      into vu, vt
      from sec_login x
     where (x.guid = guid)
       and systimestamp < x.sy_to
       and x.loginok = 1;
  
    if (vt - systimestamp) < 1 / 24 then
      null;
      update sec_login p
         set p.sy_to = systimestamp + (2 / 24)
       where (p.guid = guid)
         and p.loginok = 1;
    end if;
    return vu;
  exception
    when others then
      return '';
  end;

  function checkPrivileges(guid     varchar2,
                           pprogram varchar2,
                           paction  varchar2) return varchar2 is
  
    vpermission sec_permissions.permission_id%type;
    vuser       sec_users.user_code%type;
  begin
    vuser := loggeduser(guid);
  
    begin
      WITH ROLE_RECURSIVE(role_id,
      levelr) AS
       (SELECT V.role_id, 1 + 0 levelr
          FROM SEC_USERS U
         INNER JOIN SEC_USER_ROLES VU
            ON (VU.user_id = U.user_id AND VU.sy_type <> 'D' AND
               VU.role_confirmed = 1 AND VU.VALID_FROM <= sysdate and
               nvl(vu.valid_to, sysdate + 1) >= sysdate)
         INNER JOIN SEC_ROLES V
            ON (V.role_id = VU.role_id AND V.sy_type <> 'D')
         WHERE U.sy_type <> 'D'
           AND U.user_code = vuser
        UNION ALL
        SELECT V.role_id, SUPERX.levelr + 1 levelr
          FROM SEC_ROLES V
         INNER JOIN SEC_ROLES_OF_ROLES VV
            ON (VV.role_id_post = V.role_id AND VV.sy_type <> 'D' AND
               VV.VALID_FROM <= sysdate and
               nvl(vv.valid_to, sysdate + 1) >= sysdate),
         ROLE_RECURSIVE SUPERX
         WHERE VV.role_id_pre = SUPERX.role_id
           AND SUPERX.levelr < 3
           AND V.sy_type <> 'D')
      SELECT count(PO.Permission_Id)
        into vpermission
        FROM ROLE_RECURSIVE VREC
       INNER JOIN SEC_PERMISSIONS PO
          ON (PO.role_id = VREC.role_id AND PO.sy_type <> 'D')
       INNER JOIN SEC_ACTIONS AK
          ON (PO.action_id = AK.action_id AND AK.sy_type <> 'D')
       INNER JOIN SEC_PROGRAMS O
          ON (PO.program_id = O.program_id AND O.sy_type <> 'D')
       WHERE PO.VALID_FROM <= SYSDATE
         AND COALESCE(PO.VALID_TO, SYSDATE) >= SYSDATE
         AND O.PROGRAM_CODE = pprogram
         AND AK.ACTION_CODE = paction;
    
      saveToSecLoginWork(guid, pprogram, paction, vuser, '', '', '');
    
      if vpermission > 0 then
        return 'OK';
      else
        return '';
      end if;
    exception
      when no_data_found then
        return '';
    end;
    return '';
  end;

  function saveCheckData(guid      varchar2,
                         pprogram  varchar2,
                         psql      varchar2,
                         pporpouse varchar2) return varchar2 is
    priv  varchar2(10);
    vuser sec_users.user_code%type;
  begin
    vuser := loggeduser(guid);
  
    priv := checkPrivileges(guid, pprogram, 'SELECT');
    if (priv != 'OK') then
      return '';
    end if;
  
    saveToSecLoginWork(guid,
                       pprogram,
                       'SELECT',
                       vuser,
                       pporpouse,
                       psql,
                       '');
    return psql;
  end;

  function saveGUIResult(guid       varchar2,
                         pprogram   varchar2,
                         paction    varchar2,
                         pguiresult clob) return varchar2 is
    priv  varchar2(10);
    vuser sec_users.user_code%type;
  begin
    vuser := loggeduser(guid);
  
    -- prevernaje pravic
    priv := checkPrivileges(guid, pprogram, paction);
    if (priv != 'OK') then
      return 'not OK';
    end if;
  
    saveToSecLoginWork(guid, pprogram, paction, vuser, '', '', pguiresult);
  
    return 'OK';
  
  exception
    when others then
      return 'not OK';
  end;

  function getGuidFromGUI return varchar2 is
    vc OWA_COOKIE.cookie;
  begin
    vc := owa_cookie.get(c_guid_val);
    return vc.vals(1);
  exception
    when others then
      return 'NO guid';
  end;

  function getGuidFromSession(psession varchar2) return varchar2 is
    vguid sec_login.guid%type;
  begin
    select p.guid
      into vguid
      from sec_login p
     where (p.log_session = psession and p.sy_to > systimestamp)
       and p.loginok = 1;
    return vguid;
  exception
    when others then
      return 'NO guid';
  end;

  function userHasRole(guid varchar2, prole varchar2) return varchar2 is
    vn    number;
    vuser sec_users.user_code%type;
  begin
    vuser := loggeduser(guid);
  
    WITH ROLES_RECURSIVE(ROLE_ID,
    levelr) AS
     (SELECT V.ROLE_ID, 1 levelr
        FROM SEC_USERS U
       INNER JOIN SEC_USER_ROLES VU
          ON (VU.user_id = U.user_id AND VU.SY_type <> 'D' AND
             VU.role_confirmed = 1 and vu.valid_from <= trunc(sysdate) and
             nvl(vu.valid_to, sysdate + 1) >= trunc(sysdate))
       INNER JOIN SEC_ROLES V
          ON (V.role_id = VU.role_id AND V.SY_type <> 'D')
       WHERE U.SY_type <> 'D'
         and u.valid_from <= trunc(sysdate)
         and nvl(u.valid_to, sysdate + 1) >= trunc(sysdate)
         AND U.USER_CODE = upper(vuser)
      UNION ALL
      SELECT V.role_id, SUPER.levelr + 1
        FROM SEC_ROLES V
       INNER JOIN SEC_ROLES_OF_ROLES VV
          ON (VV.ROLE_ID_POST = V.role_id AND VV.SY_type <> 'D' and
             vv.valid_from <= trunc(sysdate) and
             nvl(vv.valid_to, sysdate + 1) >= trunc(sysdate)),
       ROLES_RECURSIVE SUPER
       WHERE VV.Role_Id_Pre = SUPER.role_ID
         AND SUPER.levelr < 3
         AND V.SY_Type <> 'D')
    SELECT count(*)
      into vn
      FROM ROLES_RECURSIVE VREC, SEC_ROLES v
     where vrec.role_id = v.role_id
       and v.role_code = upper(prole);
    if vn > 0 then
      return 'OK';
    else
      return 'not OK';
    end if;
  exception
    when others then
      return 'not OK';
  end;

  function getObjectURL(pprogram varchar2) return varchar2 is
    v sec_programs.program_url%type;
  begin
    select o.program_url
      into v
      from sec_programs o
     where o.program_code = pprogram;
    return v;
  exception
    when others then
      return '';
  end;
 
  function pwd_encode(p_pwd varchar2) return varchar2 is
  begin
      return utl_encode.base64_encode(utl_i18n.string_to_raw (p_pwd, 'AL32UTF8'));
  end;

  function login(pusername varchar2, ppassword varchar2, pdevice varchar2, pip varchar2)
    return varchar2 is
    vp    integer;
    vok   integer;
    vguid varchar2(50);
  begin
  
    select u.user_id
      into vp
      from sec_users u
     where u.user_code = upper(pusername)
       and u.sy_type <> 'D'
       and u.valid_from <= sysdate
       and nvl(u.valid_to, sysdate + 1) >= sysdate;
  
    select count(*)
      into vok
      from sec_devices d
     where d.user_id = vp
       and d.device_code = pdevice
       and substr(d.pass_code, 11) = SEC_RASDAPI.pwd_encode(ppassword)
       and d.sy_type <> 'D'
       and d.valid_from <= sysdate
       and nvl(d.valid_to, sysdate + 1) >= sysdate
       and d.device_confirmed = 1;
  
    if vok = 1 then
    
      select substr(str, 1, 8) || '-' || substr(str, 9, 4) || '-' ||
             substr(str, 13, 4) || '-' || substr(str, 17, 4) || '-' ||
             substr(str, 21) guid
        into vguid
        from (select dbms_random.string('X', 32) str from dual);
    dbms_output.put_line(vguid);
      insert into sec_login
        (guid,
         user_code,
         device_code,
         log_session,
         ipadd,
         loginok,
         sy_user,
         sy_change,
         sy_from,
         sy_to,
         sy_type)
      values
        (vguid,
         pusername,
         pdevice,
         '',
         pip,
         1,
         pusername,
         systimestamp,
         systimestamp,
         systimestamp + (1 / 24),
         'I');
      commit;
      return vguid;
    
    else
    
      insert into sec_login
        (guid,
         user_code,
         device_code,
         log_session,
         ipadd,
         loginok,
         sy_user,
         sy_change,
         sy_from,
         sy_to,
         sy_type)
      values
        ('Invalid  '||pusername||' '||systimestamp,
         pusername,
         pdevice,
         '',
         pip,
         0,
         pusername,
         systimestamp,
         systimestamp,
         systimestamp,
         'I');
      commit;
      raise_application_error('-20000', 'Invalid login cridentials.');
    
    end if;

  
    /*
         CertificateMail varchar2(500) := OWA_UTIL.get_cgi_env('CertificateMail');
         CertificateIssuer varchar2(500) := OWA_UTIL.get_cgi_env('CertificateIssuer');
         CertificateSubject varchar2(500) := OWA_UTIL.get_cgi_env('CertificateSubject');
         CertificateSerial varchar2(500) := OWA_UTIL.get_cgi_env('CertificateSerial');
         
      begin
     
        SELECT distinct R.SF_OSEBA
          into pstevilka
          FROM AR.PRIJAVE_UPORABNIKOV P, AR.UPORABNIKI U, AR.REGISTRACIJE R
         WHERE R.SY_TISP <> 'B'
           AND U.SY_TISP <> 'B'
           AND P.SY_TISP <> 'B'
           AND COALESCE(P.SY_VELJADO, systimestamp) >= systimestamp
           AND R.SF_OSEBA IS NOT NULL
           AND U.SF_UPORABNIK = P.SF_UPORABNIK
           AND U.ID_UPORABNIK = R.ID_UPORABNIK
           AND P.TX_CNDC = R.TX_DPOZNAKA
           AND R.SF_REGSTATUS = '2'
           and r.TX_DPSERIJSKA = CertificateSerial
           and r.TX_DPMAIL = CertificateMail
           and r.TX_DPOZNAKA = CertificateSubject
           and r.TX_DPIZDAJATELJ = CertificateIssuer;
    */
  
  exception
    when no_data_found then

      insert into sec_login
        (guid,
         user_code,
         device_code,
         log_session,
         ipadd,
         loginok,
         sy_user,
         sy_change,
         sy_from,
         sy_to,
         sy_type)
      values
        ('Invalid '||pusername||' '||systimestamp,
         pusername,
         pdevice,
         '',
         pip,
         0,
         pusername,
         systimestamp,
         systimestamp,
         systimestamp,
         'I');
      commit;
      raise_application_error('-20000', 'Invalid user.');
  end;

  function logout(guid varchar2) return varchar2 is
  
  begin
  
    update sec_login x
       set sy_change = systimestamp, sy_to = systimestamp, sy_type = 'D'
     where x.guid = guid
       and sy_type <> 'D';
  
    commit;
  
    return 'OK';
  
  exception
    when others then
      return '';
    
  end;

end;
/

