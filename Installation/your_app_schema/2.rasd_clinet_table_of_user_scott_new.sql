set feedback off
set define off
spool 2.rasd_clinet_table_of_user_scott_new.log

prompt
prompt Creating table BONUS
prompt ====================
prompt
create table BONUS
(
  ename VARCHAR2(10 BYTE),
  job   VARCHAR2(9 BYTE),
  sal   NUMBER,
  comm  NUMBER
)
;

prompt
prompt Creating table DEPT
prompt ===================
prompt
create table DEPT
(
  deptno NUMBER(2) not null,
  dname  VARCHAR2(14 BYTE),
  loc    VARCHAR2(13 BYTE)
)
;
alter table DEPT
  add constraint PK_DEPT primary key (DEPTNO);

prompt
prompt Creating table DOCUMENTS
prompt ========================
prompt
create table DOCUMENTS
(
  name         VARCHAR2(256 BYTE) not null,
  mime_type    VARCHAR2(128 BYTE),
  doc_size     NUMBER,
  dad_charset  VARCHAR2(128 BYTE),
  last_updated DATE,
  content_type VARCHAR2(128 BYTE),
  blob_content BLOB
)
;

prompt
prompt Creating table EMP
prompt ==================
prompt
create table EMP
(
  empno    NUMBER(4) not null,
  ename    VARCHAR2(10 BYTE),
  job      VARCHAR2(9 BYTE),
  mgr      NUMBER(4),
  hiredate DATE,
  sal      NUMBER(7,2),
  comm     NUMBER(7,2),
  deptno   NUMBER(2),
  note     VARCHAR2(300 BYTE)
)
;
alter table EMP
  add constraint PK_EMP primary key (EMPNO);
alter table EMP
  add constraint FK_DEPTNO foreign key (DEPTNO)
  references DEPT (DEPTNO);

prompt
prompt Creating table JOBS
prompt ===================
prompt
create table JOBS
(
  job         VARCHAR2(9 BYTE) not null,
  description VARCHAR2(40 BYTE)
)
;

prompt
prompt Creating table MWP_CONTENT
prompt ==========================
prompt
create table MWP_CONTENT
(
  page_id    NUMBER not null,
  id         NUMBER generated always as identity,
  name       VARCHAR2(100) not null,
  content    CLOB not null,
  parent_id  NUMBER,
  valid_from DATE not null,
  valid_to   DATE
)
;

prompt
prompt Creating table MWP_SETTINGS
prompt ===========================
prompt
create table MWP_SETTINGS
(
  page_id     NUMBER generated always as identity,
  code        VARCHAR2(100) not null,
  name        VARCHAR2(100) not null,
  header_html VARCHAR2(4000),
  footer_html VARCHAR2(4000)
)
;

prompt
prompt Creating table SALGRADE
prompt =======================
prompt
create table SALGRADE
(
  grade NUMBER,
  losal NUMBER,
  hisal NUMBER
)
;

prompt
prompt Creating table SEC_ACTIONS
prompt ==========================
prompt
create table SEC_ACTIONS
(
  action_id   INTEGER not null,
  action_code VARCHAR2(10),
  action_name VARCHAR2(100),
  sy_user     VARCHAR2(30) default USER,
  sy_change   TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from     TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to       TIMESTAMP(6),
  sy_type     VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_ACTIONS
  is 'Actions for permissions like MODIFY, DELETE, SELECT, EXECUTE, ....';
alter table SEC_ACTIONS
  add constraint SEC_ACTIONS_PK primary key (ACTION_ID);

prompt
prompt Creating table SEC_DEVICES
prompt ==========================
prompt
create table SEC_DEVICES
(
  device_id        INTEGER not null,
  user_id          INTEGER not null,
  device_name      VARCHAR2(50) not null,
  pass_code        VARCHAR2(2000) not null,
  device_code      VARCHAR2(4000) not null,
  device_type      VARCHAR2(20) not null,
  valid_from       DATE not null,
  valid_to         DATE,
  device_confirmed INTEGER default 1 not null,
  sy_user          VARCHAR2(30) default USER,
  sy_change        TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from          TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to            TIMESTAMP(6),
  sy_type          VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_DEVICES
  is 'Devices registered to user.';
alter table SEC_DEVICES
  add constraint SEC_DEVICES_PK primary key (DEVICE_ID);

prompt
prompt Creating table SEC_LOGIN
prompt ========================
prompt
create table SEC_LOGIN
(
  guid        VARCHAR2(100) not null,
  user_code   VARCHAR2(50) not null,
  device_code VARCHAR2(4000),
  log_session VARCHAR2(500),
  ipadd       VARCHAR2(50) not null,
  loginok     NUMBER,
  sy_user     VARCHAR2(30) default USER,
  sy_change   TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from     TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to       TIMESTAMP(6),
  sy_type     VARCHAR2(1) default 'I' not null
)
;
create unique index SEC_LOGIN_PKI on SEC_LOGIN (GUID);

prompt
prompt Creating table SEC_LOGIN_WORK
prompt =============================
prompt
create table SEC_LOGIN_WORK
(
  login_work_id INTEGER not null,
  guid          VARCHAR2(100) not null,
  program_id    INTEGER not null,
  action_id     INTEGER not null,
  user_id       INTEGER not null,
  work_time     TIMESTAMP(6) not null,
  sql_request   CLOB,
  gui_result    CLOB,
  porpouse      VARCHAR2(500),
  sy_user       VARCHAR2(30) default USER,
  sy_change     TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from       TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to         TIMESTAMP(6),
  sy_type       VARCHAR2(1) default 'I' not null
)
;

prompt
prompt Creating table SEC_PROGRAM_TYPES
prompt ================================
prompt
create table SEC_PROGRAM_TYPES
(
  program_type_id   INTEGER not null,
  program_type_code VARCHAR2(10),
  program_type_name VARCHAR2(100),
  sy_user           VARCHAR2(30) default USER,
  sy_change         TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from           TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to             TIMESTAMP(6),
  sy_type           VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_PROGRAM_TYPES
  is 'Program typs like : PRG - program, MEN - menue, ....';
alter table SEC_PROGRAM_TYPES
  add constraint SEC_PROGRAM_TYPES_PK primary key (PROGRAM_TYPE_ID);

prompt
prompt Creating table SEC_PROGRAMS
prompt ===========================
prompt
create table SEC_PROGRAMS
(
  program_id            INTEGER not null,
  program_code          VARCHAR2(35),
  program_name          VARCHAR2(100),
  program_type_id       INTEGER not null,
  program_of_program_id INTEGER,
  program_url           VARCHAR2(2000),
  program_order         NUMBER,
  valid_from            DATE not null,
  valid_to              DATE,
  sy_user               VARCHAR2(30) default USER,
  sy_change             TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from               TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to                 TIMESTAMP(6),
  sy_type               VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_PROGRAMS
  is 'Registred programs in system.';
create unique index SEC_PROGRAMS_INX on SEC_PROGRAMS (PROGRAM_CODE);
alter table SEC_PROGRAMS
  add constraint SEC_PROGRAMS_PK primary key (PROGRAM_ID);
alter table SEC_PROGRAMS
  add constraint SEC_PROGRAMS_OF_PROGRAM_FK foreign key (PROGRAM_OF_PROGRAM_ID)
  references SEC_PROGRAMS (PROGRAM_ID);
alter table SEC_PROGRAMS
  add constraint SEC_PROGRAM_TYPES_FK foreign key (PROGRAM_TYPE_ID)
  references SEC_PROGRAM_TYPES (PROGRAM_TYPE_ID);

prompt
prompt Creating table SEC_ROLES
prompt ========================
prompt
create table SEC_ROLES
(
  role_id         INTEGER not null,
  role_code       VARCHAR2(30),
  role_name       VARCHAR2(100),
  role_of_role_id INTEGER,
  valid_from      DATE not null,
  valid_to        DATE,
  sy_user         VARCHAR2(30) default USER,
  sy_change       TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from         TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to           TIMESTAMP(6),
  sy_type         VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_ROLES
  is 'Roles in system';
create unique index SEC_ROLES_INX on SEC_ROLES (ROLE_CODE);
alter table SEC_ROLES
  add constraint SEC_ROLES_PK primary key (ROLE_ID);
alter table SEC_ROLES
  add constraint ROLE_OF_ROLE_FK foreign key (ROLE_OF_ROLE_ID)
  references SEC_ROLES (ROLE_ID);

prompt
prompt Creating table SEC_PERMISSIONS
prompt ==============================
prompt
create table SEC_PERMISSIONS
(
  permission_id INTEGER not null,
  action_id     INTEGER not null,
  role_id       INTEGER not null,
  program_id    INTEGER not null,
  valid_from    DATE not null,
  valid_to      DATE,
  sy_user       VARCHAR2(30) default USER,
  sy_change     TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from       TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to         TIMESTAMP(6),
  sy_type       VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_PERMISSIONS
  is 'Permissions on roles to objects.';
create index SEC_PERMISSIONS_INX on SEC_PERMISSIONS (PROGRAM_ID, ROLE_ID, ACTION_ID);
alter table SEC_PERMISSIONS
  add constraint SEC_PERMISSIONS_PK primary key (PERMISSION_ID);
alter table SEC_PERMISSIONS
  add constraint SEC_PERMISSIONS_P_FK foreign key (PROGRAM_ID)
  references SEC_PROGRAMS (PROGRAM_ID);
alter table SEC_PERMISSIONS
  add constraint SEC_PERMISSIONS_R_FK foreign key (ROLE_ID)
  references SEC_ROLES (ROLE_ID);

prompt
prompt Creating table SEC_ROLES_OF_ROLES
prompt =================================
prompt
create table SEC_ROLES_OF_ROLES
(
  roles_of_roles_id INTEGER not null,
  role_id_pre       INTEGER not null,
  role_id_post      INTEGER not null,
  valid_from        DATE not null,
  valid_to          DATE,
  sy_user           VARCHAR2(30) default USER,
  sy_change         TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from           TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to             TIMESTAMP(6),
  sy_type           VARCHAR2(1) default 'I' not null
)
;

prompt
prompt Creating table SEC_USERS
prompt ========================
prompt
create table SEC_USERS
(
  user_id       INTEGER not null,
  user_code     VARCHAR2(50) not null,
  user_name     VARCHAR2(50),
  user_sec_name VARCHAR2(50),
  phone_number  VARCHAR2(30),
  e_mail        VARCHAR2(50),
  valid_from    DATE,
  valid_to      DATE,
  sy_user       VARCHAR2(30) default USER,
  sy_change     TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from       TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to         TIMESTAMP(6),
  sy_type       VARCHAR2(1) default 'I' not null
)
;
comment on table SEC_USERS
  is 'Users in sistem.';
comment on column SEC_USERS.sy_user
  is 'Who create or change record';
comment on column SEC_USERS.sy_change
  is 'Time of change.';
comment on column SEC_USERS.sy_from
  is 'Time when record inserted.';
comment on column SEC_USERS.sy_to
  is 'Time when record logicali deleted - set to type D';
comment on column SEC_USERS.sy_type
  is 'Type of change; I - Insert, C - Change, D - Delete.';
create unique index SEC_USERS_INX on SEC_USERS (USER_CODE);
alter table SEC_USERS
  add constraint SEC_USERS_PK primary key (USER_ID);

prompt
prompt Creating table SEC_USER_ROLES
prompt =============================
prompt
create table SEC_USER_ROLES
(
  user_role_id   INTEGER not null,
  user_id        INTEGER not null,
  role_id        INTEGER not null,
  valid_from     DATE not null,
  valid_to       DATE,
  sy_user        VARCHAR2(30) default USER,
  sy_change      TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_from        TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
  sy_to          TIMESTAMP(6),
  sy_type        VARCHAR2(1) default 'I' not null,
  role_confirmed INTEGER default 1 not null
)
;
comment on table SEC_USER_ROLES
  is 'User roles.';
create index SEC_USER_ROLES_INX on SEC_USER_ROLES (USER_ID, ROLE_ID);
alter table SEC_USER_ROLES
  add constraint SEC_USER_ROLES_PK primary key (USER_ROLE_ID);
alter table SEC_USER_ROLES
  add constraint SEC_USER_ROLES_R_FK foreign key (ROLE_ID)
  references SEC_ROLES (ROLE_ID);
alter table SEC_USER_ROLES
  add constraint SEC_USER_ROLES_U_FK foreign key (USER_ID)
  references SEC_USERS (USER_ID);

prompt
prompt Creating sequence SEC_DEVICES_SEQ
prompt =================================
prompt
create sequence SEC_DEVICES_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 41
increment by 1
cache 20;

prompt
prompt Creating sequence SEC_LOGIN_WORK_SEQ
prompt ====================================
prompt
create sequence SEC_LOGIN_WORK_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

prompt
prompt Creating sequence SEC_USERS_SEQ
prompt ===============================
prompt
create sequence SEC_USERS_SEQ
minvalue 1
maxvalue 9999999999999999999999999999
start with 101
increment by 1
cache 20;


prompt Done
spool off
set define on
