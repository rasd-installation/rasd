create or replace package SECREGISTER is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: SECREGISTER generated on 18.08.22 by user RASDCLI.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;
/

create or replace package body SECREGISTER is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: SECREGISTER generated on 18.08.22 by user RASDCLI.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(256)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);  RECNUMB10                     varchar2(4000) := 1
;
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);  GBUTTONBCK                    varchar2(4000) := 'GBUTTONBCK'
;  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  GBUTTONFWD                    varchar2(4000) := 'GBUTTONFWD'
;  GBUTTONRES                    varchar2(4000) := 'GBUTTONRES'
;  GBUTTONSAVE                   varchar2(4000) := 'Register me'
;  GBUTTONSRC                    varchar2(4000) := 'GBUTTONSRC'
;
  MESSAGE                       varchar2(4000);  PAGE                          number := 0
;
  WARNING                       varchar2(4000);  MYCERT                        varchar2(4000) := nvl(OWA_UTIL.get_cgi_env('CertificateSerial'),'Not exist')
;  MYDEVICE                      varchar2(4000) := '<div id=B10DEV_1_RASD_LAB></div>'
;
  MYDEVICETYPE                  varchar2(4000);  MYIP                          varchar2(4000) := OWA_UTIL.get_cgi_env('REMOTE_ADDR')
;
  B10RS                         ctab;
  B10USER_ID                    ntab;
  B10USER_CODE                  ctab;
  B10USER_CODE#SET               stab;
  B10USER_NAME                  ctab;
  B10USER_NAME#SET               stab;
  B10USER_SEC_NAME              ctab;
  B10USER_SEC_NAME#SET           stab;
  B10PHONE_NUMBER               ctab;
  B10E_MAIL                     ctab;
  B10E_MAIL#SET                  stab;
  B10VALID_FROM                 dtab;
  B10VALID_TO                   dtab;
  B10PASSWORD                   ctab;
  B10PASSWORD#SET                stab;
  B10PASSWORDCHK                ctab;
  B10PASSWORDCHK#SET             stab;
  B10IP                         ctab;
  B10DEV                        ctab;
  B10CET                        ctab;
  B10GBTNSUB                    ctab;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('SECREGISTER',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
       begin
        return  '
$(function() {
    
    var module = {
        options: [],
        header: [navigator.platform, navigator.userAgent, navigator.appVersion, navigator.vendor, window.opera],
        dataos: [
            { name: ''Windows Phone'', value: ''Windows Phone'', version: ''OS'' },
            { name: ''Windows'', value: ''Win'', version: ''NT'' },
            { name: ''iPhone'', value: ''iPhone'', version: ''OS'' },
            { name: ''iPad'', value: ''iPad'', version: ''OS'' },
            { name: ''Kindle'', value: ''Silk'', version: ''Silk'' },
            { name: ''Android'', value: ''Android'', version: ''Android'' },
            { name: ''PlayBook'', value: ''PlayBook'', version: ''OS'' },
            { name: ''BlackBerry'', value: ''BlackBerry'', version: ''/'' },
            { name: ''Macintosh'', value: ''Mac'', version: ''OS X'' },
            { name: ''Linux'', value: ''Linux'', version: ''rv'' },
            { name: ''Palm'', value: ''Palm'', version: ''PalmOS'' }
        ],
        databrowser: [
            { name: ''Chrome'', value: ''Chrome'', version: ''Chrome'' },
            { name: ''Firefox'', value: ''Firefox'', version: ''Firefox'' },
            { name: ''Safari'', value: ''Safari'', version: ''Version'' },
            { name: ''Internet Explorer'', value: ''MSIE'', version: ''MSIE'' },
            { name: ''Opera'', value: ''Opera'', version: ''Opera'' },
            { name: ''BlackBerry'', value: ''CLDC'', version: ''CLDC'' },
            { name: ''Mozilla'', value: ''Mozilla'', version: ''Mozilla'' }
        ],
        init: function () {
            var agent = this.header.join('' ''),
                os = this.matchItem(agent, this.dataos),
                browser = this.matchItem(agent, this.databrowser);
            
            return { os: os, browser: browser };
        },
        matchItem: function (string, data) {
            var i = 0,
                j = 0,
                html = '''',
                regex,
                regexv,
                match,
                matches,
                version;
            
            for (i = 0; i < data.length; i += 1) {
                regex = new RegExp(data[i].value, ''i'');
                match = regex.test(string);
                if (match) {
                    regexv = new RegExp(data[i].version + ''[- /:;]([\\d._]+)'', ''i'');
                    matches = string.match(regexv);
                    version = '''';
                    if (matches) { if (matches[1]) { matches = matches[1]; } }
                    if (matches) {
                        matches = matches.split(/[._]+/);
                        for (j = 0; j < matches.length; j += 1) {
                            if (j === 0) {
                                version += matches[j] + ''.'';
                            } else {
                                version += matches[j];
                            }
                        }
                    } else {
                        version = ''0'';
                    }
                    return {
                        name: data[i].name,
                        version: parseFloat(version)
                    };
                }
            }
            return { name: ''unknown'', version: 0 };
        }
    };
//    
   var e = module.init(),
        debug = '''';

  document.getElementById(''MYDEVICE_RASD'').value = e.os.name;
  document.getElementById(''B10DEV_1_RASD_LAB'').innerHTML = e.os.name;
  
//    
//    debug += ''os.name = '' + e.os.name + ''<br/>'';
//    debug += ''os.version = '' + e.os.version + ''<br/>'';
//    debug += ''browser.name = '' + e.browser.name + ''<br/>'';
//    debug += ''browser.version = '' + e.browser.version + ''<br/>'';
//    
//    debug += ''<br/>'';
//    debug += ''navigator.userAgent = '' + navigator.userAgent + ''<br/>'';
//    debug += ''navigator.appVersion = '' + navigator.appVersion + ''<br/>'';
//    debug += ''navigator.platform = '' + navigator.platform + ''<br/>'';
//    debug += ''navigator.vendor = '' + navigator.vendor + ''<br/>'';
//    
//    document.getElementById(''B20_DIV'').innerHTML = debug;


  

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
  
  showMandatoryFields();
  showErrorFields();
  



  
 });
        ';
       end; 
     function form_css return clob is
       begin
        return '

        ';
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';  
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else 
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />'; 
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then 
      on_session;              
    end if;                   
  if lower(p_lov) = lower('link$CHKBXD') then
    v_description := 'CHKBXD';
        v_lov(1).output := 'N';
        v_lov(1).p1 := 'N';
        v_lov(2).output := 'Y';
        v_lov(2).p1 := 'Y';
        v_counter := 2; 
        if 1=2 then null;
        end if;          
  else
   return;
  end if;
if v_call = 'PLSQL' then 
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;   
  end loop;  
elsif v_call = 'REST' then 
if RESTRESTYPE = 'XML' then 
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');      
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop; 
 htp.p('</result></openLOV>');
else 
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );      
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then 
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop; 
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Register form')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!SECREGISTER.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20220818152136'; 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
 rasd_client.sessionStart;
declare vc varchar2(2000); begin
null;
if MYDEVICE is null then vc := rasd_client.sessionGetValue('MYDEVICE'); MYDEVICE  := vc;  end if; 
exception when others then  null; end;    rasd_client.sessionClose;  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 varchar2(4000) PATH '$.recnumb10'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,MYDEVICE varchar2(4000) PATH '$.mydevice'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'mydevice') > 0 then MYDEVICE := r__.MYDEVICE; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10RS varchar2(4000) PATH '$.b10rs'
  ,B10USER_CODE varchar2(4000) PATH '$.b10user_code'
  ,B10USER_NAME varchar2(4000) PATH '$.b10user_name'
  ,B10USER_SEC_NAME varchar2(4000) PATH '$.b10user_sec_name'
  ,B10PHONE_NUMBER varchar2(4000) PATH '$.b10phone_number'
  ,B10E_MAIL varchar2(4000) PATH '$.b10e_mail'
  ,B10PASSWORD varchar2(4000) PATH '$.b10password'
  ,B10PASSWORDCHK varchar2(4000) PATH '$.b10passwordchk'
  ,B10IP varchar2(4000) PATH '$.b10ip'
  ,B10DEV varchar2(4000) PATH '$.b10dev'
  ,B10CET varchar2(4000) PATH '$.b10cet'
  ,B10GBTNSUB varchar2(4000) PATH '$.b10gbtnsub'
)) jt ) loop
 if instr(RESTREQUEST,'b10rs') > 0 then B10RS(i__) := r__.B10RS; end if;
 if instr(RESTREQUEST,'b10user_code') > 0 then B10USER_CODE(i__) := r__.B10USER_CODE; end if;
 if instr(RESTREQUEST,'b10user_name') > 0 then B10USER_NAME(i__) := r__.B10USER_NAME; end if;
 if instr(RESTREQUEST,'b10user_sec_name') > 0 then B10USER_SEC_NAME(i__) := r__.B10USER_SEC_NAME; end if;
 if instr(RESTREQUEST,'b10phone_number') > 0 then B10PHONE_NUMBER(i__) := r__.B10PHONE_NUMBER; end if;
 if instr(RESTREQUEST,'b10e_mail') > 0 then B10E_MAIL(i__) := r__.B10E_MAIL; end if;
 if instr(RESTREQUEST,'b10password') > 0 then B10PASSWORD(i__) := r__.B10PASSWORD; end if;
 if instr(RESTREQUEST,'b10passwordchk') > 0 then B10PASSWORDCHK(i__) := r__.B10PASSWORDCHK; end if;
 if instr(RESTREQUEST,'b10ip') > 0 then B10IP(i__) := r__.B10IP; end if;
 if instr(RESTREQUEST,'b10dev') > 0 then B10DEV(i__) := r__.B10DEV; end if;
 if instr(RESTREQUEST,'b10cet') > 0 then B10CET(i__) := r__.B10CET; end if;
 if instr(RESTREQUEST,'b10gbtnsub') > 0 then B10GBTNSUB(i__) := r__.B10GBTNSUB; end if;
i__ := i__ + 1;
end loop;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := value_array(i__);
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := value_array(i__);
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONBCK') then GBUTTONBCK := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONFWD') then GBUTTONFWD := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MYCERT') then MYCERT := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MYDEVICE') then MYDEVICE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MYDEVICETYPE') then MYDEVICETYPE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MYIP') then MYIP := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10RS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_ID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10USER_ID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10USER_CODE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10USER_CODE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_NAME_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10USER_NAME(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_SEC_NAME_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10USER_SEC_NAME(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PHONE_NUMBER_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PHONE_NUMBER(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10E_MAIL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10E_MAIL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10VALID_FROM_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10VALID_FROM(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := to_date(value_array(i__), rasd_client.c_date_format);
      elsif  upper(name_array(i__)) = upper('B10VALID_TO_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10VALID_TO(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := to_date(value_array(i__), rasd_client.c_date_format);
      elsif  upper(name_array(i__)) = upper('B10PASSWORD_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PASSWORD(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PASSWORDCHK_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PASSWORDCHK(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10IP_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10IP(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10DEV_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10DEV(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10CET_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10CET(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10GBTNSUB_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GBTNSUB(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10RS') and B10RS.count = 0 and value_array(i__) is not null then
        B10RS(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_ID') and B10USER_ID.count = 0 and value_array(i__) is not null then
        B10USER_ID(1) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10USER_CODE') and B10USER_CODE.count = 0 and value_array(i__) is not null then
        B10USER_CODE(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_NAME') and B10USER_NAME.count = 0 and value_array(i__) is not null then
        B10USER_NAME(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10USER_SEC_NAME') and B10USER_SEC_NAME.count = 0 and value_array(i__) is not null then
        B10USER_SEC_NAME(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PHONE_NUMBER') and B10PHONE_NUMBER.count = 0 and value_array(i__) is not null then
        B10PHONE_NUMBER(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10E_MAIL') and B10E_MAIL.count = 0 and value_array(i__) is not null then
        B10E_MAIL(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10VALID_FROM') and B10VALID_FROM.count = 0 and value_array(i__) is not null then
        B10VALID_FROM(1) := to_date(value_array(i__), rasd_client.c_date_format);
      elsif  upper(name_array(i__)) = upper('B10VALID_TO') and B10VALID_TO.count = 0 and value_array(i__) is not null then
        B10VALID_TO(1) := to_date(value_array(i__), rasd_client.c_date_format);
      elsif  upper(name_array(i__)) = upper('B10PASSWORD') and B10PASSWORD.count = 0 and value_array(i__) is not null then
        B10PASSWORD(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PASSWORDCHK') and B10PASSWORDCHK.count = 0 and value_array(i__) is not null then
        B10PASSWORDCHK(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10IP') and B10IP.count = 0 and value_array(i__) is not null then
        B10IP(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10DEV') and B10DEV.count = 0 and value_array(i__) is not null then
        B10DEV(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10CET') and B10CET.count = 0 and value_array(i__) is not null then
        B10CET(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10GBTNSUB') and B10GBTNSUB.count = 0 and value_array(i__) is not null then
        B10GBTNSUB(1) := value_array(i__);
      end if;
    end loop;
-- organize records
declare
v_last number := 
B10USER_ID
.last;
v_curr number := 
B10USER_ID
.first;
i__ number;
begin
 if v_last <> 
B10USER_ID
.count then 
   v_curr := 
B10USER_ID
.FIRST;  
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10RS.exists(v_curr) then B10RS(i__) := B10RS(v_curr); end if;
      if B10USER_ID.exists(v_curr) then B10USER_ID(i__) := B10USER_ID(v_curr); end if;
      if B10USER_CODE.exists(v_curr) then B10USER_CODE(i__) := B10USER_CODE(v_curr); end if;
      if B10USER_NAME.exists(v_curr) then B10USER_NAME(i__) := B10USER_NAME(v_curr); end if;
      if B10USER_SEC_NAME.exists(v_curr) then B10USER_SEC_NAME(i__) := B10USER_SEC_NAME(v_curr); end if;
      if B10PHONE_NUMBER.exists(v_curr) then B10PHONE_NUMBER(i__) := B10PHONE_NUMBER(v_curr); end if;
      if B10E_MAIL.exists(v_curr) then B10E_MAIL(i__) := B10E_MAIL(v_curr); end if;
      if B10VALID_FROM.exists(v_curr) then B10VALID_FROM(i__) := B10VALID_FROM(v_curr); end if;
      if B10VALID_TO.exists(v_curr) then B10VALID_TO(i__) := B10VALID_TO(v_curr); end if;
      if B10PASSWORD.exists(v_curr) then B10PASSWORD(i__) := B10PASSWORD(v_curr); end if;
      if B10PASSWORDCHK.exists(v_curr) then B10PASSWORDCHK(i__) := B10PASSWORDCHK(v_curr); end if;
      if B10IP.exists(v_curr) then B10IP(i__) := B10IP(v_curr); end if;
      if B10DEV.exists(v_curr) then B10DEV(i__) := B10DEV(v_curr); end if;
      if B10CET.exists(v_curr) then B10CET(i__) := B10CET(v_curr); end if;
      if B10GBTNSUB.exists(v_curr) then B10GBTNSUB(i__) := B10GBTNSUB(v_curr); end if;
      i__ := i__ + 1;
      v_curr := 
B10USER_ID
.NEXT(v_curr);  
   END LOOP;
      B10RS.DELETE(i__ , v_last);
      B10USER_ID.DELETE(i__ , v_last);
      B10USER_CODE.DELETE(i__ , v_last);
      B10USER_NAME.DELETE(i__ , v_last);
      B10USER_SEC_NAME.DELETE(i__ , v_last);
      B10PHONE_NUMBER.DELETE(i__ , v_last);
      B10E_MAIL.DELETE(i__ , v_last);
      B10VALID_FROM.DELETE(i__ , v_last);
      B10VALID_TO.DELETE(i__ , v_last);
      B10PASSWORD.DELETE(i__ , v_last);
      B10PASSWORDCHK.DELETE(i__ , v_last);
      B10IP.DELETE(i__ , v_last);
      B10DEV.DELETE(i__ , v_last);
      B10CET.DELETE(i__ , v_last);
      B10GBTNSUB.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10RS.count > v_max then v_max := B10RS.count; end if;
    if B10USER_ID.count > v_max then v_max := B10USER_ID.count; end if;
    if B10USER_CODE.count > v_max then v_max := B10USER_CODE.count; end if;
    if B10USER_NAME.count > v_max then v_max := B10USER_NAME.count; end if;
    if B10USER_SEC_NAME.count > v_max then v_max := B10USER_SEC_NAME.count; end if;
    if B10PHONE_NUMBER.count > v_max then v_max := B10PHONE_NUMBER.count; end if;
    if B10E_MAIL.count > v_max then v_max := B10E_MAIL.count; end if;
    if B10VALID_FROM.count > v_max then v_max := B10VALID_FROM.count; end if;
    if B10VALID_TO.count > v_max then v_max := B10VALID_TO.count; end if;
    if B10PASSWORD.count > v_max then v_max := B10PASSWORD.count; end if;
    if B10PASSWORDCHK.count > v_max then v_max := B10PASSWORDCHK.count; end if;
    if B10IP.count > v_max then v_max := B10IP.count; end if;
    if B10DEV.count > v_max then v_max := B10DEV.count; end if;
    if B10CET.count > v_max then v_max := B10CET.count; end if;
    if B10GBTNSUB.count > v_max then v_max := B10GBTNSUB.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10RS.exists(i__) then
        B10RS(i__) := null;
      end if;
      if not B10USER_ID.exists(i__) then
        B10USER_ID(i__) := to_number(null);
      end if;
      if not B10USER_CODE.exists(i__) then
        B10USER_CODE(i__) := null;
      end if;
      if not B10USER_CODE#SET.exists(i__) then
        B10USER_CODE#SET(i__).visible := true;
      end if;
      if not B10USER_NAME.exists(i__) then
        B10USER_NAME(i__) := null;
      end if;
      if not B10USER_NAME#SET.exists(i__) then
        B10USER_NAME#SET(i__).visible := true;
      end if;
      if not B10USER_SEC_NAME.exists(i__) then
        B10USER_SEC_NAME(i__) := null;
      end if;
      if not B10USER_SEC_NAME#SET.exists(i__) then
        B10USER_SEC_NAME#SET(i__).visible := true;
      end if;
      if not B10PHONE_NUMBER.exists(i__) then
        B10PHONE_NUMBER(i__) := null;
      end if;
      if not B10E_MAIL.exists(i__) then
        B10E_MAIL(i__) := null;
      end if;
      if not B10E_MAIL#SET.exists(i__) then
        B10E_MAIL#SET(i__).visible := true;
      end if;
      if not B10VALID_FROM.exists(i__) then
        B10VALID_FROM(i__) := to_date(null);
      end if;
      if not B10VALID_TO.exists(i__) then
        B10VALID_TO(i__) := to_date(null);
      end if;
      if not B10PASSWORD.exists(i__) then
        B10PASSWORD(i__) := null;
      end if;
      if not B10PASSWORD#SET.exists(i__) then
        B10PASSWORD#SET(i__).visible := true;
      end if;
      if not B10PASSWORDCHK.exists(i__) then
        B10PASSWORDCHK(i__) := null;
      end if;
      if not B10PASSWORDCHK#SET.exists(i__) then
        B10PASSWORDCHK#SET(i__).visible := true;
      end if;
      if not B10IP.exists(i__) or B10IP(i__) is null then
        B10IP(i__) := 'N';
      end if;
      if not B10DEV.exists(i__) or B10DEV(i__) is null then
        B10DEV(i__) := 'N';
      end if;
      if not B10CET.exists(i__) or B10CET(i__) is null then
        B10CET(i__) := 'N';
      end if;
      if not B10GBTNSUB.exists(i__) then
        B10GBTNSUB(i__) := GBUTTONSAVE;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin

    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
 if pstart = 0 then k__ := k__ + 
B10USER_ID
.count(); end if;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10RS(i__) := null;
        B10USER_ID(i__) := null;
        B10USER_CODE(i__) := null;
        B10USER_NAME(i__) := null;
        B10USER_SEC_NAME(i__) := null;
        B10PHONE_NUMBER(i__) := null;
        B10E_MAIL(i__) := null;
        B10VALID_FROM(i__) := null;
        B10VALID_TO(i__) := null;
        B10PASSWORD(i__) := null;
        B10PASSWORDCHK(i__) := null;
        B10IP(i__) := null;
        B10DEV(i__) := null;
        B10CET(i__) := null;
        B10GBTNSUB(i__) := GBUTTONSAVE;
        B10USER_CODE#SET(i__).visible := true;
        B10USER_NAME#SET(i__).visible := true;
        B10USER_SEC_NAME#SET(i__).visible := true;
        B10E_MAIL#SET(i__).visible := true;
        B10PASSWORD#SET(i__).visible := true;
        B10PASSWORDCHK#SET(i__).visible := true;
        B10RS(i__) := 'I';

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := 1;
    ERROR := null;
    GBUTTONBCK := 'GBUTTONBCK';
    GBUTTONCLR := 'GBUTTONCLR';
    GBUTTONFWD := 'GBUTTONFWD';
    GBUTTONRES := 'GBUTTONRES';
    GBUTTONSAVE := 'Register me';
    GBUTTONSRC := 'GBUTTONSRC';
    MESSAGE := null;
    PAGE := 0;
    WARNING := null;
    MYCERT := nvl(OWA_UTIL.get_cgi_env('CertificateSerial'),'Not exist');
    MYDEVICE := '<div id=B10DEV_1_RASD_LAB></div>';
    MYDEVICETYPE := null;
    MYIP := OWA_UTIL.get_cgi_env('REMOTE_ADDR');
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_B10(0);

  null;
  end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10USER_ID.delete;
      B10USER_CODE.delete;
      B10USER_NAME.delete;
      B10USER_SEC_NAME.delete;
      B10PHONE_NUMBER.delete;
      B10E_MAIL.delete;
      B10VALID_FROM.delete;
      B10VALID_TO.delete;

      declare 
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR 
--<SQL formid="80" blockid="B10">
SELECT 
USER_ID,
USER_CODE,
USER_NAME,
USER_SEC_NAME,
PHONE_NUMBER,
E_MAIL,
VALID_FROM,
VALID_TO FROM SEC_USERS where 1=2
--</SQL>
;
        i__ := 1;
        LOOP 
          FETCH c__ INTO
            B10USER_ID(i__)
           ,B10USER_CODE(i__)
           ,B10USER_NAME(i__)
           ,B10USER_SEC_NAME(i__)
           ,B10PHONE_NUMBER(i__)
           ,B10E_MAIL(i__)
           ,B10VALID_FROM(i__)
           ,B10VALID_TO(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =1;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B10RS.delete(1);
          B10USER_ID.delete(1);
          B10USER_CODE.delete(1);
          B10USER_NAME.delete(1);
          B10USER_SEC_NAME.delete(1);
          B10PHONE_NUMBER.delete(1);
          B10E_MAIL.delete(1);
          B10VALID_FROM.delete(1);
          B10VALID_TO.delete(1);
          B10PASSWORD.delete(1);
          B10PASSWORDCHK.delete(1);
          B10IP.delete(1);
          B10DEV.delete(1);
          B10CET.delete(1);
          B10GBTNSUB.delete(1);
          i__ := 0;
        end if; 
        CLOSE c__;
      end;
      pclear_B10(B10USER_ID.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 then 
      pselect_B10;
    end if;

  null;
 end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10RS.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if substr(B10RS(i__),1,1) = 'I' then --INSERT
        if B10USER_CODE(i__) is not null
 or B10USER_NAME(i__) is not null
 or B10USER_SEC_NAME(i__) is not null
 or B10PHONE_NUMBER(i__) is not null
 or B10E_MAIL(i__) is not null
 or B10PASSWORD(i__) is not null
 or B10PASSWORDCHK(i__) is not null
 then 
--<pre_insert formid="80" blockid="B10">
select SEC_USERS_SEQ.nextval into B10USER_ID(1)
from dual;

B10USER_CODE(1) := upper (B10USER_CODE(1));
B10VALID_FROM(1) := sysdate;


if B10USER_CODE(1) is null then
B10USER_CODE#SET(1).error := ' Field is mandatory.';
error := 'Error on form!';
end if;

if  regexp_like(B10USER_CODE(1),'^[A-Z0-9]+$')
then
 null;
else

B10USER_CODE#SET(1).error := B10USER_CODE#SET(1).error || ' Username should only have A-Z 0-9.';
error := 'Error on form!';

end if;

if B10E_MAIL(1) is null then
B10E_MAIL#SET(1).error := ' Field is mandatory.';
error := 'Error on form!';
end if;

if regexp_like (B10E_MAIL(1),'^[A-Za-z]+[A-Za-z0-9.]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$')
then
 null;
else

B10E_MAIL#SET(1).error := B10E_MAIL#SET(1).error || ' Mail structure is not valid.';
error := 'Error on form!';

end if;


if B10PASSWORD(1) is null then
B10PASSWORD#SET(1).error := ' Field is mandatory.';
error := 'Error on form!';
end if;


if  regexp_like(B10PASSWORD(1),'^.*[A-Z]') 
and regexp_like(B10PASSWORD(1),'^.*[0-9]')
and length(B10PASSWORD(1)) >=8 then
 null;
else

B10PASSWORD#SET(1).error := B10PASSWORD#SET(1).error|| ' Password should have A-Z a-z 0-9 at least long 8 chars.';
error := 'Error on form!';

end if;




if B10PASSWORD(1) <> B10PASSWORDCHK(1) then

B10PASSWORD#SET(1).error := B10PASSWORD#SET(1).error || 'Passwords are not the same.';
error := 'Error on form!';

end if;

if error is not null then
    mydevice := '<div id=B10DEV_1_RASD_LAB></div>';
    return;
end if;

--</pre_insert>
-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

-- Generated INSERT statement. Use (i__) to access fields values.
if substr(B10RS(i__),1,1) = 'I' then
insert into SEC_USERS (
  USER_ID
 ,USER_CODE
 ,USER_NAME
 ,USER_SEC_NAME
 ,PHONE_NUMBER
 ,E_MAIL
 ,VALID_FROM
) values (
  B10USER_ID(i__)
 ,B10USER_CODE(i__)
 ,B10USER_NAME(i__)
 ,B10USER_SEC_NAME(i__)
 ,B10PHONE_NUMBER(i__)
 ,B10E_MAIL(i__)
 ,B10VALID_FROM(i__)
);
 MESSAGE := 'Data is changed.';
end if;
--<post_insert formid="80" blockid="B10">
if B10IP(1) = 'Y' then
  mydevicetype := 'Ip';
  MYdevice := OWA_UTIL.get_cgi_env('REMOTE_ADDR');
elsif B10DEV(1) = 'Y' then
  mydevicetype := 'System';
  MYdevice := MYdevice;
elsif B10CET(1) = 'Y' and OWA_UTIL.get_cgi_env('CertificateSerial') is not null then
  mydevicetype := 'Certificate';
  MYdevice := OWA_UTIL.get_cgi_env('CertificateSerial');   
else 
  mydevicetype := 'Password';
  MYdevice := 'Password';
end if;

insert into sec_devices
  (device_id, user_id, device_name, pass_code, device_code, device_type, valid_from,  device_confirmed, sy_user)
values
  (sec_devices_seq.nextval, B10USER_ID(1), MYdevice, dbms_random.string('X', 10)||SEC_RASDAPI.pwd_encode(B10PASSWORD(1)) , MYdevice, mydevicetype , systimestamp,  1 , user);

message := 'User is registered.';
mydevice := '<div id=B10DEV_1_RASD_LAB></div>';
--</post_insert>
        null; end if;
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 then 
       pcommit_B10;
    end if;

  null; 
  end;
  procedure poutput is
  function ShowFieldERROR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONBCK return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONCLR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONFWD return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONRES return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONSAVE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONSRC return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMYCERT return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMYDEVICETYPE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMYIP return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then  
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption>
<table border="0" id="B10_TABLE"><tr id="B10_BLOCK"><span id="" value="1" name="" class="hiddenRowItems"><input name="B10RS_1" id="B10RS_1_RASD" type="hidden" value="'||B10RS(1)||'"/>
</span><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10USER_CODE">');  if B10USER_CODE#SET(1).visible then  
htp.prn('<span id="B10USER_CODE_LAB" class="label">Username(chars+numbers)</span>');  end if;  
htp.prn('</td><td class="rasdTxB10USER_CODE rasdTxTypeC" id="rasdTxB10USER_CODE_1">');  if B10USER_CODE#SET(1).visible then  
htp.prn('<input name="B10USER_CODE_1" id="B10USER_CODE_1_RASD" type="text" value="'||B10USER_CODE(1)||'" class="rasdTextC ');  if B10USER_CODE#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10USER_CODE#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10USER_CODE#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10USER_CODE#SET(1).custom , instr(upper(B10USER_CODE#SET(1).custom),'CLASS="')+7 , instr(upper(B10USER_CODE#SET(1).custom),'"',instr(upper(B10USER_CODE#SET(1).custom),'CLASS="')+8)-instr(upper(B10USER_CODE#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10USER_CODE#SET(1).custom);  
htp.prn('');  if B10USER_CODE#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10USER_CODE#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10USER_CODE#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10USER_CODE#SET(1).error is not null then htp.p(' title="'||B10USER_CODE#SET(1).error||'"'); end if;  
htp.prn('');  if B10USER_CODE#SET(1).info is not null then htp.p(' title="'||B10USER_CODE#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10USER_NAME">');  if B10USER_NAME#SET(1).visible then  
htp.prn('<span id="B10USER_NAME_LAB" class="label">First name</span>');  end if;  
htp.prn('</td><td class="rasdTxB10USER_NAME rasdTxTypeC" id="rasdTxB10USER_NAME_1">');  if B10USER_NAME#SET(1).visible then  
htp.prn('<input name="B10USER_NAME_1" id="B10USER_NAME_1_RASD" type="text" value="'||B10USER_NAME(1)||'" class="rasdTextC ');  if B10USER_NAME#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10USER_NAME#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10USER_NAME#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10USER_NAME#SET(1).custom , instr(upper(B10USER_NAME#SET(1).custom),'CLASS="')+7 , instr(upper(B10USER_NAME#SET(1).custom),'"',instr(upper(B10USER_NAME#SET(1).custom),'CLASS="')+8)-instr(upper(B10USER_NAME#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10USER_NAME#SET(1).custom);  
htp.prn('');  if B10USER_NAME#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10USER_NAME#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10USER_NAME#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10USER_NAME#SET(1).error is not null then htp.p(' title="'||B10USER_NAME#SET(1).error||'"'); end if;  
htp.prn('');  if B10USER_NAME#SET(1).info is not null then htp.p(' title="'||B10USER_NAME#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10USER_SEC_NAME">');  if B10USER_SEC_NAME#SET(1).visible then  
htp.prn('<span id="B10USER_SEC_NAME_LAB" class="label">Last name</span>');  end if;  
htp.prn('</td><td class="rasdTxB10USER_SEC_NAME rasdTxTypeC" id="rasdTxB10USER_SEC_NAME_1">');  if B10USER_SEC_NAME#SET(1).visible then  
htp.prn('<input name="B10USER_SEC_NAME_1" id="B10USER_SEC_NAME_1_RASD" type="text" value="'||B10USER_SEC_NAME(1)||'" class="rasdTextC ');  if B10USER_SEC_NAME#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10USER_SEC_NAME#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10USER_SEC_NAME#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10USER_SEC_NAME#SET(1).custom , instr(upper(B10USER_SEC_NAME#SET(1).custom),'CLASS="')+7 , instr(upper(B10USER_SEC_NAME#SET(1).custom),'"',instr(upper(B10USER_SEC_NAME#SET(1).custom),'CLASS="')+8)-instr(upper(B10USER_SEC_NAME#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10USER_SEC_NAME#SET(1).custom);  
htp.prn('');  if B10USER_SEC_NAME#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10USER_SEC_NAME#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10USER_SEC_NAME#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10USER_SEC_NAME#SET(1).error is not null then htp.p(' title="'||B10USER_SEC_NAME#SET(1).error||'"'); end if;  
htp.prn('');  if B10USER_SEC_NAME#SET(1).info is not null then htp.p(' title="'||B10USER_SEC_NAME#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PHONE_NUMBER"><span id="B10PHONE_NUMBER_LAB" class="label">Phone number</span></td><td class="rasdTxB10PHONE_NUMBER rasdTxTypeC" id="rasdTxB10PHONE_NUMBER_1"><input name="B10PHONE_NUMBER_1" id="B10PHONE_NUMBER_1_RASD" type="text" value="'||B10PHONE_NUMBER(1)||'" class="rasdTextC "/>
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10E_MAIL">');  if B10E_MAIL#SET(1).visible then  
htp.prn('<span id="B10E_MAIL_LAB" class="label">E-mail</span>');  end if;  
htp.prn('</td><td class="rasdTxB10E_MAIL rasdTxTypeC" id="rasdTxB10E_MAIL_1">');  if B10E_MAIL#SET(1).visible then  
htp.prn('<input name="B10E_MAIL_1" id="B10E_MAIL_1_RASD" type="text" value="'||B10E_MAIL(1)||'" class="rasdTextC ');  if B10E_MAIL#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10E_MAIL#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10E_MAIL#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10E_MAIL#SET(1).custom , instr(upper(B10E_MAIL#SET(1).custom),'CLASS="')+7 , instr(upper(B10E_MAIL#SET(1).custom),'"',instr(upper(B10E_MAIL#SET(1).custom),'CLASS="')+8)-instr(upper(B10E_MAIL#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10E_MAIL#SET(1).custom);  
htp.prn('');  if B10E_MAIL#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10E_MAIL#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10E_MAIL#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10E_MAIL#SET(1).error is not null then htp.p(' title="'||B10E_MAIL#SET(1).error||'"'); end if;  
htp.prn('');  if B10E_MAIL#SET(1).info is not null then htp.p(' title="'||B10E_MAIL#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PASSWORD">');  if B10PASSWORD#SET(1).visible then  
htp.prn('<span id="B10PASSWORD_LAB" class="label">Password</span>');  end if;  
htp.prn('</td><td class="rasdTxB10PASSWORD rasdTxTypeC" id="rasdTxB10PASSWORD_1">');  if B10PASSWORD#SET(1).visible then  
htp.prn('<input name="B10PASSWORD_1" id="B10PASSWORD_1_RASD" type="text" value="'||B10PASSWORD(1)||'" class="rasdTextC ');  if B10PASSWORD#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10PASSWORD#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10PASSWORD#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PASSWORD#SET(1).custom , instr(upper(B10PASSWORD#SET(1).custom),'CLASS="')+7 , instr(upper(B10PASSWORD#SET(1).custom),'"',instr(upper(B10PASSWORD#SET(1).custom),'CLASS="')+8)-instr(upper(B10PASSWORD#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10PASSWORD#SET(1).custom);  
htp.prn('');  if B10PASSWORD#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10PASSWORD#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10PASSWORD#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10PASSWORD#SET(1).error is not null then htp.p(' title="'||B10PASSWORD#SET(1).error||'"'); end if;  
htp.prn('');  if B10PASSWORD#SET(1).info is not null then htp.p(' title="'||B10PASSWORD#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PASSWORDCHK">');  if B10PASSWORDCHK#SET(1).visible then  
htp.prn('<span id="B10PASSWORDCHK_LAB" class="label">Retype password</span>');  end if;  
htp.prn('</td><td class="rasdTxB10PASSWORDCHK rasdTxTypeC" id="rasdTxB10PASSWORDCHK_1">');  if B10PASSWORDCHK#SET(1).visible then  
htp.prn('<input name="B10PASSWORDCHK_1" id="B10PASSWORDCHK_1_RASD" type="text" value="'||B10PASSWORDCHK(1)||'" class="rasdTextC ');  if B10PASSWORDCHK#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10PASSWORDCHK#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10PASSWORDCHK#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PASSWORDCHK#SET(1).custom , instr(upper(B10PASSWORDCHK#SET(1).custom),'CLASS="')+7 , instr(upper(B10PASSWORDCHK#SET(1).custom),'"',instr(upper(B10PASSWORDCHK#SET(1).custom),'CLASS="')+8)-instr(upper(B10PASSWORDCHK#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  htp.p(' '||B10PASSWORDCHK#SET(1).custom);  
htp.prn('');  if B10PASSWORDCHK#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  if B10PASSWORDCHK#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('
');  if B10PASSWORDCHK#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10PASSWORDCHK#SET(1).error is not null then htp.p(' title="'||B10PASSWORDCHK#SET(1).error||'"'); end if;  
htp.prn('');  if B10PASSWORDCHK#SET(1).info is not null then htp.p(' title="'||B10PASSWORDCHK#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10IP"><span id="B10IP_LAB" class="label">Lock on IP: '||myIP||'</span></td><td class="rasdTxB10IP rasdTxTypeC" id="rasdTxB10IP_1"><input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10IP_1'');" name="B10IP_1" id="B10IP_1_RASD" type="checkbox" value="'||B10IP(1)||'" class="rasdCheckbox "/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10IP(1)||''' , ''B10IP_1'' ); }) </SCRIPT>
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10DEV"><span id="B10DEV_LAB" class="label">Lock on system: '||mydevice||'</span></td><td class="rasdTxB10DEV rasdTxTypeC" id="rasdTxB10DEV_1"><input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10DEV_1'');" name="B10DEV_1" id="B10DEV_1_RASD" type="checkbox" value="'||B10DEV(1)||'" class="rasdCheckbox "/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10DEV(1)||''' , ''B10DEV_1'' ); }) </SCRIPT>
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10CET"><span id="B10CET_LAB" class="label">Lock on my dig. certificate: '||mycert||'</span></td><td class="rasdTxB10CET rasdTxTypeC" id="rasdTxB10CET_1"><input ONCLICK="js_Clink$CHKBXDclick(this.value, ''B10CET_1'');" name="B10CET_1" id="B10CET_1_RASD" type="checkbox" value="'||B10CET(1)||'" class="rasdCheckbox "/><SCRIPT LANGUAGE="JavaScript"> document.addEventListener(''DOMContentLoaded'', function(event) {  js_Clink$CHKBXDinit( '''||B10CET(1)||''' , ''B10CET_1'' ); }) </SCRIPT>
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10GBTNSUB"><span id="B10GBTNSUB_LAB" class="label"></span></td><td class="rasdTxB10GBTNSUB rasdTxTypeC" id="rasdTxB10GBTNSUB_1"><input onclick=" ACTION.value=this.value; submit();" name="B10GBTNSUB_1" id="B10GBTNSUB_1_RASD" type="button" value="'||B10GBTNSUB(1)||'" class="rasdButton"/>
</td></tr><tr></tr></table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<script language="JavaScript">');
    htp.p('function js_Clink$CHKBXDinit(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.value == ''Y'') {  element.checked = true;  }  ');
    htp.p('          else { element.checked = false;  } ');
    htp.p('}');
    htp.p('function js_Clink$CHKBXDclick(pvalue, pobjectname) {');
    htp.p('          var element = document.getElementById(pobjectname + ''_RASD'');  ');
    htp.p('          if (element.checked) { element.value=''Y''; } else { element.value=''N'';}  ');
    htp.p('}');
    htp.p('</script>');
    htp.p('<script language="JavaScript">');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('</script>');
    htp.prn('<html>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Register form')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head>
<body><div id="SECREGISTER_LAB" class="rasdFormLab">Register form '|| rasd_client.getHtmlDataTable('SECREGISTER_LAB') ||'     </div><div id="SECREGISTER_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('SECREGISTER_MENU') ||'     </div>
<form name="SECREGISTER" method="post" action="?"><div id="SECREGISTER_DIV" class="rasdForm"><div id="SECREGISTER_HEAD" class="rasdFormHead"><input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||RECNUMB10||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="MYDEVICE" id="MYDEVICE_RASD" type="hidden" value="'||MYDEVICE||'"/>
</div><div id="SECREGISTER_BODY" class="rasdFormBody">'); output_B10_DIV; htp.p('</div><div id="SECREGISTER_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="SECREGISTER_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="SECREGISTER_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div><div id="SECREGISTER_FOOTER" class="rasdFormFooter">'|| rasd_client.getHtmlFooter(version , substr('SECREGISTER_FOOTER',1,instr('SECREGISTER_FOOTER', '_',-1)-1) , '') ||'</div></div></form></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="SECREGISTER" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<recnumb10><![CDATA['||RECNUMB10||']]></recnumb10>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<page>'||PAGE||'</page>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('<mydevice><![CDATA['||MYDEVICE||']]></mydevice>'); 
    htpp('</formfields>'); 
    if ShowBlockb10_DIV then 
    htpp('<b10>'); 
  for i__ in 1..
B10USER_ID
.count loop 
    htpp('<element>'); 
    htpp('<b10rs><![CDATA['||B10RS(i__)||']]></b10rs>'); 
    htpp('<b10user_code><![CDATA['||B10USER_CODE(i__)||']]></b10user_code>'); 
    htpp('<b10user_name><![CDATA['||B10USER_NAME(i__)||']]></b10user_name>'); 
    htpp('<b10user_sec_name><![CDATA['||B10USER_SEC_NAME(i__)||']]></b10user_sec_name>'); 
    htpp('<b10phone_number><![CDATA['||B10PHONE_NUMBER(i__)||']]></b10phone_number>'); 
    htpp('<b10e_mail><![CDATA['||B10E_MAIL(i__)||']]></b10e_mail>'); 
    htpp('<b10password><![CDATA['||B10PASSWORD(i__)||']]></b10password>'); 
    htpp('<b10passwordchk><![CDATA['||B10PASSWORDCHK(i__)||']]></b10passwordchk>'); 
    htpp('<b10ip><![CDATA['||B10IP(i__)||']]></b10ip>'); 
    htpp('<b10dev><![CDATA['||B10DEV(i__)||']]></b10dev>'); 
    htpp('<b10cet><![CDATA['||B10CET(i__)||']]></b10cet>'); 
    htpp('<b10gbtnsub><![CDATA['||B10GBTNSUB(i__)||']]></b10gbtnsub>'); 
    htpp('</element>'); 
  end loop; 
  htpp('</b10>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"SECREGISTER","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"recnumb10":"'||escapeRest(RECNUMB10)||'"'); 
    htpp(',"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"page":"'||PAGE||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp(',"mydevice":"'||escapeRest(MYDEVICE)||'"'); 
    htpp('},'); 
    if ShowBlockb10_DIV then 
    htpp('"b10":['); 
  v_firstrow__ := true;
  for i__ in 1..
B10USER_ID
.count loop 
    if v_firstrow__ then
     htpp('{'); 
     v_firstrow__ := false;
    else
     htpp(',{'); 
    end if;
    htpp('"b10rs":"'||escapeRest(B10RS(i__))||'"'); 
    htpp(',"b10user_code":"'||escapeRest(B10USER_CODE(i__))||'"'); 
    htpp(',"b10user_name":"'||escapeRest(B10USER_NAME(i__))||'"'); 
    htpp(',"b10user_sec_name":"'||escapeRest(B10USER_SEC_NAME(i__))||'"'); 
    htpp(',"b10phone_number":"'||escapeRest(B10PHONE_NUMBER(i__))||'"'); 
    htpp(',"b10e_mail":"'||escapeRest(B10E_MAIL(i__))||'"'); 
    htpp(',"b10password":"'||escapeRest(B10PASSWORD(i__))||'"'); 
    htpp(',"b10passwordchk":"'||escapeRest(B10PASSWORDCHK(i__))||'"'); 
    htpp(',"b10ip":"'||escapeRest(B10IP(i__))||'"'); 
    htpp(',"b10dev":"'||escapeRest(B10DEV(i__))||'"'); 
    htpp(',"b10cet":"'||escapeRest(B10CET(i__))||'"'); 
    htpp(',"b10gbtnsub":"'||escapeRest(B10GBTNSUB(i__))||'"'); 
    htpp('}'); 
  end loop; 
    htpp(']'); 
  else 
    htpp('"b10":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

--<ON_ACTION formid="80" blockid="">
  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('SECREGISTER',ACTION);  
  if ACTION is null then null;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONBCK then     pselect;
    poutput;
  elsif ACTION = GBUTTONFWD then     pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     
    pcommit;
	if error is null then
       pselect;
	end if;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

--</ON_ACTION>
  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Register form')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head><body><div id="SECREGISTER_LAB" class="rasdFormLab">Register form '|| rasd_client.getHtmlDataTable('SECREGISTER_LAB') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'SECREGISTER' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('SECREGISTER_FOOTER',1,instr('SECREGISTER_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('SECREGISTER',ACTION);  
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your oown.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR, GBUTTONSAVE ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
    poutput;
  end if;

-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('SECREGISTER',ACTION);  
  if ACTION is null then null;
    RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONBCK then     pselect;
    poutputrest;
  elsif ACTION = GBUTTONFWD then     pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMB10 := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="SECREGISTER" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"SECREGISTER","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>80</formid><form>SECREGISTER</form><version>1</version><change>18.08.2022 03/21/36</change><user>RASDCLI</user><label><![CDATA[Register form]]></label><lobid>SEC</lobid><program>?</program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>N</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>11.08.2022 08/59/07</change><compileyn>N</compileyn><application>Users</application><owner>sec</owner><editor>sec</editor></info></compiledInfo><blocks></blocks><fields></fields><links></links><pages></pages><triggers></triggers><elements></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_SECREGISTER_v.1.1.20220818152136.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
end SECREGISTER;
/

