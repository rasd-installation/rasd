create or replace package DOCUMENTS_API2 is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DOCUMENTS_API2 generated on 07.01.23 by user RASDCLI.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
  
  procedure page(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
 
   procedure pageupload(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  
  procedure page_old(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
    procedure upload (
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  PROCEDURE download_direct (file  IN  VARCHAR2);
  
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
 
end;
/

create or replace package body DOCUMENTS_API2 is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DOCUMENTS_API2 generated on 07.01.23 by user RASDCLI.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  PLINK                         ctab;
  PFILE                         ctab;
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

   procedure page(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);
   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = 'FIN' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = 'POSTUPLOADURL' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = 'SHOWFILES' then showfilelist:= value_array(i);
            end if;
            
            --htp.p(name_array(i)||'='||value_array(i));
         end loop;

      htp.p('
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<script type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jquery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">
			
<style>
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
	  
</style>
			
</HEAD>

<BODY>
   <div id="fileHandler">
			
			<p> </p>
	  <form  enctype="multipart/form-data" action="!DOCUMENTS_API2.pageupload" method="post"> 		
      <input type="file" name="file">
      <button type="submit" id="btnUpload" class="rasdButton">Upload</button>
	  <input type="hidden" name="FIN" value="'||RETURNFILE||'"/>	
	  <input type="hidden" name="POSTUPLOADURL" value="'||POSTUPLOADURL||'"/>	
	  <input type="hidden" name="SHOWFILES" value="'||showfilelist||'"/>	
	  </form>
			<p> </p>
    </div>
</BODY>
</HTML>			
');

   end;		 
  
   procedure pageupload(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  v_out varchar2(10000);
  v_outjs varchar2(10000);  
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);
  l_real_name  VARCHAR2(1000);
  file varchar2(1000);
   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = 'FIN' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = 'POSTUPLOADURL' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = 'SHOWFILES' then showfilelist:= value_array(i);

            elsif upper(name_array(i)) = 'FILE' then file:= value_array(i);
            end if;
            
            --htp.p(name_array(i)||'='||value_array(i));
         end loop;


	if file is not null then	
	begin 
  
      l_real_name := user||'/'||SUBSTR(file, INSTR(file, '/') + 1);

    DELETE FROM documents
    WHERE  name = l_real_name;

    -- Update the prefixed name with the real file name.
    UPDATE documents
    SET    name = l_real_name
    WHERE  name = file;

v_out := '<p>Uploaded file: <A HREF="DOCUMENTS_API2.download_direct?file='||l_real_name||'" target="_bank">'||l_real_name||'</A></p>';		


if POSTUPLOADURL is not null then 

owa_util.redirect_url(
utl_url.escape(
replace(POSTUPLOADURL,'RETURNFILE',l_real_name)
,false,'UTF-8')
);
/*
owa_util.redirect_url(
  replace(
    replace(
      replace(
       replace( 
        replace(utl_url.escape(POSTUPLOADURL),'RETURNFILE',l_real_name),'%3F','?'),'%3D','='),'%26','&'),'%21','!')  
);
*/
end if;



if RETURNFILE is not null then 
v_outjs := '<script type="text/javascript">';
v_outjs := v_outjs||'      window.opener.'||RETURNFILE||'.value = '''||l_real_name||''';';
v_outjs := v_outjs||'      window.close();';
v_outjs := v_outjs||'</script>';
end if;


    exception when others then
	
v_out := '<p>Uploaded failed.</A></p>';		
	
    end;
    else

v_out := '<p>Uploaded failed.</A></p>';		
	
	end if;



      htp.p('
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<script type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jquery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">
'||v_outjs||'			
<style>
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
			
   #list {
    font-size: 12;
    margin: 40px;
   } 			
	  
</style>
			
</HEAD>

<BODY>
   <div id="fileHandler">
			');

htp.p(v_out);

if showfilelist = 'true' then
htp.p('
<br />
<br/>
	  These uploaded files are only accessed to your application with documents_api2.download_direct?file= URL. To store objects in library you shoud put object in /rasdlib project.
');
end if;			
htp.p('			<p> </p>

         <div id="list">

');   
if showfilelist = 'true' then
for r in (select name from documents  order by name) loop

htp.p('<A HREF="DOCUMENTS_API2.download_direct?file='||r.name||'" target="_bank">'||r.name||'</A></br>');

end loop;
end if;
htp.p('
     </div>				
			<p> </p>
	      </div>

	  </BODY>
</HTML>			
');  
  
  
   end; 
 
 
  
   procedure page_old(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);

   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = 'FIN' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = 'POSTUPLOADURL' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = 'SHOWFILES' then showfilelist:= value_array(i);
            end if;
            
           -- htp.p(name_array(i)||'='||value_array(i));
         end loop;

   
      htp.p('
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<script type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jquery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">

<script type="text/javascript">

 $(function() {
     addSpinner();   
  });

   var chunkSize = 4000;
   var bytesUploaded = 0;
   
   //$(document).ready(function() {
   //   $("#btnUpload").prop("disabled",true);
   //});

   function sendfile() {
      var files = document.getElementById(''files'').files;
      var datoteka = files[0];
      var reader = new FileReader();

      //progress.style.width = "0%"; //IE ERROR
      progress.textContent = "0%";

      reader.onloadend = function(evt) {
         if(evt.target.readyState == FileReader.DONE) { 
            //console.log("zacetk: "+evt.target.result.byteLength);
            sendChunk(evt.target.result, datoteka);
         }
      };
      reader.readAsArrayBuffer(datoteka);
   }
   
   function outputLink() {
      var files = document.getElementById(''files'').files;
      var datoteka = files[0];
     $("#rasdSpinner").hide(); 
     $("#btnUpload").prop("disabled",false);
     $("#download").html( ''Download file: <A HREF="documents_api2.download_direct?file='||user||'/''+ encodeURIComponent(datoteka.name) +''" target="_bank">'' + datoteka.name+''</A>'');     
');
if POSTUPLOADURL is not null then 

htp.p('      window.location = "'||
  replace(
    replace(
      replace(
       replace(
        replace(POSTUPLOADURL,'RETURNFILE',user||'/"+encodeURIComponent(datoteka.name)+"'),'%3F','?'),'%3D','='),'%26','&'),'%21','!')
        ||'";')
;


end if;

if RETURNFILE is not null then 
htp.p('      window.opener.'||RETURNFILE||'.value = '''||user||'/''+ encodeURIComponent(datoteka.name);');
end if;
htp.p('		
   }   
	
   function _arrayBufferToBase64(buffer) {
      var binary = '''';
      var bytes = new Uint8Array(buffer);
      var len = bytes.byteLength;
      for(var i = 0; i < len; i++) {
         binary += String.fromCharCode(bytes[i]);
      }
      return window.btoa(binary);
   }
   
   function output(perc) {
       // console.log("percentLoaded: "+perc + " %");

       // progress.style.width = perc + ''%''; //IE ERROR

        progress.textContent = perc + ''% uploaded'';
        
        //$("#fileUploadProgress").html(perc + " %");
        //document.getElementById("fileUploadProgress2").value( perc + " %");
        //alert(perc);                   
   }
   
   function sendChunk(fileText, file) {
      var vText = fileText;
      var vSendText = "";
      var txtUrl = "";
      var i = 1;
      var offset = 0;
      var percentLoaded = 0;
   
      while(vText.byteLength > 0 && i <10000000) {
         
         vSendText = vText.slice(0,chunkSize);
         vText = vText.slice(chunkSize);
         off = chunkSize * (i-1);
         
         vSendText = encodeURIComponent(_arrayBufferToBase64(vSendText));
         txtUrl = "!documents_api2.upload?filename="+file.name+"&offset="+off+"&chunksize="+chunkSize+"&chunkNumber="+i+"&contenttype="+file.type+"&page="+vSendText;
         
         $.ajax({
            url: txtUrl,
            type: "GET",
            async: false,
            processData: false,
            success: function (data, status) {
               //console.log(status);
               bytesUploaded += chunkSize;
               if (bytesUploaded > file.size) {bytesUploaded = file.size;}
              // console.log("bytesUploaded="+bytesUploaded);
              // console.log("file.size="+file.size);
               percentLoaded = Math.floor((bytesUploaded / file.size) * 100);
               output(percentLoaded);
            },
            error: function(xhr, desc, err) {
               console.log(desc);
               console.log(err);
            }                 
         });
         i++;
      }
     
     outputLink(); 
   }
</script>

<style>
   #progress_bar {
      margin: 10px 0;
      padding: 3px;
      border: 1px solid #000;
      font-size: 14px;
      clear: both;
      opacity: 0;
      -moz-transition: opacity 1s linear;
      -o-transition: opacity 1s linear;
      -webkit-transition: opacity 1s linear;
   }
   #progress_bar.loading {
      opacity: 1.0;
   }
   #progress_bar .percent {
      background-color: #99ccff;
      height: auto;
      width: 0;
   }
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
   #fileUploadProgress {
      padding:10px;
   }
   #list {
    font-size: 12;
    margin: 40px;
   } 	  
</style>

</HEAD>

<BODY>
   <div id="rasdSpinner" class="rasdSpinner" style="display:none;"></div> 
   <div id="fileHandler">
      <input type="file" id="files" name="file" />
      <button onclick="abortRead();" id="btnCancel" class="rasdButton">Cancel read</button>
      <button onclick="sendfile(); " id="btnUpload" class="rasdButton">Upload</button>');
if showfilelist = 'true' then
htp.p('
<br />
<br/>
	  These uploaded files are only accessed to your application with documents_api2.download_direct?file= URL. To store objects in library you shoud put object in /rasdlib project.

');
end if;
htp.p('	  <br/>
      <div id="progress_bar">
         <div class="percent">0%</div>
      </div>
      
      <div id="download">
      </div>

         <div id="list">

');   
if showfilelist = 'true' then
for r in (select name from documents  order by name) loop

htp.p('<A HREF="DOCUMENTS_API2.download_direct?file='||r.name||'" target="_bank">'||r.name||'</A></br>');

end loop;
end if;
htp.p('
     </div>	  
	  
   </div>

   <script>
   
      var reader;
      var progress = $(".percent");
      
      function abortRead() {
         $("#download").html("");
         reader.abort();
      }
      
      function errorHandler(evt) {
         switch(evt.target.error.code) {
            case evt.target.error.NOT_FOUND_ERR:
               alert(''File Not Found!'');
               break;
            case evt.target.error.NOT_READABLE_ERR:
               alert(''File is not readable'');
               break;
            case evt.target.error.ABORT_ERR:
               break; // noop
            default:
               alert(''An error occurred reading this file.'');
         };
      }
      
      function updateProgress(evt) {
         if(evt.lengthComputable) {
            var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
            if(percentLoaded < 100) {
               progress.style.width = percentLoaded + ''%'';
               progress.textContent = percentLoaded + ''%'';
            }
         }
      }
      
      function handleFileSelect(evt) {
         $("#download").html("");

         //progress.style.width = ''0%''; //IE ERROR
         //progress.textContent = ''0%'';
         
         reader = new FileReader();
         reader.onerror = errorHandler;
         reader.onprogress = updateProgress;
         reader.onabort = function(e) {
            alert(''File read cancelled'');
         };
         reader.onloadstart = function(e) {
            document.getElementById(''progress_bar'').className = ''loading'';
         };
         reader.onload = function(e) {
            $("#btnUpload").prop("disabled",false);
            $("#btnCancel").prop("disabled",true);
            //progress.style.width = ''100%'';
            //progress.textContent = ''100%'';
         }
         //reader.readAsBinaryString(evt.target.files[0]); //IE error
      }

      var el = document.getElementById(''files'');
      if (el.addEventListener) {
         el.addEventListener(''change'', handleFileSelect, false); 
      } else if (el.attachEvent) {
         el.attachEvent(''onchange'', handleFileSelect);
      }

      
      
   </script>

</BODY>
</HTML>
');
   end ;
  
   procedure upload(name_array  in owa.vc_arr, value_array in owa.vc_arr) is 
   begin
      declare
         p_b blob;
         p_body blob;
         p_offset number;
         p_filename varchar2(4000);
         p_raw long raw;
         p_chunksize varchar2(200);
         p_status varchar2(200);

         offset number;
         filename varchar2(4000);
         chunksize varchar2(200);
         chunkNumber number;
         contenttype documents.CONTENT_TYPE%type;
         page blob;
         x varchar2(32000);
      begin
         
         for i in 1..name_array.count loop    
            if name_array(i) = 'offset' then offset:= value_array(i);
            elsif name_array(i) = 'filename' then filename:= value_array(i);
            elsif name_array(i) = 'chunksize' then chunksize:= value_array(i);
            elsif name_array(i) = 'chunkNumber' then chunkNumber:= value_array(i);
            elsif name_array(i) = 'contenttype' then contenttype:= value_array(i);
            elsif name_array(i) = 'page' then page:= utl_encode.base64_decode(utl_raw.cast_to_raw(value_array(i)));
            end if;
            
   --         htp.p(name_array(i)||'='||value_array(i));
         end loop;
         
         -- pull the binds into locals
         p_offset:= OFFSET + 1;
         p_body:= page;
         p_filename:= filename;
         p_chunksize:= chunksize;
         
         -- if there is already a file with this name nuke it since this is chunk number one.
         if chunkNumber = 1 then
            p_status:= 'DELETING';
            delete from documents where name = user||'/'||p_filename;
         end if;
         
         -- grab the blob storing the first chunks
         select d.blob_content, doc_size into p_b, p_offset
           from documents d
          where name = user||'/'||p_filename 
            for update of blob_content;
         
         p_status:=' WRITING';
         
         -- append it
         dbms_lob.append(p_b, p_body);
         commit;
      exception 
         when no_data_found then
            -- if no blob found above do the first insert
            p_status:=' INSERTING';
            insert into documents(name, blob_content, doc_size, mime_type, last_updated)
            values (user||'/'||p_filename, p_body, p_offset, contenttype, sysdate);
            commit;
         when others then
            -- when something blows out print the error message to the client
            htp.p(p_status);
            htp.p(SQLERRM);
      end;   
   end upload;
   
   PROCEDURE download_direct (file  IN  VARCHAR2) AS
-- ----------------------------------------------------------------------------
  l_blob_content  documents.blob_content%TYPE;
  l_mime_type     documents.content_type%TYPE;
BEGIN

     select x.blob_content, nvl(x.mime_type,'xtext/plain')
    INTO   l_blob_content,
         l_mime_type
           from documents x
          where name = file;

  OWA_UTIL.mime_header(l_mime_type, FALSE);
  HTP.p('Content-Length: ' || DBMS_LOB.getlength(l_blob_content));
  HTP.p('Content-Disposition: filename="dwnld_'||file||'"');

  OWA_UTIL.http_header_close;

  WPG_DOCLOAD.download_file(l_blob_content);
EXCEPTION
  WHEN OTHERS THEN
    HTP.htmlopen;
    HTP.headopen;
    HTP.title('File Downloaded');
    HTP.headclose;

    HTP.bodyopen;
    HTP.header(1, 'Download Status');
    HTP.print('file='||file);
    HTP.print(SQLERRM);
    HTP.bodyclose;
    HTP.htmlclose;
END download_direct;

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('DOCUMENTS_API2',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
       begin
        return  '
$(function() {

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
 });
        ';
       end; 
     function form_css return clob is
       begin
        return '

        ';
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.2.20230107211230'; 
  end;
  function this_form return varchar2 is
  begin
   return 'DOCUMENTS_API2';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version ); 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
 rasd_client.sessionStart;
declare vc varchar2(2000); begin
null;
exception when others then  null; end;    rasd_client.sessionClose;  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,ACTION varchar2(4000) PATH '$.action'
)) jt ) loop
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PLINK varchar2(4000) PATH '$.plink'
  ,PFILE varchar2(4000) PATH '$.pfile'
)) jt ) loop
 if instr(RESTREQUEST,'plink') > 0 then PLINK(i__) := r__.PLINK; end if;
 if instr(RESTREQUEST,'pfile') > 0 then PFILE(i__) := r__.PFILE; end if;
i__ := i__ + 1;
end loop;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := value_array(i__);
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PLINK_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PLINK(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PFILE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PFILE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PLINK') and PLINK.count = 0 and value_array(i__) is not null then
        PLINK(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PFILE') and PFILE.count = 0 and value_array(i__) is not null then
        PFILE(1) := value_array(i__);
      end if;
    end loop;
-- organize records
-- init fields
    v_max := 0;
    if PLINK.count > v_max then v_max := PLINK.count; end if;
    if PFILE.count > v_max then v_max := PFILE.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PLINK.exists(i__) then
        PLINK(i__) := 'Upload FILE';
      end if;
      if not PFILE.exists(i__) then
        PFILE(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin

    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PLINK(i__) := 'Upload FILE';
        PFILE(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      pclear_P(PLINK.count);
  null; end;
  procedure pselect is
  begin


  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PLINK.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit is
  begin


  null; 
  end;
  procedure formgen_js is
  begin
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
  function ShowFieldERROR return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    return true;
  end; 
  function ShowBlockP_DIV return boolean is 
  begin 
    return true;
  end; 
  function js_link$link(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PLINK%' then
      v_return := v_return || '''!DOCUMENTS_API2.page?FIN=DOCUMENTS_API2.PFILE_1''';
    elsif name is null then
      v_return := v_return ||'''!DOCUMENTS_API2.page?FIN=DOCUMENTS_API2.PFILE_1''';
    end if;
    return v_return;
  end;
  procedure js_link$link(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$link(value, name));
  end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then  
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock"></div></caption>
<table border="0" id="P_TABLE"><tr id="P_BLOCK"><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPLINK"><span id="PLINK_LAB" class="label">Upload call documents_api2.page:</span></td><td class="rasdTxPLINK rasdTxTypeC" id="rasdTxPLINK_1"><input onclick="javascript: var link=window.open(encodeURI('); js_link$link(PLINK(1),'PLINK_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="PLINK_1" id="PLINK_1_RASD" type="button" value="'||PLINK(1)||'" class="rasdButton"/>
</td></tr><tr><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPFILE"><span id="PFILE_LAB" class="label">Name of uploaded file in database:</span></td><td class="rasdTxPFILE rasdTxTypeC" id="rasdTxPFILE_1"><input name="PFILE_1" id="PFILE_1_RASD" type="text" value="'||PFILE(1)||'" class="rasdTextC "/>
</td></tr><tr></tr></table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','New DOCUMENTS API Library for ORDS')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>'); 
 
htp.prn('</head>
<body><div id="DOCUMENTS_API2_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('DOCUMENTS_API2_LAB','New DOCUMENTS API Library for ORDS') ||'     </div><div id="DOCUMENTS_API2_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('DOCUMENTS_API2_MENU') ||'     </div>
<form name="DOCUMENTS_API2" method="post"><div id="DOCUMENTS_API2_DIV" class="rasdForm"><div id="DOCUMENTS_API2_HEAD" class="rasdFormHead"><input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
</div><div id="DOCUMENTS_API2_BODY" class="rasdFormBody">'); output_P_DIV; htp.p('</div><div id="DOCUMENTS_API2_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="DOCUMENTS_API2_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="DOCUMENTS_API2_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div><div id="DOCUMENTS_API2_FOOTER" class="rasdFormFooter">'|| rasd_client.getHtmlFooter(version , substr('DOCUMENTS_API2_FOOTER',1,instr('DOCUMENTS_API2_FOOTER', '_',-1)-1) , '') ||'</div></div></form></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockP_DIV return boolean is 
  begin 
    return true;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="DOCUMENTS_API2" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('</formfields>'); 
    if ShowBlockp_DIV then 
    htpp('<p>'); 
    htpp('<element>'); 
    htpp('<plink><![CDATA['||PLINK(1)||']]></plink>'); 
    htpp('<pfile><![CDATA['||PFILE(1)||']]></pfile>'); 
    htpp('</element>'); 
  htpp('</p>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"DOCUMENTS_API2","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp('},'); 
    if ShowBlockp_DIV then 
    htpp('"p":['); 
     htpp('{'); 
    htpp('"plink":"'||escapeRest(PLINK(1))||'"'); 
    htpp(',"pfile":"'||escapeRest(PFILE(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp('"p":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DOCUMENTS_API2',ACTION);  
  if ACTION is null then null;
    pselect;
    poutput;
  end if;

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','New DOCUMENTS API Library for ORDS')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head><body><div id="DOCUMENTS_API2_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('DOCUMENTS_API2_LAB','New DOCUMENTS API Library for ORDS') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'DOCUMENTS_API2' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('DOCUMENTS_API2_FOOTER',1,instr('DOCUMENTS_API2_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DOCUMENTS_API2',ACTION);  

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
    poutput;
  end if;

-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('DOCUMENTS_API2',ACTION);  
  if ACTION is null then null;
    pselect;
    poutputrest;
  end if;

  -- The execution after default execution based on  ACTION.
  if  ACTION is not null then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="DOCUMENTS_API2" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"DOCUMENTS_API2","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>79</formid><form>DOCUMENTS_API2</form><version>2</version><change>07.01.2023 09/12/30</change><user>RASDCLI</user><label><![CDATA[New DOCUMENTS API Library for ORDS]]></label><lobid>RASD</lobid><program></program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>30.09.2021 01/21/17</change><compileyn>Y</compileyn><application>RASD lib&apos;s</application><owner>rasd</owner><editor>rasd</editor></info></compiledInfo><blocks><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>FILE</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>20</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFILE</nameid><label><![CDATA[Name of uploaded file in database:]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field><field><blockid>P</blockid><fieldid>LINK</fieldid><type>C</type><format></format><element>INPUT_BUTTON</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''Upload FILE'']]></defaultval><elementyn>Y</elementyn><nameid>PLINK</nameid><label><![CDATA[Upload call documents_api2.page:]]></label><linkid>link$link</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type>';
 v_vc(2) := '<format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn></pkyn><selectyn></selectyn><insertyn></insertyn><updateyn></updateyn><deleteyn></deleteyn><insertnnyn></insertnnyn><notnullyn></notnullyn><lockyn></lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>G</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn></pkyn><selectyn></selectyn><insertyn></insertyn><updateyn></updateyn><deleteyn></deleteyn><insertnnyn></insertnnyn><notnullyn></notnullyn><lockyn></lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>G</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn></pkyn><selectyn></selectyn><insertyn></insertyn><updateyn></updateyn><deleteyn></deleteyn><insertnnyn></insertnnyn><notnullyn></notnullyn><lockyn></lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>G</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn></pkyn><selectyn></selectyn><insertyn></insertyn><updateyn></updateyn><deleteyn></deleteyn><insertnnyn></insertnnyn><notnullyn></notnullyn><lockyn></lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>G</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis></includevis></field></fields><links><link><linkid>link$link</linkid><link>LINK</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!DOCUMENTS_API2.page?FIN=DOCUMENTS_API2.PFILE_1]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link></lin';
 v_vc(3) := 'ks><pages></pages><triggers><trigger><blockid></blockid><triggerid>uplddownld</triggerid><plsql><![CDATA[/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

   procedure page(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);
   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = ''FIN'' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = ''POSTUPLOADURL'' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = ''SHOWFILES'' then showfilelist:= value_array(i);
            end if;
            
            --htp.p(name_array(i)||''=''||value_array(i));
         end loop;

      htp.p(''
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<script type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jquery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">
			
<style>
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
	  
</style>
			
</HEAD>

<BODY>
   <div id="fileHandler">
			
			<p> </p>
	  <form  enctype="multipart/form-data" action="!DOCUMENTS_API2.pageupload" method="post"> 		
      <input type="file" name="file">
      <button type="submit" id="btnUpload" class="rasdButton">Upload</button>
	  <input type="hidden" name="FIN" value="''||RETURNFILE||''"/>	
	  <input type="hidden" name="POSTUPLOADURL" value="''||POSTUPLOADURL||''"/>	
	  <input type="hidden" name="SHOWFILES" value="''||showfilelist||''"/>	
	  </form>
			<p>';
 v_vc(4) := ' </p>
    </div>
</BODY>
</HTML>			
'');

   end;		 
  
   procedure pageupload(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  v_out varchar2(10000);
  v_outjs varchar2(10000);  
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);
  l_real_name  VARCHAR2(1000);
  file varchar2(1000);
   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = ''FIN'' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = ''POSTUPLOADURL'' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = ''SHOWFILES'' then showfilelist:= value_array(i);

            elsif upper(name_array(i)) = ''FILE'' then file:= value_array(i);
            end if;
            
            --htp.p(name_array(i)||''=''||value_array(i));
         end loop;


	if file is not null then	
	begin 
  
      l_real_name := user||''/''||SUBSTR(file, INSTR(file, ''/'') + 1);

    DELETE FROM documents
    WHERE  name = l_real_name;

    -- Update the prefixed name with the real file name.
    UPDATE documents
    SET    name = l_real_name
    WHERE  name = file;

v_out := ''<p>Uploaded file: <A HREF="DOCUMENTS_API2.download_direct?file=''||l_real_name||''" target="_bank">''||l_real_name||''</A></p>'';		


if POSTUPLOADURL is not null then 

owa_util.redirect_url(
utl_url.escape(
replace(POSTUPLOADURL,''RETURNFILE'',l_real_name)
,false,''UTF-8'')
);
/*
owa_util.redirect_url(
  replace(
    replace(
      replace(
       replace( 
        replace(utl_url.escape(POSTUPLOADURL),''RETURNFILE'',l_real_name),''%3F'',''?''),''%3D'',''=''),''%26'',''&''),''%21'',''!'')  
);
*/
end if;



if RETURNFILE is not null then 
v_outjs := ''<script type="text/javascript">'';
v_outjs := v_outjs||''      window.opener.''||RETURNFILE||''.value = ''''''||l_real_name||'''''';'';
v_outjs := v_outjs||''      window.close();'';
v_outjs := v_outjs||''</script>'';
end if;


    exception when others then
	
v_out := ''<p>Uploaded failed.</A></p>'';		
	
    end;
    else

v_out := ''<p>Uploaded failed.</A></p>'';		
	
	end if;



      htp.p(''
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<scrip';
 v_vc(5) := 't type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jquery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">
''||v_outjs||''			
<style>
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
			
   #list {
    font-size: 12;
    margin: 40px;
   } 			
	  
</style>
			
</HEAD>

<BODY>
   <div id="fileHandler">
			'');

htp.p(v_out);

if showfilelist = ''true'' then
htp.p(''
<br />
<br/>
	  These uploaded files are only accessed to your application with documents_api2.download_direct?file= URL. To store objects in library you shoud put object in /rasdlib project.
'');
end if;			
htp.p(''			<p> </p>

         <div id="list">

'');   
if showfilelist = ''true'' then
for r in (select name from documents  order by name) loop

htp.p(''<A HREF="DOCUMENTS_API2.download_direct?file=''||r.name||''" target="_bank">''||r.name||''</A></br>'');

end loop;
end if;
htp.p(''
     </div>				
			<p> </p>
	      </div>

	  </BODY>
</HTML>			
'');  
  
  
   end; 
 
 
  
   procedure page_old(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  RETURNFILE varchar2(1000);
  POSTUPLOADURL varchar2(1000);
  showfilelist varchar2(100);

   begin

         for i in 1..name_array.count loop    
            if upper(name_array(i)) = ''FIN'' then RETURNFILE:= value_array(i);
            elsif upper(name_array(i)) = ''POSTUPLOADURL'' then POSTUPLOADURL:= value_array(i);
            elsif upper(name_array(i)) = ''SHOWFILES'' then showfilelist:= value_array(i);
            end if;
            
           -- htp.p(name_array(i)||''=''||value_array(i));
         end loop;

   
      htp.p(''
<HTML>
<HEAD>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="documents_api.download_direct?file=rasd/rasd.css">
<script type="text/javascript" src="documents_api.download_direct?file=rasd/rasd.js"></script>
<script type="text/javascript" src="/rasdlib/docs/jquery-1.11.3.min.js"></script>
<script src="/rasdlib/docs/jquery-ui-1.11.4.custom.zip/jquery-ui-1.11.4.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="/rasdlib/docs/jq';
 v_vc(6) := 'uery-ui-themes-1.11.4.zip/jquery-ui-themes-1.11.4/themes/redmond/jquery-ui.css">

<script type="text/javascript">

 $(function() {
     addSpinner();   
  });

   var chunkSize = 4000;
   var bytesUploaded = 0;
   
   //$(document).ready(function() {
   //   $("#btnUpload").prop("disabled",true);
   //});

   function sendfile() {
      var files = document.getElementById(''''files'''').files;
      var datoteka = files[0];
      var reader = new FileReader();

      //progress.style.width = "0%"; //IE ERROR
      progress.textContent = "0%";

      reader.onloadend = function(evt) {
         if(evt.target.readyState == FileReader.DONE) { 
            //console.log("zacetk: "+evt.target.result.byteLength);
            sendChunk(evt.target.result, datoteka);
         }
      };
      reader.readAsArrayBuffer(datoteka);
   }
   
   function outputLink() {
      var files = document.getElementById(''''files'''').files;
      var datoteka = files[0];
     $("#rasdSpinner").hide(); 
     $("#btnUpload").prop("disabled",false);
     $("#download").html( ''''Download file: <A HREF="documents_api2.download_direct?file=''||user||''/''''+ encodeURIComponent(datoteka.name) +''''" target="_bank">'''' + datoteka.name+''''</A>'''');     
'');
if POSTUPLOADURL is not null then 

htp.p(''      window.location = "''||
  replace(
    replace(
      replace(
       replace(
        replace(POSTUPLOADURL,''RETURNFILE'',user||''/"+encodeURIComponent(datoteka.name)+"''),''%3F'',''?''),''%3D'',''=''),''%26'',''&''),''%21'',''!'')
        ||''";'')
;


end if;

if RETURNFILE is not null then 
htp.p(''      window.opener.''||RETURNFILE||''.value = ''''''||user||''/''''+ encodeURIComponent(datoteka.name);'');
end if;
htp.p(''		
   }   
	
   function _arrayBufferToBase64(buffer) {
      var binary = '''''''';
      var bytes = new Uint8Array(buffer);
      var len = bytes.byteLength;
      for(var i = 0; i < len; i++) {
         binary += String.fromCharCode(bytes[i]);
      }
      return window.btoa(binary);
   }
   
   function output(perc) {
       // console.log("percentLoaded: "+perc + " %");

       // progress.style.width = perc + ''''%''''; //IE ERROR

        progress.textContent = perc + ''''% uploaded'''';
        
        //$("#fileUploadProgress").html(perc + " %");
        //document.getElementById("fileUploadProgress2").value( perc + " %");
        //alert(perc';
 v_vc(7) := ');                   
   }
   
   function sendChunk(fileText, file) {
      var vText = fileText;
      var vSendText = "";
      var txtUrl = "";
      var i = 1;
      var offset = 0;
      var percentLoaded = 0;
   
      while(vText.byteLength > 0 && i <10000000) {
         
         vSendText = vText.slice(0,chunkSize);
         vText = vText.slice(chunkSize);
         off = chunkSize * (i-1);
         
         vSendText = encodeURIComponent(_arrayBufferToBase64(vSendText));
         txtUrl = "!documents_api2.upload?filename="+file.name+"&offset="+off+"&chunksize="+chunkSize+"&chunkNumber="+i+"&contenttype="+file.type+"&page="+vSendText;
         
         $.ajax({
            url: txtUrl,
            type: "GET",
            async: false,
            processData: false,
            success: function (data, status) {
               //console.log(status);
               bytesUploaded += chunkSize;
               if (bytesUploaded > file.size) {bytesUploaded = file.size;}
              // console.log("bytesUploaded="+bytesUploaded);
              // console.log("file.size="+file.size);
               percentLoaded = Math.floor((bytesUploaded / file.size) * 100);
               output(percentLoaded);
            },
            error: function(xhr, desc, err) {
               console.log(desc);
               console.log(err);
            }                 
         });
         i++;
      }
     
     outputLink(); 
   }
</script>

<style>
   #progress_bar {
      margin: 10px 0;
      padding: 3px;
      border: 1px solid #000;
      font-size: 14px;
      clear: both;
      opacity: 0;
      -moz-transition: opacity 1s linear;
      -o-transition: opacity 1s linear;
      -webkit-transition: opacity 1s linear;
   }
   #progress_bar.loading {
      opacity: 1.0;
   }
   #progress_bar .percent {
      background-color: #99ccff;
      height: auto;
      width: 0;
   }
   #fileHandler {
      width: 450px;
      height: auto;
      border-radius: 5px;
      border: 2px solid black;
      padding:10px;
   }
   #fileUploadProgress {
      padding:10px;
   }
   #list {
    font-size: 12;
    margin: 40px;
   } 	  
</style>

</HEAD>

<BODY>
   <div id="rasdSpinner" class="rasdSpinner" style="display:none;"></div> 
   <div id="fileHandler">
      <input type="file" id="files';
 v_vc(8) := '" name="file" />
      <button onclick="abortRead();" id="btnCancel" class="rasdButton">Cancel read</button>
      <button onclick="sendfile(); " id="btnUpload" class="rasdButton">Upload</button>'');
if showfilelist = ''true'' then
htp.p(''
<br />
<br/>
	  These uploaded files are only accessed to your application with documents_api2.download_direct?file= URL. To store objects in library you shoud put object in /rasdlib project.

'');
end if;
htp.p(''	  <br/>
      <div id="progress_bar">
         <div class="percent">0%</div>
      </div>
      
      <div id="download">
      </div>

         <div id="list">

'');   
if showfilelist = ''true'' then
for r in (select name from documents  order by name) loop

htp.p(''<A HREF="DOCUMENTS_API2.download_direct?file=''||r.name||''" target="_bank">''||r.name||''</A></br>'');

end loop;
end if;
htp.p(''
     </div>	  
	  
   </div>

   <script>
   
      var reader;
      var progress = $(".percent");
      
      function abortRead() {
         $("#download").html("");
         reader.abort();
      }
      
      function errorHandler(evt) {
         switch(evt.target.error.code) {
            case evt.target.error.NOT_FOUND_ERR:
               alert(''''File Not Found!'''');
               break;
            case evt.target.error.NOT_READABLE_ERR:
               alert(''''File is not readable'''');
               break;
            case evt.target.error.ABORT_ERR:
               break; // noop
            default:
               alert(''''An error occurred reading this file.'''');
         };
      }
      
      function updateProgress(evt) {
         if(evt.lengthComputable) {
            var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
            if(percentLoaded < 100) {
               progress.style.width = percentLoaded + ''''%'''';
               progress.textContent = percentLoaded + ''''%'''';
            }
         }
      }
      
      function handleFileSelect(evt) {
         $("#download").html("");

         //progress.style.width = ''''0%''''; //IE ERROR
         //progress.textContent = ''''0%'''';
         
         reader = new FileReader();
         reader.onerror = errorHandler;
         reader.onprogress = updateProgress;
         reader.onabort = function(e) {
            alert(''''File read cancelled'''');
         };
         reader.onloads';
 v_vc(9) := 'tart = function(e) {
            document.getElementById(''''progress_bar'''').className = ''''loading'''';
         };
         reader.onload = function(e) {
            $("#btnUpload").prop("disabled",false);
            $("#btnCancel").prop("disabled",true);
            //progress.style.width = ''''100%'''';
            //progress.textContent = ''''100%'''';
         }
         //reader.readAsBinaryString(evt.target.files[0]); //IE error
      }

      var el = document.getElementById(''''files'''');
      if (el.addEventListener) {
         el.addEventListener(''''change'''', handleFileSelect, false); 
      } else if (el.attachEvent) {
         el.attachEvent(''''onchange'''', handleFileSelect);
      }

      
      
   </script>

</BODY>
</HTML>
'');
   end ;
  
   procedure upload(name_array  in owa.vc_arr, value_array in owa.vc_arr) is 
   begin
      declare
         p_b blob;
         p_body blob;
         p_offset number;
         p_filename varchar2(4000);
         p_raw long raw;
         p_chunksize varchar2(200);
         p_status varchar2(200);

         offset number;
         filename varchar2(4000);
         chunksize varchar2(200);
         chunkNumber number;
         contenttype documents.CONTENT_TYPE%type;
         page blob;
         x varchar2(32000);
      begin
         
         for i in 1..name_array.count loop    
            if name_array(i) = ''offset'' then offset:= value_array(i);
            elsif name_array(i) = ''filename'' then filename:= value_array(i);
            elsif name_array(i) = ''chunksize'' then chunksize:= value_array(i);
            elsif name_array(i) = ''chunkNumber'' then chunkNumber:= value_array(i);
            elsif name_array(i) = ''contenttype'' then contenttype:= value_array(i);
            elsif name_array(i) = ''page'' then page:= utl_encode.base64_decode(utl_raw.cast_to_raw(value_array(i)));
            end if;
            
   --         htp.p(name_array(i)||''=''||value_array(i));
         end loop;
         
         -- pull the binds into locals
         p_offset:= OFFSET + 1;
         p_body:= page;
         p_filename:= filename;
         p_chunksize:= chunksize;
         
         -- if there is already a file with this name nuke it since this is chunk number one.
         if chunkNumber = 1 then
            p_status:= ''DELETING'';
            delete from documents';
 v_vc(10) := ' where name = user||''/''||p_filename;
         end if;
         
         -- grab the blob storing the first chunks
         select d.blob_content, doc_size into p_b, p_offset
           from documents d
          where name = user||''/''||p_filename 
            for update of blob_content;
         
         p_status:='' WRITING'';
         
         -- append it
         dbms_lob.append(p_b, p_body);
         commit;
      exception 
         when no_data_found then
            -- if no blob found above do the first insert
            p_status:='' INSERTING'';
            insert into documents(name, blob_content, doc_size, mime_type, last_updated)
            values (user||''/''||p_filename, p_body, p_offset, contenttype, sysdate);
            commit;
         when others then
            -- when something blows out print the error message to the client
            htp.p(p_status);
            htp.p(SQLERRM);
      end;   
   end upload;
   
   PROCEDURE download_direct (file  IN  VARCHAR2) AS
-- ----------------------------------------------------------------------------
  l_blob_content  documents.blob_content%TYPE;
  l_mime_type     documents.content_type%TYPE;
BEGIN

     select x.blob_content, nvl(x.mime_type,''xtext/plain'')
    INTO   l_blob_content,
         l_mime_type
           from documents x
          where name = file;

  OWA_UTIL.mime_header(l_mime_type, FALSE);
  HTP.p(''Content-Length: '' || DBMS_LOB.getlength(l_blob_content));
  HTP.p(''Content-Disposition: filename="dwnld_''||file||''"'');

  OWA_UTIL.http_header_close;

  WPG_DOCLOAD.download_file(l_blob_content);
EXCEPTION
  WHEN OTHERS THEN
    HTP.htmlopen;
    HTP.headopen;
    HTP.title(''File Downloaded'');
    HTP.headclose;

    HTP.bodyopen;
    HTP.header(1, ''Download Status'');
    HTP.print(''file=''||file);
    HTP.print(SQLERRM);
    HTP.bodyclose;
    HTP.htmlclose;
END download_direct;

/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

]]></plsql><plsqlspec><![CDATA[';
 v_vc(11) := '/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
  
  procedure page(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
 
   procedure pageupload(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  
  procedure page_old(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
    procedure upload (
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  PROCEDURE download_direct (file  IN  VARCHAR2);
  
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/
 
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform><';
 v_vc(12) := '/rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% 
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''New DOCUMENTS API Library for ORDS'')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
%>]]></value><valuecode><![CDATA['');  
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''New DOCUMENTS API Library for ORDS'')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
 
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn><';
 v_vc(13) := '/hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>DOCUMENTS_API2</id><nameid>DOCUMENTS_API2</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[DOCUMENTS_API2]]></value><valuecode><![CDATA[="DOCUMENTS_API2"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><tex';
 v_vc(14) := 't></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>DOCUMENTS_API2_LAB</id><nameid>DOCUMENTS_API2_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_LAB]]></value><valuecode><![CDATA[="DOCUMENTS_API2_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forlo';
 v_vc(15) := 'op><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''DOCUMENTS_API2_LAB'',''New DOCUMENTS API Library for ORDS'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''DOCUMENTS_API2_LAB'',''New DOCUMENTS API Library for ORDS'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>DOCUMENTS_API2_MENU</id><nameid>DOCUMENTS_API2_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_MENU]]></value><valuecode><![CDATA[="DOCUMENTS_API2_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value';
 v_vc(16) := '><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''DOCUMENTS_API2_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''DOCUMENTS_API2_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>DOCUMENTS_API2_DIV</id><nameid>DOCUMENTS_API2_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_DIV]]></value><valuecode><![CDATA[="DOCUMENTS_API2_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop>';
 v_vc(17) := '</forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>19</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>DOCUMENTS_API2_HEAD</id><nameid>DOCUMENTS_API2_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_HEAD]]></value><valuecode><![CDATA[="DOCUMENTS_API2_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>19</pelementid><orderby>2</orderby><element>FORM_BODY</element><type>F</type><id>DOCUMENTS_API2_BODY</id><nameid>DOCUMENTS_API2_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></ri';
 v_vc(18) := 'd><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_BODY]]></value><valuecode><![CDATA[="DOCUMENTS_API2_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>19</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>DOCUMENTS_API2_MESSAGE</id><nameid>DOCUMENTS_API2_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attr';
 v_vc(19) := 'ibute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_MESSAGE]]></value><valuecode><![CDATA[="DOCUMENTS_API2_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>19</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>DOCUMENTS_API2_ERROR</id><nameid>DOCUMENTS_API2_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_ERROR]]></value><valuecode><![CDATA[="DOCUMENTS_API2_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><';
 v_vc(20) := '![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>19</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>DOCUMENTS_API2_WARNING</id><nameid>DOCUMENTS_API2_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_WARNING]]></value><valuecode><![CDATA[="DOCUMENTS_API2_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><sourc';
 v_vc(21) := 'e></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>19</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>DOCUMENTS_API2_FOOTER</id><nameid>DOCUMENTS_API2_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[DOCUMENTS_API2_FOOTER]]></value><valuecode><![CDATA[="DOCUMENTS_API2_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''DOCUMENTS_API2_FOOTER'',1,instr(''DOCUMENTS_API2_FOOTER'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''DOCUMENTS_API2_FOOTER'',1,inst';
 v_vc(22) := 'r(''DOCUMENTS_API2_FOOTER'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>21</pelementid><orderby>102</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop';
 v_vc(23) := '><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>26</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>27</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name';
 v_vc(24) := '><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>26</pelementid><orderby>103</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[="P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid>';
 v_vc(25) := '</rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_RASD]]></value><valuecode><![CDATA[="P_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>20</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>A';
 v_vc(26) := 'CTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7<';
 v_vc(27) := '/orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hi';
 v_vc(28) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valueco';
 v_vc(29) := 'de><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>23</pelementid><orderby>9996</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></ri';
 v_vc(30) := 'd></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></';
 v_vc(31) := 'textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>22</pelementid><orderby>9998</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><v';
 v_vc(32) := 'aluecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elemen';
 v_vc(33) := 'tid>35</elementid><pelementid>30</pelementid><orderby>19</orderby><element>TX_</element><type>E</type><id></id><nameid>PLINK_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPLINK]]></value><valuecode><![CDATA[="rasdTxLabPLINK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>35</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PLINK_LAB</id><nameid>PLINK_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop>';
 v_vc(34) := '</endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PLINK_LAB]]></value><valuecode><![CDATA[="PLINK_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[Upload call documents_api2.page:]]></value><valuecode><![CDATA[Upload call documents_api2.page:]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>29</pelementid><orderby>22</orderby><element>TR_</element><type>B</type><id></id><nameid>P_BLOCK22</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orde';
 v_vc(35) := 'rby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>30</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>PLINK_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPLINK rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPLINK rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPLINK_NAME]]></value><valuecode><![CDATA[="rasdTxPLINK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>38</pelementid><orderby>1</orderby><element>INPUT_BUTTON</element><type>D</type><id>PLINK</id><nameid>PLINK</nam';
 v_vc(36) := 'eid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PLINK_NAME_RASD]]></value><valuecode><![CDATA[="PLINK_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PLINK_NAME]]></value><valuecode><![CDATA[="PLINK_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[link$link]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$link(PLINK(1),''PLINK_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></';
 v_vc(37) := 'endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PLINK_VALUE]]></value><valuecode><![CDATA[="''||PLINK(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>37</pelementid><orderby>39</orderby><element>TX_</element><type>E</type><id></id><nameid>PFILE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><or';
 v_vc(38) := 'derby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPFILE]]></value><valuecode><![CDATA[="rasdTxLabPFILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>40</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PFILE_LAB</id><nameid>PFILE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFILE_LAB]]></value><valuecode><![CDATA[="PFILE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloo';
 v_vc(39) := 'p></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[Name of uploaded file in database:]]></value><valuecode><![CDATA[Name of uploaded file in database:]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>29</pelementid><orderby>42</orderby><element>TR_</element><type>B</type><id></id><nameid>P_BLOCK42</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>37</pelementid><orderby>40</orderby><element>TX_</element><type>E</type><id></id><nameid>PFILE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS';
 v_vc(40) := '</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPFILE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPFILE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPFILE_NAME]]></value><valuecode><![CDATA[="rasdTxPFILE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>43</pelementid><orderby>1</orderby><element>INPUT_TEXT</element><type>D</type><id>PFILE</id><nameid>PFILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextC ]]></value><valuecode><![CDATA[="rasdTextC "]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endlo';
 v_vc(41) := 'op></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFILE_NAME_RASD]]></value><valuecode><![CDATA[="PFILE_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFILE_NAME]]></value><valuecode><![CDATA[="PFILE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_SIZE</attribute><type>A</type><text></text><name><![CDATA[size]]></name><value><![CDATA[10]]></value><valuecode><![CDATA[="10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[text]]></value><valuecode><![CDATA[="text"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute';
 v_vc(42) := '><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFILE_VALUE]]></value><valuecode><![CDATA[="''||PFILE(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_DOCUMENTS_API2_v.1.2.20230107211230.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end DOCUMENTS_API2;
/

