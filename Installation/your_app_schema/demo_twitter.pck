create or replace package DEMO_TWITTER is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DEMO_TWITTER generated on 06.01.23 by user RASDCLI.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
end;
/

create or replace package body DEMO_TWITTER is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DEMO_TWITTER generated on 06.01.23 by user RASDCLI.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);  GBUTTONRES                    varchar2(4000) := 'GBUTTONRES'
;  GBUTTONSRC                    varchar2(4000) := 'GBUTTONSRC'
;
  MESSAGE                       varchar2(4000);  PAGE                          number := 0
;
  WARNING                       varchar2(4000);
  B10AUTHOR_ID                  ctab;
  B10TEXT                       ctab;
  B10EDIT_HISTORY_TWEET_IDS     ctab;
  B10ID                         ctab;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('DEMO_TWITTER',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
       begin
        return  '
$(function() {

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
 });
        ';
       end; 
     function form_css return clob is
       begin
        return '

        ';
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;
  function version return varchar2 is
  begin
   return 'v.1.1.20230106123000'; 
  end;
  function this_form return varchar2 is
  begin
   return 'DEMO_TWITTER';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version ); 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
 rasd_client.sessionStart;
declare vc varchar2(2000); begin
null;
exception when others then  null; end;    rasd_client.sessionClose;  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,ACTION varchar2(4000) PATH '$.action'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONSRC varchar2(4000) PATH '$.gbuttonsrc'
  ,PAGE number PATH '$.page'
)) jt ) loop
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonsrc') > 0 then GBUTTONSRC := r__.GBUTTONSRC; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
)) jt ) loop
if r__.x__ is not null then
i__ := i__ + 1;
end if;
end loop;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := value_array(i__);
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10AUTHOR_ID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10AUTHOR_ID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10TEXT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10TEXT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10EDIT_HISTORY_TWEET_IDS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10EDIT_HISTORY_TWEET_IDS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10ID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10ID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10AUTHOR_ID') and B10AUTHOR_ID.count = 0 and value_array(i__) is not null then
        B10AUTHOR_ID(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10TEXT') and B10TEXT.count = 0 and value_array(i__) is not null then
        B10TEXT(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10EDIT_HISTORY_TWEET_IDS') and B10EDIT_HISTORY_TWEET_IDS.count = 0 and value_array(i__) is not null then
        B10EDIT_HISTORY_TWEET_IDS(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10ID') and B10ID.count = 0 and value_array(i__) is not null then
        B10ID(1) := value_array(i__);
      end if;
    end loop;
-- organize records
declare
v_last number := 
B10AUTHOR_ID
.last;
v_curr number := 
B10AUTHOR_ID
.first;
i__ number;
begin
 if v_last <> 
B10AUTHOR_ID
.count then 
   v_curr := 
B10AUTHOR_ID
.FIRST;  
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10AUTHOR_ID.exists(v_curr) then B10AUTHOR_ID(i__) := B10AUTHOR_ID(v_curr); end if;
      if B10TEXT.exists(v_curr) then B10TEXT(i__) := B10TEXT(v_curr); end if;
      if B10EDIT_HISTORY_TWEET_IDS.exists(v_curr) then B10EDIT_HISTORY_TWEET_IDS(i__) := B10EDIT_HISTORY_TWEET_IDS(v_curr); end if;
      if B10ID.exists(v_curr) then B10ID(i__) := B10ID(v_curr); end if;
      i__ := i__ + 1;
      v_curr := 
B10AUTHOR_ID
.NEXT(v_curr);  
   END LOOP;
      B10AUTHOR_ID.DELETE(i__ , v_last);
      B10TEXT.DELETE(i__ , v_last);
      B10EDIT_HISTORY_TWEET_IDS.DELETE(i__ , v_last);
      B10ID.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10AUTHOR_ID.count > v_max then v_max := B10AUTHOR_ID.count; end if;
    if B10TEXT.count > v_max then v_max := B10TEXT.count; end if;
    if B10EDIT_HISTORY_TWEET_IDS.count > v_max then v_max := B10EDIT_HISTORY_TWEET_IDS.count; end if;
    if B10ID.count > v_max then v_max := B10ID.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B10AUTHOR_ID.exists(i__) then
        B10AUTHOR_ID(i__) := null;
      end if;
      if not B10TEXT.exists(i__) then
        B10TEXT(i__) := null;
      end if;
      if not B10EDIT_HISTORY_TWEET_IDS.exists(i__) then
        B10EDIT_HISTORY_TWEET_IDS(i__) := null;
      end if;
      if not B10ID.exists(i__) then
        B10ID(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin

    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B10AUTHOR_ID.delete; end if;
      for i__ in 1..j__ loop
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10AUTHOR_ID(i__) := null;
        B10TEXT(i__) := null;
        B10EDIT_HISTORY_TWEET_IDS(i__) := null;
        B10ID(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    ERROR := null;
    GBUTTONRES := 'GBUTTONRES';
    GBUTTONSRC := 'GBUTTONSRC';
    MESSAGE := null;
    PAGE := 0;
    WARNING := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_B10(0);

  null;
  end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10AUTHOR_ID.delete;
      B10TEXT.delete;
      B10EDIT_HISTORY_TWEET_IDS.delete;
      B10ID.delete;

      declare 
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR 
  select *
  from json_table(
/* instead of fixed json you can use:
	dd_REST.request(method => 'GET',
                             url => 'https://api.twitter.com/2/tweets/search/recent',
                             qpr => '{"query":[{"name":"query", "value": "(a)(lang:sl)"}
                                              ,{"name":"expansions", "value": "author_id,geo.place_id&place.fields=contained_within,country,country_code,full_name,geo,id,name,place_type"}]}',
                             bearer => 'your token',
                             hdr => '')
*/	
	'{
    "data": [
        {
            "author_id": "2159123897",
            "text": "RT @MatevzDular: - Ta nov National Geographic!\n- Ja, ugani kdo je not...\n- Kdo, a Dončić?\n- Ma ne, en boljši!\n...\n- Oči!!!... Pfijuuu... \"P…",
            "edit_history_tweet_ids": [
                "1611320234509570048"
            ],
            "id": "1611320234509570048"
        },
        {
            "author_id": "1470020867929354245",
            "text": "Ta nekdanji F1 dirkač je postal prvi lastnik Rimacove Nevere, a ob testni vožnji ni https://t.co/b71AmHRgEi #Tehnologija",
            "edit_history_tweet_ids": [
                "1611320179639857152"
            ],
            "id": "1611320179639857152"
        },
        {
            "author_id": "1028087575",
            "text": "RT @Tw33tSimone: @spagetyuse @BreznikTina A se ni Levica na zacetku svoje parlamentarne poti zaobljubljala, da ne bo noben poslanec Levice…",
            "edit_history_tweet_ids": [
                "1611320085041512450"
            ],
            "id": "1611320085041512450"
        }
    ],
    "includes": {
        "users": [
            {
                "id": "2159123897",
                "name": "Janč",
                "username": "YanchMb"
            },
            {
                "id": "1470020867929354245",
                "name": "Telex-si - tehnologija",
                "username": "Telexsi_tech"
            }
]
}
}',
                  '$.data[*]'
                  COLUMNS(author_id varchar2(1000) PATH '$.author_id',text varchar2(1000) PATH '$.text',  edit_history_tweet_ids varchar2(1000) FORMAT JSON PATH '$.edit_history_tweet_ids',id varchar2(1000) PATH '$.id')) jt;
        i__ := 1;
        LOOP 
          FETCH c__ INTO
            B10AUTHOR_ID(i__)
           ,B10TEXT(i__)
           ,B10EDIT_HISTORY_TWEET_IDS(i__)
           ,B10ID(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B10AUTHOR_ID.delete(1);
          B10TEXT.delete(1);
          B10EDIT_HISTORY_TWEET_IDS.delete(1);
          B10ID.delete(1);
          i__ := 0;
        end if; 
        CLOSE c__;
      end;
      pclear_B10(B10AUTHOR_ID.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 then 
      pselect_B10;
    end if;

  null;
 end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10AUTHOR_ID.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 then 
       pcommit_B10;
    end if;

  null; 
  end;
  procedure formgen_js is
  begin
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB10 pls_integer;
  function ShowFieldMESSAGE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldERROR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONSRC return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONRES return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then  
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption><table border="1" id="B10_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10AUTHOR_ID"><span id="B10AUTHOR_ID_LAB" class="label">author_id</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10TEXT"><span id="B10TEXT_LAB" class="label">text</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10EDIT_HISTORY_TWEET_IDS"><span id="B10EDIT_HISTORY_TWEET_IDS_LAB" class="label">edit_history_tweet_ids</span></td><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10ID"><span id="B10ID_LAB" class="label">id</span></td></tr></thead>'); for iB10 in 1..B10AUTHOR_ID.count loop 
htp.prn('<tr id="B10_BLOCK"><td class="rasdTxB10AUTHOR_ID rasdTxTypeC" id="rasdTxB10AUTHOR_ID_'||iB10||'"><font id="B10AUTHOR_ID_'||iB10||'_RASD" class="rasdFont">'||B10AUTHOR_ID(iB10)||'</font></td><td class="rasdTxB10TEXT rasdTxTypeC" id="rasdTxB10TEXT_'||iB10||'"><font id="B10TEXT_'||iB10||'_RASD" class="rasdFont">'||B10TEXT(iB10)||'</font></td><td class="rasdTxB10EDIT_HISTORY_TWEET_IDS rasdTxTypeC" id="rasdTxB10EDIT_HISTORY_TWEET_IDS_'||iB10||'"><font id="B10EDIT_HISTORY_TWEET_IDS_'||iB10||'_RASD" class="rasdFont">'||B10EDIT_HISTORY_TWEET_IDS(iB10)||'</font></td><td class="rasdTxB10ID rasdTxTypeC" id="rasdTxB10ID_'||iB10||'"><font id="B10ID_'||iB10||'_RASD" class="rasdFont">'||B10ID(iB10)||'</font></td></tr>'); end loop; 
htp.prn('</table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','Last messages Twitter - sl language')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>'); 
 
htp.prn('</head>
<body><div id="DEMO_TWITTER_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('DEMO_TWITTER_LAB','Last messages Twitter - sl language') ||'     </div><div id="DEMO_TWITTER_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('DEMO_TWITTER_MENU') ||'     </div>
<form name="DEMO_TWITTER" method="post" action="?"><div id="DEMO_TWITTER_DIV" class="rasdForm"><div id="DEMO_TWITTER_HEAD" class="rasdFormHead"><input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
');  
if  ShowFieldGBUTTONSRC  then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSRC" id="GBUTTONSRC_RASD" type="button" value="'||GBUTTONSRC||'" class="rasdButton"/>');  end if;  
htp.prn('
');  
if  ShowFieldGBUTTONRES  then  
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton"/>');  end if;  
htp.prn('
</div><div id="DEMO_TWITTER_BODY" class="rasdFormBody">'); output_B10_DIV; htp.p('</div><div id="DEMO_TWITTER_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="DEMO_TWITTER_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="DEMO_TWITTER_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div><div id="DEMO_TWITTER_FOOTER" class="rasdFormFooter">');  
if  ShowFieldGBUTTONSRC  then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSRC" id="GBUTTONSRC_RASD" type="button" value="'||GBUTTONSRC||'" class="rasdButton"/>');  end if;  
htp.prn('
');  
if  ShowFieldGBUTTONRES  then  
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton"/>');  end if;  
htp.prn('
'|| rasd_client.getHtmlFooter(version , substr('DEMO_TWITTER_FOOTER',1,instr('DEMO_TWITTER_FOOTER', '_',-1)-1) , '') ||'</div></div></form></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="DEMO_TWITTER" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>'); 
    htpp('<gbuttonsrc><![CDATA['||GBUTTONSRC||']]></gbuttonsrc>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<page>'||PAGE||'</page>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('</formfields>'); 
    if ShowBlockb10_DIV then 
    htpp('<b10>'); 
  for i__ in 1..
B10AUTHOR_ID
.count loop 
    htpp('<element>'); 
    htpp('<b10author_id><![CDATA['||B10AUTHOR_ID(i__)||']]></b10author_id>'); 
    htpp('<b10text><![CDATA['||B10TEXT(i__)||']]></b10text>'); 
    htpp('<b10edit_history_tweet_ids><![CDATA['||B10EDIT_HISTORY_TWEET_IDS(i__)||']]></b10edit_history_tweet_ids>'); 
    htpp('<b10id><![CDATA['||B10ID(i__)||']]></b10id>'); 
    htpp('</element>'); 
  end loop; 
  htpp('</b10>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"DEMO_TWITTER","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"'); 
    htpp(',"gbuttonsrc":"'||escapeRest(GBUTTONSRC)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"page":"'||PAGE||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp('},'); 
    if ShowBlockb10_DIV then 
    htpp('"b10":['); 
  v_firstrow__ := true;
  for i__ in 1..
B10AUTHOR_ID
.count loop 
    if v_firstrow__ then
     htpp('{'); 
     v_firstrow__ := false;
    else
     htpp(',{'); 
    end if;
    htpp('"b10author_id":"'||escapeRest(B10AUTHOR_ID(i__))||'"'); 
    htpp(',"b10text":"'||escapeRest(B10TEXT(i__))||'"'); 
    htpp(',"b10edit_history_tweet_ids":"'||escapeRest(B10EDIT_HISTORY_TWEET_IDS(i__))||'"'); 
    htpp(',"b10id":"'||escapeRest(B10ID(i__))||'"'); 
    htpp('}'); 
  end loop; 
    htpp(']'); 
  else 
    htpp('"b10":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_TWITTER',ACTION);  
  if ACTION is null then null;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     pselect;
    poutput;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD','Last messages Twitter - sl language')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head><body><div id="DEMO_TWITTER_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('DEMO_TWITTER_LAB','Last messages Twitter - sl language') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'DEMO_TWITTER' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('DEMO_TWITTER_FOOTER',1,instr('DEMO_TWITTER_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_TWITTER',ACTION);  

  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your oown.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
    poutput;
  end if;

-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_TWITTER',ACTION);  
  if ACTION is null then null;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     pselect;
    poutputrest;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="DEMO_TWITTER" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"DEMO_TWITTER","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>84</formid><form>DEMO_TWITTER</form><version>1</version><change>06.01.2023 12/30/00</change><user>RASDCLI</user><label><![CDATA[Last messages Twitter - sl language]]></label><lobid>RASD</lobid><program>?</program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>N</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>06.01.2023 12/20/29</change><compileyn>N</compileyn><application>Demo</application><owner>demo</owner><editor>demo</editor></info></compiledInfo><blocks></blocks><fields></fields><links></links><pages></pages><triggers></triggers><elements></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_DEMO_TWITTER_v.1.1.20230106123000.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end DEMO_TWITTER;
/

