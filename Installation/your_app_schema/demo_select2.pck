create or replace package DEMO_SELECT2 is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DEMO_SELECT2 generated on 28.09.22 by user RASDCLI.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;
/

create or replace package body DEMO_SELECT2 is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: DEMO_SELECT2 generated on 28.09.22 by user RASDCLI.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(256)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);
  ACTION                        varchar2(4000);
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);  GBUTTONSRC                    varchar2(4000) := 'Osveži'
;
  B10S1                         ctab;
  B10S2                         ctab;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('DEMO_SELECT2',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '

';
       end; 
     function form_js return clob is
       begin
        return  '
$(function() {

  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
//   setShowHideDiv("BLOCK_NAME_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
  
  
  $(''#B10S2_1_RASD'').select2();
  
 });
        ';
       end; 
     function form_css return clob is
       begin
        return '

        ';
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';  
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
cursor c_lov$department(p_id varchar2) is 
--<lovsql formid="81" linkid="lov$department">
select id, label from (select DEPTNO id, DEPTNO ||' ' || DNAME label from DEPT 
order by 1
 ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%' 
--</lovsql>
;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else 
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />'; 
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then 
      on_session;              
    end if;                   
  if lower(p_lov) = lower('lov$department') then
    v_description := 'DEPARTMENT';
    for r in c_lov$department(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;          
  else
   return;
  end if;
if v_call = 'PLSQL' then 
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;   
  end loop;  
elsif v_call = 'REST' then 
if RESTRESTYPE = 'XML' then 
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');      
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop; 
 htp.p('</result></openLOV>');
else 
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );      
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then 
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop; 
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Demo select2 ')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!DEMO_SELECT2.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20220928085109'; 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
 rasd_client.sessionStart;
declare vc varchar2(2000); begin
null;
if B10S1(i__) is null then vc := rasd_client.sessionGetValue('B10S1'); B10S1(i__)  := vc;  end if; 
if B10S2(i__) is null then vc := rasd_client.sessionGetValue('B10S2'); B10S2(i__)  := vc;  end if; 
exception when others then  null; end;    rasd_client.sessionClose;  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,ACTION varchar2(4000) PATH '$.action'
  ,GBUTTONSRC varchar2(4000) PATH '$.gbuttonsrc'
)) jt ) loop
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'gbuttonsrc') > 0 then GBUTTONSRC := r__.GBUTTONSRC; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10S1 varchar2(4000) PATH '$.b10s1'
  ,B10S2 varchar2(4000) PATH '$.b10s2'
)) jt ) loop
 if instr(RESTREQUEST,'b10s1') > 0 then B10S1(i__) := r__.B10S1; end if;
 if instr(RESTREQUEST,'b10s2') > 0 then B10S2(i__) := r__.B10S2; end if;
i__ := i__ + 1;
end loop;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := value_array(i__);
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := value_array(i__);
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := value_array(i__);
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := value_array(i__);
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := value_array(i__);
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10S1_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10S1(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10S2_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10S2(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10S1') and B10S1.count = 0 and value_array(i__) is not null then
        B10S1(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10S2') and B10S2.count = 0 and value_array(i__) is not null then
        B10S2(1) := value_array(i__);
      end if;
    end loop;
-- organize records
-- init fields
    v_max := 0;
    if B10S1.count > v_max then v_max := B10S1.count; end if;
    if B10S2.count > v_max then v_max := B10S2.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10S1.exists(i__) then
        B10S1(i__) := null;
      end if;
      if not B10S2.exists(i__) then
        B10S2(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="81" blockid="">
message := 'Value for S1 is:'||B10S1(1) || ' and for S2 is: '||B10S2(1);
--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10S1(i__) := null;
        B10S2(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    GBUTTONSRC := 'Osveži';
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_B10(0);

  null;
  end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      pclear_B10(B10S1.count);
  null; end;
  procedure pselect is
  begin


  null;
 end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10S1.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit is
  begin


  null; 
  end;
  procedure poutput is
  function ShowFieldERROR return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldGBUTTONSRC return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    return true;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    return true;
  end; 
  function ShowBlockB10_DIV return boolean is 
  begin 
    return true;
  end; 
  procedure js_Slov$department(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$department('''||value||''','''||name||''');');
    htp.p('</script>');
  end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then  
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption>
<table border="0" id="B10_TABLE"><tr id="B10_BLOCK"><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10S1"><span id="B10S1_LAB" class="label">Normal drop down:</span></td><td class="rasdTxB10S1 rasdTxTypeC" id="rasdTxB10S1_1"><select name="B10S1_1" ID="B10S1_1_RASD" class="rasdSelect ">'); js_Slov$department(B10S1(1),'B10S1_1'); 
htp.prn('</select></td></tr><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10S2"><span id="B10S2_LAB" class="label">Drop down using select2</span></td><td class="rasdTxB10S2 rasdTxTypeC" id="rasdTxB10S2_1"><select name="B10S2_1" ID="B10S2_1_RASD" class="rasdSelect ">'); js_Slov$department(B10S2(1),'B10S2_1'); 
htp.prn('</select></td></tr><tr></tr></table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.p('<script language="JavaScript">');
    htp.p('function js_Slov$department(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue+''"'');'); 
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
    begin
    for r__ in (
--<lovsql formid="81" linkid="lov$department">
select DEPTNO id, DEPTNO ||' ' || DNAME label from DEPT 
order by 1

--</lovsql>
    ) loop
      htp.p('  var option = document.createElement("option"); option.value="'||r__.id||'"; option.text = "'||replace(r__.label,'''','\''')||'"; option.selected = ((pvalue=='''||r__.id||''')?'' selected '':''''); x.add(option);');
    end loop;
    exception when others then
      raise_application_error('-20000','Error in lov$department: '||sqlerrm);  
    end;
    htp.p('}');
    htp.prn('</script>');
    htp.p('<script language="JavaScript">');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('</script>');
    htp.prn('<html>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Demo select2 ')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head>
<body><div id="DEMO_SELECT2_LAB" class="rasdFormLab">Demo select2  '|| rasd_client.getHtmlDataTable('DEMO_SELECT2_LAB') ||'     </div><div id="DEMO_SELECT2_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('DEMO_SELECT2_MENU') ||'     </div>
<form name="DEMO_SELECT2" method="post" action="?"><div id="DEMO_SELECT2_DIV" class="rasdForm"><div id="DEMO_SELECT2_HEAD" class="rasdFormHead"><input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
');  
if  ShowFieldGBUTTONSRC  then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSRC" id="GBUTTONSRC_RASD" type="button" value="'||GBUTTONSRC||'" class="rasdButton"/>');  end if;  
htp.prn('
</div><div id="DEMO_SELECT2_BODY" class="rasdFormBody">'); output_B10_DIV; htp.p('</div><div id="DEMO_SELECT2_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="DEMO_SELECT2_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="DEMO_SELECT2_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div><div id="DEMO_SELECT2_FOOTER" class="rasdFormFooter">');  
if  ShowFieldGBUTTONSRC  then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSRC" id="GBUTTONSRC_RASD" type="button" value="'||GBUTTONSRC||'" class="rasdButton"/>');  end if;  
htp.prn('
'|| rasd_client.getHtmlFooter(version , substr('DEMO_SELECT2_FOOTER',1,instr('DEMO_SELECT2_FOOTER', '_',-1)-1) , '') ||'</div></div></form></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is 
  begin 
    return true;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="DEMO_SELECT2" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('<gbuttonsrc><![CDATA['||GBUTTONSRC||']]></gbuttonsrc>'); 
    htpp('</formfields>'); 
    if ShowBlockb10_DIV then 
    htpp('<b10>'); 
    htpp('<element>'); 
    htpp('<b10s1><![CDATA['||B10S1(1)||']]></b10s1>'); 
    htpp('<b10s2><![CDATA['||B10S2(1)||']]></b10s2>'); 
    htpp('</element>'); 
  htpp('</b10>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"DEMO_SELECT2","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp(',"gbuttonsrc":"'||escapeRest(GBUTTONSRC)||'"'); 
    htpp('},'); 
    if ShowBlockb10_DIV then 
    htpp('"b10":['); 
     htpp('{'); 
    htpp('"b10s1":"'||escapeRest(B10S1(1))||'"'); 
    htpp(',"b10s2":"'||escapeRest(B10S2(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp('"b10":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_SELECT2',ACTION);  
  if ACTION is null then null;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     pselect;
    poutput;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  htpClob(rasd_client.getHtmlJSLibrary('HEAD','Demo select2 ')); htpClob(FORM_UIHEAD); htp.p('<style type="text/css">'); htpClob(FORM_CSS); htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>');  
htp.prn('</head><body><div id="DEMO_SELECT2_LAB" class="rasdFormLab">Demo select2  '|| rasd_client.getHtmlDataTable('DEMO_SELECT2_LAB') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'DEMO_SELECT2' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('DEMO_SELECT2_FOOTER',1,instr('DEMO_SELECT2_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_SELECT2',ACTION);  

  -- The execution after default execution based on  ACTION.
  -- Delete this code (if) when you have new actions and add your oown.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
    poutput;
  end if;

-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('DEMO_SELECT2',ACTION);  
  if ACTION is null then null;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     pselect;
    poutputrest;
  end if;

  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONSRC ) then 
    raise_application_error('-20000', 'ACTION="'||ACTION||'" is not defined. Define it in POST_ACTION trigger.');
  end if;

-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="DEMO_SELECT2" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"DEMO_SELECT2","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>81</formid><form>DEMO_SELECT2</form><version>1</version><change>28.09.2022 08/51/09</change><user>RASDCLI</user><label><![CDATA[Demo select2 ]]></label><lobid>RASD</lobid><program>?</program><referenceyn>N</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>N</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>28.09.2022 08/40/11</change><compileyn>N</compileyn><application></application><owner>demo</owner><editor>demo</editor></info></compiledInfo><blocks></blocks><fields></fields><links></links><pages></pages><triggers></triggers><elements></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_DEMO_SELECT2_v.1.1.20220928085109.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
end DEMO_SELECT2;
/

