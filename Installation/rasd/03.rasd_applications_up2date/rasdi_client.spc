create or replace package rasdi_client IS
  /*
  // +----------------------------------------------------------------------+
  // | RASD - Rapid Application Service Development                         |
  // +----------------------------------------------------------------------+
  // | Copyright (C) 2014       http://rasd.sourceforge.net                 |
  // +----------------------------------------------------------------------+
  // | This program is free software; you can redistribute it and/or modify |
  // | it under the terms of the GNU General Public License as published by |
  // | the Free Software Foundation; either version 2 of the License, or    |
  // | (at your option) any later version.                                  |
  // |                                                                      |
  // | This program is distributed in the hope that it will be useful       |
  // | but WITHOUT ANY WARRANTY; without even the implied warranty of       |
  // | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         |
  // | GNU General Public License for more details.                         |
  // +----------------------------------------------------------------------+
  // | Author: Domen Dolar       <domendolar@users.sourceforge.net>         |
  // +----------------------------------------------------------------------+
  */
  function version(p_log out varchar2) return varchar2;

  type rtab is table of rowid index by binary_integer;
  type ntab is table of number index by binary_integer;
  type dtab is table of date index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of varchar2(32000) index by binary_integer;
  type itab is table of pls_integer index by binary_integer;

  e_finished exception;

  
  /* do not change*/ C_DATE_FORMAT    constant varchar2(10) := 'mm/dd/yyyy';
  /* do not change*/ C_DATE_TIME_FORMAT    constant varchar2(30) := 'mm/dd/yyyy hh24:mi:ss';
  /* do not change*/ C_TIMESTAMP_FORMAT constant varchar2(30) := 'mm/dd/yyyy hh24:mi:ss.ff';
  /* do not change*/ C_NUMBER_DECIMAL constant varchar2(1) := ',';
  /* do not change*/ C_NUMBER_THOUSAND constant varchar2(1) := '.';
  
  type dt is varray(2) of varchar2(30);
  type dttab is table of dt;
  c_date_transform constant dttab := dttab(
      -- PL/SQL type , jQuery type : format is case sensitive
      dt('yyyy-mm-dd','yy-mm-dd') ,
      dt('yyyy/mm/dd','yy/mm/dd') ,
      dt('dd.mm.yyyy','dd.mm.yy') ,
      dt('dd/mm/yyyy', 'dd/mm/yy') ,
      dt('dd-mm-yyyy', 'dd-mm-yy') ,
      dt('mm/dd/yyyy', 'mm/dd/yy') ,
      -- default type NULL must always be the last element
      dt('NULL','mm/dd/yy')
  );
  type dtt is varray(2) of varchar2(30);
  type dtttab is table of dtt;
  c_time_transform constant dtttab := dtttab(
      -- PL/SQL type , jQuery type : ime format is case sensitive
      dtt('hh24:mi','hh:mm') ,
      dtt('hh24:mi:ss','hh:mm:ss'),
      -- default type NULL must always be the last element
      dtt('NULL','hh:mm')
  );
    
  c_registerTo varchar2(100) := 'place your enviorment in RASDI_CLIENT - c_registerTo'; 
  c_defaultLanguage      varchar2(30) := 'EN'; -- translates are in table rasd_translates
  c_WalletLocation       varchar2(50) := 'file:/u01/../wallet';
  c_WalletPwd            varchar2(50) := 'pwd';
  c_GITLocation          varchar2(100) := '';

  -- DOCUMENT table for RASD
  c_HtmlDocumentTable varchar2(30) := 'rasd_files';
  -- DOCUMENT TABLE for LOB/s - if you have many LOBs change RASDC2_CSSJS function getDocumentTable
  c_HtmlDocumentTable_LOB1 varchar2(30) := 'my_app_schema.documents';
  --c_HtmlDocumentTable_LOB2 varchar2(30) := 'your_app_schema2.documents';

  c_HtmlJSLibraryFile varchar2(30) := 'rasd/rasd_jslib.html';
  c_HtmlDataTableFile varchar2(30) := 'rasd/rasd_datatable.html';
  c_HtmlDataTable2File varchar2(30) := 'rasd/rasd_datatable2.html';
  c_HtmlHeaderFile  varchar2(30) := 'rasd/rasd_header.html';
  c_HtmlHtmlDatePickerFile varchar2(30) := 'rasd/rasd_datepicker.html';
  c_HtmlFooterFile varchar2(30) := 'rasd/rasd_footer.html';
  
  function varchr2number(p_value varchar2) return number;


  /*Security functions*/
  function secSuperUsers return varchar2;
  function secGetUsername return varchar2;
  function secGetLOB return varchar2;
  procedure secCheckPermission(p_form varchar2, p_action varchar2);
  procedure secCheckCredentials(p_username varchar2,
                                p_password varchar2,
                                p_other    varchar2 default null);

  procedure sessionStart;
  procedure sessionSetValue(pname varchar2, pvalue varchar2);
  function  sessionGetValue(pname varchar2) return varchar2;
  procedure sessionClose;

  --New RASD
  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2;
  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlMenuList(p_formname varchar2) return varchar2;
  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2;
  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2;

  /*Security functions new RASD*/
  function secGet_HTTP_ACCEPT return varchar2;
  function secGet_HTTP_ACCEPT_ENCODING return varchar2;
  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2;
  function secGet_HTTP_ACCEPT_CHARSET return varchar2;
  function secGet_HTTP_HOST return varchar2;
  function secGet_HTTP_PORT return varchar2;
  function secGet_HTTP_USER_AGENT return varchar2;
  function secGet_PATH_INFO return varchar2;
  function secGet_PATH_ALIAS return varchar2;
  function secGet_REMOTE_ADDR return varchar2;
  function secGet_REQUEST_CHARSET return varchar2;
  function secGet_SCRIPT_NAME return varchar2;  
  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr);
    

  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 );

end;
/
