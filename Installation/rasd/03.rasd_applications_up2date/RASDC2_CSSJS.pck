create or replace package RASDC2_CSSJS is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_CSSJS generated on 01.11.23 by user RASDDEV.     
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
function version return varchar2;
function this_form return varchar2;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
function metadata return clob;
procedure metadata;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure rlog(v_clob clob);
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  );
  function changes(p_log out varchar2) return varchar2 ;
procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2, m varchar2);
function getDocumentTable return varchar2;

procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
);
end;
/
create or replace package body RASDC2_CSSJS is
/*
// +----------------------------------------------------------------------+
// | RASD - Rapid Application Service Development                         |
//   Program: RASDC2_CSSJS generated on 01.11.23 by user RASDDEV.    
// +----------------------------------------------------------------------+
// | http://rasd.sourceforge.net                                          |
// +----------------------------------------------------------------------+
// | This program is generated form RASD version 1.                       |
// +----------------------------------------------------------------------+
*/    
  type rtab is table of rowid          index by binary_integer;
  type ntab is table of number         index by binary_integer;
  type dtab is table of date           index by binary_integer;
  type ttab is table of timestamp      index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of clob index by binary_integer;
  type itab is table of pls_integer    index by binary_integer;
  type set_type is record
  (
    visible boolean default true,
    readonly boolean default false,
    disabled boolean default false,
    required boolean default false,
    error varchar2(4000) ,
    info varchar2(4000) ,
    custom   varchar2(2000)
  );
  type stab is table of set_type index by binary_integer;
  log__ clob := '';
  set_session_block__ clob := '';
  RESTREQUEST clob := '';
  TYPE LOVrec__ IS RECORD (label varchar2(4000),id varchar2(4000) );
  TYPE LOVtab__ IS TABLE OF LOVrec__ INDEX BY BINARY_INTEGER;
  LOV__ LOVtab__;
  RESTRESTYPE varchar2(4000);  RECNUMB10                     number := 1
;  RECNUMPG                      number := 1
;  RECNUMB10G                    number := 1
;  RECNUMP                       number := 1
;
  ACTION                        varchar2(4000);  GBUTTONBCK                    varchar2(4000) := 'GBUTTONBCK'
;  GBUTTONCLR                    varchar2(4000) := 'GBUTTONCLR'
;  GBUTTONFWD                    varchar2(4000) := 'GBUTTONFWD'
;  PAGE                          number := 0
;
  LANG                          varchar2(4000);
  PFORMID                       varchar2(4000);
  PFORM                         varchar2(4000);  GBTNNAVIG                     varchar2(4000) := RASDI_TRNSLT.text('Form navigator',LANG)
;  GBUTTONSRC                    varchar2(4000) := RASDI_TRNSLT.text('Search',LANG)
;  GBUTTONSAVE                   varchar2(4000) := RASDI_TRNSLT.text('Save',LANG)
;
  GBUTTONSAVE#SET                set_type;  GBUTTONCOMPILE                varchar2(4000) := RASDI_TRNSLT.text('Compile',LANG)
;
  GBUTTONCOMPILE#SET             set_type;  GBUTTONRES                    varchar2(4000) := RASDI_TRNSLT.text('Reset',LANG)
;
  GBUTTONRES#SET                 set_type;  GBUTTONPREV                   varchar2(4000) := RASDI_TRNSLT.text('Preview',LANG)
;
  GBUTTONPREV#SET                set_type;
  ERROR                         varchar2(4000);
  MESSAGE                       varchar2(4000);
  WARNING                       varchar2(4000);
  VUSER                         varchar2(4000);
  VLOB                          varchar2(4000);
  COMPID                        varchar2(4000);
  DOCPRIVSOK                    varchar2(4000);  GBUTTONDELETE                 varchar2(4000) := RASDI_TRNSLT.text('Delete',LANG)
;
  G_TABLE                       varchar2(4000);
  HINTCONTENT                   varchar2(4000);
  PGOPTIONS                     clob;
  PMIRROR                       varchar2(4000);
  UNLINK                        varchar2(4000);
  PBLOKPROZILECNOV              ctab;
  PBLOKPROZILECNOV#SET           stab;
  PGBLOKPROZILECNOV             ctab;
  PGBLOKPROZILECNOV#SET          stab;
  B10RS                         ctab;
  B10GRS                        ctab;
  B10GRID                       rtab;
  B10RID                        rtab;
  PPF                           ctab;
  PPF#SET                        stab;
  PGPF                          ctab;
  PGPF#SET                       stab;
  PGBTNSRC                      ctab;
  PGBTNSRC#SET                   stab;
  PGGBTNSRC                     ctab;
  PGGBTNSRC#SET                  stab;
  PGGBTNSAVE                    ctab;
  PGGBTNSAVE#SET                 stab;
  PGBTNSAVE                     ctab;
  PGBTNSAVE#SET                  stab;
  PLDELETE                      ctab;
  PLDELETE#SET                   stab;
  PUPLOAD                       ctab;
  PUPLOAD#SET                    stab;
  PGUPLOAD                      ctab;
  PGUPLOAD#SET                   stab;
  PGHINT                        ctab;
  PHINT                         ctab;
  PSESSSTORAGEENABLED           ctab;
  PGSESSSTORAGEENABLED          ctab;
  PTEXTTYPE                     ctab;
  PGTEXTTYPE                    ctab;
  B20TEMPLATE                   ctab;
  B10GBLOBVC                    cctab;
  B10GBLOBVC#SET                 stab;
  B10GNAME                      ctab;
  B20EDITOR                     ctab;
  B20REGEX                      ctab;
  B30TEXT                       ctab;
  B10FORMID                     ntab;
  B10BLOCKID                    ctab;
  B10TRIGGERID                  ctab;
  B10PLSQL                      cctab;
  B10PLSQL#SET                   stab;
  B10PLSQLSPEC                  cctab;
  B10PLSQLSPEC#SET               stab;
  B10SOURCE                     ctab;
  B10HIDDENYN                   ctab;
  B10RLOBID                     ctab;
  B10RFORM                      ctab;
  B10RBLOCKID                   ctab;
  B10LDELETE                    ctab;
  function changes(p_log out varchar2) return varchar2 is
  begin

    p_log := '/* Change LOG:
20230301 - Created new 2 version	
20200410 - Added new compilation message     
20181129 - Added character counter    
20180917 - Added order by in drop down lists    
20180608 - Default code for JS, ...    
20180423 - Added option to upload file for RASD engine - do not use these in build forms - applications     
20170119 - Added new trigger FORM_UIHEAD - for customizing form element <HEAD>
20160704 - Added button Preview    
20160629 - Added log function for RASD.      
20160629 - Added CSS_REF and JS_REF triggers.          
20160627 - Included reference form future.    
20160408 - Solved bug with large data in general CSS,JS part
20160324 - Added Compile button
20160310 - Included CodeMirror    
20151202 - Included session variables in filters    
20150817 - Added tab look
20150814 - Added superuser    
20150813 - Added option to create JS and CSS for custom FORM
20141027 - Added footer on all pages
20141126 - solved error on package - using method currentDADUser
*/';
    return version;
 end;	
procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2, m varchar2) is
begin

htp.p('
function rasd_codemirror_'||n||'() {	  
 
var mime;
var m = '''||m||''';	  
if (m==''JS'') {
  mime = "text/javascript";
} else if (m==''CSS'') {
  mime = "text/css";
} else if (m==''HTML'') {
  mime = "text/html";
} else {
  mime = "text/sql";
}	  
  // get mime type
  if (window.location.href.indexOf("mime=") > -1) {
    mime = window.location.href.substr(window.location.href.indexOf("mime=") + 5);
  }

if (document.getElementById("'||n||'") != null) {
  
window.rasd_CMEditor'||n||' = CodeMirror.fromTextArea(document.getElementById("'||n||'"), {
    mode: mime, 
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
	lineWrapping: true,
    matchBrackets : true,
    autofocus: true,
    foldGutter: true,  
	gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
    foldOptions: {
  rangeFinder: (cm, pos) => {	  							   
    var line=window.rasd_CMEditor'||n||'.getLine(pos.line);    
	  var match=line.match(/\/\/\/\/ /);

    if (match==null) {
      return CodeMirror.fold.auto(cm,pos);
    }
    else {
      var lineend = window.rasd_CMEditor'||n||'.lineCount();
      for (let i = pos.line+1; i < lineend; i++) {
         if ( window.rasd_CMEditor'||n||'.getLine(i).match(/\/\/\/\/ /) == null ) 
            {}
         else 
            {lineend = i-1; break; } 
      }
        var startPos=CodeMirror.Pos(pos.line+1, 0);      
	    var endPos=CodeMirror.Pos(lineend, 0);
      return endPos && CodeMirror.cmpPos(endPos, startPos) > 0 ? {from: startPos, to: endPos} : null;
    }
  }
    },	  
    extraKeys: {"Ctrl-Space": "autocomplete",
	            "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },
	            "Ctrl-Y": cm => CodeMirror.commands.foldAll(cm),
                "Ctrl-I": cm => CodeMirror.commands.unfoldAll(cm),
                "Alt-S": function(instance) { document.'||this_form||'.ACTION.value='''||GBUTTONSAVE||'''; document.'||this_form||'.submit(); },
	            "F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));},
	            "Esc": function(cm) { if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);}
	  }
    });  
	  
  //window.rasd_CMEditor'||n||'.foldCode(CodeMirror.Pos(1, 0));  
}
	  
}');


end;



function getDocumentTable return varchar2 is
  v_table varchar2(100);
begin

  if instr(owa_util.get_cgi_env('DOCUMENT_TABLE'),'.') = 0 then 
      v_table := owa_util.get_cgi_env('REMOTE_USER')||'.'||owa_util.get_cgi_env('DOCUMENT_TABLE'); 
    else v_table:=owa_util.get_cgi_env('DOCUMENT_TABLE');         
  end if;
  
  -- Set location of your DOCUMENT_TABLE like set in your LOB RASD_CLIENT.c_HtmlDocumentTable
  --  v_table := 'your_app_schema.documents';
v_table := 'rasdcli.documents'; 
  -- Uncomment and change upper row.	
  if v_table is not null then
 
  begin   
  select distinct owner||'.'||table_name into v_table
  from table_privileges where owner||'.'||table_name = upper(v_table);  

  exception when others then
    raise_application_error ('-20000','In RASDC2_CSSJS function getDocumentTable set DOCUMENT TABLE or set priviliges on it.');
  end;  
  
  else

    raise_application_error ('-20000','In RASDC2_CSSJS function getDocumentTable set DOCUMENT TABLE.');
  end if;
  
  return v_table;

end;
  function myTabs(page varchar2 , lang varchar2, pformid varchar2) return varchar2 is
    v_tabs varchar2(32000);
  begin
rlog('ee'||page);  
    v_tabs := '
  <ul style="display: none;">
    <li><a href='; 
	if page= 1 then 
	  v_tabs := v_tabs ||'"#idselected"'; 
	else v_tabs := 
	  v_tabs ||'"#x"'; 
	end if; 
	v_tabs := v_tabs ||' onclick="javascript: location=encodeURI(''?page=1&LANG='||lang||'&PFORMID='||pformid||''');">
	'||RASDI_TRNSLT.text('Form code (javascript, CSS)',LANG)||'
	</a></li>
    <li><a href='; if page= 2 then v_tabs := v_tabs ||'"#idselected"'; else v_tabs := v_tabs ||'"#x"'; 
	end if; 
	v_tabs := v_tabs ||' onclick="javascript: location=encodeURI(''?page=2&LANG='||lang||'&PFORMID='||pformid||''');">
	'||RASDI_TRNSLT.text('Global code (javascript, CSS)',LANG)||'
	</a></li>
  </ul>
' ;

   return v_tabs;
  end;
function showLabel(plabel varchar2, pcolor varchar2 default 'U', pshowdialog number default 0) 
return varchar2 is
   v__ varchar2(32000);
begin

v__ := RASDI_TRNSLT.text(plabel, lang);

if pshowdialog = 1 then 
v__ := v__ || rasdc_hints.linkDialog(replace(plabel,' ',''),lang,replace(this_form,'2','')||'_DIALOG');
end if;

if pcolor is null then

return v__;

else

return '<font color="'||pcolor||'">'||v__||'</font>';

end if;


end;
procedure post_submit_template is
begin
RASDC_LIBRARY.checkprivileges(PFORMID);
begin
      select upper(form)
        into pform
        from RASD_FORMS
       where formid = PFORMID;
exception when others then null;
end;

if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then
  if rasdc_library.allowEditing(pformid) then
     null;  
  else	 
     ACTION := GBUTTONSRC;
	 message := message || RASDI_TRNSLT.text('User has no privileges to save data!', lang);
  end if;
end if;


if rasdc_library.allowEditing(pformid) then
   GBUTTONSAVE#SET.visible := true;
   GBUTTONCOMPILE#SET.visible := true;
else
   GBUTTONSAVE#SET.visible := false;
   GBUTTONCOMPILE#SET.visible := false;
end if;


VUSER := rasdi_client.secGetUsername;
VLOB := rasdi_client.secGetLOB;
if lang is null then lang := rasdi_client.c_defaultLanguage; end if;

end;
procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''  , pcompid in out number) is 
      v_server RASD_ENGINES.server%type;
      cid      pls_integer;
      n        pls_integer;
      vup      varchar2(30) := rasdi_client.secGetUsername;
begin
        rasdc_library.log(this_form,pformid, 'COMPILE_S', pcompid);          
        begin
          if pform in ( 'RASDC2_BLOCKSONFORM',
                        'RASDC2_FIELDSONBLOCK',
                        'RASDC2_TRIGGERS',
                        'RASDC2_LINKS',
                        'RASDC2_PAGES',
                        'RASDC2_FORMS',
                        'RASDC2_REFERENCES') then
        
            sporocilo := RASDI_TRNSLT.text('From is not generated.', lang);            
          else
            
            select server
              into v_server
              from RASD_FORMS_COMPILED fg, RASD_ENGINES g
             where fg.engineid = g.engineid
               and fg.formid = PFORMID
               and fg.editor = vup
               and (fg.lobid = rasdi_client.secGetLOB or
                   fg.lobid is null and rasdi_client.secGetLOB is null);
            
            cid     := dbms_sql.open_cursor;
            
            dbms_sql.parse(cid,
                           'begin ' || v_server || '.c_debug := false;'|| v_server || '.form(' || PFORMID ||
                           ',''' || lang || ''');end;',
                           dbms_sql.native);
            
            n       := dbms_sql.execute(cid);
            
            dbms_sql.close_cursor(cid);
            
            sporocilo := RASDI_TRNSLT.text('From is generated.', lang) || rasdc_library.checknumberofsubfields(PFORMID);

        if refform is not null then
           sporocilo :=  sporocilo || '<br/> - '||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';
        end if; 
                    
          end if;
        exception 
          when others then
            if sqlcode = -24344 then
              
            sporocilo := RASDI_TRNSLT.text('Form is generated with compilation error. Check your code.', lang)||'('||sqlerrm||')';
             
            else
            sporocilo := RASDI_TRNSLT.text('Form is NOT generated - internal RASD error.', lang) || '('||sqlerrm||')<br>'||
                         RASDI_TRNSLT.text('To debug run: ', lang) || 'begin ' || v_server || '.form(' || PFORMID ||
                         ',''' || lang || ''');end;' ;
            end if;
        end;
        rasdc_library.log(this_form,pformid, 'COMPILE_E', pcompid);     
end;
  function poutputrest return clob;
     procedure htpClob(v_clob clob) is
        i number := 0;
        v clob := v_clob;
       begin
       while length(v) > 0 and i < 100000 loop
        htp.prn(substr(v,1,10000));
        i := i + 1;
        v := substr(v,10001);
       end loop; 
       end; 
     procedure rlog(v_clob clob) is
       begin
        log__ := log__ ||systimestamp||':'||v_clob||'<br/>';
        rasd_client.callLog('RASDC2_CSSJS',v_clob, systimestamp, '' );
       end; 
procedure pLog is begin htpClob('<div class="debug">'||log__||'</div>'); end;
     function FORM_UIHEAD return clob is
       begin
        return  '
<script type="text/javascript" src="RASDC2_CSSJS.codemirrorjs?n='||pmirror||'&h=150&d='||PSESSSTORAGEENABLED(1)||'&f='||pformid||'&m='||PTEXTTYPE(1)||'"></script>
';
       end; 
     function form_js return clob is
          v_out clob;
       begin
        v_out :=  '
$(function() {

//  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
   setShowHideDiv("B30_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
  
  
HighLightBack("referenceBlock", "#aaccf7");

  
  
$(".rasdTxPTRIGGERIDPROC INPUT").attr("maxlength", 30);
 
  
 });



$(function() {

  
  rasd_codemirror_'||PMIRROR||'();
   
  window.rasd_CMEditor'||PMIRROR||'.setSize("1400","450");
  //32768 max each spec char un UTF8 has 2byts but in editor counts 1byte
  window.rasd_CMEditor'||PMIRROR||'.setOption("maxLength", 32000); //32000 is limit of vc_arr type in plsql for length of value  of one element

  window.rasd_CMEditor'||PMIRROR||'.on("beforeChange", function (cm, change) {
    var maxLength = cm.getOption("maxLength");
    var isChrome =  window.chrome;
    if (maxLength && change.update) {
        var str = change.text.join("\n");
        var delta = str.length-(cm.indexFromPos(change.to) - cm.indexFromPos(change.from));
        var aaa = cm.doc.getValue();
        if (delta <= 0) { return true; }
        xval = cm.getValue()
        if(isChrome){
          xval = xval.replace(/(\r\n|\n|\r)/g,"  ");
        }          
        delta = xval.length+delta-maxLength;
        document.getElementById("PLSQLCOUNT").innerHTML = "'|| RASDI_TRNSLT.text('Characters left:', lang) ||' " + ((delta*-1)+1) ;
        if (delta > 0) {
            str = str.substr(0, str.length-delta);
            change.update(change.from, change.to, str.split("\n"));
        }
    }
    return true;  
  });  

//// Tabulators  
$(function() {    
  $( "#tabs" ).tabs({active:''||(page-1)||''});
  $( "#tabs ul" ).css("display", "block");
});  
  

  
  
});

$(function() {

  addSpinner();
  
});

$(function() {

  $(".rasdFormMenu").html("'|| RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) ||'");   

  
  $(document).ready(function () {
   $(".dialog").dialog({ autoOpen: false }); 
  });
  
  
});
        ';
        return v_out;
       end; 
     function form_css return clob is
          v_out clob;
       begin
        v_out :=  '
.rasdTxPTRIGGERIDPROC INPUT{
   width: 300px;
}  

.rasdTxPF INPUT{
   width: 300px;
}  


#tabs {
height: 650px;
}
        ';
        return v_out;
       end; 
procedure form_js(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is begin htpClob(form_js); end;
procedure form_css(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is begin htpClob(form_css); end;

 procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr);
 procedure on_session;
 procedure formgen_js;

function openLOV(
  p_lov varchar2,
  p_value varchar2
) return lovtab__ is
  name_array   owa.vc_arr;
  value_array  owa.vc_arr;
begin
  name_array(1) := 'PLOV';
  value_array(1) := p_lov;
  name_array(2) := 'PID';
  value_array(2) := p_value;
  name_array(3) := 'CALL';
  value_array(3) := 'PLSQL';  
  openLOV(name_array, value_array);
  return lov__;
end;
procedure openLOV(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
  num_entries number := name_array.count;
cursor c_lov$PBLOKPROZILECNOV_LOV(p_id varchar2) is 
--<lovsql formid="90" linkid="lov$PBLOKPROZILECNOV_LOV">
select id, label from (select '/.../FORM_CSS' id, 'FORM_CSS' label, 1 from dual union
select '/.../FORM_JS' id, 'FORM_JS' label, 1 from dual union
select '/.../FORM_UIHEAD' id, 'FORM_UIHEAD', 1 label from dual union
select '/.../'||triggerid , triggerid|| decode(t.rform,null,'','-R') label , 2 vr
  from rasd_triggers t
 where t.formid = pformid
   and (triggerid like 'FORM_CSS_REF%' or triggerid like 'FORM_JS_REF%' or
       triggerid like 'FORM_UIHEAD_REF%')
 order by 1, 2 ) where upper(id) like '%'||upper(p_id)||'%' or upper(label) like '%'||upper(p_id)||'%' 
--</lovsql>
;
TYPE pLOVType IS RECORD (
output varchar2(500),
p1 varchar2(200)
);
  TYPE tab_pLOVType IS TABLE OF pLOVType INDEX BY BINARY_INTEGER;
  v_lov tab_pLOVType;
  v_lovf tab_pLOVType;
  v_counter number := 1;
  v_description varchar2(100);
  p_lov varchar2(100);
  p_nameid varchar2(100);
  p_id varchar2(100);
  v_output boolean;
  v_call varchar2(10);
  v_hidden_fields varchar2(32000) := '';
  v_opener_tekst  varchar2(32000) := '';
  RESTRESTYPE varchar2(10);
begin
  on_submit(name_array, value_array);
  for i in 1..num_entries loop
    if name_array(i) = 'PLOV' then p_lov := value_array(i);
    elsif name_array(i) = 'FIN' then p_nameid := value_array(i);
    elsif name_array(i) = 'PID' then p_id := value_array(i);
    elsif upper(name_array(i)) = 'CALL' then v_call := value_array(i);
    elsif upper(name_array(i)) = upper('RESTRESTYPE') then RESTRESTYPE := value_array(i);
    else 
      if name_array(i) not in ('LOVlist') then
        v_hidden_fields := v_hidden_fields||'<input type="hidden" name="'||name_array(i)||'" value="'||value_array(i)||'" />'; 
      end if;
    end if;
  end loop;
    if v_call not in ('PLSQL','REST') then 
      on_session;              
    end if;                   
  if lower(p_lov) = lower('lov$PBLOKPROZILECNOV_LOV') then
    v_description := 'PBLOKPROZILECNOV_LOV';
    for r in c_lov$PBLOKPROZILECNOV_LOV(p_id) loop
        v_lov(v_counter).p1 := r.id;
        v_lov(v_counter).output := r.label;
        v_counter := v_counter + 1;
    end loop;
    v_counter := v_counter - 1;
        if 1=2 then null;
        end if;          
  elsif lower(p_lov) = lower('link$CHKBXD') then
    v_description := 'CHKBXD';
        v_lov(1).output := 'N';
        v_lov(1).p1 := 'N';
        v_lov(2).output := 'Y';
        v_lov(2).p1 := 'Y';
        v_counter := 2; 
        if 1=2 then null;
        end if;          
  else
   return;
  end if;
if v_call = 'PLSQL' then 
  lov__.delete;
  for i in 1..v_lov.count loop
   lov__(i).id := v_lov(i).p1;
   lov__(i).label := v_lov(i).output;   
  end loop;  
elsif v_call = 'REST' then 
if RESTRESTYPE = 'XML' then 
    OWA_UTIL.mime_header('text/xml', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('<?xml version="1.0" encoding="UTF-8"?>
<openLOV LOV="'||p_lov||'" filter="'||p_id||'">');      
 htp.p('<result>');
 for i in 1..v_counter loop
 htp.p('<element><code>'||v_lov(i).p1||'</code><description>'||v_lov(i).output||'</description></element>');
 end loop; 
 htp.p('</result></openLOV>');
else 
    OWA_UTIL.mime_header('application/json', FALSE);
    OWA_UTIL.http_header_close;	
 htp.p('{"openLOV":{"@LOV":"'||p_lov||'","@filter":"'||p_id||'",' );      
 htp.p('"result":[');
 for i in 1..v_counter loop
  if i = 1 then 
 htp.p('{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  else
 htp.p(',{"code":"'||v_lov(i).p1||'","description":"'||v_lov(i).output||'"}');
  end if;
 end loop; 
 htp.p(']}}');
end if;
else
 htp.p('
<html>');
    htp.prn('<head>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||'')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head></head>
    ');
 htp.bodyOpen('','');
htp.p('
<script language="JavaScript">
        $(function() {
        document.getElementById("PID").select();
        });
   function closeLOV() {
     this.close();
   }
   function selectLOV() {
     var value = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].value;
     var tekst = window.document.'||p_lov||'.LOVlist.options[window.document.'||p_lov||'.LOVlist.selectedIndex].text;
     window.opener.'||p_nameid||'.value = value;
     '||v_opener_tekst||'
     event = new Event(''change'');
     window.opener.'||p_nameid||'.dispatchEvent(event);
     ');
htp.p('this.close ();
   }
  with (document) {
  if (screen.availWidth < 900){
    moveTo(-4,-4)}
  }
</script>');
 htp.p('<div class="rasdLovName">'||v_description||'</div>');
 htp.formOpen(curl=>'!RASDC2_CSSJS.openLOV',
                 cattributes=>'name="'||p_lov||'"');
 htp.p('<input type="hidden" name="PLOV" value="'||p_lov||'">');
 htp.p('<input type="hidden" name="FIN" value="'||p_nameid||'">');
 htp.p(v_hidden_fields);
 htp.p('<div class="rasdLov" align="center"><center>');
 htp.p('Filter:<input type="text" id="PID" autofocus="autofocus" name="PID" value="'||p_id||'" ></BR><input type="submit" class="rasdButton" value="Search"><input class="rasdButton" type="button" value="Clear" onclick="document.'||p_lov||'.PID.value=''''; document.'||p_lov||'.submit();"></BR>');
 htp.formselectOpen('LOVlist',cattributes=>'size=15 width="100%"');
 for i in 1..v_counter loop
  if i = 1 then -- fokus na prvem
    htp.formSelectOption(cvalue=>v_lov(i).output,cselected=>1,Cattributes => 'value="'||v_lov(i).p1||'"');
  else
    htp.formSelectOption(cvalue=>v_lov(i).output,Cattributes => 'value="'||v_lov(i).p1||'"');
  end if;
 end loop;
 htp.formselectClose;
 htp.p('');
 htp.line;
 htp.p('<input type="button" class="rasdButton" value="Select and Confirm" onClick="selectLOV();">');
 htp.p('<input type="button" class="rasdButton" value="Close" onClick="closeLOV();">');
 htp.p('</center></div>');
 htp.p('</form>');
 htp.p('</body>');
 htp.p('</html>');
end if;
end;
  function version return varchar2 is
  begin
   return 'v.1.1.20231101005324'; 
  end;
  function this_form return varchar2 is
  begin
   return 'RASDC2_CSSJS';
  end;
procedure version(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
  ) is
  begin
   htp.p( version ); 
  end;
  procedure on_session is
    i__ pls_integer := 1;
  begin
  if ACTION is not null then 
set_session_block__ := set_session_block__ || 'begin ';
set_session_block__ := set_session_block__ || 'rasd_client.sessionStart;';
   if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then null; 
  if ACTION = GBUTTONCLR then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PBLOKPROZILECNOV'', '''' ); ';
  else 
    if length( PBLOKPROZILECNOV(i__)) < 512 then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PBLOKPROZILECNOV'', '''||replace(PBLOKPROZILECNOV(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PBLOKPROZILECNOV'', '''' ); ';
     end if;
 end if; 
  if ACTION = GBUTTONCLR then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PF'', '''' ); ';
  else 
    if length( PPF(i__)) < 512 then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PF'', '''||replace(PPF(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''PF'', '''' ); ';
     end if;
 end if; 
  if ACTION = GBUTTONCLR then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
  else 
    if length( PSESSSTORAGEENABLED(i__)) < 512 then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''||replace(PSESSSTORAGEENABLED(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
     end if;
 end if; 
    end if;
   if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 or nvl(PAGE,0) = 99 then null; 
  if ACTION = GBUTTONCLR then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
  else 
    if length( PGSESSSTORAGEENABLED(i__)) < 512 then 
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''||replace(PGSESSSTORAGEENABLED(i__),'''','''''')||''' ); ';
     else
set_session_block__ := set_session_block__ || 'rasd_client.sessionSetValue(''SESSSTORAGEENABLED'', '''' ); ';
     end if;
 end if; 
    end if;
set_session_block__ := set_session_block__ || ' rasd_client.sessionClose;';
set_session_block__ := set_session_block__ || 'exception when others then null; rasd_client.sessionClose; end;';
  else 
declare vc varchar2(2000); begin
null;
if PBLOKPROZILECNOV(i__) is null then vc := rasd_client.sessionGetValue('PBLOKPROZILECNOV'); PBLOKPROZILECNOV(i__)  := vc;  end if; 
if PPF(i__) is null then vc := rasd_client.sessionGetValue('PF'); PPF(i__)  := vc;  end if; 
if PSESSSTORAGEENABLED(i__) is null then vc := rasd_client.sessionGetValue('SESSSTORAGEENABLED'); PSESSSTORAGEENABLED(i__)  := vc;  end if; 
if PGSESSSTORAGEENABLED(i__) is null then vc := rasd_client.sessionGetValue('SESSSTORAGEENABLED'); PGSESSSTORAGEENABLED(i__)  := vc;  end if; 
exception when others then  null; end;
  end if;
  end;
  procedure on_readrest is
    i__ pls_integer := 1;
  begin
for r__  in (select * from json_table( RESTREQUEST , '$.form.formfields' COLUMNS(
   x__ varchar2(1) PATH '$.X__'
  ,RECNUMB10 number PATH '$.recnumb10'
  ,RECNUMPG number PATH '$.recnumpg'
  ,RECNUMB10G number PATH '$.recnumb10g'
  ,RECNUMP number PATH '$.recnump'
  ,ACTION varchar2(4000) PATH '$.action'
  ,PAGE number PATH '$.page'
  ,LANG varchar2(4000) PATH '$.lang'
  ,PFORMID varchar2(4000) PATH '$.pformid'
  ,GBTNNAVIG varchar2(4000) PATH '$.gbtnnavig'
  ,GBUTTONSAVE varchar2(4000) PATH '$.gbuttonsave'
  ,GBUTTONCOMPILE varchar2(4000) PATH '$.gbuttoncompile'
  ,GBUTTONRES varchar2(4000) PATH '$.gbuttonres'
  ,GBUTTONPREV varchar2(4000) PATH '$.gbuttonprev'
  ,HINTCONTENT varchar2(4000) PATH '$.hintcontent'
)) jt ) loop
 if instr(RESTREQUEST,'recnumb10') > 0 then RECNUMB10 := r__.RECNUMB10; end if;
 if instr(RESTREQUEST,'recnumpg') > 0 then RECNUMPG := r__.RECNUMPG; end if;
 if instr(RESTREQUEST,'recnumb10g') > 0 then RECNUMB10G := r__.RECNUMB10G; end if;
 if instr(RESTREQUEST,'recnump') > 0 then RECNUMP := r__.RECNUMP; end if;
 if instr(RESTREQUEST,'action') > 0 then ACTION := r__.ACTION; end if;
 if instr(RESTREQUEST,'page') > 0 then PAGE := r__.PAGE; end if;
 if instr(RESTREQUEST,'lang') > 0 then LANG := r__.LANG; end if;
 if instr(RESTREQUEST,'pformid') > 0 then PFORMID := r__.PFORMID; end if;
 if instr(RESTREQUEST,'gbtnnavig') > 0 then GBTNNAVIG := r__.GBTNNAVIG; end if;
 if instr(RESTREQUEST,'gbuttonsave') > 0 then GBUTTONSAVE := r__.GBUTTONSAVE; end if;
 if instr(RESTREQUEST,'gbuttoncompile') > 0 then GBUTTONCOMPILE := r__.GBUTTONCOMPILE; end if;
 if instr(RESTREQUEST,'gbuttonres') > 0 then GBUTTONRES := r__.GBUTTONRES; end if;
 if instr(RESTREQUEST,'gbuttonprev') > 0 then GBUTTONPREV := r__.GBUTTONPREV; end if;
 if instr(RESTREQUEST,'hintcontent') > 0 then HINTCONTENT := r__.HINTCONTENT; end if;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.p' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PBLOKPROZILECNOV varchar2(4000) PATH '$.pblokprozilecnov'
  ,PPF varchar2(4000) PATH '$.pf'
  ,PGBTNSRC varchar2(4000) PATH '$.pgbtnsrc'
  ,PGBTNSAVE varchar2(4000) PATH '$.pgbtnsave'
  ,PLDELETE varchar2(4000) PATH '$.pldelete'
  ,PUPLOAD varchar2(4000) PATH '$.pupload'
  ,PHINT varchar2(4000) PATH '$.phint'
  ,PSESSSTORAGEENABLED varchar2(4000) PATH '$.sessstorageenabled'
)) jt ) loop
 if instr(RESTREQUEST,'pblokprozilecnov') > 0 then PBLOKPROZILECNOV(i__) := r__.PBLOKPROZILECNOV; end if;
 if instr(RESTREQUEST,'pf') > 0 then PPF(i__) := r__.PPF; end if;
 if instr(RESTREQUEST,'pgbtnsrc') > 0 then PGBTNSRC(i__) := r__.PGBTNSRC; end if;
 if instr(RESTREQUEST,'pgbtnsave') > 0 then PGBTNSAVE(i__) := r__.PGBTNSAVE; end if;
 if instr(RESTREQUEST,'pldelete') > 0 then PLDELETE(i__) := r__.PLDELETE; end if;
 if instr(RESTREQUEST,'pupload') > 0 then PUPLOAD(i__) := r__.PUPLOAD; end if;
 if instr(RESTREQUEST,'phint') > 0 then PHINT(i__) := r__.PHINT; end if;
 if instr(RESTREQUEST,'sessstorageenabled') > 0 then PSESSSTORAGEENABLED(i__) := r__.PSESSSTORAGEENABLED; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.pg' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,PGBLOKPROZILECNOV varchar2(4000) PATH '$.pgblokprozilecnov'
  ,PGGBTNSRC varchar2(4000) PATH '$.pggbtnsrc'
  ,PGGBTNSAVE varchar2(4000) PATH '$.pggbtnsave'
  ,PGUPLOAD varchar2(4000) PATH '$.pgupload'
  ,PGHINT varchar2(4000) PATH '$.pghint'
  ,PGSESSSTORAGEENABLED varchar2(4000) PATH '$.sessstorageenabled'
)) jt ) loop
 if instr(RESTREQUEST,'pgblokprozilecnov') > 0 then PGBLOKPROZILECNOV(i__) := r__.PGBLOKPROZILECNOV; end if;
 if instr(RESTREQUEST,'pggbtnsrc') > 0 then PGGBTNSRC(i__) := r__.PGGBTNSRC; end if;
 if instr(RESTREQUEST,'pggbtnsave') > 0 then PGGBTNSAVE(i__) := r__.PGGBTNSAVE; end if;
 if instr(RESTREQUEST,'pgupload') > 0 then PGUPLOAD(i__) := r__.PGUPLOAD; end if;
 if instr(RESTREQUEST,'pghint') > 0 then PGHINT(i__) := r__.PGHINT; end if;
 if instr(RESTREQUEST,'sessstorageenabled') > 0 then PGSESSSTORAGEENABLED(i__) := r__.PGSESSSTORAGEENABLED; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10RS varchar2(4000) PATH '$.b10rs'
  ,B10RID varchar2(4000) PATH '$.b10rid'
  ,B10PLSQL clob PATH '$.b10plsql'
)) jt ) loop
 if instr(RESTREQUEST,'b10rs') > 0 then B10RS(i__) := r__.B10RS; end if;
 if instr(RESTREQUEST,'b10rid') > 0 then B10RID(i__) := r__.B10RID; end if;
 if instr(RESTREQUEST,'b10plsql') > 0 then B10PLSQL(i__) := r__.B10PLSQL; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b10g' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B10GRS varchar2(4000) PATH '$.b10grs'
  ,B10GRID varchar2(4000) PATH '$.b10grid'
  ,B10GBLOBVC clob PATH '$.b10gblobvc'
)) jt ) loop
 if instr(RESTREQUEST,'b10grs') > 0 then B10GRS(i__) := r__.B10GRS; end if;
 if instr(RESTREQUEST,'b10grid') > 0 then B10GRID(i__) := r__.B10GRID; end if;
 if instr(RESTREQUEST,'b10gblobvc') > 0 then B10GBLOBVC(i__) := r__.B10GBLOBVC; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b20' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
  ,B20TEMPLATE varchar2(4000) PATH '$.b20template'
  ,B20EDITOR varchar2(4000) PATH '$.b20editor'
  ,B20REGEX varchar2(4000) PATH '$.b20regex'
)) jt ) loop
 if instr(RESTREQUEST,'b20template') > 0 then B20TEMPLATE(i__) := r__.B20TEMPLATE; end if;
 if instr(RESTREQUEST,'b20editor') > 0 then B20EDITOR(i__) := r__.B20EDITOR; end if;
 if instr(RESTREQUEST,'b20regex') > 0 then B20REGEX(i__) := r__.B20REGEX; end if;
i__ := i__ + 1;
end loop;
i__ := 1;for r__  in (select * from json_table( RESTREQUEST , '$.form.b30[*]' COLUMNS(
   x__ varchar2(1) PATH '$.rs'
)) jt ) loop
if r__.x__ is not null then
i__ := i__ + 1;
end if;
end loop;
  end;
  function validate_submit(v_text varchar2) return varchar2 is
    v_outt varchar2(32000) := v_text;
  begin
    if instr(v_outt,'"') > 0 then v_outt := replace(v_outt,'"','&quot;');
    elsif instr(v_outt,'%22') > 0 then v_outt := replace(v_outt,'%22','&quot;');
    elsif instr(lower(v_outt),'<script') > 0 then v_outt := replace(v_outt,'<script','&lt;script');
    end if;
    return v_outt;
  end;
  procedure on_submit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
    num_entries number := name_array.count;
    v_max  pls_integer := 0;
  begin
-- submit fields
    for i__ in 1..nvl(num_entries,0) loop
      if 1 = 2 then null;
      elsif  upper(name_array(i__)) = 'RESTRESTYPE' then RESTRESTYPE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = 'RESTREQUEST' then RESTREQUEST := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10') then RECNUMB10 := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMPG') then RECNUMPG := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMB10G') then RECNUMB10G := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('RECNUMP') then RECNUMP := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ACTION') then ACTION := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONBCK') then GBUTTONBCK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCLR') then GBUTTONCLR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONFWD') then GBUTTONFWD := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PAGE') then PAGE := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('LANG') then LANG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORMID') then PFORMID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PFORM') then PFORM := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBTNNAVIG') then GBTNNAVIG := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSRC') then GBUTTONSRC := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONSAVE') then GBUTTONSAVE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONCOMPILE') then GBUTTONCOMPILE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONRES') then GBUTTONRES := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONPREV') then GBUTTONPREV := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('ERROR') then ERROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('MESSAGE') then MESSAGE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('WARNING') then WARNING := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VUSER') then VUSER := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('VLOB') then VLOB := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('COMPID') then COMPID := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('DOCPRIVSOK') then DOCPRIVSOK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('GBUTTONDELETE') then GBUTTONDELETE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('G_TABLE') then G_TABLE := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('HINTCONTENT') then HINTCONTENT := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGOPTIONS') then PGOPTIONS := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PMIRROR') then PMIRROR := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('UNLINK') then UNLINK := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PBLOKPROZILECNOV_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PBLOKPROZILECNOV(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBLOKPROZILECNOV_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGBLOKPROZILECNOV(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GRS_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GRS(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GRID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GRID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PPF(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGPF_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGPF(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGBTNSRC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGGBTNSRC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGGBTNSRC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGGBTNSAVE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGGBTNSAVE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSAVE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGBTNSAVE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PLDELETE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PLDELETE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PUPLOAD_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PUPLOAD(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGUPLOAD_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGUPLOAD(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGHINT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGHINT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PHINT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PHINT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PSESSSTORAGEENABLED(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGSESSSTORAGEENABLED(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PTEXTTYPE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PTEXTTYPE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGTEXTTYPE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        PGTEXTTYPE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20TEMPLATE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20TEMPLATE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GBLOBVC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GBLOBVC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10GNAME_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10GNAME(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20EDITOR_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20EDITOR(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20REGEX_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B20REGEX(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30TEXT_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B30TEXT(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORMID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10FORMID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOCKID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10BLOCKID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10TRIGGERID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10TRIGGERID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PLSQL_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PLSQL(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PLSQLSPEC_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10PLSQLSPEC(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10SOURCE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10SOURCE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10HIDDENYN_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10HIDDENYN(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RLOBID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RLOBID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RFORM_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RFORM(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RBLOCKID_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10RBLOCKID(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10LDELETE_'||substr(name_array(i__),instr(name_array(i__),'_',-1)+1)) then
        B10LDELETE(to_number(substr(name_array(i__),instr(name_array(i__),'_',-1)+1))) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PBLOKPROZILECNOV') and PBLOKPROZILECNOV.count = 0 and value_array(i__) is not null then
        PBLOKPROZILECNOV(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBLOKPROZILECNOV') and PGBLOKPROZILECNOV.count = 0 and value_array(i__) is not null then
        PGBLOKPROZILECNOV(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RS') and B10RS.count = 0 and value_array(i__) is not null then
        B10RS(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GRS') and B10GRS.count = 0 and value_array(i__) is not null then
        B10GRS(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GRID') and B10GRID.count = 0 and value_array(i__) is not null then
        B10GRID(1) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RID') and B10RID.count = 0 and value_array(i__) is not null then
        B10RID(1) := chartorowid(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PF') and PPF.count = 0 and value_array(i__) is not null then
        PPF(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGPF') and PGPF.count = 0 and value_array(i__) is not null then
        PGPF(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSRC') and PGBTNSRC.count = 0 and value_array(i__) is not null then
        PGBTNSRC(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGGBTNSRC') and PGGBTNSRC.count = 0 and value_array(i__) is not null then
        PGGBTNSRC(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGGBTNSAVE') and PGGBTNSAVE.count = 0 and value_array(i__) is not null then
        PGGBTNSAVE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGBTNSAVE') and PGBTNSAVE.count = 0 and value_array(i__) is not null then
        PGBTNSAVE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PLDELETE') and PLDELETE.count = 0 and value_array(i__) is not null then
        PLDELETE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PUPLOAD') and PUPLOAD.count = 0 and value_array(i__) is not null then
        PUPLOAD(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGUPLOAD') and PGUPLOAD.count = 0 and value_array(i__) is not null then
        PGUPLOAD(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGHINT') and PGHINT.count = 0 and value_array(i__) is not null then
        PGHINT(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PHINT') and PHINT.count = 0 and value_array(i__) is not null then
        PHINT(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED') and PSESSSTORAGEENABLED.count = 0 and value_array(i__) is not null then
        PSESSSTORAGEENABLED(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('SESSSTORAGEENABLED') and PGSESSSTORAGEENABLED.count = 0 and value_array(i__) is not null then
        PGSESSSTORAGEENABLED(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PTEXTTYPE') and PTEXTTYPE.count = 0 and value_array(i__) is not null then
        PTEXTTYPE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('PGTEXTTYPE') and PGTEXTTYPE.count = 0 and value_array(i__) is not null then
        PGTEXTTYPE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20TEMPLATE') and B20TEMPLATE.count = 0 and value_array(i__) is not null then
        B20TEMPLATE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10GBLOBVC') and B10GBLOBVC.count = 0 and value_array(i__) is not null then
        B10GBLOBVC(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10GNAME') and B10GNAME.count = 0 and value_array(i__) is not null then
        B10GNAME(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20EDITOR') and B20EDITOR.count = 0 and value_array(i__) is not null then
        B20EDITOR(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B20REGEX') and B20REGEX.count = 0 and value_array(i__) is not null then
        B20REGEX(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B30TEXT') and B30TEXT.count = 0 and value_array(i__) is not null then
        B30TEXT(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10FORMID') and B10FORMID.count = 0 and value_array(i__) is not null then
        B10FORMID(1) := rasd_client.varchr2number(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10BLOCKID') and B10BLOCKID.count = 0 and value_array(i__) is not null then
        B10BLOCKID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10TRIGGERID') and B10TRIGGERID.count = 0 and value_array(i__) is not null then
        B10TRIGGERID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10PLSQL') and B10PLSQL.count = 0 and value_array(i__) is not null then
        B10PLSQL(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10PLSQLSPEC') and B10PLSQLSPEC.count = 0 and value_array(i__) is not null then
        B10PLSQLSPEC(1) := value_array(i__);
      elsif  upper(name_array(i__)) = upper('B10SOURCE') and B10SOURCE.count = 0 and value_array(i__) is not null then
        B10SOURCE(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10HIDDENYN') and B10HIDDENYN.count = 0 and value_array(i__) is not null then
        B10HIDDENYN(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RLOBID') and B10RLOBID.count = 0 and value_array(i__) is not null then
        B10RLOBID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RFORM') and B10RFORM.count = 0 and value_array(i__) is not null then
        B10RFORM(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10RBLOCKID') and B10RBLOCKID.count = 0 and value_array(i__) is not null then
        B10RBLOCKID(1) := validate_submit(value_array(i__));
      elsif  upper(name_array(i__)) = upper('B10LDELETE') and B10LDELETE.count = 0 and value_array(i__) is not null then
        B10LDELETE(1) := validate_submit(value_array(i__));
      end if;
    end loop;
-- organize records
declare
v_last number := 
B10RID
.last;
v_curr number := 
B10RID
.first;
i__ number;
begin
 if v_last <> 
B10RID
.count then 
   v_curr := 
B10RID
.FIRST;  
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10RS.exists(v_curr) then B10RS(i__) := B10RS(v_curr); end if;
      if B10RID.exists(v_curr) then B10RID(i__) := B10RID(v_curr); end if;
      if B10FORMID.exists(v_curr) then B10FORMID(i__) := B10FORMID(v_curr); end if;
      if B10BLOCKID.exists(v_curr) then B10BLOCKID(i__) := B10BLOCKID(v_curr); end if;
      if B10TRIGGERID.exists(v_curr) then B10TRIGGERID(i__) := B10TRIGGERID(v_curr); end if;
      if B10PLSQL.exists(v_curr) then B10PLSQL(i__) := B10PLSQL(v_curr); end if;
      if B10PLSQLSPEC.exists(v_curr) then B10PLSQLSPEC(i__) := B10PLSQLSPEC(v_curr); end if;
      if B10SOURCE.exists(v_curr) then B10SOURCE(i__) := B10SOURCE(v_curr); end if;
      if B10HIDDENYN.exists(v_curr) then B10HIDDENYN(i__) := B10HIDDENYN(v_curr); end if;
      if B10RLOBID.exists(v_curr) then B10RLOBID(i__) := B10RLOBID(v_curr); end if;
      if B10RFORM.exists(v_curr) then B10RFORM(i__) := B10RFORM(v_curr); end if;
      if B10RBLOCKID.exists(v_curr) then B10RBLOCKID(i__) := B10RBLOCKID(v_curr); end if;
      if B10LDELETE.exists(v_curr) then B10LDELETE(i__) := B10LDELETE(v_curr); end if;
      i__ := i__ + 1;
      v_curr := 
B10RID
.NEXT(v_curr);  
   END LOOP;
      B10RS.DELETE(i__ , v_last);
      B10RID.DELETE(i__ , v_last);
      B10FORMID.DELETE(i__ , v_last);
      B10BLOCKID.DELETE(i__ , v_last);
      B10TRIGGERID.DELETE(i__ , v_last);
      B10PLSQL.DELETE(i__ , v_last);
      B10PLSQLSPEC.DELETE(i__ , v_last);
      B10SOURCE.DELETE(i__ , v_last);
      B10HIDDENYN.DELETE(i__ , v_last);
      B10RLOBID.DELETE(i__ , v_last);
      B10RFORM.DELETE(i__ , v_last);
      B10RBLOCKID.DELETE(i__ , v_last);
      B10LDELETE.DELETE(i__ , v_last);
end if;
end;
declare
v_last number := 
B10GRID
.last;
v_curr number := 
B10GRID
.first;
i__ number;
begin
 if v_last <> 
B10GRID
.count then 
   v_curr := 
B10GRID
.FIRST;  
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B10GRS.exists(v_curr) then B10GRS(i__) := B10GRS(v_curr); end if;
      if B10GRID.exists(v_curr) then B10GRID(i__) := B10GRID(v_curr); end if;
      if B10GBLOBVC.exists(v_curr) then B10GBLOBVC(i__) := B10GBLOBVC(v_curr); end if;
      if B10GNAME.exists(v_curr) then B10GNAME(i__) := B10GNAME(v_curr); end if;
      i__ := i__ + 1;
      v_curr := 
B10GRID
.NEXT(v_curr);  
   END LOOP;
      B10GRS.DELETE(i__ , v_last);
      B10GRID.DELETE(i__ , v_last);
      B10GBLOBVC.DELETE(i__ , v_last);
      B10GNAME.DELETE(i__ , v_last);
end if;
end;
declare
v_last number := 
B30TEXT
.last;
v_curr number := 
B30TEXT
.first;
i__ number;
begin
 if v_last <> 
B30TEXT
.count then 
   v_curr := 
B30TEXT
.FIRST;  
   i__ := 1;
   WHILE v_curr IS NOT NULL LOOP
      if B30TEXT.exists(v_curr) then B30TEXT(i__) := B30TEXT(v_curr); end if;
      i__ := i__ + 1;
      v_curr := 
B30TEXT
.NEXT(v_curr);  
   END LOOP;
      B30TEXT.DELETE(i__ , v_last);
end if;
end;
-- init fields
    v_max := 0;
    if B10RS.count > v_max then v_max := B10RS.count; end if;
    if B10RID.count > v_max then v_max := B10RID.count; end if;
    if B10FORMID.count > v_max then v_max := B10FORMID.count; end if;
    if B10BLOCKID.count > v_max then v_max := B10BLOCKID.count; end if;
    if B10TRIGGERID.count > v_max then v_max := B10TRIGGERID.count; end if;
    if B10PLSQL.count > v_max then v_max := B10PLSQL.count; end if;
    if B10PLSQLSPEC.count > v_max then v_max := B10PLSQLSPEC.count; end if;
    if B10SOURCE.count > v_max then v_max := B10SOURCE.count; end if;
    if B10HIDDENYN.count > v_max then v_max := B10HIDDENYN.count; end if;
    if B10RLOBID.count > v_max then v_max := B10RLOBID.count; end if;
    if B10RFORM.count > v_max then v_max := B10RFORM.count; end if;
    if B10RBLOCKID.count > v_max then v_max := B10RBLOCKID.count; end if;
    if B10LDELETE.count > v_max then v_max := B10LDELETE.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10RS.exists(i__) then
        B10RS(i__) := null;
      end if;
      if not B10RID.exists(i__) then
        B10RID(i__) := null;
      end if;
      if not B10FORMID.exists(i__) then
        B10FORMID(i__) := to_number(null);
      end if;
      if not B10BLOCKID.exists(i__) then
        B10BLOCKID(i__) := null;
      end if;
      if not B10TRIGGERID.exists(i__) then
        B10TRIGGERID(i__) := null;
      end if;
      if not B10PLSQL.exists(i__) then
        B10PLSQL(i__) := null;
      end if;
      if not B10PLSQL#SET.exists(i__) then
        B10PLSQL#SET(i__).visible := true;
      end if;
      if not B10PLSQLSPEC.exists(i__) then
        B10PLSQLSPEC(i__) := null;
      end if;
      if not B10PLSQLSPEC#SET.exists(i__) then
        B10PLSQLSPEC#SET(i__).visible := true;
      end if;
      if not B10SOURCE.exists(i__) then
        B10SOURCE(i__) := null;
      end if;
      if not B10HIDDENYN.exists(i__) then
        B10HIDDENYN(i__) := null;
      end if;
      if not B10RLOBID.exists(i__) then
        B10RLOBID(i__) := null;
      end if;
      if not B10RFORM.exists(i__) then
        B10RFORM(i__) := null;
      end if;
      if not B10RBLOCKID.exists(i__) then
        B10RBLOCKID(i__) := null;
      end if;
      if not B10LDELETE.exists(i__) then
        B10LDELETE(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B10GRS.count > v_max then v_max := B10GRS.count; end if;
    if B10GRID.count > v_max then v_max := B10GRID.count; end if;
    if B10GBLOBVC.count > v_max then v_max := B10GBLOBVC.count; end if;
    if B10GNAME.count > v_max then v_max := B10GNAME.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B10GRS.exists(i__) then
        B10GRS(i__) := null;
      end if;
      if not B10GRID.exists(i__) then
        B10GRID(i__) := null;
      end if;
      if not B10GBLOBVC.exists(i__) then
        B10GBLOBVC(i__) := null;
      end if;
      if not B10GBLOBVC#SET.exists(i__) then
        B10GBLOBVC#SET(i__).visible := true;
      end if;
      if not B10GNAME.exists(i__) then
        B10GNAME(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if B20TEMPLATE.count > v_max then v_max := B20TEMPLATE.count; end if;
    if B20EDITOR.count > v_max then v_max := B20EDITOR.count; end if;
    if B20REGEX.count > v_max then v_max := B20REGEX.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not B20TEMPLATE.exists(i__) then
        B20TEMPLATE(i__) := 'RASD CSS,HTML,JS template';
      end if;
      if not B20EDITOR.exists(i__) then
        B20EDITOR(i__) := 'External HTML editor';
      end if;
      if not B20REGEX.exists(i__) then
        B20REGEX(i__) := 'External RegEX editor';
      end if;
    null; end loop;
    v_max := 0;
    if B30TEXT.count > v_max then v_max := B30TEXT.count; end if;
    if v_max = 0 then v_max := 0; end if;
    for i__ in 1..v_max loop
      if not B30TEXT.exists(i__) then
        B30TEXT(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PBLOKPROZILECNOV.count > v_max then v_max := PBLOKPROZILECNOV.count; end if;
    if PPF.count > v_max then v_max := PPF.count; end if;
    if PGBTNSRC.count > v_max then v_max := PGBTNSRC.count; end if;
    if PGBTNSAVE.count > v_max then v_max := PGBTNSAVE.count; end if;
    if PLDELETE.count > v_max then v_max := PLDELETE.count; end if;
    if PUPLOAD.count > v_max then v_max := PUPLOAD.count; end if;
    if PHINT.count > v_max then v_max := PHINT.count; end if;
    if PSESSSTORAGEENABLED.count > v_max then v_max := PSESSSTORAGEENABLED.count; end if;
    if PTEXTTYPE.count > v_max then v_max := PTEXTTYPE.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PBLOKPROZILECNOV.exists(i__) then
        PBLOKPROZILECNOV(i__) := null;
      end if;
      if not PBLOKPROZILECNOV#SET.exists(i__) then
        PBLOKPROZILECNOV#SET(i__).visible := true;
      end if;
      if not PPF.exists(i__) then
        PPF(i__) := null;
      end if;
      if not PPF#SET.exists(i__) then
        PPF#SET(i__).visible := true;
      end if;
      if not PGBTNSRC.exists(i__) then
        PGBTNSRC(i__) := gbuttonsrc;
      end if;
      if not PGBTNSRC#SET.exists(i__) then
        PGBTNSRC#SET(i__).visible := true;
      end if;
      if not PGBTNSAVE.exists(i__) then
        PGBTNSAVE(i__) := gbuttonsave;
      end if;
      if not PGBTNSAVE#SET.exists(i__) then
        PGBTNSAVE#SET(i__).visible := true;
      end if;
      if not PLDELETE.exists(i__) then
        PLDELETE(i__) := null;
      end if;
      if not PLDELETE#SET.exists(i__) then
        PLDELETE#SET(i__).visible := true;
      end if;
      if not PUPLOAD.exists(i__) then
        PUPLOAD(i__) := null;
      end if;
      if not PUPLOAD#SET.exists(i__) then
        PUPLOAD#SET(i__).visible := true;
      end if;
      if not PHINT.exists(i__) then
        PHINT(i__) := null;
      end if;
      if not PSESSSTORAGEENABLED.exists(i__) then
        PSESSSTORAGEENABLED(i__) := null;
      end if;
      if not PTEXTTYPE.exists(i__) then
        PTEXTTYPE(i__) := null;
      end if;
    null; end loop;
    v_max := 0;
    if PGBLOKPROZILECNOV.count > v_max then v_max := PGBLOKPROZILECNOV.count; end if;
    if PGPF.count > v_max then v_max := PGPF.count; end if;
    if PGGBTNSRC.count > v_max then v_max := PGGBTNSRC.count; end if;
    if PGGBTNSAVE.count > v_max then v_max := PGGBTNSAVE.count; end if;
    if PGUPLOAD.count > v_max then v_max := PGUPLOAD.count; end if;
    if PGHINT.count > v_max then v_max := PGHINT.count; end if;
    if PGSESSSTORAGEENABLED.count > v_max then v_max := PGSESSSTORAGEENABLED.count; end if;
    if PGTEXTTYPE.count > v_max then v_max := PGTEXTTYPE.count; end if;
    if v_max = 0 then v_max := 1; end if;
    for i__ in 1..v_max loop
      if not PGBLOKPROZILECNOV.exists(i__) then
        PGBLOKPROZILECNOV(i__) := null;
      end if;
      if not PGBLOKPROZILECNOV#SET.exists(i__) then
        PGBLOKPROZILECNOV#SET(i__).visible := true;
      end if;
      if not PGPF.exists(i__) then
        PGPF(i__) := null;
      end if;
      if not PGPF#SET.exists(i__) then
        PGPF#SET(i__).visible := true;
      end if;
      if not PGGBTNSRC.exists(i__) then
        PGGBTNSRC(i__) := gbuttonsrc;
      end if;
      if not PGGBTNSRC#SET.exists(i__) then
        PGGBTNSRC#SET(i__).visible := true;
      end if;
      if not PGGBTNSAVE.exists(i__) then
        PGGBTNSAVE(i__) := gbuttonsave;
      end if;
      if not PGGBTNSAVE#SET.exists(i__) then
        PGGBTNSAVE#SET(i__).visible := true;
      end if;
      if not PGUPLOAD.exists(i__) then
        PGUPLOAD(i__) := null;
      end if;
      if not PGUPLOAD#SET.exists(i__) then
        PGUPLOAD#SET(i__).visible := true;
      end if;
      if not PGHINT.exists(i__) then
        PGHINT(i__) := null;
      end if;
      if not PGSESSSTORAGEENABLED.exists(i__) then
        PGSESSSTORAGEENABLED(i__) := null;
      end if;
      if not PGTEXTTYPE.exists(i__) then
        PGTEXTTYPE(i__) := null;
      end if;
    null; end loop;
  end;
  procedure post_submit is
  begin
--<POST_SUBMIT formid="90" blockid="">
----put procedure in the begining of trigger;
post_submit_template;

      if action is null then
        action := GBUTTONSRC;
      end if;

      if nvl(page,0) = 0 then page := 1; end if;

if page = 1 then


PBLOKPROZILECNOV#SET(1).visible := true;
if PBLOKPROZILECNOV(1) is null then PBLOKPROZILECNOV(1) := '/.../FORM_CSS'; end if;

if B10PLSQL(1) is null then
B10PLSQL(1) := trim(rasd_engine11.createtriggertemplateplsql(pformid, substr(PBLOKPROZILECNOV(1), instr(PBLOKPROZILECNOV(1), '/.../') + 5) , ''));
end if;

elsif page = 2 then

  g_table := getDocumentTable;
  
end if;

if rasdc_library.allowEditing(pformid) then
   GBUTTONSAVE#SET.visible := true;
   GBUTTONCOMPILE#SET.visible := true;
   PGBTNSAVE#SET(1).visible := true;
   PGGBTNSAVE#SET(1).visible := true;
else
   GBUTTONSAVE#SET.visible := false;
   GBUTTONCOMPILE#SET.visible := false;
   PGBTNSAVE#SET(1).visible := false;   
   PGGBTNSAVE#SET(1).visible := false;
end if;

begin
  DOCPRIVSOK := getDocumentTable;
exception when others then
  DOCPRIVSOK := 'N';
end;  
--</POST_SUBMIT>
    null;
  end;
  procedure psubmit(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_session;
    post_submit;
  end;
  procedure psubmitrest(name_array  in owa.vc_arr, value_array in owa.vc_arr) is
  begin
-- Reading post variables into fields.
    on_submit(name_array ,value_array); on_readrest;
    post_submit;
  end;
  procedure pclear_P(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PBLOKPROZILECNOV(i__) := null;
        PPF(i__) := null;
        PGBTNSRC(i__) := gbuttonsrc;
        PGBTNSAVE(i__) := gbuttonsave;
        PLDELETE(i__) := null;
        PUPLOAD(i__) := null;
        PHINT(i__) := null;
        PSESSSTORAGEENABLED(i__) := null;
        PTEXTTYPE(i__) := null;
        PBLOKPROZILECNOV#SET(i__).visible := true;
        PPF#SET(i__).visible := true;
        PGBTNSRC#SET(i__).visible := true;
        PGBTNSAVE#SET(i__).visible := true;
        PLDELETE#SET(i__).visible := true;
        PUPLOAD#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_PG(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        PGBLOKPROZILECNOV(i__) := null;
        PGPF(i__) := null;
        PGGBTNSRC(i__) := gbuttonsrc;
        PGGBTNSAVE(i__) := gbuttonsave;
        PGUPLOAD(i__) := null;
        PGHINT(i__) := null;
        PGSESSSTORAGEENABLED(i__) := null;
        PGTEXTTYPE(i__) := null;
        PGBLOKPROZILECNOV#SET(i__).visible := true;
        PGPF#SET(i__).visible := true;
        PGGBTNSRC#SET(i__).visible := true;
        PGGBTNSAVE#SET(i__).visible := true;
        PGUPLOAD#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B10(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
 if pstart = 0 then k__ := k__ + 
B10RID
.count(); end if;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10RS(i__) := null;
        B10RID(i__) := null;
        B10FORMID(i__) := null;
        B10BLOCKID(i__) := null;
        B10TRIGGERID(i__) := null;
        B10PLSQL(i__) := null;
        B10PLSQLSPEC(i__) := null;
        B10SOURCE(i__) := null;
        B10HIDDENYN(i__) := null;
        B10RLOBID(i__) := null;
        B10RFORM(i__) := null;
        B10RBLOCKID(i__) := null;
        B10LDELETE(i__) := null;
        B10PLSQL#SET(i__).visible := true;
        B10PLSQLSPEC#SET(i__).visible := true;
        B10RS(i__) := 'I';

      end loop;
  end;
  procedure pclear_B10G(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
 if pstart = 0 then k__ := k__ + 
B10GRID
.count(); end if;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B10GRS(i__) := null;
        B10GRID(i__) := null;
        B10GBLOBVC(i__) := null;
        B10GNAME(i__) := null;
        B10GBLOBVC#SET(i__).visible := true;

      end loop;
  end;
  procedure pclear_B20(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 1;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 1 then  k__ := i__ + 0;
       else k__ := 0 + 1;
       end if;
      end if;
      j__ := i__;
if pstart = 0 and 0 + 1=0 then j__ := 0; k__:= 1; end if;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B20TEMPLATE(i__) := 'RASD CSS,HTML,JS template';
        B20EDITOR(i__) := 'External HTML editor';
        B20REGEX(i__) := 'External RegEX editor';

      end loop;
  end;
  procedure pclear_B30(pstart number) is
    i__ pls_integer;
    j__ pls_integer;
    k__ pls_integer;
    v_numrows pls_integer := 0;
  begin
      i__ := pstart;
      if v_numrows = 0 then k__ := i__ + 0;
      else  
       if i__ > 0 then  k__ := i__ + 0;
       else k__ := 0 + 0;
       end if;
      end if;
      j__ := i__;
if pstart = 0 then B30TEXT.delete; end if;
      for i__ in 1..j__ loop
      null;
      end loop;
      for i__ in j__+1..k__ loop
-- Generated initialization of the fields in new record. Use (i__) to access fields values.
        B30TEXT(i__) := null;

      end loop;
  end;
  procedure pclear_form is
  begin
    RECNUMB10 := 1;
    RECNUMPG := 1;
    RECNUMB10G := 1;
    RECNUMP := 1;
    GBUTTONBCK := 'GBUTTONBCK';
    GBUTTONCLR := 'GBUTTONCLR';
    GBUTTONFWD := 'GBUTTONFWD';
    PAGE := 0;
    LANG := null;
    PFORMID := null;
    PFORM := null;
    GBTNNAVIG := RASDI_TRNSLT.text('Form navigator',LANG);
    GBUTTONSRC := RASDI_TRNSLT.text('Search',LANG);
    GBUTTONSAVE := RASDI_TRNSLT.text('Save',LANG);
    GBUTTONCOMPILE := RASDI_TRNSLT.text('Compile',LANG);
    GBUTTONRES := RASDI_TRNSLT.text('Reset',LANG);
    GBUTTONPREV := RASDI_TRNSLT.text('Preview',LANG);
    ERROR := null;
    MESSAGE := null;
    WARNING := null;
    VUSER := null;
    VLOB := null;
    COMPID := null;
    DOCPRIVSOK := null;
    GBUTTONDELETE := RASDI_TRNSLT.text('Delete',LANG);
    G_TABLE := null;
    HINTCONTENT := null;
    PGOPTIONS := null;
    PMIRROR := null;
    UNLINK := null;
  null; end;
  procedure pclear is
  begin
-- Clears all fields on form and blocks.
    pclear_form;
    pclear_P(0);
    pclear_PG(0);
    pclear_B10(0);
    pclear_B10G(0);
    pclear_B20(0);
    pclear_B30(0);

  null;
  end;
  procedure pselect_P is
    i__ pls_integer;
  begin
      pclear_P(PBLOKPROZILECNOV.count);
  null; end;
  procedure pselect_PG is
    i__ pls_integer;
  begin
      pclear_PG(PGBLOKPROZILECNOV.count);
  null; end;
  procedure pselect_B10 is
    i__ pls_integer;
  begin
      B10RID.delete;
      B10FORMID.delete;
      B10BLOCKID.delete;
      B10TRIGGERID.delete;
      B10PLSQL.delete;
      B10PLSQLSPEC.delete;
      B10RFORM.delete;
      B10LDELETE.delete;

      declare 
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR 
            select rowid     rid,
                 formid    formid,
                 blockid   blockid,
                 triggerid,
                 plsql,
                 plsqlspec, rform,
				 'rasdc_files.showfile?pfile=pict/gumbrisi.jpg' ldelete
            from RASD_TRIGGERS
           where formid = PFORMID
             and triggerid = substr(PBLOKPROZILECNOV(1),
                                          instr(PBLOKPROZILECNOV(1), '/.../') + 5)
             and upper(triggerid||':'||plsql||':'||plsqlspec) like upper('%'||PPF(1)||'%') 
           order by triggerid ;
        i__ := 1;
        LOOP 
          FETCH c__ INTO
            B10RID(i__)
           ,B10FORMID(i__)
           ,B10BLOCKID(i__)
           ,B10TRIGGERID(i__)
           ,B10PLSQL(i__)
           ,B10PLSQLSPEC(i__)
           ,B10RFORM(i__)
           ,B10LDELETE(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  nvl(RECNUMB10,1) then
            B10RS(i__) := 'U';
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.

--<post_select formid="90" blockid="B10">
if B10RFORM(1) is not null then

B10PLSQL#SET(1).custom := 'class="referenceBlock"';
B10PLSQLSPEC#SET(1).custom := 'class="referenceBlock"';

end if;



--</post_select>
            exit when i__ =1;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  nvl(RECNUMB10,1) then
          B10RS.delete(1);
          B10RID.delete(1);
          B10FORMID.delete(1);
          B10BLOCKID.delete(1);
          B10TRIGGERID.delete(1);
          B10PLSQL.delete(1);
          B10PLSQLSPEC.delete(1);
          B10SOURCE.delete(1);
          B10HIDDENYN.delete(1);
          B10RLOBID.delete(1);
          B10RFORM.delete(1);
          B10RBLOCKID.delete(1);
          B10LDELETE.delete(1);
          i__ := 0;
        end if; 
        CLOSE c__;
      end;
      pclear_B10(B10RID.count);
  null; end;
  procedure pselect_B10G is
    i__ pls_integer;
  begin
      B10GRID.delete;
      B10GBLOBVC.delete;
      B10GNAME.delete;
--<pre_select formid="90" blockid="B10G">
      pgoptions := '';
      declare
        TYPE ctype__ is REF CURSOR;
         c__ ctype__;
         v_id varchar2(300);
         v_label  varchar2(300);
      begin
      OPEN c__ FOR 
                  'select name id, name label
                    from '||g_table||'
                   where upper(name) like ''%RASD%.HTML''
                     or upper(name) like ''%RASD%.JS''
                     or upper(name) like ''%RASD%.CSS''  
                   order by decode(instr(name,''.html''),0,decode(instr(name,''.css''),0,decode(instr(name,''.js''),0, 5 ,1),2),3), name                   
                   ';
      loop
        FETCH c__
            INTO v_id, v_label;
          exit when c__%notfound;
        pgoptions := pgoptions||'<option class=selectp ';
		if PGBLOKPROZILECNOV(1) = v_id then 
		    pgoptions := pgoptions||' selected '; 
	    elsif PGBLOKPROZILECNOV(1) is null then
		    pgoptions := pgoptions||' selected '; 	
			PGBLOKPROZILECNOV(1) := v_id;
	    end if;
       pgoptions := pgoptions||' value="' || v_id || '">' || v_label || '</option>';
      end loop;
      end;
--</pre_select>
      declare 
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
--<on_select formid="90" blockid="B10G">
OPEN c__ FOR
          'select rowid rid,
                  '||g_table||'_API.Blob2Clob(blob_content) BLOBVC
                 --UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(blob_content,
                   --                                       32767,
                     --                                     1)) BLOBVC
				  ,name
           from '||g_table||'
           where upper(name) = upper('''||PGBLOKPROZILECNOV(1)||''')
           order by name';
--</on_select>
        i__ := 1;
        LOOP 
          FETCH c__ INTO
            B10GRID(i__)
           ,B10GBLOBVC(i__)
           ,B10GNAME(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  nvl(RECNUMB10G,1) then
            B10GRS(i__) := 'U';
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =1;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  nvl(RECNUMB10G,1) then
          B10GRS.delete(1);
          B10GRID.delete(1);
          B10GBLOBVC.delete(1);
          B10GNAME.delete(1);
          i__ := 0;
        end if; 
        CLOSE c__;
      end;
      pclear_B10G(B10GRID.count);
  null; end;
  procedure pselect_B20 is
    i__ pls_integer;
  begin
      pclear_B20(B20TEMPLATE.count);
  null; end;
  procedure pselect_B30 is
    i__ pls_integer;
  begin
      B30TEXT.delete;

      declare 
        TYPE ctype__ is REF CURSOR;
        c__ ctype__;
      begin
-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR 
          select '<A HREF="javascript:var x = window.open(encodeURI(''!RASDC_ERRORS.Program?PPROGRAM=' ||
                 upper(name) || '#' || substr(type, 1, 1) ||
                 substr(type, instr(type, ' ') + 1, 1) || to_char(line) ||
                 '''),''nx'','''');"  style="color: Red;">ERR: (' ||
                 to_char(line) || ',' || to_char(position) || ')  ' || text ||
                 '</A>' text
            from all_errors
           where upper(name) = upper(PFORM)
             and owner = rasdc_library.currentDADUser
           order by line, position;
        i__ := 1;
        LOOP 
          FETCH c__ INTO
            B30TEXT(i__)
          ;
          exit when c__%notfound;
           if c__%rowcount >=  1 then
-- Generated code for setting lock value based on fiels checked Locked (combination with ON_LOCK trigger). Use (i__) to access fields values.


            exit when i__ =0;
            i__ := i__ + 1;
          end if;
        END LOOP;
         if c__%rowcount <  1 then
          B30TEXT.delete(1);
          i__ := 0;
        end if; 
        CLOSE c__;
      end;
      pclear_B30(B30TEXT.count);
  null; end;
  procedure pselect is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
      pselect_B10;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 then 
      pselect_B10G;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
      pselect_B30;
    end if;
--<post_select formid="90" blockid="">
PBLOKPROZILECNOV#SET(1).custom := 'onchange="document.getElementById(''ACTION_RASD'').value='''||GBUTTONSRC||''';  document.'||this_form||'.submit() ;"';		
PPF#SET(1).custom      := 'onchange="document.getElementById(''ACTION_RASD'').value='''||GBUTTONSRC||''';  document.'||this_form||'.submit() ;"';
PLDELETE(1) := B10LDELETE(1);

PGUPLOAD(1) := 'rasdc_files.showfile?pfile=pict/gumbupload.jpg';
PGUPLOAD#SET(1).custom := 'title="'||RASDI_TRNSLT.text('Customize your RASD enviorment',lang)||'"';
PUPLOAD(1) := 'rasdc_files.showfile?pfile=pict/gumbupload.jpg';
PUPLOAD#SET(1).custom := 'title="'||RASDI_TRNSLT.text('Upload new custom images for projects',lang)||'"';

if instr(PBLOKPROZILECNOV(1),'_JS') > 0 or instr(upper(PGBLOKPROZILECNOV(1)),'.JS') > 0 then
  PTEXTTYPE(1) := 'JS';
elsif instr(PBLOKPROZILECNOV(1),'_CSS') > 0 or instr(upper(PGBLOKPROZILECNOV(1)),'.CSS') > 0 then
  PTEXTTYPE(1) := 'CSS';
elsif instr(PBLOKPROZILECNOV(1),'_UIHEAD') > 0 or instr(upper(PGBLOKPROZILECNOV(1)),'.HTML') > 0 then
  PTEXTTYPE(1) := 'HTML';
end if;  

if page = 1 then 
PMIRROR := 'B10PLSQL_1_RASD';
elsif page = 2 then
PMIRROR := 'B10GBLOBVC_1_RASD';
end if;
--</post_select>
  null;
 end;
  procedure pcommit_P is
  begin
    for i__ in 1..PBLOKPROZILECNOV.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_PG is
  begin
    for i__ in 1..PGBLOKPROZILECNOV.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B10 is
  begin
    for i__ in 1..B10RS.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if substr(B10RS(i__),1,1) = 'I' then --INSERT
        if B10PLSQL(i__) is not null
 or B10PLSQLSPEC(i__) is not null
 then 

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

--<on_insert formid="90" blockid="B10">
declare
      v_def clob;
      v_code clob;
begin

v_def :=  trim(replace(replace(rasd_engine11.createtriggertemplateplsql(pformid, substr(PBLOKPROZILECNOV(1), instr(PBLOKPROZILECNOV(1), '/.../') + 5) , ''),' ',''),'
','X'));
v_code := trim(replace(replace(replace(B10PLSQL(1),' ',''),chr(10),'X'),chr(13),''));

           
            if nvl(v_code,'XX:#:XX') <>  nvl(v_def,'XX:#:XX')
              then
            B10FORMID(1) := PFORMID;

              B10BLOCKID(1) := null;
              B10TRIGGERID(1) := substr(PBLOKPROZILECNOV(1),
                                          instr(PBLOKPROZILECNOV(1), '/.../') + 5);
            insert into RASD_TRIGGERS
              (formid, blockid, triggerid, plsql, plsqlspec)
            values
              (B10FORMID(1) ,
               B10BLOCKID(1),
               B10TRIGGERID(1),
               B10PLSQL(1),
               B10PLSQLSPEC(1)
			  );

            null;
            end if;
end;
--</on_insert>

        null; end if;
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

        if B10PLSQL(i__) is null
 then --DELETE

--<on_delete formid="90" blockid="B10">
if substr(B10RS(i__),1,1) = 'U' then

delete RASD_TRIGGERS where ROWID = B10RID(1) and rform is null;

end if;			
--</on_delete>

        else --UPDATE

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

--<on_update formid="90" blockid="B10">
if substr(B10RS(1),1,1) = 'U' then
declare
              vtig_first pls_integer;
            begin

              select count(1)
                into vtig_first
                from rasd_triggers_code_types c, rasd_triggers s
               where c.tctype = s.triggerid
                 and c.language = 'P'
                 and s.rowid = B10RID(1);
                                  
              if vtig_first = 0 then

                update RASD_TRIGGERS s
                   set plsql = B10PLSQL(1)
                   ,plsqlspec =  B10PLSQLSPEC(1)
,                   rform  = decode (unlink,'Y',null,rform)
                 where ROWID = B10RID(1) and (rform is null or unlink = 'Y');
              else

                update RASD_TRIGGERS s
                   set plsql = B10PLSQL(1), plsqlspec = B10PLSQLSPEC(1)
,                   rform  = decode (unlink,'Y',null,rform)
                 where ROWID = B10RID(1) and (rform is null or unlink = 'Y');

              end if;

            end;
end if;			
--</on_update>

       null;  end if;
      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B10G is
  begin
    for i__ in 1..B10GRS.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if substr(B10GRS(i__),1,1) = 'I' then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

        if B10GBLOBVC(i__) is null
 then --DELETE

--<on_delete formid="90" blockid="B10G">
null;
--</on_delete>

        else --UPDATE

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

--<on_update formid="90" blockid="B10G">
if substr(B10GRS(1),1,1) = 'U' then

         execute immediate
          'update '||g_table||'
             set BLOB_CONTENT = '||g_table||'_API.Clob2Blob('''||replace(B10GBLOBVC(1),'''','''''')||''')
          where ROWID = '''||B10GRID(i__)||'''';


end if;			
--</on_update>

       null;  end if;
      null; end if;
    null; end loop;
  null; end;
  procedure pcommit_B20 is
  begin
    for i__ in 1..B20TEMPLATE.count loop
-- Validating field values before DML. Use (i__) to access fields values.
    null; end loop;
  null; end;
  procedure pcommit_B30 is
  begin
    for i__ in 1..B30TEXT.count loop
-- Validating field values before DML. Use (i__) to access fields values.
      if 1=2 then --INSERT
      null; else -- UPDATE or DELETE;
-- Generated code for lock value based on fields checked Locked (combination with ON_LOCK_VALUE trigger). Use (i__) to access fields values.

-- Generated code for mandatory statment based on fiels checked Mandatory. Use (i__) to access fields values.

      null; end if;
    null; end loop;
  null; end;
  procedure pcommit is
  begin

    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       pcommit_B10;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 then 
       pcommit_B10G;
    end if;
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       pcommit_B30;
    end if;
--<post_commit formid="90" blockid="">
update RASD_FORMS set change = sysdate where formid = PFORMID;

--</post_commit>
  null; 
  end;
  procedure formgen_js is
  begin
  --Input parameter in JS: PFORM
  --Input parameter in JS: PFORMID
    htp.p('function js_Slov$PBLOKPROZILECNOV_LOV(pvalue, pobjectname) {');
      htp.p(' eval(''val_''+pobjectname+''="''+pvalue.replace(/"/g, "&#34;")+''"'');'); 
      htp.p(' var x = document.getElementById(pobjectname+''_RASD''); ');
    begin
    for r__ in (
--<lovsql formid="90" linkid="lov$PBLOKPROZILECNOV_LOV">
select '/.../FORM_CSS' id, 'FORM_CSS' label, 1 from dual union
select '/.../FORM_JS' id, 'FORM_JS' label, 1 from dual union
select '/.../FORM_UIHEAD' id, 'FORM_UIHEAD', 1 label from dual union
select '/.../'||triggerid , triggerid|| decode(t.rform,null,'','-R') label , 2 vr
  from rasd_triggers t
 where t.formid = pformid
   and (triggerid like 'FORM_CSS_REF%' or triggerid like 'FORM_JS_REF%' or
       triggerid like 'FORM_UIHEAD_REF%')
 order by 1, 2
--</lovsql>
    ) loop
      htp.p('  var option = document.createElement("option"); option.value="'||replace(replace(r__.id,'''','\'''),'"','\"')||'"; option.text = "'||replace(replace(r__.label,'''','\'''),'"','\"')||'"; option.selected = ((pvalue=='''||replace(replace(r__.id,'''','\'''),'"','\"')||''')?'' selected '':''''); x.append(option);');
    end loop;
    exception when others then
      raise_application_error('-20000','Error in lov$PBLOKPROZILECNOV_LOV: '||sqlerrm);  
    end;
    htp.p('}');
    htp.p('function cMFP() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFPG() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB10() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB10G() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB20() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMFB30() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
    htp.p('function cMF() {');
    htp.p('var i = 0;');
    htp.p('if (i > 0) { return false; } else { return true; }');
    htp.p('}');
  end;
  procedure poutput is
    iB30 pls_integer;
  function ShowFieldCOMPID return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldDOCPRIVSOK return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldERROR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBTNNAVIG return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONBCK return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONCLR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONCOMPILE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONDELETE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONFWD return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONPREV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONRES return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONSAVE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldGBUTTONSRC return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldG_TABLE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldHINTCONTENT return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldMESSAGE return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldPFORM return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldPGOPTIONS return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldPMIRROR return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldUNLINK return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldVLOB return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldVUSER return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 then 
       return true;
    end if;
    return false;
  end; 
  function ShowFieldWARNING return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10G_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB20_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB30_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockP_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockPG_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 or nvl(PAGE,0) = 99 then 
       return true;
    end if;
    return false;
  end; 
  function js_link$uploadcust(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PUPLOAD%' then
      v_return := v_return || '''!DOCUMENTS_API2.page?showfiles=true''';
    elsif name is null then
      v_return := v_return ||'''!DOCUMENTS_API2.page?showfiles=true''';
    end if;
    return v_return;
  end;
  procedure js_link$uploadcust(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$uploadcust(value, name));
  end;
  function js_link$deletelink(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PLDELETE%' then
      v_return := v_return || '''''';
    elsif name is null then
      v_return := v_return ||'''''';
    end if;
    return v_return;
  end;
  procedure js_link$deletelink(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$deletelink(value, name));
  end;
  function js_link$editor(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'B20EDITOR%' then
      v_return := v_return || '''https://htmlg.com/html-editor/''';
    elsif name is null then
      v_return := v_return ||'''https://htmlg.com/html-editor/''';
    end if;
    return v_return;
  end;
  procedure js_link$editor(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$editor(value, name));
  end;
  function js_link$formnavlink(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'GBTNNAVIG%' then
      v_return := v_return || '''!RASDC2_EXECUTION.webclient?LANG=''+document.RASDC2_CSSJS.LANG.value+
''&PFORMID=''+document.RASDC2_CSSJS.PFORMID.value+
''''';
    elsif name is null then
      v_return := v_return ||'''!RASDC2_EXECUTION.webclient?LANG=''+document.RASDC2_CSSJS.LANG.value+
''&PFORMID=''+document.RASDC2_CSSJS.PFORMID.value+
''''';
    end if;
    return v_return;
  end;
  procedure js_link$formnavlink(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$formnavlink(value, name));
  end;
  function js_link$template(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'B20TEMPLATE%' then
      v_return := v_return || '''https://jsbin.com/woqokaquju/edit?html,css,js,output''';
    elsif name is null then
      v_return := v_return ||'''https://jsbin.com/woqokaquju/edit?html,css,js,output''';
    end if;
    return v_return;
  end;
  procedure js_link$template(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$template(value, name));
  end;
  function js_link$uploadgeneral(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'PGUPLOAD%' then
      v_return := v_return || '''!RASDC_FILES.page''';
    elsif name is null then
      v_return := v_return ||'''!RASDC_FILES.page''';
    end if;
    return v_return;
  end;
  procedure js_link$uploadgeneral(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$uploadgeneral(value, name));
  end;
  function js_link$regex(value varchar2, name varchar2 default null) return varchar2 is
   v_return varchar2(32000) := '';
  begin
    if 1=2 then null;
    elsif name like 'B20REGEX%' then
      v_return := v_return || '''https://regex101.com/''';
    elsif name is null then
      v_return := v_return ||'''https://regex101.com/''';
    end if;
    return v_return;
  end;
  procedure js_link$regex(value varchar2, name varchar2 default null) is
  begin
      htp.prn(js_link$regex(value, name));
  end;
  procedure js_Slov$PBLOKPROZILECNOV_LOV(value varchar2, name varchar2 default null) is
  begin
    htp.p('<script language="JavaScript">');
    htp.p('js_Slov$PBLOKPROZILECNOV_LOV('''||replace(replace(value,'''','\'''),'"','\"')||''','''||name||''');');
    htp.p('</script>');
  end;
procedure output_B10_DIV is begin htp.p('');  if  ShowBlockB10_DIV  then  
htp.prn('<div  id="B10_DIV" class="rasdblock"><div>
<caption><div id="B10_LAB" class="labelblock"></div></caption>
<table border="0" id="B10_TABLE"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10" id="rasdTxLabB10PLSQL">');  if B10PLSQL#SET(1).visible then  
htp.prn('<span id="B10PLSQL_LAB" class="label">'|| showLabel('Code','',0) ||'<div id=PLSQLCOUNT>�</div></span>');  end if;  
htp.prn('</td></tr></thead><tr id="B10_BLOCK"><span id="" value="1" name="" class="hiddenRowItems"><input name="B10RS_1" id="B10RS_1_RASD" type="hidden" value="'||B10RS(1)||'"/>
</span><span id="" value="1" name="" class="hiddenRowItems"><input name="B10RID_1" id="B10RID_1_RASD" type="hidden" value="'||to_char(B10RID(1))||'"/>
</span><td class="rasdTxB10PLSQL rasdTxTypeL" id="rasdTxB10PLSQL_1">');  if B10PLSQL#SET(1).visible then  
htp.prn('<textarea name="B10PLSQL_1" id="B10PLSQL_1_RASD" class="rasdTextarea ');  if B10PLSQL#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10PLSQL#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10PLSQL#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10PLSQL#SET(1).custom , instr(upper(B10PLSQL#SET(1).custom),'CLASS="')+7 , instr(upper(B10PLSQL#SET(1).custom),'"',instr(upper(B10PLSQL#SET(1).custom),'CLASS="')+8)-instr(upper(B10PLSQL#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if B10PLSQL#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10PLSQL#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10PLSQL#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||B10PLSQL#SET(1).custom);  
htp.prn('');  if B10PLSQL#SET(1).error is not null then htp.p(' title="'||B10PLSQL#SET(1).error||'"'); end if;  
htp.prn('');  if B10PLSQL#SET(1).info is not null then htp.p(' title="'||B10PLSQL#SET(1).info||'"'); end if;  
htp.prn('>');  htpClob( ''||B10PLSQL(1)||'' );  
htp.prn('</textarea>');  end if;  
htp.prn('</td></tr></table></div></div>');  end if;  
htp.prn(''); end;
procedure post_output_B10_DIV is  begin
--<post_ui formid="90" blockid="B10">
if page = 1 then
htp.p('<div class="label">');
htp.p(RASDI_TRNSLT.text('Press CTRL+SPACE to open variable list, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, ALT+S save, F11 to maximize window. (fold string is //// )',lang));
htp.p('</div>');
end if;
--</post_ui>
  end;
procedure output_B10G_DIV is begin htp.p('');  if  ShowBlockB10G_DIV  then  
htp.prn('<div  id="B10G_DIV" class="rasdblock"><div>
<caption><div id="B10G_LAB" class="labelblock"></div></caption>
<table border="0" id="B10G_TABLE_RASD"><thead><tr><td class="rasdTxLab rasdTxLabBlockB10G" id="rasdTxLabB10GBLOBVC">');  if B10GBLOBVC#SET(1).visible then  
htp.prn('<span id="B10GBLOBVC_LAB" class="label">'|| showLabel('Code','',0) ||'<div id=PLSQLCOUNT>�</div></span>');  end if;  
htp.prn('</td></tr></thead><tr id="B10G_BLOCK"><span id="" value="1" name="" class="hiddenRowItems"><input name="B10GRS_1" id="B10GRS_1_RASD" type="hidden" value="'||B10GRS(1)||'"/>
</span><span id="" value="1" name="" class="hiddenRowItems"><input name="B10GRID_1" id="B10GRID_1_RASD" type="hidden" value="'||to_char(B10GRID(1))||'"/>
</span><td class="rasdTxB10GBLOBVC rasdTxTypeL" id="rasdTxB10GBLOBVC_1">');  if B10GBLOBVC#SET(1).visible then  
htp.prn('<textarea name="B10GBLOBVC_1" id="B10GBLOBVC_1_RASD" class="rasdTextarea ');  if B10GBLOBVC#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if B10GBLOBVC#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(B10GBLOBVC#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(B10GBLOBVC#SET(1).custom , instr(upper(B10GBLOBVC#SET(1).custom),'CLASS="')+7 , instr(upper(B10GBLOBVC#SET(1).custom),'"',instr(upper(B10GBLOBVC#SET(1).custom),'CLASS="')+8)-instr(upper(B10GBLOBVC#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if B10GBLOBVC#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if B10GBLOBVC#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if B10GBLOBVC#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||B10GBLOBVC#SET(1).custom);  
htp.prn('');  if B10GBLOBVC#SET(1).error is not null then htp.p(' title="'||B10GBLOBVC#SET(1).error||'"'); end if;  
htp.prn('');  if B10GBLOBVC#SET(1).info is not null then htp.p(' title="'||B10GBLOBVC#SET(1).info||'"'); end if;  
htp.prn('>');  htpClob( ''||B10GBLOBVC(1)||'' );  
htp.prn('</textarea>');  end if;  
htp.prn('</td></tr></table></div></div>');  end if;  
htp.prn(''); end;
procedure post_output_B10G_DIV is  begin
--<post_ui formid="90" blockid="B10G">
if page = 2 then
htp.p('<div class="label">');
htp.p(RASDI_TRNSLT.text('Press CTRL+SPACE to open variable list, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, ALT+S save, F11 to maximize window. (fold string is //// )',lang));
htp.p('</div>');
end if;


if instr ( nvl(rasd_client.secSuperUsers,' '), rasd_client.secGetUsername ) > 0 and DOCPRIVSOK <> 'N' then
htp.p('</div></div>');
end if;
--</post_ui>
  end;
procedure output_B20_DIV is begin htp.p('');  if  ShowBlockB20_DIV  then  
htp.prn('<div  id="B20_DIV" class="rasdblock"><div>
<caption><div id="B20_LAB" class="labelblock">External links</div></caption>
<table border="0" id="B20_TABLE"><tr id="B20_BLOCK"><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20TEMPLATE"><span id="B20TEMPLATE_LAB" class="label"></span></td><td class="rasdTxB20TEMPLATE rasdTxTypeC" id="rasdTxB20TEMPLATE_1"><a href="javascript: var link=window.open(encodeURI('); js_link$template(B20TEMPLATE(1),'B20TEMPLATE_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="B20TEMPLATE_1" id="B20TEMPLATE_1_RASD" ');  if  B20TEMPLATE(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn('>'||B20TEMPLATE(1)||'</a></td></tr><tr><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20EDITOR"><span id="B20EDITOR_LAB" class="label"></span></td><td class="rasdTxB20EDITOR rasdTxTypeC" id="rasdTxB20EDITOR_1"><a href="javascript: var link=window.open(encodeURI('); js_link$editor(B20EDITOR(1),'B20EDITOR_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="B20EDITOR_1" id="B20EDITOR_1_RASD" ');  if  B20EDITOR(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn('>'||B20EDITOR(1)||'</a></td></tr><tr><td class="rasdTxLab rasdTxLabBlockB20" id="rasdTxLabB20REGEX"><span id="B20REGEX_LAB" class="label"></span></td><td class="rasdTxB20REGEX rasdTxTypeC" id="rasdTxB20REGEX_1"><a href="javascript: var link=window.open(encodeURI('); js_link$regex(B20REGEX(1),'B20REGEX_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="B20REGEX_1" id="B20REGEX_1_RASD" ');  if  B20REGEX(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn('>'||B20REGEX(1)||'</a></td></tr><tr></tr></table></div></div>');  end if;  
htp.prn(''); end;
procedure output_B30_DIV is begin htp.p('');  if  ShowBlockB30_DIV  then  
htp.prn('<div  id="B30_DIV" class="rasdblock"><div>
<caption><div id="B30_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Errors',LANG)||'</div></caption><table border="1" id="B30_TABLE" class="rasdTableN display"><thead><tr><td class="rasdTxLab rasdTxLabBlockB30" id="rasdTxLabB30TEXT"><span id="B30TEXT_LAB" class="label"></span></td></tr></thead>'); for iB30 in 1..B30TEXT.count loop 
htp.prn('<tr id="B30_BLOCK"><td class="rasdTxB30TEXT rasdTxTypeC" id="rasdTxB30TEXT_'||iB30||'"><font id="B30TEXT_'||iB30||'_RASD" class="rasdFont">'||B30TEXT(iB30)||'</font></td></tr>'); end loop; 
htp.prn('</table></div></div>');  end if;  
htp.prn(''); end;
procedure pre_output_P_DIV is  begin
--<pre_ui formid="90" blockid="P">
if instr ( nvl(rasd_client.secSuperUsers,' '), rasd_client.secGetUsername ) > 0 and DOCPRIVSOK <> 'N' then
htp.p('<div id="tabs">');

htp.p( myTabs(page, lang, pformid) );

htp.p('<div id="idselected">
<script>
$(function() {    
  $( "#tabs" ).tabs({active:'||(page-1)||'});
});   
</script>   
	  ');


end if;	  
--</pre_ui>
  end;
procedure output_P_DIV is begin htp.p('');  if  ShowBlockP_DIV  then  
htp.prn('<div  id="P_DIV" class="rasdblock"><div>
<caption><div id="P_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Form code (javascript, CSS)',LANG)||'</div></caption>
<table border="0" id="P_TABLE"><thead><tr><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPBLOKPROZILECNOV">');  if PBLOKPROZILECNOV#SET(1).visible then  
htp.prn('<span id="PBLOKPROZILECNOV_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPF">');  if PPF#SET(1).visible then  
htp.prn('<span id="PF_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPGBTNSRC">');  if PGBTNSRC#SET(1).visible then  
htp.prn('<span id="PGBTNSRC_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPGBTNSAVE">');  if PGBTNSAVE#SET(1).visible then  
htp.prn('<span id="PGBTNSAVE_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPLDELETE">');  if PLDELETE#SET(1).visible then  
htp.prn('<span id="PLDELETE_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPUPLOAD">');  if PUPLOAD#SET(1).visible then  
htp.prn('<span id="PUPLOAD_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockP" id="rasdTxLabPHINT"><span id="PHINT_LAB" class="label"></span></td></tr></thead><tr id="P_BLOCK"><span id="" value="1" name="" class="hiddenRowItems"><input name="SESSSTORAGEENABLED_1" id="SESSSTORAGEENABLED_1_RASD" type="hidden" value="'||PSESSSTORAGEENABLED(1)||'"/>
</span><td class="rasdTxPBLOKPROZILECNOV rasdTxTypeC" id="rasdTxPBLOKPROZILECNOV_1">');  if PBLOKPROZILECNOV#SET(1).visible then  
htp.prn('<select name="PBLOKPROZILECNOV_1" ID="PBLOKPROZILECNOV_1_RASD" class="rasdSelect ');  if PBLOKPROZILECNOV#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PBLOKPROZILECNOV#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PBLOKPROZILECNOV#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PBLOKPROZILECNOV#SET(1).custom , instr(upper(PBLOKPROZILECNOV#SET(1).custom),'CLASS="')+7 , instr(upper(PBLOKPROZILECNOV#SET(1).custom),'"',instr(upper(PBLOKPROZILECNOV#SET(1).custom),'CLASS="')+8)-instr(upper(PBLOKPROZILECNOV#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PBLOKPROZILECNOV#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PBLOKPROZILECNOV#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PBLOKPROZILECNOV#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PBLOKPROZILECNOV#SET(1).custom);  
htp.prn('');  if PBLOKPROZILECNOV#SET(1).error is not null then htp.p(' title="'||PBLOKPROZILECNOV#SET(1).error||'"'); end if;  
htp.prn('');  if PBLOKPROZILECNOV#SET(1).info is not null then htp.p(' title="'||PBLOKPROZILECNOV#SET(1).info||'"'); end if;  
htp.prn('>'); js_Slov$PBLOKPROZILECNOV_LOV(PBLOKPROZILECNOV(1),'PBLOKPROZILECNOV_1'); 
htp.prn('</select>');  end if;  
htp.prn('</td><td class="rasdTxPF rasdTxTypeC" id="rasdTxPF_1">');  if PPF#SET(1).visible then  
htp.prn('<input name="PF_1" id="PF_1_RASD" type="text" value="'||PPF(1)||'" class="rasdTextC ');  if PPF#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PPF#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PPF#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PPF#SET(1).custom , instr(upper(PPF#SET(1).custom),'CLASS="')+7 , instr(upper(PPF#SET(1).custom),'"',instr(upper(PPF#SET(1).custom),'CLASS="')+8)-instr(upper(PPF#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PPF#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PPF#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PPF#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PPF#SET(1).custom);  
htp.prn('');  if PPF#SET(1).error is not null then htp.p(' title="'||PPF#SET(1).error||'"'); end if;  
htp.prn('');  if PPF#SET(1).info is not null then htp.p(' title="'||PPF#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td><td class="rasdTxPGBTNSRC rasdTxTypeC" id="rasdTxPGBTNSRC_1">');  if PGBTNSRC#SET(1).visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="PGBTNSRC_1" id="PGBTNSRC_1_RASD" type="button" value="'||PGBTNSRC(1)||'" class="rasdButton');  if PGBTNSRC#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PGBTNSRC#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PGBTNSRC#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PGBTNSRC#SET(1).custom , instr(upper(PGBTNSRC#SET(1).custom),'CLASS="')+7 , instr(upper(PGBTNSRC#SET(1).custom),'"',instr(upper(PGBTNSRC#SET(1).custom),'CLASS="')+8)-instr(upper(PGBTNSRC#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PGBTNSRC#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGBTNSRC#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGBTNSRC#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGBTNSRC#SET(1).custom);  
htp.prn('');  if PGBTNSRC#SET(1).error is not null then htp.p(' title="'||PGBTNSRC#SET(1).error||'"'); end if;  
htp.prn('');  if PGBTNSRC#SET(1).info is not null then htp.p(' title="'||PGBTNSRC#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td><td class="rasdTxPGBTNSAVE rasdTxTypeC" id="rasdTxPGBTNSAVE_1">');  if PGBTNSAVE#SET(1).visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="PGBTNSAVE_1" id="PGBTNSAVE_1_RASD" type="button" value="'||PGBTNSAVE(1)||'" class="rasdButton');  if PGBTNSAVE#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PGBTNSAVE#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PGBTNSAVE#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PGBTNSAVE#SET(1).custom , instr(upper(PGBTNSAVE#SET(1).custom),'CLASS="')+7 , instr(upper(PGBTNSAVE#SET(1).custom),'"',instr(upper(PGBTNSAVE#SET(1).custom),'CLASS="')+8)-instr(upper(PGBTNSAVE#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PGBTNSAVE#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGBTNSAVE#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGBTNSAVE#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGBTNSAVE#SET(1).custom);  
htp.prn('');  if PGBTNSAVE#SET(1).error is not null then htp.p(' title="'||PGBTNSAVE#SET(1).error||'"'); end if;  
htp.prn('');  if PGBTNSAVE#SET(1).info is not null then htp.p(' title="'||PGBTNSAVE#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td><td class="rasdTxPLDELETE rasdTxTypeC" id="rasdTxPLDELETE_1">');  if PLDELETE#SET(1).visible then  
htp.prn('<IMG ONCLICK="javascript: if (confirm('''|| GBUTTONDELETE||'?'') == true) { window.rasd_CMEditorB10PLSQL_1_RASD.setValue(''''); document.RASDC2_CSSJS.ACTION.value=''Save''; document.RASDC2_CSSJS.submit();}" ');  if  PLDELETE(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn(' ID="PLDELETE_1_RASD" CLASS="rasdImg');  if PLDELETE#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PLDELETE#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PLDELETE#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PLDELETE#SET(1).custom , instr(upper(PLDELETE#SET(1).custom),'CLASS="')+7 , instr(upper(PLDELETE#SET(1).custom),'"',instr(upper(PLDELETE#SET(1).custom),'CLASS="')+8)-instr(upper(PLDELETE#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('" SRC="'||PLDELETE(1)||'"');  if PLDELETE#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PLDELETE#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PLDELETE#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PLDELETE#SET(1).custom);  
htp.prn('');  if PLDELETE#SET(1).error is not null then htp.p(' title="'||PLDELETE#SET(1).error||'"'); end if;  
htp.prn('');  if PLDELETE#SET(1).info is not null then htp.p(' title="'||PLDELETE#SET(1).info||'"'); end if;  
htp.prn('></IMG>');  end if;  
htp.prn('</td><td class="rasdTxPUPLOAD rasdTxTypeC" id="rasdTxPUPLOAD_1">');  if PUPLOAD#SET(1).visible then  
htp.prn('<IMG ONCLICK="javascript: var link=window.open(encodeURI('); js_link$uploadcust(PUPLOAD(1),'PUPLOAD_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" ');  if  PUPLOAD(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn(' ID="PUPLOAD_1_RASD" CLASS="rasdImg');  if PUPLOAD#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PUPLOAD#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PUPLOAD#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PUPLOAD#SET(1).custom , instr(upper(PUPLOAD#SET(1).custom),'CLASS="')+7 , instr(upper(PUPLOAD#SET(1).custom),'"',instr(upper(PUPLOAD#SET(1).custom),'CLASS="')+8)-instr(upper(PUPLOAD#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('" SRC="'||PUPLOAD(1)||'"');  if PUPLOAD#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PUPLOAD#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PUPLOAD#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PUPLOAD#SET(1).custom);  
htp.prn('');  if PUPLOAD#SET(1).error is not null then htp.p(' title="'||PUPLOAD#SET(1).error||'"'); end if;  
htp.prn('
');  if PUPLOAD#SET(1).info is not null then htp.p(' title="'||PUPLOAD#SET(1).info||'"'); end if;  
htp.prn('></IMG>');  end if;  
htp.prn('</td><td class="rasdTxPHINT rasdTxTypeC" id="rasdTxPHINT_1"><span id="PHINT_1_RASD">');  rasdc_hints.link(replace(this_form,'2',''), lang);  
htp.prn('</span></td></tr></table></div></div>');  end if;  
htp.prn(''); end;
procedure output_PG_DIV is begin htp.p('');  if  ShowBlockPG_DIV  then  
htp.prn('<div  id="PG_DIV" class="rasdblock"><div>
<caption><div id="PG_LAB" class="labelblock">'|| RASDI_TRNSLT.text('Global code (javascript, CSS)',LANG)||'</div></caption>
<table border="0" id="PG_TABLE_RASD"><thead><tr><td class="rasdTxLab rasdTxLabBlockPG" id="rasdTxLabPGBLOKPROZILECNOV">');  if PGBLOKPROZILECNOV#SET(1).visible then  
htp.prn('<span id="PGBLOKPROZILECNOV_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockPG" id="rasdTxLabPGGBTNSRC">');  if PGGBTNSRC#SET(1).visible then  
htp.prn('<span id="PGGBTNSRC_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockPG" id="rasdTxLabPGGBTNSAVE">');  if PGGBTNSAVE#SET(1).visible then  
htp.prn('<span id="PGGBTNSAVE_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockPG" id="rasdTxLabPGUPLOAD">');  if PGUPLOAD#SET(1).visible then  
htp.prn('<span id="PGUPLOAD_LAB" class="label"></span>');  end if;  
htp.prn('</td><td class="rasdTxLab rasdTxLabBlockPG" id="rasdTxLabPGHINT"><span id="PGHINT_LAB" class="label"></span></td></tr></thead><tr id="PG_BLOCK"><td class="rasdTxPGBLOKPROZILECNOV rasdTxTypeC" id="rasdTxPGBLOKPROZILECNOV_1">');  if PGBLOKPROZILECNOV#SET(1).visible then  
htp.prn('<span id="PGBLOKPROZILECNOV_1_RASD"');  if PGBLOKPROZILECNOV#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGBLOKPROZILECNOV#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGBLOKPROZILECNOV#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGBLOKPROZILECNOV#SET(1).custom);  
htp.prn('');  if PGBLOKPROZILECNOV#SET(1).error is not null then htp.p(' title="'||PGBLOKPROZILECNOV#SET(1).error||'"'); end if;  
htp.prn('');  if PGBLOKPROZILECNOV#SET(1).info is not null then htp.p(' title="'||PGBLOKPROZILECNOV#SET(1).info||'"'); end if;  
htp.prn('>');  htp.p('<select name="PGBLOKPROZILECNOV_1" id="PGBLOKPROZILECNOV_X_1_RASD" class="rasdSelect " onchange="document.getElementById(''ACTION_RASD'').value='''||GBUTTONSRC||''';  document.'||this_form||'.submit() ;">');
htp.p(pgoptions); --created in pre select B10g.
htp.p('</select>');  
htp.prn('</span>');  end if;  
htp.prn('</td><td class="rasdTxPGGBTNSRC rasdTxTypeC" id="rasdTxPGGBTNSRC_1">');  if PGGBTNSRC#SET(1).visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="PGGBTNSRC_1" id="PGGBTNSRC_1_RASD" type="button" value="'||PGGBTNSRC(1)||'" class="rasdButton');  if PGGBTNSRC#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PGGBTNSRC#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PGGBTNSRC#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PGGBTNSRC#SET(1).custom , instr(upper(PGGBTNSRC#SET(1).custom),'CLASS="')+7 , instr(upper(PGGBTNSRC#SET(1).custom),'"',instr(upper(PGGBTNSRC#SET(1).custom),'CLASS="')+8)-instr(upper(PGGBTNSRC#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PGGBTNSRC#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGGBTNSRC#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGGBTNSRC#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGGBTNSRC#SET(1).custom);  
htp.prn('');  if PGGBTNSRC#SET(1).error is not null then htp.p(' title="'||PGGBTNSRC#SET(1).error||'"'); end if;  
htp.prn('');  if PGGBTNSRC#SET(1).info is not null then htp.p(' title="'||PGGBTNSRC#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td><td class="rasdTxPGGBTNSAVE rasdTxTypeC" id="rasdTxPGGBTNSAVE_1">');  if PGGBTNSAVE#SET(1).visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="PGGBTNSAVE_1" id="PGGBTNSAVE_1_RASD" type="button" value="'||PGGBTNSAVE(1)||'" class="rasdButton');  if PGGBTNSAVE#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PGGBTNSAVE#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PGGBTNSAVE#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PGGBTNSAVE#SET(1).custom , instr(upper(PGGBTNSAVE#SET(1).custom),'CLASS="')+7 , instr(upper(PGGBTNSAVE#SET(1).custom),'"',instr(upper(PGGBTNSAVE#SET(1).custom),'CLASS="')+8)-instr(upper(PGGBTNSAVE#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if PGGBTNSAVE#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGGBTNSAVE#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGGBTNSAVE#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGGBTNSAVE#SET(1).custom);  
htp.prn('');  if PGGBTNSAVE#SET(1).error is not null then htp.p(' title="'||PGGBTNSAVE#SET(1).error||'"'); end if;  
htp.prn('');  if PGGBTNSAVE#SET(1).info is not null then htp.p(' title="'||PGGBTNSAVE#SET(1).info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
</td><td class="rasdTxPGUPLOAD rasdTxTypeC" id="rasdTxPGUPLOAD_1">');  if PGUPLOAD#SET(1).visible then  
htp.prn('<IMG ONCLICK="javascript: var link=window.open(encodeURI('); js_link$uploadgeneral(PGUPLOAD(1),'PGUPLOAD_1'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" ');  if  PGUPLOAD(1) is not null and '' is not null  then htp.prn('title=""'); end if;  
htp.prn(' ID="PGUPLOAD_1_RASD" CLASS="rasdImg');  if PGUPLOAD#SET(1).error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if PGUPLOAD#SET(1).info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(PGUPLOAD#SET(1).custom) ,'CLASS="') > 0 then  htp.p(' '||substr(PGUPLOAD#SET(1).custom , instr(upper(PGUPLOAD#SET(1).custom),'CLASS="')+7 , instr(upper(PGUPLOAD#SET(1).custom),'"',instr(upper(PGUPLOAD#SET(1).custom),'CLASS="')+8)-instr(upper(PGUPLOAD#SET(1).custom),'CLASS="')-7) ); end if;  
htp.prn('" SRC="'||PGUPLOAD(1)||'"');  if PGUPLOAD#SET(1).readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if PGUPLOAD#SET(1).disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if PGUPLOAD#SET(1).required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||PGUPLOAD#SET(1).custom);  
htp.prn('');  if PGUPLOAD#SET(1).error is not null then htp.p(' title="'||PGUPLOAD#SET(1).error||'"'); end if;  
htp.prn('');  if PGUPLOAD#SET(1).info is not null then htp.p(' title="'||PGUPLOAD#SET(1).info||'"'); end if;  
htp.prn('></IMG>');  end if;  
htp.prn('</td><td class="rasdTxPGHINT rasdTxTypeC" id="rasdTxPGHINT_1"><span id="PGHINT_1_RASD">');  rasdc_hints.link(replace(this_form,'2',''), lang);  
htp.prn('</span></td></tr></table></div></div>');  end if;  
htp.prn(''); end;
  begin
if set_session_block__ is not null then  execute immediate set_session_block__;  end if;
    htp.prn('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||'')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">'); htpClob(FORM_JS); htp.p('</script>'); 
 
htp.prn('</head>
<body><div id="RASDC2_CSSJS_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_CSSJS_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div id="RASDC2_CSSJS_MENU" class="rasdFormMenu">'|| rasd_client.getHtmlMenuList('RASDC2_CSSJS_MENU') ||'     </div>
<form name="RASDC2_CSSJS" method="post" action="?"><div id="RASDC2_CSSJS_DIV" class="rasdForm"><div id="RASDC2_CSSJS_HEAD" class="rasdFormHead"><input name="RECNUMB10" id="RECNUMB10_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB10))||'"/>
<input name="RECNUMPG" id="RECNUMPG_RASD" type="hidden" value="'||ltrim(to_char(RECNUMPG))||'"/>
<input name="RECNUMP" id="RECNUMP_RASD" type="hidden" value="'||ltrim(to_char(RECNUMP))||'"/>
<input name="RECNUMB10G" id="RECNUMB10G_RASD" type="hidden" value="'||ltrim(to_char(RECNUMB10G))||'"/>
<input name="PAGE" id="PAGE_RASD" type="hidden" value="'||ltrim(to_char(PAGE))||'"/>
<input name="ACTION" id="ACTION_RASD" type="hidden" value="'||ACTION||'"/>
<input name="LANG" id="LANG_RASD" type="hidden" value="'||LANG||'"/>
<input name="PFORMID" id="PFORMID_RASD" type="hidden" value="'||PFORMID||'"/>
');  
if  ShowFieldGBTNNAVIG  then  
htp.prn('<input onclick="javascript: var link=window.open(encodeURI('); js_link$formnavlink(GBTNNAVIG,'GBTNNAVIG'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="GBTNNAVIG" id="GBTNNAVIG_RASD" type="button" value="'||GBTNNAVIG||'" class="rasdButton"/>');  end if;  
htp.prn('
');  
if  ShowFieldGBUTTONSAVE  then  
htp.prn('');  if GBUTTONSAVE#SET.visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);  
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONCOMPILE  then  
htp.prn('');  if GBUTTONCOMPILE#SET.visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);  
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONRES  then  
htp.prn('');  if GBUTTONRES#SET.visible then  
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);  
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONPREV  then  
htp.prn('');  if GBUTTONPREV#SET.visible then  
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);  
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;  
htp.prn('>');  end if;  
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||
                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||
                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,
                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',
                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,
                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||
                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '
									 ,
                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '
                                )
			       ) || '>'
);  
htp.prn('</span>');  end if;  
htp.prn('');  
if  ShowFieldHINTCONTENT  then  
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('');  
htp.prn('</span>');  end if;  
htp.prn('</div><div id="RASDC2_CSSJS_RESPONSE" class="rasdFormResponse"><div id="RASDC2_CSSJS_ERROR" class="rasdFormMessage error"><font id="ERROR_RASD" class="rasdFont">'||ERROR||'</font></div><div id="RASDC2_CSSJS_WARNING" class="rasdFormMessage warning"><font id="WARNING_RASD" class="rasdFont">'||WARNING||'</font></div><div id="RASDC2_CSSJS_MESSAGE" class="rasdFormMessage"><font id="MESSAGE_RASD" class="rasdFont">'||MESSAGE||'</font></div></div><div id="RASDC2_CSSJS_BODY" class="rasdFormBody">'); pre_output_P_DIV; output_P_DIV; htp.p(''); output_PG_DIV; htp.p(''); output_B10_DIV; post_output_B10_DIV; htp.p(''); output_B10G_DIV; post_output_B10G_DIV; htp.p(''); output_B20_DIV; htp.p(''); output_B30_DIV; htp.p('</div><div id="RASDC2_CSSJS_FOOTER" class="rasdFormFooter">');  
if  ShowFieldGBTNNAVIG  then  
htp.prn('<input onclick="javascript: var link=window.open(encodeURI('); js_link$formnavlink(GBTNNAVIG,'GBTNNAVIG'); 
htp.prn('),''x1'',''resizable,scrollbars,width=680,height=550'');" name="GBTNNAVIG" id="GBTNNAVIG_RASD" type="button" value="'||GBTNNAVIG||'" class="rasdButton"/>');  end if;  
htp.prn('
');  
if  ShowFieldGBUTTONSAVE  then  
htp.prn('');  if GBUTTONSAVE#SET.visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONSAVE" id="GBUTTONSAVE_RASD" type="button" value="'||GBUTTONSAVE||'" class="rasdButton');  if GBUTTONSAVE#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONSAVE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONSAVE#SET.custom),'"',instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONSAVE#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONSAVE#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONSAVE#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONSAVE#SET.custom);  
htp.prn('');  if GBUTTONSAVE#SET.error is not null then htp.p(' title="'||GBUTTONSAVE#SET.error||'"'); end if;  
htp.prn('
');  if GBUTTONSAVE#SET.info is not null then htp.p(' title="'||GBUTTONSAVE#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONCOMPILE  then  
htp.prn('');  if GBUTTONCOMPILE#SET.visible then  
htp.prn('<input onclick=" ACTION.value=this.value; submit();" name="GBUTTONCOMPILE" id="GBUTTONCOMPILE_RASD" type="button" value="'||GBUTTONCOMPILE||'" class="rasdButton');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),'"',instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONCOMPILE#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONCOMPILE#SET.custom);  
htp.prn('');  if GBUTTONCOMPILE#SET.error is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONCOMPILE#SET.info is not null then htp.p(' title="'||GBUTTONCOMPILE#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONRES  then  
htp.prn('');  if GBUTTONRES#SET.visible then  
htp.prn('<input name="GBUTTONRES" id="GBUTTONRES_RASD" type="reset" value="'||GBUTTONRES||'" class="rasdButton');  if GBUTTONRES#SET.error is not null then htp.p(' errorField'); end if;  
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' infoField'); end if;  
htp.prn('');  if instr(upper(GBUTTONRES#SET.custom) ,'CLASS="') > 0 then  htp.p(' '||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),'CLASS="')+7 , instr(upper(GBUTTONRES#SET.custom),'"',instr(upper(GBUTTONRES#SET.custom),'CLASS="')+8)-instr(upper(GBUTTONRES#SET.custom),'CLASS="')-7) ); end if;  
htp.prn('"');  if GBUTTONRES#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONRES#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONRES#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONRES#SET.custom);  
htp.prn('');  if GBUTTONRES#SET.error is not null then htp.p(' title="'||GBUTTONRES#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONRES#SET.info is not null then htp.p(' title="'||GBUTTONRES#SET.info||'"'); end if;  
htp.prn('/>');  end if;  
htp.prn('
');  end if;  
htp.prn('');  
if  ShowFieldGBUTTONPREV  then  
htp.prn('');  if GBUTTONPREV#SET.visible then  
htp.prn('<span id="GBUTTONPREV_RASD"');  if GBUTTONPREV#SET.readonly then htp.p(' readonly="readonly"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.disabled then htp.p(' disabled="disabled"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.required then htp.p(' required="required"'); end if;  
htp.prn('');  htp.p(' '||GBUTTONPREV#SET.custom);  
htp.prn('');  if GBUTTONPREV#SET.error is not null then htp.p(' title="'||GBUTTONPREV#SET.error||'"'); end if;  
htp.prn('');  if GBUTTONPREV#SET.info is not null then htp.p(' title="'||GBUTTONPREV#SET.info||'"'); end if;  
htp.prn('>');  end if;  
htp.prn('');  htp.p( '<input type=button class="rasdButton" value="' ||
                   RASDI_TRNSLT.text('Preview', lang) || '" ' ||
                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,
                                'disabled="disabled" style="background-color: red;" title="' ||RASDI_TRNSLT.text('Program has ERRORS!',lang)||'" ',
                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,
                                     'style="background-color: orange;" title="' ||RASDI_TRNSLT.text('Programa has changes. Compile it.',lang) ||
                                     '" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '
									 ,
                                     'style="background-color: green;" onclick="x=window.open(''!' ||pform ||'.webclient'','''','''')" '
                                )
			       ) || '>'
);  
htp.prn('</span>');  end if;  
htp.prn('');  
if  ShowFieldHINTCONTENT  then  
htp.prn('<span id="HINTCONTENT_RASD">');  htp.p('');  
htp.prn('</span>');  end if;  
htp.prn('</div></div></form><div id="RASDC2_CSSJS_BOTTOM" class="rasdFormBottom">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_CSSJS_BOTTOM',1,instr('RASDC2_CSSJS_BOTTOM', '_',-1)-1) , '') ||'</div></body></html>
    ');
  null; end;
  function poutputrest return clob is
    v_firstrow__ boolean;
    v_clob__ clob;
    procedure htpp(v_str varchar2) is 
    begin
      v_clob__ := v_clob__ || v_str;
    end;
    function escapeRest(v_str varchar2) return varchar2 is 
    begin
      return replace(v_str,'"','&quot;');
    end;
    function escapeRest(v_str clob) return clob is 
    begin
      return replace(v_str,'"','&quot;');
    end;
  function ShowBlockB10_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB10G_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB20_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 2 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockB30_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockP_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 1 or nvl(PAGE,0) = 99 then 
       return true;
    end if;
    return false;
  end; 
  function ShowBlockPG_DIV return boolean is 
  begin 
    if  nvl(PAGE,0) = 0 or nvl(PAGE,0) = 2 or nvl(PAGE,0) = 99 then 
       return true;
    end if;
    return false;
  end; 
  begin
if RESTRESTYPE = 'XML' then
    htpp('<?xml version="1.0" encoding="UTF-8"?>'); 
    htpp('<form name="RASDC2_CSSJS" version="'||version||'">'); 
    htpp('<formfields>'); 
    htpp('<recnumb10>'||RECNUMB10||'</recnumb10>'); 
    htpp('<recnumpg>'||RECNUMPG||'</recnumpg>'); 
    htpp('<recnumb10g>'||RECNUMB10G||'</recnumb10g>'); 
    htpp('<recnump>'||RECNUMP||'</recnump>'); 
    htpp('<action><![CDATA['||ACTION||']]></action>'); 
    htpp('<page>'||PAGE||'</page>'); 
    htpp('<lang><![CDATA['||LANG||']]></lang>'); 
    htpp('<pformid><![CDATA['||PFORMID||']]></pformid>'); 
    htpp('<gbtnnavig><![CDATA['||GBTNNAVIG||']]></gbtnnavig>'); 
    htpp('<gbuttonsave><![CDATA['||GBUTTONSAVE||']]></gbuttonsave>'); 
    htpp('<gbuttoncompile><![CDATA['||GBUTTONCOMPILE||']]></gbuttoncompile>'); 
    htpp('<gbuttonres><![CDATA['||GBUTTONRES||']]></gbuttonres>'); 
    htpp('<gbuttonprev><![CDATA['||GBUTTONPREV||']]></gbuttonprev>'); 
    htpp('<error><![CDATA['||ERROR||']]></error>'); 
    htpp('<message><![CDATA['||MESSAGE||']]></message>'); 
    htpp('<warning><![CDATA['||WARNING||']]></warning>'); 
    htpp('<hintcontent><![CDATA['||HINTCONTENT||']]></hintcontent>'); 
    htpp('</formfields>'); 
    if ShowBlockp_DIV then 
    htpp('<p>'); 
    htpp('<element>'); 
    htpp('<pblokprozilecnov><![CDATA['||PBLOKPROZILECNOV(1)||']]></pblokprozilecnov>'); 
    htpp('<pf><![CDATA['||PPF(1)||']]></pf>'); 
    htpp('<pgbtnsrc><![CDATA['||PGBTNSRC(1)||']]></pgbtnsrc>'); 
    htpp('<pgbtnsave><![CDATA['||PGBTNSAVE(1)||']]></pgbtnsave>'); 
    htpp('<pldelete><![CDATA['||PLDELETE(1)||']]></pldelete>'); 
    htpp('<pupload><![CDATA['||PUPLOAD(1)||']]></pupload>'); 
    htpp('<phint><![CDATA['||PHINT(1)||']]></phint>'); 
    htpp('<sessstorageenabled><![CDATA['||PSESSSTORAGEENABLED(1)||']]></sessstorageenabled>'); 
    htpp('</element>'); 
  htpp('</p>'); 
  end if; 
    if ShowBlockpg_DIV then 
    htpp('<pg>'); 
    htpp('<element>'); 
    htpp('<pgblokprozilecnov><![CDATA['||PGBLOKPROZILECNOV(1)||']]></pgblokprozilecnov>'); 
    htpp('<pggbtnsrc><![CDATA['||PGGBTNSRC(1)||']]></pggbtnsrc>'); 
    htpp('<pggbtnsave><![CDATA['||PGGBTNSAVE(1)||']]></pggbtnsave>'); 
    htpp('<pgupload><![CDATA['||PGUPLOAD(1)||']]></pgupload>'); 
    htpp('<pghint><![CDATA['||PGHINT(1)||']]></pghint>'); 
    htpp('<sessstorageenabled><![CDATA['||PGSESSSTORAGEENABLED(1)||']]></sessstorageenabled>'); 
    htpp('</element>'); 
  htpp('</pg>'); 
  end if; 
    if ShowBlockb10_DIV then 
    htpp('<b10>'); 
  for i__ in 1..
B10RID
.count loop 
    htpp('<element>'); 
    htpp('<b10rs><![CDATA['||B10RS(i__)||']]></b10rs>'); 
    htpp('<b10rid>'||B10RID(i__)||'</b10rid>'); 
    htpp('<b10plsql><![CDATA['||B10PLSQL(i__)||']]></b10plsql>'); 
    htpp('</element>'); 
  end loop; 
  htpp('</b10>'); 
  end if; 
    if ShowBlockb10g_DIV then 
    htpp('<b10g>'); 
  for i__ in 1..
B10GRID
.count loop 
    htpp('<element>'); 
    htpp('<b10grs><![CDATA['||B10GRS(i__)||']]></b10grs>'); 
    htpp('<b10grid>'||B10GRID(i__)||'</b10grid>'); 
    htpp('<b10gblobvc><![CDATA['||B10GBLOBVC(i__)||']]></b10gblobvc>'); 
    htpp('</element>'); 
  end loop; 
  htpp('</b10g>'); 
  end if; 
    if ShowBlockb20_DIV then 
    htpp('<b20>'); 
    htpp('<element>'); 
    htpp('<b20template><![CDATA['||B20TEMPLATE(1)||']]></b20template>'); 
    htpp('<b20editor><![CDATA['||B20EDITOR(1)||']]></b20editor>'); 
    htpp('<b20regex><![CDATA['||B20REGEX(1)||']]></b20regex>'); 
    htpp('</element>'); 
  htpp('</b20>'); 
  end if; 
    if ShowBlockb30_DIV then 
    htpp('<b30>'); 
  for i__ in 1..
B30TEXT
.count loop 
    htpp('<element>'); 
    htpp('<b30text><![CDATA['||B30TEXT(i__)||']]></b30text>'); 
    htpp('</element>'); 
  end loop; 
  htpp('</b30>'); 
  end if; 
    htpp('</form>'); 
else
    htpp('{"form":{"@name":"RASDC2_CSSJS","@version":"'||version||'",' ); 
    htpp('"formfields": {'); 
    htpp('"recnumb10":"'||RECNUMB10||'"'); 
    htpp(',"recnumpg":"'||RECNUMPG||'"'); 
    htpp(',"recnumb10g":"'||RECNUMB10G||'"'); 
    htpp(',"recnump":"'||RECNUMP||'"'); 
    htpp(',"action":"'||escapeRest(ACTION)||'"'); 
    htpp(',"page":"'||PAGE||'"'); 
    htpp(',"lang":"'||escapeRest(LANG)||'"'); 
    htpp(',"pformid":"'||escapeRest(PFORMID)||'"'); 
    htpp(',"gbtnnavig":"'||escapeRest(GBTNNAVIG)||'"'); 
    htpp(',"gbuttonsave":"'||escapeRest(GBUTTONSAVE)||'"'); 
    htpp(',"gbuttoncompile":"'||escapeRest(GBUTTONCOMPILE)||'"'); 
    htpp(',"gbuttonres":"'||escapeRest(GBUTTONRES)||'"'); 
    htpp(',"gbuttonprev":"'||escapeRest(GBUTTONPREV)||'"'); 
    htpp(',"error":"'||escapeRest(ERROR)||'"'); 
    htpp(',"message":"'||escapeRest(MESSAGE)||'"'); 
    htpp(',"warning":"'||escapeRest(WARNING)||'"'); 
    htpp(',"hintcontent":"'||escapeRest(HINTCONTENT)||'"'); 
    htpp('},'); 
    if ShowBlockp_DIV then 
    htpp('"p":['); 
     htpp('{'); 
    htpp('"pblokprozilecnov":"'||escapeRest(PBLOKPROZILECNOV(1))||'"'); 
    htpp(',"pf":"'||escapeRest(PPF(1))||'"'); 
    htpp(',"pgbtnsrc":"'||escapeRest(PGBTNSRC(1))||'"'); 
    htpp(',"pgbtnsave":"'||escapeRest(PGBTNSAVE(1))||'"'); 
    htpp(',"pldelete":"'||escapeRest(PLDELETE(1))||'"'); 
    htpp(',"pupload":"'||escapeRest(PUPLOAD(1))||'"'); 
    htpp(',"phint":"'||escapeRest(PHINT(1))||'"'); 
    htpp(',"sessstorageenabled":"'||escapeRest(PSESSSTORAGEENABLED(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp('"p":[]'); 
  end if; 
    if ShowBlockpg_DIV then 
    htpp(',"pg":['); 
     htpp('{'); 
    htpp('"pgblokprozilecnov":"'||escapeRest(PGBLOKPROZILECNOV(1))||'"'); 
    htpp(',"pggbtnsrc":"'||escapeRest(PGGBTNSRC(1))||'"'); 
    htpp(',"pggbtnsave":"'||escapeRest(PGGBTNSAVE(1))||'"'); 
    htpp(',"pgupload":"'||escapeRest(PGUPLOAD(1))||'"'); 
    htpp(',"pghint":"'||escapeRest(PGHINT(1))||'"'); 
    htpp(',"sessstorageenabled":"'||escapeRest(PGSESSSTORAGEENABLED(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp(',"pg":[]'); 
  end if; 
    if ShowBlockb10_DIV then 
    htpp(',"b10":['); 
  v_firstrow__ := true;
  for i__ in 1..
B10RID
.count loop 
    if v_firstrow__ then
     htpp('{'); 
     v_firstrow__ := false;
    else
     htpp(',{'); 
    end if;
    htpp('"b10rs":"'||escapeRest(B10RS(i__))||'"'); 
    htpp(',"b10rid":"'||B10RID(i__)||'"'); 
    htpp(',"b10plsql":"'||escapeRest(B10PLSQL(i__))||'"'); 
    htpp('}'); 
  end loop; 
    htpp(']'); 
  else 
    htpp(',"b10":[]'); 
  end if; 
    if ShowBlockb10g_DIV then 
    htpp(',"b10g":['); 
  v_firstrow__ := true;
  for i__ in 1..
B10GRID
.count loop 
    if v_firstrow__ then
     htpp('{'); 
     v_firstrow__ := false;
    else
     htpp(',{'); 
    end if;
    htpp('"b10grs":"'||escapeRest(B10GRS(i__))||'"'); 
    htpp(',"b10grid":"'||B10GRID(i__)||'"'); 
    htpp(',"b10gblobvc":"'||escapeRest(B10GBLOBVC(i__))||'"'); 
    htpp('}'); 
  end loop; 
    htpp(']'); 
  else 
    htpp(',"b10g":[]'); 
  end if; 
    if ShowBlockb20_DIV then 
    htpp(',"b20":['); 
     htpp('{'); 
    htpp('"b20template":"'||escapeRest(B20TEMPLATE(1))||'"'); 
    htpp(',"b20editor":"'||escapeRest(B20EDITOR(1))||'"'); 
    htpp(',"b20regex":"'||escapeRest(B20REGEX(1))||'"'); 
    htpp('}'); 
    htpp(']'); 
  else 
    htpp(',"b20":[]'); 
  end if; 
    if ShowBlockb30_DIV then 
    htpp(',"b30":['); 
  v_firstrow__ := true;
  for i__ in 1..
B30TEXT
.count loop 
    if v_firstrow__ then
     htpp('{'); 
     v_firstrow__ := false;
    else
     htpp(',{'); 
    end if;
    htpp('"b30text":"'||escapeRest(B30TEXT(i__))||'"'); 
    htpp('}'); 
  end loop; 
    htpp(']'); 
  else 
    htpp(',"b30":[]'); 
  end if; 
    htpp('}}'); 
end if;
return v_clob__;
null; end;
procedure poutputrest is
begin
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
else
    OWA_UTIL.mime_header('application/json', FALSE ,'utf-8');
    OWA_UTIL.http_header_close;	
end if;
htpclob(poutputrest);
end;
procedure webclient(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

--<ON_ACTION formid="90" blockid="">
  rasdc_library.log(this_form,pformid, 'START', compid);
	
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission(this_form,ACTION);  
  
  RASDC_LIBRARY.checkprivileges(PFORMID);
 
  if ACTION is null then null;
    RECNUMB10 := 1;
    RECNUMP := 1;
    pselect;
    poutput;
   elsif ACTION = GBUTTONSRC then     
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     
        pcommit;
        rasdc_library.RefData(PFORMID);   
                       
        pselect;
        message := 'Changes are saved.';
        
        if B10RFORM(1) is not null then
           message :=  message ||  RASDI_TRNSLT.text('To unlink referenced code check:', lang)||'<input type="checkbox" name="UNLINK" value="Y"/>.';
        end if; 
		
	    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
	
  elsif ACTION = GBUTTONCOMPILE then      

        rasdc_library.log(this_form,pformid, 'COMMIT_S', compid);       
        pcommit;
        rasdc_library.log(this_form,pformid, 'COMMIT_E', compid);       
        commit;		
        rasdc_library.log(this_form,pformid, 'REF_S', compid);       
        rasdc_library.RefData(PFORMID);           
        rasdc_library.log(this_form,pformid, 'REF_E', compid);       

        compile(pformid , pform , lang ,  message, B10RFORM(1)  , compid);
  
        rasdc_library.log(this_form,pformid, 'SELECT_S', compid);       
        pselect;
        rasdc_library.log(this_form,pformid, 'SELECT_E', compid);       

      rasdc_library.log(this_form,pformid, 'POUTPUT_S', compid);       
    poutput;
      rasdc_library.log(this_form,pformid, 'POUTPUT_E', compid);        
  
  end if;
 rasdc_library.log(this_form,pformid, 'END', compid); 
--</ON_ACTION>
--<POST_ACTION formid="90" blockid="">
--

/*
-- manjka �e:

branje cookia za podalj�anje se�na



*/
--</POST_ACTION>
    pLog;
exception
  when rasd_client.e_finished then pLog;
  when others then
    htp.p('<html>
<head>');  
htpClob(rasd_client.getHtmlJSLibrary('HEAD',''|| rasdc_library.formName(PFORMID, LANG) ||'')); 
htp.p('');
htp.p('<script type="text/javascript">'); 
formgen_js;
htp.p('</script>'); 	
htpClob(FORM_UIHEAD); 
htp.p('<style type="text/css">'); 
htpClob(FORM_CSS); 
htp.p('</style><script type="text/javascript">');  htp.p('</script>'); 
 
htp.prn('</head><body><div id="RASDC2_CSSJS_LAB" class="rasdFormLab">'|| rasd_client.getHtmlHeaderDataTable('RASDC2_CSSJS_LAB',''|| rasdc_library.formName(PFORMID, LANG) ||'') ||'     </div><div class="rasdForm"><div class="rasdFormHead"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton"></div><div class="rasdHtmlError">  <div class="rasdHtmlErrorText"><div class="rasdHtmlErrorText">'||sqlerrm||'('||sqlcode||')</div></div><div class="rasdHtmlErrorTextDetail">');declare   v_trace varchar2(32000) := DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;   v_nl varchar2(2) := chr(10); begin rlog('ERROR:'||v_trace); htp.p ( 'Error trace'||':'||'<br/>'|| replace(v_trace, v_nl ,'<br/>'));htp.p ( '</div><div class="rasdHtmlErrorTextDetail">'||'Error stack'||':'||'<br/>'|| replace(DBMS_UTILITY.FORMAT_ERROR_STACK, v_nl ,'<br/>'));rlog('ERROR:'||DBMS_UTILITY.FORMAT_ERROR_STACK); htp.p('</div>');rlog('ERROR:...'); declare   v_line  number;  v_x varchar2(32000); begin v_x := substr(v_trace,1,instr(v_trace, v_nl )-1 );  v_line := substr(v_x,instr(v_x,' ',-1));for r in  (select line, text from user_source s where s.name = 'RASDC2_CSSJS' and line > v_line-5 and line < v_line+5 ) loop rlog('ERROR:'||r.line||' - '||r.text); end loop;  rlog('ERROR:...'); exception when others then null;end;end;htp.p('</div><div class="rasdFormFooter"><input onclick="history.go(-1);" type="button" value="Back" class="rasdButton">'|| rasd_client.getHtmlFooter(version , substr('RASDC2_CSSJS_FOOTER',1,instr('RASDC2_CSSJS_FOOTER', '_',-1)-1) , '') ||'</div></div></body></html>    ');
    pLog;
end; 
procedure main(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_CSSJS',ACTION);  
  if ACTION = GBUTTONSAVE then     pselect;
    pcommit;
  end if;

--<POST_ACTION formid="90" blockid="">
--

/*
-- manjka �e:

branje cookia za podalj�anje se�na



*/
--</POST_ACTION>
-- Error handler for the main program.
 exception
  when rasd_client.e_finished then null;

end; 
procedure rest(
  name_array  in owa.vc_arr,
  value_array in owa.vc_arr
) is
begin  
  rasd_client.secCheckCredentials(  name_array , value_array ); 

  -- The program execution sequence based on  ACTION defined.
  psubmitrest(name_array ,value_array);
  rasd_client.secCheckPermission('RASDC2_CSSJS',ACTION);  
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMPG := 1;
    RECNUMB10 := 1;
    RECNUMB10G := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then
      RECNUMP := RECNUMP-1;
    else
      RECNUMP := 1;
    end if;
    if RECNUMPG > 1 then
      RECNUMPG := RECNUMPG-1;
    else
      RECNUMPG := 1;
    end if;
    if RECNUMB10 > 1 then
      RECNUMB10 := RECNUMB10-1;
    else
      RECNUMB10 := 1;
    end if;
    if RECNUMB10G > 1 then
      RECNUMB10G := RECNUMB10G-1;
    else
      RECNUMB10G := 1;
    end if;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;
    RECNUMPG := RECNUMPG+1;
    RECNUMB10 := RECNUMB10+1;
    RECNUMB10G := RECNUMB10G+1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSRC or ACTION is null  then     RECNUMP := 1;
    RECNUMPG := 1;
    RECNUMB10 := 1;
    RECNUMB10G := 1;
    pselect;
    poutputrest;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := 'Form is changed.';
    --end if;
    poutputrest;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutputrest;
  end if;

--<POST_ACTION formid="90" blockid="">
--

/*
-- manjka �e:

branje cookia za podalj�anje se�na



*/
--</POST_ACTION>
-- Error handler for the rest program.
 exception
  when rasd_client.e_finished then null;
  when others then
if RESTRESTYPE = 'XML' then
    OWA_UTIL.mime_header('text/xml', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('<?xml version="1.0" encoding="UTF-8"?>
<form name="RASDC2_CSSJS" version="'||version||'">');     htp.p('<error>');     htp.p('  <errorcode>'||sqlcode||'</errorcode>');     htp.p('  <errormessage>'||replace(sqlerrm,'<','&lt;')||'</errormessage>');     htp.p('</error>');     htp.p('</form>'); else
    OWA_UTIL.mime_header('application/json', FALSE,'utf-8');
    OWA_UTIL.http_header_close;	
    htp.p('{"form":{"@name":"RASDC2_CSSJS","@version":"'||version||'",' );     htp.p('"error":{');     htp.p('  "errorcode":"'||sqlcode||'",');     htp.p('  "errormessage":"'||replace(sqlerrm,'"','\"')||'"');     htp.p('}');     htp.p('}}'); end if;

end; 
function metadata_xml return cctab is
  v_clob clob := '';
  v_vc cctab;
  begin
 v_vc(1) := '<form><formid>90</formid><form>RASDC2_CSSJS</form><version>1</version><change>01.11.2023 12/53/25</change><user>RASDDEV</user><label><![CDATA[<%= rasdc_library.formName(PFORMID, LANG) %>]]></label><lobid>RASDDEV</lobid><program>?</program><referenceyn>Y</referenceyn><autodeletehtmlyn>Y</autodeletehtmlyn><autocreaterestyn>Y</autocreaterestyn><autocreatebatchyn>Y</autocreatebatchyn><addmetadatainfoyn>Y</addmetadatainfoyn><compiler><engineid>11</engineid><server>rasd_engine11</server><client>rasd_enginehtml11</client><library>rasd_client</library></compiler><compiledInfo><info><engineid>11</engineid><change>27.03.2023 05/02/54</change><compileyn>Y</compileyn><application>RASD 2.0</application><owner>domen</owner><editor>domen</editor></info></compiledInfo><blocks><block><blockid>B10</blockid><sqltable>RASD_TRIGGERS</sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>Y</rowidyn><pagingyn>Y</pagingyn><clearyn>Y</clearyn><sqltext><![CDATA[            select rowid     rid,
                 formid    formid,
                 blockid   blockid,
                 triggerid,
                 plsql,
                 plsqlspec, rform,
				 ''rasdc_files.showfile?pfile=pict/gumbrisi.jpg'' ldelete
            from RASD_TRIGGERS
           where formid = PFORMID
             and triggerid = substr(PBLOKPROZILECNOV,
                                          instr(PBLOKPROZILECNOV, ''/.../'') + 5)
             and upper(triggerid||'':''||plsql||'':''||plsqlspec) like upper(''%''||ppf||''%'') 
           order by triggerid ]]></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10</blockid><fieldid>BLOCKID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100014</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>Y</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10BLOCKID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>FORMID</fieldid><type>N</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100012</orderby><pkyn';
 v_vc(2) := '>N</pkyn><selectyn>Y</selectyn><insertyn>Y</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10FORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>HIDDENYN</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100024</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10HIDDENYN</nameid><label><![CDATA[Hiddenyn]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>LDELETE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10LDELETE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>PLSQL</fieldid><type>L</type><format></format><element>TEXTAREA_</element><hiddenyn></hiddenyn><orderby>100018</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>Y</insertyn><updateyn>Y</updateyn><deleteyn>Y</deleteyn><insertnnyn>Y</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10PLSQL</nameid><label><![CDATA[<%= showLabel(''Code'','''',0) %><div id=PLSQLCOUNT>�</div>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>PLSQLSPEC</fieldid><type>L</type><format></format><element>TEXTAREA_</element><hiddenyn></hiddenyn><orderby>100020</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>Y</insertyn><updateyn>Y</updateyn><';
 v_vc(3) := 'deleteyn>N</deleteyn><insertnnyn>Y</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10PLSQLSPEC</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10</blockid><fieldid>RBLOCKID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100030</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10RBLOCKID</nameid><label><![CDATA[Rblockid]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100028</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>Y</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10RFORM</nameid><label><![CDATA[Rform]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RID</fieldid><type>R</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>101</orderby><pkyn>Y</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RLOBID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100026</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><name';
 v_vc(4) := 'id>B10RLOBID</nameid><label><![CDATA[Rlobid]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>RS</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>100</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10RS</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>SOURCE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100022</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10SOURCE</nameid><label><![CDATA[Source]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10</blockid><fieldid>TRIGGERID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>100016</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>Y</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10TRIGGERID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B10G</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>Y</rowidyn><pagingyn>Y</pagingyn><clearyn>Y</clearyn><sqltext></sqltext><label></label><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B10G</blockid><fieldid>BLOBVC</fieldid><type>L</type><format></format><element>TEXTAREA_</element><hiddenyn></hiddenyn><orderby>1003</orderby><pkyn>N</pkyn><selectyn>Y</sele';
 v_vc(5) := 'ctyn><insertyn>N</insertyn><updateyn>Y</updateyn><deleteyn>Y</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10GBLOBVC</nameid><label><![CDATA[<%= showLabel(''Code'','''',0) %><div id=PLSQLCOUNT>�</div>]]></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>B10G</blockid><fieldid>NAME</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>1004</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>B10GNAME</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10G</blockid><fieldid>RID</fieldid><type>R</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>101</orderby><pkyn>Y</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10GRID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B10G</blockid><fieldid>RS</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>100</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B10GRS</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B20</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label><![CDATA[External links]]></label><source>V</source><hiddenyn></hiddenyn><r';
 v_vc(6) := 'lobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B20</blockid><fieldid>EDITOR</fieldid><type>C</type><format></format><element>A_</element><hiddenyn></hiddenyn><orderby>1010</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''External HTML editor'']]></defaultval><elementyn>Y</elementyn><nameid>B20EDITOR</nameid><label></label><linkid>link$editor</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B20</blockid><fieldid>REGEX</fieldid><type>C</type><format></format><element>A_</element><hiddenyn></hiddenyn><orderby>1020</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''External RegEX editor'']]></defaultval><elementyn>Y</elementyn><nameid>B20REGEX</nameid><label></label><linkid>link$regex</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>B20</blockid><fieldid>TEMPLATE</fieldid><type>C</type><format></format><element>A_</element><hiddenyn></hiddenyn><orderby>1000</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''RASD CSS,HTML,JS template'']]></defaultval><elementyn>Y</elementyn><nameid>B20TEMPLATE</nameid><label></label><linkid>link$template</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>B30</blockid><sqltable></sqltable><numrows>0</numrows><emptyrows></emptyrows><dbblockyn>Y</dbblockyn><rowidyn>N</rowidyn><pagingyn>N</pagingyn><clearyn>N</clearyn><sqltext><![CDATA[          select ''<A HREF="javascript:var x = window.open(encodeURI(''''!RASDC_ERRORS.Program?PPROGRAM='' ||
                 upper(name) || ''#'' || substr(type, 1, 1) ||
                 substr(type, instr(type, '' '') + 1, 1) || to_char(line) ||
                 ''''''),''''nx'''','''''''');"  style="color: Red;">ERR: ('' ||
         ';
 v_vc(7) := '        to_char(line) || '','' || to_char(position) || '')  '' || text ||
                 ''</A>'' text
            from all_errors
           where upper(name) = upper(PFORM)
             and owner = rasdc_library.currentDADUser
           order by line, position]]></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Errors'',LANG)%>]]></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>B30</blockid><fieldid>TEXT</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>10000</orderby><pkyn>N</pkyn><selectyn>Y</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>B30TEXT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field></fields></block><block><blockid>P</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>Y</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Form code (javascript, CSS)'',LANG)%>]]></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>P</blockid><fieldid>BLOKPROZILECNOV</fieldid><type>C</type><format></format><element>SELECT_</element><hiddenyn></hiddenyn><orderby>10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PBLOKPROZILECNOV</nameid><label></label><linkid>lov$PBLOKPROZILECNOV_LOV</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>GBTNSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>240</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsave]]></defaultval><elementyn>Y</elementyn><nameid>PGB';
 v_vc(8) := 'TNSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>GBTNSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>120</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsrc]]></defaultval><elementyn>Y</elementyn><nameid>PGBTNSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>HINT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>310</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PHINT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>LDELETE</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>250</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PLDELETE</nameid><label></label><linkid>link$deletelink</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>P</blockid><fieldid>PF</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>110</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PF</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</in';
 v_vc(9) := 'cludevis></field><field><blockid>P</blockid><fieldid>SESSSTORAGEENABLED</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>400</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>SESSSTORAGEENABLED</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>TEXTTYPE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>500</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PTEXTTYPE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>P</blockid><fieldid>UPLOAD</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>260</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PUPLOAD</nameid><label></label><linkid>link$uploadcust</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field></fields></block><block><blockid>PG</blockid><sqltable></sqltable><numrows>1</numrows><emptyrows></emptyrows><dbblockyn>N</dbblockyn><rowidyn>N</rowidyn><pagingyn>Y</pagingyn><clearyn>N</clearyn><sqltext></sqltext><label><![CDATA[<%= RASDI_TRNSLT.text(''Global code (javascript, CSS)'',LANG)%>]]></label><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid><fields><field><blockid>PG</blockid><fieldid>BLOKPROZILECNOV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>11</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnn';
 v_vc(10) := 'yn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PGBLOKPROZILECNOV</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>PG</blockid><fieldid>GBTNSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>240</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsave]]></defaultval><elementyn>Y</elementyn><nameid>PGGBTNSAVE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>PG</blockid><fieldid>GBTNSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>120</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[gbuttonsrc]]></defaultval><elementyn>Y</elementyn><nameid>PGGBTNSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>PG</blockid><fieldid>HINT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>310</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PGHINT</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>PG</blockid><fieldid>PF</fieldid><type>C</type><format></format><element>INPUT_TEXT</element><hiddenyn></hiddenyn><orderby>110</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PGPF</nameid><';
 v_vc(11) := 'label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field><field><blockid>PG</blockid><fieldid>SESSSTORAGEENABLED</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>400</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>SESSSTORAGEENABLED</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>PG</blockid><fieldid>TEXTTYPE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>500</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PGTEXTTYPE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid>PG</blockid><fieldid>UPLOAD</fieldid><type>C</type><format></format><element>IMG_</element><hiddenyn></hiddenyn><orderby>260</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PGUPLOAD</nameid><label></label><linkid>link$uploadgeneral</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>Y</includevis></field></fields></block></blocks><fields><field><blockid></blockid><fieldid>ACTION</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ACTION</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblo';
 v_vc(12) := 'ckid></rblockid><rfieldid>ACTION</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>COMPID</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>COMPID</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>DOCPRIVSOK</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>DOCPRIVSOK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>ERROR</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>50</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>ERROR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>ERROR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBTNNAVIG</fieldid><type>C</type><format></format><element>INPUT_BUTTON</element><hiddenyn></hiddenyn><orderby>8</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Form navigator'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBTNNAVIG</nameid><label></label><linkid>link$formnavlink</linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></bl';
 v_vc(13) := 'ockid><fieldid>GBUTTONBCK</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONBCK'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONBCK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCLR</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONCLR'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONCLR</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCLR</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>13</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Compile'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONCOMPILE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONCOMPILE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONDELETE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Delete'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONDELETE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rf';
 v_vc(14) := 'ieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONFWD</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[''GBUTTONFWD'']]></defaultval><elementyn>N</elementyn><nameid>GBUTTONFWD</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>GBUTTONPREV</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>15</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Preview'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONPREV</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONPREV</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONRES</fieldid><type>C</type><format></format><element>INPUT_RESET</element><hiddenyn></hiddenyn><orderby>14</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Reset'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONRES</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONRES</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>12</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Save'',LANG)]]></defaultval><elementyn>Y</elementyn><nameid>GBUTTONSAVE</nameid><label></label><linkid></linkid><source>V</source>';
 v_vc(15) := '<rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSAVE</rfieldid><includevis>Y</includevis></field><field><blockid></blockid><fieldid>GBUTTONSRC</fieldid><type>C</type><format></format><element>INPUT_SUBMIT</element><hiddenyn></hiddenyn><orderby>9</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[RASDI_TRNSLT.text(''Search'',LANG)]]></defaultval><elementyn>N</elementyn><nameid>GBUTTONSRC</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>GBUTTONSRC</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>G_TABLE</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>G_TABLE</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>HINTCONTENT</fieldid><type>C</type><format></format><element>PLSQL_</element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>HINTCONTENT</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>HINTCONTENT</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>LANG</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>5</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>LANG</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RAS';
 v_vc(16) := 'DC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>LANG</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>MESSAGE</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>51</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>MESSAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>MESSAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PAGE</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>0</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[0]]></defaultval><elementyn>Y</elementyn><nameid>PAGE</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PAGE</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORM</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>7</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PFORM</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORM</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PFORMID</fieldid><type>C</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>6</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>PFORMID</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>PFORMID</rfieldid><includevis>N<';
 v_vc(17) := '/includevis></field><field><blockid></blockid><fieldid>PGOPTIONS</fieldid><type>L</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PGOPTIONS</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>PMIRROR</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>PMIRROR</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-10</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMB10G</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMB10G</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMP</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></h';
 v_vc(18) := 'iddenyn><orderby>-2</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMP</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>RECNUMPG</fieldid><type>N</type><format></format><element>INPUT_HIDDEN</element><hiddenyn></hiddenyn><orderby>-3</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval><![CDATA[1]]></defaultval><elementyn>Y</elementyn><nameid>RECNUMPG</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>UNLINK</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>290875</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>UNLINK</nameid><label></label><linkid></linkid><source>V</source><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VLOB</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>81</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VLOB</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VLOB</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>VUSER</fieldid><type>C</type><format></format><element></element><hiddenyn></hiddenyn><orderby>80</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><no';
 v_vc(19) := 'tnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>N</elementyn><nameid>VUSER</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>VUSER</rfieldid><includevis>N</includevis></field><field><blockid></blockid><fieldid>WARNING</fieldid><type>C</type><format></format><element>FONT_</element><hiddenyn></hiddenyn><orderby>52</orderby><pkyn>N</pkyn><selectyn>N</selectyn><insertyn>N</insertyn><updateyn>N</updateyn><deleteyn>N</deleteyn><insertnnyn>N</insertnnyn><notnullyn>N</notnullyn><lockyn>N</lockyn><defaultval></defaultval><elementyn>Y</elementyn><nameid>WARNING</nameid><label></label><linkid></linkid><source>V</source><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid><rfieldid>WARNING</rfieldid><includevis>N</includevis></field></fields><links><link><linkid>link$CHKBXD</linkid><link>CHKBXD</link><type>C</type><location></location><text></text><source>G</source><hiddenyn>N</hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>FALSE</paramid><type>FALSE</type><orderby></orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>N</code><value><![CDATA[N]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>TRUE</paramid><type>TRUE</type><orderby>1</orderby><blockid></blockid><fieldid></fieldid><namecid></namecid><code>Y</code><value><![CDATA[Y]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$deletelink</linkid><link>DELETELINK</link><type>F</type><location><![CDATA[I]]></location><text></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>#JSLINK#16</paramid><type>OUT</type><orderby>16</orderby><blockid></blockid><fieldid>#JSLINK#</fieldid><namecid></namecid><code></code><value><![CDATA[if (confirm(''<%= GBUTTONDELETE%>?'') == true) { window.rasd_CMEditorB10PLSQL_1_RASD.setValue(''''); document.RASDC2_CSSJS.ACTION.value=''Save''; document.RASDC2_CSSJS.submit();}]]></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$editor</linkid><link>EDITOR</link><type>U</type><location><![CDATA[N]]></location><text><![CDATA[https://htmlg.com/html-editor/]]></text><source>V</source><hiddenyn></hiddenyn><rl';
 v_vc(20) := 'obid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>link$formnavlink</linkid><link>FORMNAVIGLINK</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!RASDC2_EXECUTION.webclient]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params><param><paramid>LANG2</paramid><type>OUT</type><orderby>2</orderby><blockid></blockid><fieldid>LANG</fieldid><namecid>LANG</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param><param><paramid>PFORMID3</paramid><type>OUT</type><orderby>3</orderby><blockid></blockid><fieldid>PFORMID</fieldid><namecid>PFORMID</namecid><code></code><value></value><rlobid></rlobid><rform></rform><rlinkid></rlinkid></param></params></link><link><linkid>link$regex</linkid><link>REGEX</link><type>U</type><location><![CDATA[N]]></location><text><![CDATA[https://regex101.com/]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>link$template</linkid><link>TEMPLATE</link><type>U</type><location><![CDATA[N]]></location><text><![CDATA[https://jsbin.com/woqokaquju/edit?html,css,js,output]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>link$uploadcust</linkid><link>UPLOADCUST</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!DOCUMENTS_API2.page?showfiles=true]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>link$uploadgeneral</linkid><link>UPLOADGENERAL</link><type>F</type><location><![CDATA[N]]></location><text><![CDATA[!RASDC_FILES.page]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link><link><linkid>lov$PBLOKPROZILECNOV_LOV</linkid><link>PBLOKPROZILECNOV_LOV</link><type>S</type><location></location><text><![CDATA[select ''/.../FORM_CSS'' id, ''FORM_CSS'' label, 1 from dual union
select ''/.../FORM_JS'' id, ''FORM_JS'' label, 1 from dual union
select ''/.../FORM_UIHEAD'' id, ''FORM_UIHEAD'', 1 label from dual union
select ''/.../''||triggerid , triggerid|| decode(t.rform,null,'''',''-R'') label , 2 vr
  from rasd_triggers t
 where t.formid = pformid
   and (triggerid like ''FORM_CSS_';
 v_vc(21) := 'REF%'' or triggerid like ''FORM_JS_REF%'' or
       triggerid like ''FORM_UIHEAD_REF%'')
 order by 1, 2]]></text><source>V</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rlinkid></rlinkid><params></params></link></links><pages><pagedata><page>0</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B10</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B10G</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid>B10G</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B20</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B20</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid>B20</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>B30</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>B30</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>99</page><blockid>P</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid>PG</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid>PG</blockid><fieldid></fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>99</page><blockid>PG</blockid><fieldid></fieldid><';
 v_vc(22) := 'rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>COMPID</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBTNNEW</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>ERROR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBTNNAVIG</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONBCK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata>';
 v_vc(23) := '<pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONCLR</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONCOMPILE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONDELETE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONDELETE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONDELETE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONFWD</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONPREV</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</pa';
 v_vc(24) := 'ge><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONRES</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONSAVE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>GBUTTONSRC</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>G_TABLE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>G_TABLE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>HINTCONTENT</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>MESSAGE</fieldid>';
 v_vc(25) := '<rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>MESSAGE</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>PFORM</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>UNLINK</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>1</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>2</page><blockid></blockid><fieldid>WARNING</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata><pagedata><page>0</page><blockid></blockid><fieldid>PF</fieldid><rlobid></rlobid><rform></rform><rblockid></rblockid><rfieldid></rfieldid></pagedata></pages><triggers><trigger><blockid></blockid><triggerid>DocumentTable</triggerid><plsql><![CDATA[function getDocumentTable return varchar2 is
  v_table varchar2(100);
begin

  if instr(owa_util.get_cgi_env(''DOCUMENT_TABLE''),''.'') = 0 then 
      v_table := owa_util.get_cgi_env(''REMOTE_USER'')||''.''||owa_util.get_cgi_env(''DOCUMENT_TABLE''); 
    else v_table:=owa_util.get_cgi_env(''DOCUMENT_TABLE'');         
  end if;
  
  -- Set location of your DOCUMENT_TABLE like set in your LOB RASD_CLIENT.c_HtmlDocumentTable
  --  v_table := ''your_app_schema.documents'';
  -- Uncomment and change upper row.	
  if v_table is not null ';
 v_vc(26) := 'then
 
  begin   
  select distinct owner||''.''||table_name into v_table
  from table_privileges where owner||''.''||table_name = upper(v_table);  

  exception when others then
    raise_application_error (''-20000'',''In RASDC2_CSSJS function getDocumentTable set DOCUMENT TABLE or set priviliges on it.'');
  end;  
  
  else

    raise_application_error (''-20000'',''In RASDC2_CSSJS function getDocumentTable set DOCUMENT TABLE.'');
  end if;
  
  return v_table;

end;
]]></plsql><plsqlspec><![CDATA[function getDocumentTable return varchar2;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_CSS</triggerid><plsql><![CDATA[.rasdTxPTRIGGERIDPROC INPUT{
   width: 300px;
}  

.rasdTxPF INPUT{
   width: 300px;
}  


#tabs {
height: 650px;
}
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS</triggerid><plsql><![CDATA[$(function() {

//  addSpinner();
//   initRowStatus();
//   transformVerticalTable("B15_TABLE", 4 );
   setShowHideDiv("B30_DIV", true);
//   CheckFieldValue(pid , pname)
//   CheckFieldMandatory(pid , pname)
  
  
HighLightBack("referenceBlock", "#aaccf7");

  
  
$(".rasdTxPTRIGGERIDPROC INPUT").attr("maxlength", 30);
 
  
 });



$(function() {

  
  rasd_codemirror_<%=PMIRROR%>();
   
  window.rasd_CMEditor<%=PMIRROR%>.setSize("1400","450");
  //32768 max each spec char un UTF8 has 2byts but in editor counts 1byte
  window.rasd_CMEditor<%=PMIRROR%>.setOption("maxLength", 32000); //32000 is limit of vc_arr type in plsql for length of value  of one element

  window.rasd_CMEditor<%=PMIRROR%>.on("beforeChange", function (cm, change) {
    var maxLength = cm.getOption("maxLength");
    var isChrome =  window.chrome;
    if (maxLength && change.update) {
        var str = change.text.join("\n");
        var delta = str.length-(cm.indexFromPos(change.to) - cm.indexFromPos(change.from));
        var aaa = cm.doc.getValue();
        if (delta <= 0) { return true; }
        xval = cm.getValue()
        if(isChrome){
          xval = xval.replace(/(\r\n|\n|\r)/g,"  ");
        }          
        delta = xval.length+delta-maxLength;
        docume';
 v_vc(27) := 'nt.getElementById("PLSQLCOUNT").innerHTML = "<%= RASDI_TRNSLT.text(''Characters left:'', lang) %> " + ((delta*-1)+1) ;
        if (delta > 0) {
            str = str.substr(0, str.length-delta);
            change.update(change.from, change.to, str.split("\n"));
        }
    }
    return true;  
  });  

//// Tabulators  
$(function() {    
  $( "#tabs" ).tabs({active:''||(page-1)||''});
  $( "#tabs ul" ).css("display", "block");
});  
  

  
  
});

]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_JS_REF(82)</triggerid><plsql><![CDATA[$(function() {

  addSpinner();
  
});

$(function() {

  $(".rasdFormMenu").html("<%= RASDC_LIBRARY.showMeni(THIS_FORM, PFORMID, null, lang) %>");   

  
  $(document).ready(function () {
   $(".dialog").dialog({ autoOpen: false }); 
  });
  
  
});
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>FORM_UIHEAD</triggerid><plsql><![CDATA[<script type="text/javascript" src="RASDC2_CSSJS.codemirrorjs?n=<%=pmirror%>&h=150&d=<%=PSESSSTORAGEENABLED(1)%>&f=<%=pformid%>&m=<%=PTEXTTYPE(1)%>"></script>
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>ON_ACTION</triggerid><plsql><![CDATA[  rasdc_library.log(this_form,pformid, ''START'', compid);
	
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission(this_form,ACTION);  
  
  RASDC_LIBRARY.checkprivileges(PFORMID);
 
  if ACTION is null then null;
    RECNUMB10 := 1;
    RECNUMP := 1;
    pselect;
    poutput;
   elsif ACTION = GBUTTONSRC then     
    RECNUMP := 1;
    RECNUMB10 := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     
        pcommit;
        rasdc_library.RefData(PFORMID);   
                       
        pselect;
        message := ''Changes are saved.'';
        
        if b10rform is not null then
           message :=  message ||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';
     ';
 v_vc(28) := '   end if; 
		
	    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
	
  elsif ACTION = GBUTTONCOMPILE then      

        rasdc_library.log(this_form,pformid, ''COMMIT_S'', compid);       
        pcommit;
        rasdc_library.log(this_form,pformid, ''COMMIT_E'', compid);       
        commit;		
        rasdc_library.log(this_form,pformid, ''REF_S'', compid);       
        rasdc_library.RefData(PFORMID);           
        rasdc_library.log(this_form,pformid, ''REF_E'', compid);       

        compile(pformid , pform , lang ,  message, B10RFORM  , compid);
  
        rasdc_library.log(this_form,pformid, ''SELECT_S'', compid);       
        pselect;
        rasdc_library.log(this_form,pformid, ''SELECT_E'', compid);       

      rasdc_library.log(this_form,pformid, ''POUTPUT_S'', compid);       
    poutput;
      rasdc_library.log(this_form,pformid, ''POUTPUT_E'', compid);        
  
  end if;
 rasdc_library.log(this_form,pformid, ''END'', compid); 
]]></plsql><plsqlspec><![CDATA[  -- The program execution sequence based on  ACTION defined.
  psubmit(name_array ,value_array);
  rasd_client.secCheckPermission(''RASDC2_CSSJS'',ACTION);  
  if ACTION is null then null;
    RECNUMP := 1;
    RECNUMPG := 1;
    RECNUMB10 := 1;
    RECNUMB10G := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONBCK then     if RECNUMP > 1 then
      RECNUMP := RECNUMP-1;
    else
      RECNUMP := 1;
    end if;
    if RECNUMPG > 1 then
      RECNUMPG := RECNUMPG-1;
    else
      RECNUMPG := 1;
    end if;
    if RECNUMB10 > 1 then
      RECNUMB10 := RECNUMB10-1;
    else
      RECNUMB10 := 1;
    end if;
    if RECNUMB10G > 1 then
      RECNUMB10G := RECNUMB10G-1;
    else
      RECNUMB10G := 1;
    end if;
    pselect;
    poutput;
  elsif ACTION = GBUTTONFWD then     RECNUMP := RECNUMP+1;
    RECNUMPG := RECNUMPG+1;
    RECNUMB10 := RECNUMB10+1;
    RECNUMB10G := RECNUMB10G+1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSRC then     RECNUMP := 1;
    RECNUMPG := 1;
    RECNUMB10 := 1;
    RECNUMB10G := 1;
    pselect;
    poutput;
  elsif ACTION = GBUTTONSAVE then     pcommit;
    pselect;
    --if MESSAGE is null then
    --MESSAGE := ''Form is changed.'';
    --end if;
    poutput;
  elsif ACTION = GBUTTONCLR then     pclear;
    poutput;
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rbl';
 v_vc(29) := 'ockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>ON_DELETE</triggerid><plsql><![CDATA[if substr(B10RS(i__),1,1) = ''U'' then

delete RASD_TRIGGERS where ROWID = B10RID and rform is null;

end if;			
]]></plsql><plsqlspec><![CDATA[-- Generated DELETE statement. Use (i__) to access fields values.
if substr(B10RS(i__),1,1) = ''U'' then
delete RASD_TRIGGERS
where ROWID = B10RID(i__);
 MESSAGE := ''Data is changed.'';
end if;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10G</blockid><triggerid>ON_DELETE</triggerid><plsql><![CDATA[null;
]]></plsql><plsqlspec><![CDATA[-- Generated DELETE statement. Use (i__) to access fields values.
if substr(B10GRS(i__),1,1) = ''U'' then
delete 
where ROWID = B10GRID(i__);
 MESSAGE := ''Data is changed.'';
end if;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>ON_INSERT</triggerid><plsql><![CDATA[declare
      v_def clob;
      v_code clob;
begin

v_def :=  trim(replace(replace(rasd_engine11.createtriggertemplateplsql(pformid, substr(PBLOKPROZILECNOV, instr(PBLOKPROZILECNOV, ''/.../'') + 5) , ''''),'' '',''''),''
'',''X''));
v_code := trim(replace(replace(replace(B10plsql,'' '',''''),chr(10),''X''),chr(13),''''));

           
            if nvl(v_code,''XX:#:XX'') <>  nvl(v_def,''XX:#:XX'')
              then
            B10formid := PFORMID;

              B10blockid := null;
              B10triggerid := substr(PBLOKPROZILECNOV,
                                          instr(PBLOKPROZILECNOV, ''/.../'') + 5);
            insert into RASD_TRIGGERS
              (formid, blockid, triggerid, plsql, plsqlspec)
            values
              (B10formid ,
               B10blockid,
               B10triggerid,
               B10plsql,
               B10plsqlspec
			  );

            null;
            end if;
end;
]]></plsql><plsqlspec><![CDATA[-- Generated INSERT statement. Use (i__) to access fields values.
if substr(B10RS(i__),1,1) = ''I'' then
insert into RASD_TRIGGERS (
  FORMID
 ,BLOCKID
 ,TRIGGERID
 ,PLSQL
 ,PLSQLSPEC
) values (
  B10FORMID(i__)
 ,B10BLOCKID(i__)
 ,B10TRIGGERID(i__)
 ,B10PLSQL(i__)
 ,B10PLSQLSPEC(i__)
);
 MESSAGE := ''Data is changed.'';
end if;
]]></plsqlspec><source></source><hiddenyn></hiddenyn>';
 v_vc(30) := '<rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10G</blockid><triggerid>ON_SELECT</triggerid><plsql><![CDATA[OPEN c__ FOR
          ''select rowid rid,
                  ''||g_table||''_API.Blob2Clob(blob_content) BLOBVC
                 --UTL_RAW.CAST_TO_VARCHAR2(DBMS_LOB.SUBSTR(blob_content,
                   --                                       32767,
                     --                                     1)) BLOBVC
				  ,name
           from ''||g_table||''
           where upper(name) = upper(''''''||PGBLOKPROZILECNOV(1)||'''''')
           order by name'';
]]></plsql><plsqlspec><![CDATA[-- Generated SELECT code. Use (i__) to access fields values.
OPEN c__ FOR 
--<SQL formid="90" blockid="B10G">
SELECT 
 ROWID RID,
BLOBVC,
NAME FROM  
--</SQL>
;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>GBUTTONPREV</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p( ''<input type=button class="rasdButton" value="'' ||
                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||
                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,
                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',
                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,
                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||
                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
									 ,
                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
                                )
			       ) || ''>''
);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid>GBUTTONPREV</rblockid></trigger><trigger><blockid>HINTCONTENT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[htp.p('''');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid>HINTCONTENT</rblockid></trigger><trigger><blockid>PGBLOKPROZILECNOV</blockid><trigger';
 v_vc(31) := 'id>ON_UI</triggerid><plsql><![CDATA[htp.p(''<select name="PGBLOKPROZILECNOV_1" id="PGBLOKPROZILECNOV_X_1_RASD" class="rasdSelect " onchange="document.getElementById(''''ACTION_RASD'''').value=''''''||GBUTTONSRC||'''''';  document.''||this_form||''.submit() ;">'');
htp.p(pgoptions); --created in pre select B10g.
htp.p(''</select>'');
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>PGHINT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[rasdc_hints.link(replace(this_form,''2'',''''), lang);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>PHINT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[rasdc_hints.link(replace(this_form,''2'',''''), lang);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>P_COPYHINT</blockid><triggerid>ON_UI</triggerid><plsql><![CDATA[rasdc_hints.link(replace(this_form,''2'',''''), lang);
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>ON_UPDATE</triggerid><plsql><![CDATA[if substr(B10RS,1,1) = ''U'' then
declare
              vtig_first pls_integer;
            begin

              select count(1)
                into vtig_first
                from rasd_triggers_code_types c, rasd_triggers s
               where c.tctype = s.triggerid
                 and c.language = ''P''
                 and s.rowid = B10RID;
                                  
              if vtig_first = 0 then

                update RASD_TRIGGERS s
                   set plsql = B10plsql
                   ,plsqlspec =  B10plsqlspec
,                   rform  = decode (unlink,''Y'',null,rform)
                 where ROWID = B10RID and (rform is null or unlink = ''Y'');
              else

                update RASD_TRIGGERS s
                   set plsql = B10plsql, plsqlspec = B10plsqlspec
,                   rform  = decode (unlink,''Y'',null,rform)
                 where ROWID = B10RID and (rform is null or unlink = ''Y'');

              end if;

            end;
end if;			';
 v_vc(32) := '
]]></plsql><plsqlspec><![CDATA[-- Generated UPDATE statement. Use (i__) to access fields values.
if substr(B10RS(i__),1,1) = ''U'' then
update RASD_TRIGGERS set
  PLSQL = B10PLSQL(i__)
 ,PLSQLSPEC = B10PLSQLSPEC(i__)
 ,RFORM = B10RFORM(i__)
where ROWID = B10RID(i__);
 MESSAGE := ''Data is changed.'';
end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10G</blockid><triggerid>ON_UPDATE</triggerid><plsql><![CDATA[if substr(B10GRS,1,1) = ''U'' then

         execute immediate
          ''update ''||g_table||''
             set BLOB_CONTENT = ''||g_table||''_API.Clob2Blob(''''''||replace(B10GBLOBVC,'''''''','''''''''''')||'''''')
          where ROWID = ''''''||b10Grid(i__)||'''''''';


end if;			
]]></plsql><plsqlspec><![CDATA[-- Generated UPDATE statement. Use (i__) to access fields values.
if substr(B10GRS(i__),1,1) = ''U'' then
update  set
  BLOBVC = B10GBLOBVC(i__)
where ROWID = B10GRID(i__);
 MESSAGE := ''Data is changed.'';
end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_ACTION</triggerid><plsql><![CDATA[--

/*
-- manjka �e:

branje cookia za podalj�anje se�na



*/
]]></plsql><plsqlspec><![CDATA[  -- The execution after default execution based on  ACTION.
  -- Delete this code when you have new actions and add your own.
  if  nvl(ACTION,GBUTTONSRC) not in (  GBUTTONBCK, GBUTTONFWD, GBUTTONSRC, GBUTTONSAVE, GBUTTONCLR ) then 
    raise_application_error(''-20000'', ''ACTION="''||ACTION||''" is not defined. Define it in POST_ACTION trigger.'');
  end if;

]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_COMMIT</triggerid><plsql><![CDATA[update RASD_FORMS set change = sysdate where formid = PFORMID;

]]></plsql><plsqlspec><![CDATA[-- Triggers after executing DML operations on DB blocks.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[if B10RFORM is not null then

B10PLSQL#SET.custom := ''class="referenceBlock"'';
B10PLSQLSPEC#SET.custom := ''class="referenceBlock"'';

end if;



]]></plsql><plsqlspec><![CDATA[-- T';
 v_vc(33) := 'riggers after executing SQL. Use (i__) to access fields values.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SELECT</triggerid><plsql><![CDATA[PBLOKPROZILECNOV#SET.custom := ''onchange="document.getElementById(''''ACTION_RASD'''').value=''''''||GBUTTONSRC||'''''';  document.''||this_form||''.submit() ;"'';		
PPF#SET.custom      := ''onchange="document.getElementById(''''ACTION_RASD'''').value=''''''||GBUTTONSRC||'''''';  document.''||this_form||''.submit() ;"'';
PLDELETE := B10LDELETE;

PGUPLOAD := ''rasdc_files.showfile?pfile=pict/gumbupload.jpg'';
PGUPLOAD#SET.custom := ''title="''||RASDI_TRNSLT.text(''Customize your RASD enviorment'',lang)||''"'';
PUPLOAD := ''rasdc_files.showfile?pfile=pict/gumbupload.jpg'';
PUPLOAD#SET.custom := ''title="''||RASDI_TRNSLT.text(''Upload new custom images for projects'',lang)||''"'';

if instr(PBLOKPROZILECNOV,''_JS'') > 0 or instr(upper(PGBLOKPROZILECNOV),''.JS'') > 0 then
  PTEXTTYPE := ''JS'';
elsif instr(PBLOKPROZILECNOV,''_CSS'') > 0 or instr(upper(PGBLOKPROZILECNOV),''.CSS'') > 0 then
  PTEXTTYPE := ''CSS'';
elsif instr(PBLOKPROZILECNOV,''_UIHEAD'') > 0 or instr(upper(PGBLOKPROZILECNOV),''.HTML'') > 0 then
  PTEXTTYPE := ''HTML'';
end if;  

if page = 1 then 
PMIRROR := ''B10PLSQL_1_RASD'';
elsif page = 2 then
PMIRROR := ''B10GBLOBVC_1_RASD'';
end if;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>POST_SUBMIT</triggerid><plsql><![CDATA[----put procedure in the begining of trigger;
post_submit_template;

      if action is null then
        action := GBUTTONSRC;
      end if;

      if nvl(page,0) = 0 then page := 1; end if;

if page = 1 then


PBLOKPROZILECNOV#SET.visible := true;
if PBLOKPROZILECNOV is null then PBLOKPROZILECNOV := ''/.../FORM_CSS''; end if;

if B10PLSQL is null then
B10PLSQL := trim(rasd_engine11.createtriggertemplateplsql(pformid, substr(PBLOKPROZILECNOV, instr(PBLOKPROZILECNOV, ''/.../'') + 5) , ''''));
end if;

elsif page = 2 then

  g_table := getDocumentTable;
  
end if;

if rasdc_library.allowEditing(pformid) then
   GBUTTONSAVE#SET.visible := true;
   GBUTTONCOMPILE#SET.visible := true;
   PGBTNSAVE#SET.visible := true;
   PGGBTNSAVE#SET.visible := true;
else
 ';
 v_vc(34) := '  GBUTTONSAVE#SET.visible := false;
   GBUTTONCOMPILE#SET.visible := false;
   PGBTNSAVE#SET.visible := false;   
   PGGBTNSAVE#SET.visible := false;
end if;

begin
  DOCPRIVSOK := getDocumentTable;
exception when others then
  DOCPRIVSOK := ''N'';
end;  
]]></plsql><plsqlspec><![CDATA[-- Executing after filling fields on submit.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10</blockid><triggerid>POST_UI</triggerid><plsql><![CDATA[if page = 1 then
htp.p(''<div class="label">'');
htp.p(RASDI_TRNSLT.text(''Press CTRL+SPACE to open variable list, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, ALT+S save, F11 to maximize window. (fold string is //// )'',lang));
htp.p(''</div>'');
end if;
]]></plsql><plsqlspec><![CDATA[-- Triggers before afther BLOCK content.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10G</blockid><triggerid>POST_UI</triggerid><plsql><![CDATA[if page = 2 then
htp.p(''<div class="label">'');
htp.p(RASDI_TRNSLT.text(''Press CTRL+SPACE to open variable list, CTRL+F to search, CTRL-I fold open, CTRL-Y fold close,  ALT+G jump to line, ALT+S save, F11 to maximize window. (fold string is //// )'',lang));
htp.p(''</div>'');
end if;


if instr ( nvl(rasd_client.secSuperUsers,'' ''), rasd_client.secGetUsername ) > 0 and DOCPRIVSOK <> ''N'' then
htp.p(''</div></div>'');
end if;
]]></plsql><plsqlspec><![CDATA[-- Triggers before afther BLOCK content.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>B10G</blockid><triggerid>PRE_SELECT</triggerid><plsql><![CDATA[      pgoptions := '''';
      declare
        TYPE ctype__ is REF CURSOR;
         c__ ctype__;
         v_id varchar2(300);
         v_label  varchar2(300);
      begin
      OPEN c__ FOR 
                  ''select name id, name label
                    from ''||g_table||''
                   where upper(name) like ''''%RASD%.HTML''''
                     or upper(name) like ''''%RASD%.JS''''
                     or upper(name) like ''''%RASD%.CSS''''  
                   order by decode(instr(name,''''.html''''),0,decode(instr(name,''''.css''''),0,decode(instr(name,''''.js''''),0, 5 ,1),2),3), name                ';
 v_vc(35) := '   
                   '';
      loop
        FETCH c__
            INTO v_id, v_label;
          exit when c__%notfound;
        pgoptions := pgoptions||''<option class=selectp '';
		if PGBLOKPROZILECNOV(1) = v_id then 
		    pgoptions := pgoptions||'' selected ''; 
	    elsif PGBLOKPROZILECNOV(1) is null then
		    pgoptions := pgoptions||'' selected ''; 	
			PGBLOKPROZILECNOV(1) := v_id;
	    end if;
       pgoptions := pgoptions||'' value="'' || v_id || ''">'' || v_label || ''</option>'';
      end loop;
      end;
]]></plsql><plsqlspec><![CDATA[-- Triggers before executing SQL.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid>P</blockid><triggerid>PRE_UI</triggerid><plsql><![CDATA[if instr ( nvl(rasd_client.secSuperUsers,'' ''), rasd_client.secGetUsername ) > 0 and DOCPRIVSOK <> ''N'' then
htp.p(''<div id="tabs">'');

htp.p( myTabs(page, lang, pformid) );

htp.p(''<div id="idselected">
<script>
$(function() {    
  $( "#tabs" ).tabs({active:''||(page-1)||''});
});   
</script>   
	  '');


end if;	  
]]></plsql><plsqlspec><![CDATA[-- Triggers before showing BLOCK content.
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>_ changes</triggerid><plsql><![CDATA[  function changes(p_log out varchar2) return varchar2 is
  begin

    p_log := ''/* Change LOG:
20230301 - Created new 2 version	
20200410 - Added new compilation message     
20181129 - Added character counter    
20180917 - Added order by in drop down lists    
20180608 - Default code for JS, ...    
20180423 - Added option to upload file for RASD engine - do not use these in build forms - applications     
20170119 - Added new trigger FORM_UIHEAD - for customizing form element <HEAD>
20160704 - Added button Preview    
20160629 - Added log function for RASD.      
20160629 - Added CSS_REF and JS_REF triggers.          
20160627 - Included reference form future.    
20160408 - Solved bug with large data in general CSS,JS part
20160324 - Added Compile button
20160310 - Included CodeMirror    
20151202 - Included session variables in filters    
20150817 - Added tab look
20150814 - Added superuser    
20150813 - Added option to create JS and CSS for custom FORM
20141027 - Added footer o';
 v_vc(36) := 'n all pages
20141126 - solved error on package - using method currentDADUser
*/'';
    return version;
 end;	
]]></plsql><plsqlspec><![CDATA[  function changes(p_log out varchar2) return varchar2 ;
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>codemirror</triggerid><plsql><![CDATA[procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2, m varchar2) is
begin

htp.p(''
function rasd_codemirror_''||n||''() {	  
 
var mime;
var m = ''''''||m||'''''';	  
if (m==''''JS'''') {
  mime = "text/javascript";
} else if (m==''''CSS'''') {
  mime = "text/css";
} else if (m==''''HTML'''') {
  mime = "text/html";
} else {
  mime = "text/sql";
}	  
  // get mime type
  if (window.location.href.indexOf("mime=") > -1) {
    mime = window.location.href.substr(window.location.href.indexOf("mime=") + 5);
  }

if (document.getElementById("''||n||''") != null) {
  
window.rasd_CMEditor''||n||'' = CodeMirror.fromTextArea(document.getElementById("''||n||''"), {
    mode: mime, 
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
	lineWrapping: true,
    matchBrackets : true,
    autofocus: true,
    foldGutter: true,  
	gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
    foldOptions: {
  rangeFinder: (cm, pos) => {	  							   
    var line=window.rasd_CMEditor''||n||''.getLine(pos.line);    
	  var match=line.match(/\/\/\/\/ /);

    if (match==null) {
      return CodeMirror.fold.auto(cm,pos);
    }
    else {
      var lineend = window.rasd_CMEditor''||n||''.lineCount();
      for (let i = pos.line+1; i < lineend; i++) {
         if ( window.rasd_CMEditor''||n||''.getLine(i).match(/\/\/\/\/ /) == null ) 
            {}
         else 
            {lineend = i-1; break; } 
      }
        var startPos=CodeMirror.Pos(pos.line+1, 0);      
	    var endPos=CodeMirror.Pos(lineend, 0);
      return endPos && CodeMirror.cmpPos(endPos, startPos) > 0 ? {from: startPos, to: endPos} : null;
    }
  }
    },	  
    extraKeys: {"Ctrl-Space": "autocomplete",
	            "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },
	            "Ctrl-Y": cm => CodeMirror.commands.foldAll(cm),
                "Ctrl-I": cm => CodeMirror.commands.unfoldAll(cm),
                "Alt-S": function(instance) { document.''||th';
 v_vc(37) := 'is_form||''.ACTION.value=''''''||GBUTTONSAVE||''''''; document.''||this_form||''.submit(); },
	            "F11": function(cm) {cm.setOption("fullScreen", !cm.getOption("fullScreen"));},
	            "Esc": function(cm) { if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);}
	  }
    });  
	  
  //window.rasd_CMEditor''||n||''.foldCode(CodeMirror.Pos(1, 0));  
}
	  
}'');


end;



]]></plsql><plsqlspec><![CDATA[procedure codemirrorjs(n varchar2, h varchar2, d varchar2, f varchar2, m varchar2);
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>myTabs</triggerid><plsql><![CDATA[  function myTabs(page varchar2 , lang varchar2, pformid varchar2) return varchar2 is
    v_tabs varchar2(32000);
  begin
rlog(''ee''||page);  
    v_tabs := ''
  <ul style="display: none;">
    <li><a href=''; 
	if page= 1 then 
	  v_tabs := v_tabs ||''"#idselected"''; 
	else v_tabs := 
	  v_tabs ||''"#x"''; 
	end if; 
	v_tabs := v_tabs ||'' onclick="javascript: location=encodeURI(''''?page=1&LANG=''||lang||''&PFORMID=''||pformid||'''''');">
	''||RASDI_TRNSLT.text(''Form code (javascript, CSS)'',LANG)||''
	</a></li>
    <li><a href=''; if page= 2 then v_tabs := v_tabs ||''"#idselected"''; else v_tabs := v_tabs ||''"#x"''; 
	end if; 
	v_tabs := v_tabs ||'' onclick="javascript: location=encodeURI(''''?page=2&LANG=''||lang||''&PFORMID=''||pformid||'''''');">
	''||RASDI_TRNSLT.text(''Global code (javascript, CSS)'',LANG)||''
	</a></li>
  </ul>
'' ;

   return v_tabs;
  end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure custom template</triggerid><plsql><![CDATA[function showLabel(plabel varchar2, pcolor varchar2 default ''U'', pshowdialog number default 0) 
return varchar2 is
   v__ varchar2(32000);
begin

v__ := RASDI_TRNSLT.text(plabel, lang);

if pshowdialog = 1 then 
v__ := v__ || rasdc_hints.linkDialog(replace(plabel,'' '',''''),lang,replace(this_form,''2'','''')||''_DIALOG'');
end if;

if pcolor is null then

return v__;

else

return ''<font color="''||pcolor||''">''||v__||''</font>'';

end if;


end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEM';
 v_vc(38) := 'PLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure post_submit template</triggerid><plsql><![CDATA[procedure post_submit_template is
begin
RASDC_LIBRARY.checkprivileges(PFORMID);
begin
      select upper(form)
        into pform
        from RASD_FORMS
       where formid = PFORMID;
exception when others then null;
end;

if ACTION = GBUTTONSAVE or ACTION = GBUTTONCOMPILE then
  if rasdc_library.allowEditing(pformid) then
     null;  
  else	 
     ACTION := GBUTTONSRC;
	 message := message || RASDI_TRNSLT.text(''User has no privileges to save data!'', lang);
  end if;
end if;


if rasdc_library.allowEditing(pformid) then
   GBUTTONSAVE#SET.visible := true;
   GBUTTONCOMPILE#SET.visible := true;
else
   GBUTTONSAVE#SET.visible := false;
   GBUTTONCOMPILE#SET.visible := false;
end if;


VUSER := rasdi_client.secGetUsername;
VLOB := rasdi_client.secGetLOB;
if lang is null then lang := rasdi_client.c_defaultLanguage; end if;

end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger><trigger><blockid></blockid><triggerid>procedure_compile_template</triggerid><plsql><![CDATA[procedure compile(pformid number, pform varchar2, lang varchar2,  sporocilo in out varchar2, refform varchar2 default ''''  , pcompid in out number) is 
      v_server RASD_ENGINES.server%type;
      cid      pls_integer;
      n        pls_integer;
      vup      varchar2(30) := rasdi_client.secGetUsername;
begin
        rasdc_library.log(this_form,pformid, ''COMPILE_S'', pcompid);          
        begin
          if pform in ( ''RASDC2_BLOCKSONFORM'',
                        ''RASDC2_FIELDSONBLOCK'',
                        ''RASDC2_TRIGGERS'',
                        ''RASDC2_LINKS'',
                        ''RASDC2_PAGES'',
                        ''RASDC2_FORMS'',
                        ''RASDC2_REFERENCES'') then
        
            sporocilo := RASDI_TRNSLT.text(''From is not generated.'', lang);            
          else
            
            select server
              into v_server
              from RASD_FORMS_COMPILED fg, RASD_ENGINES g
             where fg.engineid = g.engineid
               and fg.formid = PFORMID
               and fg.editor = vup
               and (fg.lobid';
 v_vc(39) := ' = rasdi_client.secGetLOB or
                   fg.lobid is null and rasdi_client.secGetLOB is null);
            
            cid     := dbms_sql.open_cursor;
            
            dbms_sql.parse(cid,
                           ''begin '' || v_server || ''.c_debug := false;''|| v_server || ''.form('' || PFORMID ||
                           '','''''' || lang || '''''');end;'',
                           dbms_sql.native);
            
            n       := dbms_sql.execute(cid);
            
            dbms_sql.close_cursor(cid);
            
            sporocilo := RASDI_TRNSLT.text(''From is generated.'', lang) || rasdc_library.checknumberofsubfields(PFORMID);

        if refform is not null then
           sporocilo :=  sporocilo || ''<br/> - ''||  RASDI_TRNSLT.text(''To unlink referenced code check:'', lang)||''<input type="checkbox" name="UNLINK" value="Y"/>.'';
        end if; 
                    
          end if;
        exception 
          when others then
            if sqlcode = -24344 then
              
            sporocilo := RASDI_TRNSLT.text(''Form is generated with compilation error. Check your code.'', lang)||''(''||sqlerrm||'')'';
             
            else
            sporocilo := RASDI_TRNSLT.text(''Form is NOT generated - internal RASD error.'', lang) || ''(''||sqlerrm||'')<br>''||
                         RASDI_TRNSLT.text(''To debug run: '', lang) || ''begin '' || v_server || ''.form('' || PFORMID ||
                         '','''''' || lang || '''''');end;'' ;
            end if;
        end;
        rasdc_library.log(this_form,pformid, ''COMPILE_E'', pcompid);     
end;
]]></plsql><plsqlspec><![CDATA[
]]></plsqlspec><source></source><hiddenyn></hiddenyn><rlobid>RASDDEV</rlobid><rform>RASDC2_TEMPLATE</rform><rblockid></rblockid></trigger></triggers><elements><element><elementid>1</elementid><pelementid>0</pelementid><orderby>1</orderby><element>HTML_</element><type></type><id>HTML</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></text';
 v_vc(40) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<html]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>2</elementid><pelementid>1</pelementid><orderby>1</orderby><element>HEAD_</element><type></type><id>HEAD</id><nameid>HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<head]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% 
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''<%= rasdc_library.formName(PFORMID, LANG) %>'')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
%>]]></value><valuecode><![CDATA['');  
htpClob(rasd_client.getHtmlJSLibrary(''HEAD'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''')); 
htp.p('''');
htp.p(''<script type="text/javascript">''); 
formgen_js;
htp.p(''</script>''); 	
htpClob(FORM_UIHEAD); 
htp.p(''<style type="text/css">''); 
htpClob(FORM_CSS); 
htp.p(''</style><script type="text/javascript">''); htpClob(FORM_JS); htp.p(''</script>''); 
 
htp.prn('']]';
 v_vc(41) := '></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>15</elementid><pelementid>1</pelementid><orderby>1</orderby><element>BODY_</element><type></type><id>BODY</id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<body]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>16</elementid><pelementid>15</pelementid><orderby>3</orderby><element>FORM_</element><type>F</type><id>RASDC2_CSSJS</id><nameid>RASDC2_CSSJS</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_ACTION</attribute><type>A</type><text></text><name><![CDATA[action]]></name><value><![CDATA[?]]></value><valuecode><![CDATA[="?"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_METHOD</attribute><type>A</type><text></text><name><![CDATA[method]]></name><value><![CDATA[post]]></value><valuecode><![CDATA[="post"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute>';
 v_vc(42) := '<orderby>2</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RASDC2_CSSJS]]></value><valuecode><![CDATA[="RASDC2_CSSJS"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<form]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>17</elementid><pelementid>15</pelementid><orderby>1</orderby><element>FORM_LAB</element><type>F</type><id>RASDC2_CSSJS_LAB</id><nameid>RASDC2_CSSJS_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormLab]]></value><valuecode><![CDATA[="rasdFormLab"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_LAB]]></value><valuecode><![CDATA[="RASDC2_CSSJS_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></n';
 v_vc(43) := 'ame><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlHeaderDataTable(''RASDC2_CSSJS_LAB'',''<%= rasdc_library.formName(PFORMID, LANG) %>'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlHeaderDataTable(''RASDC2_CSSJS_LAB'',''''|| rasdc_library.formName(PFORMID, LANG) ||'''') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>18</elementid><pelementid>15</pelementid><orderby>2</orderby><element>FORM_MENU</element><type>F</type><id>RASDC2_CSSJS_MENU</id><nameid>RASDC2_CSSJS_MENU</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMenu]]></value><valuecode><![CDATA[="rasdFormMenu"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_MENU]]></value><valuecode><![CDATA[="RASDC2_CSSJS_MENU"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>E_</a';
 v_vc(44) := 'ttribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlMenuList(''RASDC2_CSSJS_MENU'') %>     ]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlMenuList(''RASDC2_CSSJS_MENU'') ||''     ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>19</elementid><pelementid>15</pelementid><orderby>9999</orderby><element>FORM_BOTTOM</element><type>F</type><id>RASDC2_CSSJS_BOTTOM</id><nameid>RASDC2_CSSJS_BOTTOM</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBottom]]></value><valuecode><![CDATA[="rasdFormBottom"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_BOTTOM]]></value><valuecode><![CDATA[="RASDC2_CSSJS_BOTTOM"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text';
 v_vc(45) := '></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= rasd_client.getHtmlFooter(version , substr(''RASDC2_CSSJS_BOTTOM'',1,instr(''RASDC2_CSSJS_BOTTOM'', ''_'',-1)-1) , '''') %>]]></value><valuecode><![CDATA[''|| rasd_client.getHtmlFooter(version , substr(''RASDC2_CSSJS_BOTTOM'',1,instr(''RASDC2_CSSJS_BOTTOM'', ''_'',-1)-1) , '''') ||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>20</elementid><pelementid>16</pelementid><orderby>3</orderby><element>FORM_DIV</element><type>F</type><id>RASDC2_CSSJS_DIV</id><nameid>RASDC2_CSSJS_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdForm]]></value><valuecode><![CDATA[="rasdForm"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_DIV]]></value><valuecode><![CDATA[="RASDC2_CSSJS_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><ord';
 v_vc(46) := 'erby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>21</elementid><pelementid>20</pelementid><orderby>1</orderby><element>FORM_HEAD</element><type>F</type><id>RASDC2_CSSJS_HEAD</id><nameid>RASDC2_CSSJS_HEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormHead]]></value><valuecode><![CDATA[="rasdFormHead"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_HEAD]]></value><valuecode><![CDATA[="RASDC2_CSSJS_HEAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]';
 v_vc(47) := ']></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>22</elementid><pelementid>20</pelementid><orderby>5</orderby><element>FORM_BODY</element><type>F</type><id>RASDC2_CSSJS_BODY</id><nameid>RASDC2_CSSJS_BODY</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormBody]]></value><valuecode><![CDATA[="rasdFormBody"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_BODY]]></value><valuecode><![CDATA[="RASDC2_CSSJS_BODY"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>23</elementid><pelementid>20</pelementid><orderby>9999</orderby><element>FORM_FOOTER</element><type>F</type><id>RASDC2_CSSJS_FOOTER</id><nameid>RASDC2_CSSJS_FOOTER</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rfo';
 v_vc(48) := 'rm></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormFooter]]></value><valuecode><![CDATA[="rasdFormFooter"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_FOOTER]]></value><valuecode><![CDATA[="RASDC2_CSSJS_FOOTER"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>24</elementid><pelementid>20</pelementid><orderby>2</orderby><element>FORM_RESPONSE</element><type>F</type><id>RASDC2_CSSJS_RESPONSE</id><nameid>RASDC2_CSSJS_RESPONSE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormResponse]]></value><valuecode><![CDATA[="rasdFormResponse"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby';
 v_vc(49) := '><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_RESPONSE]]></value><valuecode><![CDATA[="RASDC2_CSSJS_RESPONSE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>25</elementid><pelementid>24</pelementid><orderby>9998</orderby><element>FORM_MESSAGE</element><type>F</type><id>RASDC2_CSSJS_MESSAGE</id><nameid>RASDC2_CSSJS_MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage]]></value><valuecode><![CDATA[="rasdFormMessage"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_MESSAGE]]></value><valuecode><![CDATA[="RASDC2_CSSJS_MESSAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></nam';
 v_vc(50) := 'e><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>26</elementid><pelementid>24</pelementid><orderby>9996</orderby><element>FORM_ERROR</element><type>F</type><id>RASDC2_CSSJS_ERROR</id><nameid>RASDC2_CSSJS_ERROR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage error]]></value><valuecode><![CDATA[="rasdFormMessage error"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_ERROR]]></value><valuecode><![CDATA[="RASDC2_CSSJS_ERROR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><';
 v_vc(51) := 'hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>27</elementid><pelementid>24</pelementid><orderby>9997</orderby><element>FORM_WARNING</element><type>F</type><id>RASDC2_CSSJS_WARNING</id><nameid>RASDC2_CSSJS_WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFormMessage warning]]></value><valuecode><![CDATA[="rasdFormMessage warning"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RASDC2_CSSJS_WARNING]]></value><valuecode><![CDATA[="RASDC2_CSSJS_WARNING"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>28</elementid><pelementid>22</pelementid><orderby>105</orderby><element>BLOCK_DIV</element><type>B</type><id>P_DIV</id><nameid>P_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><a';
 v_vc(52) := 'ttribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_DIV]]></value><valuecode><![CDATA[="P_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockP_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockP_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>29</elementid><pelementid>28</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></na';
 v_vc(53) := 'me><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>30</elementid><pelementid>29</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>P_LAB</id><nameid>P_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_LAB]]></value><valuecode><![CDATA[="P_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobi';
 v_vc(54) := 'd><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Form code (javascript, CSS)'',LANG)%>]]></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Form code (javascript, CSS)'',LANG)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>31</elementid><pelementid>28</pelementid><orderby>106</orderby><element>TABLE_</element><type></type><id>P_TABLE</id><nameid>P_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_TABLE_RASD]]></value><valuecode><![CDATA[="P_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>';
 v_vc(55) := 'S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>32</elementid><pelementid>31</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>P_BLOCK</id><nameid>P_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_BLOCK_RASD]]></value><valuecode><![CDATA[="P_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>33</elementid><pelementid>31</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>P_THEAD</id><nameid>P_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></';
 v_vc(56) := 'textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>34</elementid><pelementid>33</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>35</elementid><pelementid>34</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>PBLOKPROZILECNOV_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLab';
 v_vc(57) := 'PBLOKPROZILECNOV]]></value><valuecode><![CDATA[="rasdTxLabPBLOKPROZILECNOV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>36</elementid><pelementid>35</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PBLOKPROZILECNOV_LAB</id><nameid>PBLOKPROZILECNOV_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PBLOKPROZILECNOV_LAB]]></value><valuecode><![CDATA[="PBLOKPROZILECNOV_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><v';
 v_vc(58) := 'alueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>37</elementid><pelementid>34</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>PF_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPF]]></value><valuecode><![CDATA[="rasdTxLabPF"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></r';
 v_vc(59) := 'form><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>38</elementid><pelementid>37</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>PF_LAB</id><nameid>PF_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PF_LAB]]></value><valuecode><![CDATA[="PF_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]>';
 v_vc(60) := '</valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PPF#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>39</elementid><pelementid>34</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGBTNSRC]]></value><valuecode><![CDATA[="rasdTxLabPGBTNSRC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></texti';
 v_vc(61) := 'd><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>40</elementid><pelementid>39</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>PGBTNSRC_LAB</id><nameid>PGBTNSRC_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_LAB]]></value><valuecode><![CDATA[="PGBTNSRC_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><';
 v_vc(62) := '/name><value><![CDATA[<% if PGBTNSRC#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>41</elementid><pelementid>34</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSAVE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGBTNSAVE]]></value><valuecode><![CDATA[="rasdTxLabPGBTNSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endlo';
 v_vc(63) := 'op></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>42</elementid><pelementid>41</pelementid><orderby>4</orderby><element>FONT_</element><type>L</type><id>PGBTNSAVE_LAB</id><nameid>PGBTNSAVE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSAVE_LAB]]></value><valuecode><![CDATA[="PGBTNSAVE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></tex';
 v_vc(64) := 'tid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>43</elementid><pelementid>34</pelementid><orderby>5</orderby><element>TX_</element><type>E</type><id></id><nameid>PLDELETE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPLDELETE]]></value><valuecode><![CDATA[="rasdTxLabPLDELETE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>44</elementid><pelementid>43</pelementid><orderby>5</orderby><elemen';
 v_vc(65) := 't>FONT_</element><type>L</type><id>PLDELETE_LAB</id><nameid>PLDELETE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PLDELETE_LAB]]></value><valuecode><![CDATA[="PLDELETE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N<';
 v_vc(66) := '/hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>45</elementid><pelementid>34</pelementid><orderby>6</orderby><element>TX_</element><type>E</type><id></id><nameid>PUPLOAD_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPUPLOAD]]></value><valuecode><![CDATA[="rasdTxLabPUPLOAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>46</elementid><pelementid>45</pelementid><orderby>6</orderby><element>FONT_</element><type>L</type><id>PUPLOAD_LAB</id><nameid>PUPLOAD_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS<';
 v_vc(67) := '/attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PUPLOAD_LAB]]></value><valuecode><![CDATA[="PUPLOAD_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>47</elementid><pelementid>34</pelementid><orderby>7</orderby><element>TX_</element><type>E</type><id></id><nameid>PHINT_TX_LA';
 v_vc(68) := 'B</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockP]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPHINT]]></value><valuecode><![CDATA[="rasdTxLabPHINT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>48</elementid><pelementid>47</pelementid><orderby>7</orderby><element>FONT_</element><type>L</type><id>PHINT_LAB</id><nameid>PHINT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform>';
 v_vc(69) := '</rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PHINT_LAB]]></value><valuecode><![CDATA[="PHINT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>49</elementid><pelementid>22</pelementid><orderby>115</orderby><element>BLOCK_DIV</element><type>B</type><id>PG_DIV</id><nameid>PG_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PG_DIV]]></value><valuecode><![CDATA[="PG_DIV"]]></valuecode><forloop><';
 v_vc(70) := '/forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockPG_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockPG_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>50</elementid><pelementid>49</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]';
 v_vc(71) := ']></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>51</elementid><pelementid>50</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>PG_LAB</id><nameid>PG_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PG_LAB]]></value><valuecode><![CDATA[="PG_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Global code (javascript, CSS)'',LANG)%>]';
 v_vc(72) := ']></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Global code (javascript, CSS)'',LANG)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>52</elementid><pelementid>49</pelementid><orderby>116</orderby><element>TABLE_</element><type></type><id>PG_TABLE</id><nameid>PG_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PG_TABLE_RASD]]></value><valuecode><![CDATA[="PG_TABLE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>53</elementid><pelementid>52</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>PG_BLOCK</id><nameid>PG_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source>';
 v_vc(73) := '<hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PG_BLOCK_RASD]]></value><valuecode><![CDATA[="PG_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>54</elementid><pelementid>52</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>PG_THEAD</id><nameid>PG_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>55</elementid><pelementid>54</pelementid><orderby>2</orderby><eleme';
 v_vc(74) := 'nt>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>56</elementid><pelementid>55</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBLOKPROZILECNOV_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockPG]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGBLOKPROZILECNOV]]></value><valuecode><![CDATA[="rasdTxLabPGBLOKPROZILECNOV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><tex';
 v_vc(75) := 'tid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>57</elementid><pelementid>56</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>PGBLOKPROZILECNOV_LAB</id><nameid>PGBLOKPROZILECNOV_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBLOKPROZILECNOV_LAB]]></value><valuecode><![CDATA[="PGBLOKPROZILECNOV_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S';
 v_vc(76) := '_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>58</elementid><pelementid>55</pelementid><orderby>2</orderby><element>TX_</element><type>E</type><id></id><nameid>PGGBTNSRC_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockPG]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGGBTNSRC]]></value><valuecode><![CDATA[="rasdTxLabPGGBTNSRC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></';
 v_vc(77) := 'value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>59</elementid><pelementid>58</pelementid><orderby>2</orderby><element>FONT_</element><type>L</type><id>PGGBTNSRC_LAB</id><nameid>PGGBTNSRC_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGGBTNSRC_LAB]]></value><valuecode><![CDATA[="PGGBTNSRC_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><sou';
 v_vc(78) := 'rce></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>60</elementid><pelementid>55</pelementid><orderby>3</orderby><element>TX_</element><type>E</type><id></id><nameid>PGGBTNSAVE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockPG]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGGBTNSAVE]]></value><valuecode><![CDATA[="rasdTxLabPGGBTNSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><ele';
 v_vc(79) := 'mentid>61</elementid><pelementid>60</pelementid><orderby>3</orderby><element>FONT_</element><type>L</type><id>PGGBTNSAVE_LAB</id><nameid>PGGBTNSAVE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGGBTNSAVE_LAB]]></value><valuecode><![CDATA[="PGGBTNSAVE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><va';
 v_vc(80) := 'luecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>62</elementid><pelementid>55</pelementid><orderby>4</orderby><element>TX_</element><type>E</type><id></id><nameid>PGUPLOAD_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockPG]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGUPLOAD]]></value><valuecode><![CDATA[="rasdTxLabPGUPLOAD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>63</elementid><pelementid>62</pelementid><orderby>4</orderby><element>FONT_</element><type>L</type><id>PGUPLOAD_LAB</id><nameid>PGUPLOAD_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid>';
 v_vc(81) := '</rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGUPLOAD_LAB]]></value><valuecode><![CDATA[="PGUPLOAD_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>64</elementid><pelementid>5';
 v_vc(82) := '5</pelementid><orderby>5</orderby><element>TX_</element><type>E</type><id></id><nameid>PGHINT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockPG]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabPGHINT]]></value><valuecode><![CDATA[="rasdTxLabPGHINT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>65</elementid><pelementid>64</pelementid><orderby>5</orderby><element>FONT_</element><type>L</type><id>PGHINT_LAB</id><nameid>PGHINT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></sourc';
 v_vc(83) := 'e><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGHINT_LAB]]></value><valuecode><![CDATA[="PGHINT_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>66</elementid><pelementid>22</pelementid><orderby>125</orderby><element>BLOCK_DIV</element><type>B</type><id>B10_DIV</id><nameid>B10_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name';
 v_vc(84) := '><![CDATA[id]]></name><value><![CDATA[B10_DIV]]></value><valuecode><![CDATA[="B10_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>67</elementid><pelementid>66</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><e';
 v_vc(85) := 'ndloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>68</elementid><pelementid>67</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10_LAB</id><nameid>B10_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_LAB]]></value><valuecode><![CDATA[="B10_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><';
 v_vc(86) := 'type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>69</elementid><pelementid>66</pelementid><orderby>126</orderby><element>TABLE_</element><type></type><id>B10_TABLE</id><nameid>B10_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_TABLE_RASD]]></value><valuecode><![CDATA[="B10_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>70</elementid><pelementid>69</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10_BLOCK</id><nameid>B10_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid>';
 v_vc(87) := '</rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_BLOCK_RASD]]></value><valuecode><![CDATA[="B10_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>71</elementid><pelementid>69</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B10_THEAD</id><nameid>B10_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>72</elementid><pelementid>71</pelementid><orderby>2</orderby><element>TR_</element><type></t';
 v_vc(88) := 'ype><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>73</elementid><pelementid>72</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PLSQL_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10PLSQL]]></value><valuecode><![CDATA[="rasdTxLabB10PLSQL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid';
 v_vc(89) := '><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>74</elementid><pelementid>73</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10PLSQL_LAB</id><nameid>B10PLSQL_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PLSQL_LAB]]></value><valuecode><![CDATA[="B10PLSQL_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SE';
 v_vc(90) := 'T(1).visible then %><span]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Code'','''',0) %><div id=PLSQLCOUNT>�</div>]]></value><valuecode><![CDATA[''|| showLabel(''Code'','''',0) ||''<div id=PLSQLCOUNT>�</div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>75</elementid><pelementid>22</pelementid><orderby>135</orderby><element>BLOCK_DIV</element><type>B</type><id>B10G_DIV</id><nameid>B10G_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_DIV]]></value><valuecode><![CDATA[="B10G_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><val';
 v_vc(91) := 'uecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB10G_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB10G_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>76</elementid><pelementid>75</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>77</elementid><pelementid>76</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B10G_LAB</id><nameid>B10G_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></';
 v_vc(92) := 'rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_LAB]]></value><valuecode><![CDATA[="B10G_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>78</elementid><pelementid>75</pelementid><orderby>136</orderby><element>TABLE_</element><type></type><id>B10G_TABLE</id><nameid>B10G_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode>';
 v_vc(93) := '<forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_TABLE_RASD]]></value><valuecode><![CDATA[="B10G_TABLE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>79</elementid><pelementid>78</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B10G_BLOCK</id><nameid>B10G_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_BLOCK_RASD]]></value><valuecode><![CDATA[="B10G_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlo';
 v_vc(94) := 'bid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>80</elementid><pelementid>78</pelementid><orderby>2</orderby><element>THEAD_</element><type></type><id>B10G_THEAD</id><nameid>B10G_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>81</elementid><pelementid>80</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hi';
 v_vc(95) := 'ddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>82</elementid><pelementid>81</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B10GBLOBVC_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB10G]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB10G"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB10GBLOBVC]]></value><valuecode><![CDATA[="rasdTxLabB10GBLOBVC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>83</elementid><pelementid>82</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B10GBLOBVC_LAB</id><nameid>B10GBLOBVC_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>';
 v_vc(96) := '3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10GBLOBVC_LAB]]></value><valuecode><![CDATA[="B10GBLOBVC_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= showLabel(''Code'','''',0) %><div id=PLSQLCOUNT>�</div>]]></value><valuecode><![CDATA[''|| showLabel(''Code'','''',0) ||''<div id=PLSQLCOUNT>�</div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></a';
 v_vc(97) := 'ttribute></attributes></element><element><elementid>84</elementid><pelementid>22</pelementid><orderby>145</orderby><element>BLOCK_DIV</element><type>B</type><id>B20_DIV</id><nameid>B20_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_DIV]]></value><valuecode><![CDATA[="B20_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB20_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB20_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>85</elementid><pelementid>84</pelem';
 v_vc(98) := 'entid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>86</elementid><pelementid>85</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B20_LAB</id><nameid>B20_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_LAB]]></value><valuecode><![CDATA[="B20_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rl';
 v_vc(99) := 'obid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[External links]]></value><valuecode><![CDATA[External links]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>87</elementid><pelementid>84</pelementid><orderby>146</orderby><element>TABLE_</element><type></type><id>B20_TABLE</id><nameid>B20_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[0]]></value><valuecode><![CDATA[="0"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_TABLE_RASD]]></value><valuecode><![CDATA[="B20_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><';
 v_vc(100) := 'valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>88</elementid><pelementid>87</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B20_BLOCK</id><nameid>B20_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20_BLOCK_RASD]]></value><valuecode><![CDATA[="B20_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>89</elementid><pelementid>22</pelementid><orderby>155</orderby><element>BLOCK_DIV</element><type>B</type><id>B30_DIV</id><nameid>B30_DIV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includev';
 v_vc(101) := 'is><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdblock]]></value><valuecode><![CDATA[="rasdblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_DIV]]></value><valuecode><![CDATA[="B30_DIV"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[><div>]]></value><valuecode><![CDATA[><div>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</div></div><% end if; %>]]></value><valuecode><![CDATA[</div></div>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if  ShowBlockB30_DIV  then %><div ]]></value><valuecode><![CDATA['');  if  ShowBlockB30_DIV  then  
htp.prn(''<div ]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>90</elementid><pelementid>89</pelementid><orderby>1</orderby><element>CAPTION_</element><type>L</type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name>';
 v_vc(102) := '<![CDATA[</caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text><![CDATA[
]]></text><name><![CDATA[<caption]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode><![CDATA[
]]></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>91</elementid><pelementid>90</pelementid><orderby>1</orderby><element>FONT_</element><type>B</type><id>B30_LAB</id><nameid>B30_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[labelblock]]></value><valuecode><![CDATA[="labelblock"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_LAB]]></value><valuecode><![CDATA[="B30_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><te';
 v_vc(103) := 'xtcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<div]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<%= RASDI_TRNSLT.text(''Errors'',LANG)%>]]></value><valuecode><![CDATA[''|| RASDI_TRNSLT.text(''Errors'',LANG)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>92</elementid><pelementid>89</pelementid><orderby>156</orderby><element>TABLE_N</element><type></type><id>B30_TABLE</id><nameid>B30_TABLE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_BORDER</attribute><type>A</type><text></text><name><![CDATA[border]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTableN display]]></value><valuecode><![CDATA[="rasdTableN display"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_TABLE_RASD]]></value><valuecode><![CDATA[="B30_TABLE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5<';
 v_vc(104) := '/orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<table]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>93</elementid><pelementid>92</pelementid><orderby>3</orderby><element>TR_</element><type>B</type><id>B30_BLOCK</id><nameid>B30_BLOCK</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30_BLOCK_RASD]]></value><valuecode><![CDATA[="B30_BLOCK"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop><![CDATA[''); end loop; 
htp.prn('']]></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop><![CDATA[''); for iB30 in 1..B30TEXT.count loop 
htp.prn('']]></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>94</elementid><pelementid>92</pelementid><orderby>2</orderby><element>THEAD_</element><type>';
 v_vc(105) := '</type><id>B30_THEAD</id><nameid>B30_THEAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>1</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<thead]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>95</elementid><pelementid>94</pelementid><orderby>2</orderby><element>TR_</element><type></type><id></id><nameid></nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>96</elementid><pelementid>95</pelementid><orderby>1</orderby><element>TX_</element><type>E</type><id></id><nameid>B30TEXT_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><te';
 v_vc(106) := 'xt></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB30]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB30"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB30TEXT]]></value><valuecode><![CDATA[="rasdTxLabB30TEXT"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>97</elementid><pelementid>96</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B30TEXT_LAB</id><nameid>B30TEXT_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30TEXT_LAB]]></value><valuecode><![CDATA[="B30TEXT_LAB"]]></valuecode>';
 v_vc(107) := '<forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>98</elementid><pelementid>21</pelementid><orderby>-9</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10</id><nameid>RECNUMB10</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></t';
 v_vc(108) := 'ext><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10_NAME]]></value><valuecode><![CDATA[="RECNUMB10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB10))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></so';
 v_vc(109) := 'urce><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>99</elementid><pelementid>21</pelementid><orderby>-2</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMPG</id><nameid>RECNUMPG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMPG_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMPG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMPG_NAME]]></value><valuecode><![CDATA[="RECNUMPG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><';
 v_vc(110) := 'attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMPG_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMPG))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>100</elementid><pelementid>21</pelementid><orderby>-1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMP</id><nameid>RECNUMP</nameid><endtagelementid></endtagelementid><source></s';
 v_vc(111) := 'ource><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMP_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMP_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMP_NAME]]></value><valuecode><![CDATA[="RECNUMP"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><tex';
 v_vc(112) := 't></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMP_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMP))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>101</elementid><pelementid>21</pelementid><orderby>-1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>RECNUMB10G</id><nameid>RECNUMB10G</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><text';
 v_vc(113) := 'id></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[RECNUMB10G_NAME_RASD]]></value><valuecode><![CDATA[="RECNUMB10G_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[RECNUMB10G_NAME]]></value><valuecode><![CDATA[="RECNUMB10G"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[RECNUMB10G_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(RECNUMB10G))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute';
 v_vc(114) := '><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>102</elementid><pelementid>21</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PAGE</id><nameid>PAGE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PAGE_NAME_RASD]]></value><valuecode><![CDATA[="PAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PAGE_NAME]]></value><valuecode><![CDATA[="PAGE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hi';
 v_vc(115) := 'ddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PAGE_VALUE]]></value><valuecode><![CDATA[="''||ltrim(to_char(PAGE))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>103</elementid><pelementid>21</pelementid><orderby>';
 v_vc(116) := '1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>ACTION</id><nameid>ACTION</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ACTION_NAME_RASD]]></value><valuecode><![CDATA[="ACTION_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[ACTION_NAME]]></value><valuecode><![CDATA[="ACTION"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textco';
 v_vc(117) := 'de><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[ACTION_VALUE]]></value><valuecode><![CDATA[="''||ACTION||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>104</elementid><pelementid>21</pelementid><orderby>6</orderby><element>INPUT_HIDDEN</element><type>D</type><id>LANG</id><nameid>LANG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuec';
 v_vc(118) := 'ode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[LANG_NAME_RASD]]></value><valuecode><![CDATA[="LANG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[LANG_NAME]]></value><valuecode><![CDATA[="LANG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[LANG_VALUE]]></value><valuecode><![CDATA[="''||LANG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rl';
 v_vc(119) := 'obid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>105</elementid><pelementid>21</pelementid><orderby>7</orderby><element>INPUT_HIDDEN</element><type>D</type><id>PFORMID</id><nameid>PFORMID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PFORMID_NAME_RASD]]></value><valuecode><![CDATA[="PFORMID_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PFORMID_NAME]]></value><valuecode><![CDATA[="PFORMID"]]></valuecode';
 v_vc(120) := '><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PFORMID_VALUE]]></value><valuecode><![CDATA[="''||PFORMID||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><';
 v_vc(121) := 'elementid>106</elementid><pelementid>21</pelementid><orderby>9</orderby><element>INPUT_BUTTON</element><type>D</type><id>GBTNNAVIG</id><nameid>GBTNNAVIG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><valuecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBTNNAVIG_NAME_RASD]]></value><valuecode><![CDATA[="GBTNNAVIG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBTNNAVIG_NAME]]></value><valuecode><![CDATA[="GBTNNAVIG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[link$formnavlink]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$formnavlink(GBTNNAVIG,''GBTNNAVIG''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute';
 v_vc(122) := '><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBTNNAVIG_VALUE]]></value><valuecode><![CDATA[="''||GBTNNAVIG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBTNNAVIG  then  
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>107</elementid><pelementid>23</pelementid><orderby>10007</orderby><element>INPUT_BUTTON</element><type>D</type><id>GBTNNAVIG</id><nameid>GBTNNAVIG</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton]]></value><v';
 v_vc(123) := 'aluecode><![CDATA[="rasdButton"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBTNNAVIG_NAME_RASD]]></value><valuecode><![CDATA[="GBTNNAVIG_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBTNNAVIG_NAME]]></value><valuecode><![CDATA[="GBTNNAVIG"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[link$formnavlink]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$formnavlink(GBTNNAVIG,''GBTNNAVIG''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![';
 v_vc(124) := 'CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBTNNAVIG_VALUE]]></value><valuecode><![CDATA[="''||GBTNNAVIG||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBTNNAVIG  then  
htp.prn(''<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>108</elementid><pelementid>21</pelementid><orderby>13</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valueco';
 v_vc(125) := 'de><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text>';
 v_vc(126) := '</text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><';
 v_vc(127) := 'textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></text';
 v_vc(128) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONSAVE  then  
htp.prn('''');  if GBUTTONSAVE#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>109</elementid><pelementid>23</pelementid><orderby>10011</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONSAVE</id><nameid>GBUTTONSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONSAVE#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONSAVE#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONSAVE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONSAVE#SET.custom , instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONSAVE#SET.custom),''"'',instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONSAVE#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text';
 v_vc(129) := '><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONSAVE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONSAVE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONSAVE_NAME]]></value><valuecode><![CDATA[="GBUTTONSAVE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONSAVE_VALUE]]></valu';
 v_vc(130) := 'e><valuecode><![CDATA[="''||GBUTTONSAVE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONSAVE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONSAVE#SET.cust';
 v_vc(131) := 'om);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.error is not null then htp.p('' title="''||GBUTTONSAVE#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONSAVE#SET.info is not null then htp.p('' title="''||GBUTTONSAVE#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONSAVE  then  
htp.prn('''');  if GBUTTONSAVE#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONSAVE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>110</elementid><pelementid>21</pelementid><orderby>14</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid';
 v_vc(132) := '><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><';
 v_vc(133) := '![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><';
 v_vc(134) := '![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></texti';
 v_vc(135) := 'd><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONCOMPILE  then  
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>111</elementid><pelementid>23</pelementid><orderby>10012</orderby><element>INPUT_SUBMIT</element><type>D</type><id>GBUTTONCOMPILE</id><nameid>GBUTTONCOMPILE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBU';
 v_vc(136) := 'TTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONCOMPILE#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONCOMPILE#SET.custom , instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONCOMPILE#SET.custom),''"'',instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONCOMPILE#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONCOMPILE_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONCOMPILE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONCOMPILE_NAME]]></value><valuecode><![CDATA[="GBUTTONCOMPILE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><te';
 v_vc(137) := 'xtcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONCOMPILE_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONCOMPILE||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.disabled then ht';
 v_vc(138) := 'p.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONCOMPILE#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONCOMPILE#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.error is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONCOMPILE#SET.info is not null then htp.p('' title="''||GBUTTONCOMPILE#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby>';
 v_vc(139) := '<attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONCOMPILE  then  
htp.prn('''');  if GBUTTONCOMPILE#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONCOMPILE#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>112</elementid><pelementid>21</pelementid><orderby>15</orderby><element>INPUT_RESET</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddeny';
 v_vc(140) := 'n><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><t';
 v_vc(141) := 'ype>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTT';
 v_vc(142) := 'ONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONRES  then  
htp.prn('''');  if GBUTTONRES#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>113</elementid><pelementid>23</pelementid><orderby>10013</orderby><element>INPUT_RESE';
 v_vc(143) := 'T</element><type>D</type><id>GBUTTONRES</id><nameid>GBUTTONRES</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if; %><% if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if; %><% if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if GBUTTONRES#SET.error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if GBUTTONRES#SET.info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(GBUTTONRES#SET.custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(GBUTTONRES#SET.custom , instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+7 , instr(upper(GBUTTONRES#SET.custom),''"'',instr(upper(GBUTTONRES#SET.custom),''CLASS="'')+8)-instr(upper(GBUTTONRES#SET.custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONRES_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONRES_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![';
 v_vc(144) := 'CDATA[name]]></name><value><![CDATA[GBUTTONRES_NAME]]></value><valuecode><![CDATA[="GBUTTONRES"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[reset]]></value><valuecode><![CDATA[="reset"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[GBUTTONRES_VALUE]]></value><valuecode><![CDATA[="''||GBUTTONRES||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['')';
 v_vc(145) := ';  if GBUTTONRES#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONRES#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONRES#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.error is not null then htp.p('' title="''||GBUTTONRES#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><n';
 v_vc(146) := 'ame></name><value><![CDATA[<% if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONRES#SET.info is not null then htp.p('' title="''||GBUTTONRES#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONRES  then  
htp.prn('''');  if GBUTTONRES#SET.visible then  
htp.prn(''<input]]></name><value><![CDATA[<% if GBUTTONRES#SET.visible then %><input]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>114</elementid><pelementid>21</pelementid><orderby>16</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><ri';
 v_vc(147) := 'd></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><e';
 v_vc(148) := 'ndloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;  
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONPREV  then  
htp.prn('''');  if GBUTTONPREV#SET.visible then  
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||
                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||
                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,
                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',
                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,
                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it';
 v_vc(149) := '.'',lang) ||
                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
									 ,
                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
                                )
			       ) || ''>''
);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>115</elementid><pelementid>23</pelementid><orderby>10014</orderby><element>PLSQL_</element><type>D</type><id>GBUTTONPREV</id><nameid>GBUTTONPREV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[GBUTTONPREV_NAME_RASD]]></value><valuecode><![CDATA[="GBUTTONPREV_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTON';
 v_vc(150) := 'PREV#SET.disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||GBUTTONPREV#SET.custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||GBUTTONPREV#SET.custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.error is not null then htp.p('' title="''||GBUTTONPREV#SET.error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if GBUTTONPREV#SET.info is not null then htp.p('' title="''||GBUTTONPREV#SET.info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>';
 v_vc(151) := 'E_</attribute><type>E</type><text></text><name><![CDATA[</span>'');  end if;  
htp.prn('']]></name><value><![CDATA[</span><% end if; %>]]></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldGBUTTONPREV  then  
htp.prn('''');  if GBUTTONPREV#SET.visible then  
htp.prn(''<span]]></name><value><![CDATA[<% if GBUTTONPREV#SET.visible then %><span]]></value><valuecode><![CDATA['');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% GBUTTONPREV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p( ''<input type=button class="rasdButton" value="'' ||
                   RASDI_TRNSLT.text(''Preview'', lang) || ''" '' ||
                   owa_util.ite(RASDC_LIBRARY.formhaserrors(pform) = true,
                                ''disabled="disabled" style="background-color: red;" title="'' ||RASDI_TRNSLT.text(''Program has ERRORS!'',lang)||''" '',
                                owa_util.ite(RASDC_LIBRARY.formischanged(PFORMID) = true,
                                     ''style="background-color: orange;" title="'' ||RASDI_TRNSLT.text(''Programa has changes. Compile it.'',lang) ||
                                     ''" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
									 ,
                                     ''style="background-color: green;" onclick="x=window.open(''''!'' ||pform ||''.webclient'''','''''''','''''''')" ''
                                )
			       ) || ''>''
);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>116</elementid><pelementid>26</pelementid><orderby>10046</orderby><element>FONT_</element><type>D</type><id>ERROR</id><nameid>ERROR</nameid><endtagelementid>0</endtagelementid><source>';
 v_vc(152) := '</source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[ERROR_NAME_RASD]]></value><valuecode><![CDATA[="ERROR_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><valu';
 v_vc(153) := 'e></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[ERROR_VALUE]]></value><valuecode><![CDATA[''||ERROR||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>117</elementid><pelementid>25</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>MESSAGE</id><nameid>MESSAGE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute>';
 v_vc(154) := '<attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[MESSAGE_NAME_RASD]]></value><valuecode><![CDATA[="MESSAGE_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><te';
 v_vc(155) := 'xtcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[MESSAGE_VALUE]]></value><valuecode><![CDATA[''||MESSAGE||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>118</elementid><pelementid>27</pelementid><orderby>10049</orderby><element>FONT_</element><type>D</type><id>WARNING</id><nameid>WARNING</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[WARNING_NAME_RASD]]';
 v_vc(156) := '></value><valuecode><![CDATA[="WARNING_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</att';
 v_vc(157) := 'ribute><type>S</type><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[WARNING_VALUE]]></value><valuecode><![CDATA[''||WARNING||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>119</elementid><pelementid>21</pelementid><orderby>290876</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldHINTCONTENT  then  
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><v';
 v_vc(158) := 'aluecode><![CDATA['');  htp.p('''');  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>120</elementid><pelementid>23</pelementid><orderby>300874</orderby><element>PLSQL_</element><type>D</type><id>HINTCONTENT</id><nameid>HINTCONTENT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[HINTCONTENT_NAME_RASD]]></value><valuecode><![CDATA[="HINTCONTENT_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA['');  
if  ShowFieldHINTCONTENT  then  
htp.prn(''<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% HINTCONTENT_VALUE %>]]></value><valuecode><![CDATA['');  htp.p('''');  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>121</elementid><pelementid>32</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>P</nameid><endtagelementid>0<';
 v_vc(159) := '/endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[P_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes';
 v_vc(160) := '></element><element><elementid>122</elementid><pelementid>121</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>SESSSTORAGEENABLED</id><nameid>SESSSTORAGEENABLED</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME_RASD]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[SESSSTORAGEENABLED_NAME]]></value><valuecode><![CDATA[="SESSSTORAGEENABLED_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></v';
 v_vc(161) := 'alue><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[SESSSTORAGEENABLED_VALUE]]></value><valuecode><![CDATA[="''||PSESSSTORAGEENABLED(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>123</elementid><pelementid>32</pelementid><orderby>20</orderby><element>TX_</element><type>E</type><id></id><nameid>PBLOKPROZILECNOV_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPBLOKPROZILECNOV rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPBLOKPROZILECNOV rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valu';
 v_vc(162) := 'eid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPBLOKPROZILECNOV_NAME]]></value><valuecode><![CDATA[="rasdTxPBLOKPROZILECNOV_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>124</elementid><pelementid>123</pelementid><orderby>1</orderby><element>SELECT_</element><type>D</type><id>PBLOKPROZILECNOV</id><nameid>PBLOKPROZILECNOV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdSelect <% if PBLOKPROZILECNOV#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PBLOKPROZILECNOV#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PBLOKPROZILECNOV#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PBLOKPROZILECNOV#SET(1).custom , instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')+7 , instr(upper(PBLOKPROZILECNOV#SET(1).custom),''"'',instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')+8)-instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdSelect '');  if PBLOKPROZILECNOV#SET(1).error is not null then htp.p('' errorField''); end i';
 v_vc(163) := 'f;  
htp.prn('''');  if PBLOKPROZILECNOV#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PBLOKPROZILECNOV#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PBLOKPROZILECNOV#SET(1).custom , instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')+7 , instr(upper(PBLOKPROZILECNOV#SET(1).custom),''"'',instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')+8)-instr(upper(PBLOKPROZILECNOV#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PBLOKPROZILECNOV_NAME_RASD]]></value><valuecode><![CDATA[="PBLOKPROZILECNOV_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PBLOKPROZILECNOV_NAME]]></value><valuecode><![CDATA[="PBLOKPROZILECNOV_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endlo';
 v_vc(164) := 'op><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).req';
 v_vc(165) := 'uired then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PBLOKPROZILECNOV#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PBLOKPROZILECNOV#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).error is not null then htp.p('' title="''||PBLOKPROZILECNOV#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).error is not null then htp.p('' title="''||PBLOKPROZILECNOV#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).info is not null then htp.p('' title="''||PBLOKPROZILECNOV#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).info is not null then htp.p('' title="''||PBLOKPROZILECNOV#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</select><% end if; %>]]></value><valuecode><![CDATA[</select>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform';
 v_vc(166) := '></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PBLOKPROZILECNOV#SET(1).visible then %><select]]></value><valuecode><![CDATA['');  if PBLOKPROZILECNOV#SET(1).visible then  
htp.prn(''<select]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[lov$PBLOKPROZILECNOV_LOV]]></value><valuecode><![CDATA[''); js_Slov$PBLOKPROZILECNOV_LOV(PBLOKPROZILECNOV(1),''PBLOKPROZILECNOV_1''); 
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>125</elementid><pelementid>32</pelementid><orderby>220</orderby><element>TX_</element><type>E</type><id></id><nameid>PF_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPF rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPF rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPF_NAME]]></value><valuecode><![CDATA[="rasdTxPF_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid><';
 v_vc(167) := '/rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>126</elementid><pelementid>125</pelementid><orderby>1</orderby><element>INPUT_TEXT</element><type>D</type><id>PF</id><nameid>PF</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextC <% if PPF#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PPF#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PPF#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PPF#SET(1).custom , instr(upper(PPF#SET(1).custom),''CLASS="'')+7 , instr(upper(PPF#SET(1).custom),''"'',instr(upper(PPF#SET(1).custom),''CLASS="'')+8)-instr(upper(PPF#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdTextC '');  if PPF#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PPF#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PPF#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PPF#SET(1).custom , instr(upper(PPF#SET(1).custom),''CLASS="'')+7 , instr(upper(PPF#SET(1).custom),''"'',instr(upper(PPF#SET(1).custom),''CLASS="'')+8)-instr(upper(PPF#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A';
 v_vc(168) := '_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PF_NAME_RASD]]></value><valuecode><![CDATA[="PF_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PF_NAME]]></value><valuecode><![CDATA[="PF_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_SIZE</attribute><type>A</type><text></text><name><![CDATA[size]]></name><value><![CDATA[10]]></value><valuecode><![CDATA[="10"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[text]]></value><valuecode><![CDATA[="text"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PF_VALUE]]></value><valuecode><![CDATA[="''||PPF(1)||''"]]></valuecode><forloop></forl';
 v_vc(169) := 'oop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PPF#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PPF#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PPF#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PPF#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PPF#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><val';
 v_vc(170) := 'ueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).error is not null then htp.p('' title="''||PPF#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PPF#SET(1).error is not null then htp.p('' title="''||PPF#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).info is not null then htp.p('' title="''||PPF#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PPF#SET(1).info is not null then htp.p('' title="''||PPF#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PPF#SET(1).visible then %><input]]></value><valuecode><![CDATA['');  if PPF#SET(1).visible then  
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>127</elementid><pelementid>32</pelementid><orderby>240</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSRC_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></nam';
 v_vc(171) := 'e><value><![CDATA[rasdTxPGBTNSRC rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGBTNSRC_NAME]]></value><valuecode><![CDATA[="rasdTxPGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>128</elementid><pelementid>127</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGBTNSRC</id><nameid>PGBTNSRC</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if PGBTNSRC#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PGBTNSRC#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PGBTNSRC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGBTNSRC#SET(1).custom , instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')+7 , instr(upper(PGBTNSRC#SET(1).custom),''"'',instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')+8)-instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></val';
 v_vc(172) := 'ue><valuecode><![CDATA[="rasdButton'');  if PGBTNSRC#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PGBTNSRC#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PGBTNSRC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGBTNSRC#SET(1).custom , instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')+7 , instr(upper(PGBTNSRC#SET(1).custom),''"'',instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')+8)-instr(upper(PGBTNSRC#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSRC_NAME_RASD]]></value><valuecode><![CDATA[="PGBTNSRC_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGBTNSRC_NAME]]></value><valuecode><![CDATA[="PGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><te';
 v_vc(173) := 'xt></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGBTNSRC_VALUE]]></value><valuecode><![CDATA[="''||PGBTNSRC(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><';
 v_vc(174) := 'textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGBTNSRC#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGBTNSRC#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).error is not null then htp.p('' title="''||PGBTNSRC#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).error is not null then htp.p('' title="''||PGBTNSRC#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).info is not null then htp.p('' title="''||PGBTNSRC#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).info is not null then htp.p('' title="''||PGBTNSRC#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></text';
 v_vc(175) := 'id><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGBTNSRC#SET(1).visible then %><input]]></value><valuecode><![CDATA['');  if PGBTNSRC#SET(1).visible then  
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>129</elementid><pelementid>32</pelementid><orderby>480</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBTNSAVE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGBTNSAVE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGBTNSAVE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGBTNSAVE_NAME]]></value><valuecode><![CDATA[="rasdTxPGBTNSAVE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></';
 v_vc(176) := 'rid></attribute></attributes></element><element><elementid>130</elementid><pelementid>129</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGBTNSAVE</id><nameid>PGBTNSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if PGBTNSAVE#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PGBTNSAVE#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PGBTNSAVE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGBTNSAVE#SET(1).custom , instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')+7 , instr(upper(PGBTNSAVE#SET(1).custom),''"'',instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')+8)-instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if PGBTNSAVE#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PGBTNSAVE#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PGBTNSAVE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGBTNSAVE#SET(1).custom , instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')+7 , instr(upper(PGBTNSAVE#SET(1).custom),''"'',instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')+8)-instr(upper(PGBTNSAVE#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBTNSAVE_NAME_RASD]]></value><valuecode><![CDATA[="PGBTNSAVE_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid>';
 v_vc(177) := '<textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGBTNSAVE_NAME]]></value><valuecode><![CDATA[="PGBTNSAVE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGBTNSAVE_VALUE]]></value><valuecode><![CDATA[="''||PGBTNSAVE(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobi';
 v_vc(178) := 'd></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGBTNSAVE#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGBTNSAVE#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).error is not null then htp.p('' title="''||PGBTNSAVE#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).error is not null then htp.p('' title="''||PGBTNSAVE#SET(1).error||''"''); end if;  
htp.';
 v_vc(179) := 'prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).info is not null then htp.p('' title="''||PGBTNSAVE#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).info is not null then htp.p('' title="''||PGBTNSAVE#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGBTNSAVE#SET(1).visible then %><input]]></value><valuecode><![CDATA['');  if PGBTNSAVE#SET(1).visible then  
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>131</elementid><pelementid>32</pelementid><orderby>500</orderby><element>TX_</element><type>E</type><id></id><nameid>PLDELETE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPLDELETE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPLDELETE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA';
 v_vc(180) := '[id]]></name><value><![CDATA[rasdTxPLDELETE_NAME]]></value><valuecode><![CDATA[="rasdTxPLDELETE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>132</elementid><pelementid>131</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>PLDELETE</id><nameid>PLDELETE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[rasdImg<% if PLDELETE#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PLDELETE#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PLDELETE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PLDELETE#SET(1).custom , instr(upper(PLDELETE#SET(1).custom),''CLASS="'')+7 , instr(upper(PLDELETE#SET(1).custom),''"'',instr(upper(PLDELETE#SET(1).custom),''CLASS="'')+8)-instr(upper(PLDELETE#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdImg'');  if PLDELETE#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PLDELETE#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PLDELETE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PLDELETE#SET(1).custom , instr(upper(PLDELETE#SET(1).custom),''CLASS="'')+7 , instr(upper(PLDELETE#SET(1).custom),''"'',instr(uppe';
 v_vc(181) := 'r(PLDELETE#SET(1).custom),''CLASS="'')+8)-instr(upper(PLDELETE#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PLDELETE_NAME_RASD]]></value><valuecode><![CDATA[="PLDELETE_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$deletelink]]></value><valuecode><![CDATA[="javascript: if (confirm(''''''|| GBUTTONDELETE||''?'''') == true) { window.rasd_CMEditorB10PLSQL_1_RASD.setValue(''''''''); document.RASDC2_CSSJS.ACTION.value=''''Save''''; document.RASDC2_CSSJS.submit();}"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><';
 v_vc(182) := 'type>A</type><text></text><name><![CDATA[SRC]]></name><value><![CDATA[PLDELETE_VALUE]]></value><valuecode><![CDATA[="''||PLDELETE(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  PLDELETE_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  PLDELETE(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]';
 v_vc(183) := '></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PLDELETE#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PLDELETE#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).error is not null then htp.p('' title="''||PLDELETE#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).error is not null then htp.p('' title="''||PLDELETE#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).info is not null then htp.p('' title="''||PLDELETE#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).info is not null then htp.p('' title="''||PLDELETE#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</IMG';
 v_vc(184) := '><% end if; %>]]></value><valuecode><![CDATA[</IMG>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PLDELETE#SET(1).visible then %><IMG]]></value><valuecode><![CDATA['');  if PLDELETE#SET(1).visible then  
htp.prn(''<IMG]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>133</elementid><pelementid>32</pelementid><orderby>520</orderby><element>TX_</element><type>E</type><id></id><nameid>PUPLOAD_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPUPLOAD rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPUPLOAD rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPUPLOAD_NAME]]></value><valuecode><![CDATA[="rasdTxPUPLOAD_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]';
 v_vc(185) := '></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>134</elementid><pelementid>133</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>PUPLOAD</id><nameid>PUPLOAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[rasdImg<% if PUPLOAD#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PUPLOAD#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PUPLOAD#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PUPLOAD#SET(1).custom , instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')+7 , instr(upper(PUPLOAD#SET(1).custom),''"'',instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')+8)-instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdImg'');  if PUPLOAD#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PUPLOAD#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PUPLOAD#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PUPLOAD#SET(1).custom , instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')+7 , instr(upper(PUPLOAD#SET(1).custom),''"'',instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')+8)-instr(upper(PUPLOAD#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PUPLOAD_NAME_RASD]]></value><valuecode><![CDATA[="PUPLOAD_1';
 v_vc(186) := '_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$uploadcust]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$uploadcust(PUPLOAD(1),''PUPLOAD_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><type>A</type><text></text><name><![CDATA[SRC]]></name><value><![CDATA[PUPLOAD_VALUE]]></value><valuecode><![CDATA[="''||PUPLOAD(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  PUPLOAD_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  PUPLOAD(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribu';
 v_vc(187) := 'te><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PUPLOAD#SET(1).custom); %>]]></value><valuecode><';
 v_vc(188) := '![CDATA['');  htp.p('' ''||PUPLOAD#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).error is not null then htp.p('' title="''||PUPLOAD#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).error is not null then htp.p('' title="''||PUPLOAD#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).info is not null then htp.p('' title="''||PUPLOAD#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).info is not null then htp.p('' title="''||PUPLOAD#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</IMG><% end if; %>]]></value><valuecode><![CDATA[</IMG>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PUPLOAD#SET(1).visible then %><IMG]]></value><valuecode><![CDATA['');  if PUPLOAD#SET(1).visible then  
htp.prn(''<IMG]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>135</elementid><pelementid>32</pelementid><orderby>620</orderby><element>TX_</element><type>E</type><id></id><nameid>PHINT_TX</nameid><endtagel';
 v_vc(189) := 'ementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPHINT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPHINT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPHINT_NAME]]></value><valuecode><![CDATA[="rasdTxPHINT_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>136</elementid><pelementid>135</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>PHINT</id><nameid>PHINT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PHINT_NAME_RASD]]></value><valuecode><![CDATA[="PHINT_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></r';
 v_vc(190) := 'id></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% PHINT_VALUE %>]]></value><valuecode><![CDATA['');  rasdc_hints.link(replace(this_form,''2'',''''), lang);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>137</elementid><pelementid>53</pelementid><orderby>22</orderby><element>TX_</element><type>E</type><id></id><nameid>PGBLOKPROZILECNOV_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGBLOKPROZILECNOV rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGBLOKPROZILECNOV rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGBLOKPROZILECNOV_NAME]]></value><valuecode><![CDATA[="rasdTxPGBLOKPROZILECNOV_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orde';
 v_vc(191) := 'rby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>138</elementid><pelementid>137</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>PGBLOKPROZILECNOV</id><nameid>PGBLOKPROZILECNOV</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGBLOKPROZILECNOV_NAME_RASD]]></value><valuecode><![CDATA[="PGBLOKPROZILECNOV_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</or';
 v_vc(192) := 'derby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGBLOKPROZILECNOV#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGBLOKPROZILECNOV#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).error is not null then htp.p('' title="''||PGBLOKPROZILECNOV#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).error is not null then htp.p('' title="''||PGBLOKPROZILECNOV#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).info is not null then htp.p('' title="''||PGBLOKPROZILECNOV#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).info is';
 v_vc(193) := ' not null then htp.p('' title="''||PGBLOKPROZILECNOV#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</span><% end if; %>]]></value><valuecode><![CDATA[</span>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGBLOKPROZILECNOV#SET(1).visible then %><span]]></value><valuecode><![CDATA['');  if PGBLOKPROZILECNOV#SET(1).visible then  
htp.prn(''<span]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% PGBLOKPROZILECNOV_VALUE %>]]></value><valuecode><![CDATA['');  htp.p(''<select name="PGBLOKPROZILECNOV_1" id="PGBLOKPROZILECNOV_X_1_RASD" class="rasdSelect " onchange="document.getElementById(''''ACTION_RASD'''').value=''''''||GBUTTONSRC||'''''';  document.''||this_form||''.submit() ;">'');
htp.p(pgoptions); --created in pre select B10g.
htp.p(''</select>'');  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>139</elementid><pelementid>53</pelementid><orderby>240</orderby><element>TX_</element><type>E</type><id></id><nameid>PGGBTNSRC_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGGBTNSRC rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGGBTNSRC rasdTxTypeC"]]></valuecod';
 v_vc(194) := 'e><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGGBTNSRC_NAME]]></value><valuecode><![CDATA[="rasdTxPGGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>140</elementid><pelementid>139</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGGBTNSRC</id><nameid>PGGBTNSRC</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if PGGBTNSRC#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PGGBTNSRC#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PGGBTNSRC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGGBTNSRC#SET(1).custom , instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')+7 , instr(upper(PGGBTNSRC#SET(1).custom),''"'',instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')+8)-instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if PGGBTNSRC#SET(1).error is not null then htp.p('' errorField''); ';
 v_vc(195) := 'end if;  
htp.prn('''');  if PGGBTNSRC#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PGGBTNSRC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGGBTNSRC#SET(1).custom , instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')+7 , instr(upper(PGGBTNSRC#SET(1).custom),''"'',instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')+8)-instr(upper(PGGBTNSRC#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGGBTNSRC_NAME_RASD]]></value><valuecode><![CDATA[="PGGBTNSRC_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGGBTNSRC_NAME]]></value><valuecode><![CDATA[="PGGBTNSRC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop><';
 v_vc(196) := '/forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGGBTNSRC_VALUE]]></value><valuecode><![CDATA[="''||PGGBTNSRC(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><';
 v_vc(197) := 'orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGGBTNSRC#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGGBTNSRC#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).error is not null then htp.p('' title="''||PGGBTNSRC#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).error is not null then htp.p('' title="''||PGGBTNSRC#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).info is not null then htp.p('' title="''||PGGBTNSRC#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).info is not null then htp.p('' title="''||PGGBTNSRC#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attrib';
 v_vc(198) := 'ute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGGBTNSRC#SET(1).visible then %><input]]></value><valuecode><![CDATA['');  if PGGBTNSRC#SET(1).visible then  
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>141</elementid><pelementid>53</pelementid><orderby>480</orderby><element>TX_</element><type>E</type><id></id><nameid>PGGBTNSAVE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGGBTNSAVE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGGBTNSAVE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGGBTNSAVE_NAME]]></value><valuecode><![CDATA[="rasdTxPGGBTNSAVE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>142</elem';
 v_vc(199) := 'entid><pelementid>141</pelementid><orderby>1</orderby><element>INPUT_SUBMIT</element><type>D</type><id>PGGBTNSAVE</id><nameid>PGGBTNSAVE</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdButton<% if PGGBTNSAVE#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PGGBTNSAVE#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PGGBTNSAVE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGGBTNSAVE#SET(1).custom , instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')+7 , instr(upper(PGGBTNSAVE#SET(1).custom),''"'',instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')+8)-instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdButton'');  if PGGBTNSAVE#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PGGBTNSAVE#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PGGBTNSAVE#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGGBTNSAVE#SET(1).custom , instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')+7 , instr(upper(PGGBTNSAVE#SET(1).custom),''"'',instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')+8)-instr(upper(PGGBTNSAVE#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGGBTNSAVE_NAME_RASD]]></value><valuecode><![CDATA[="PGGBTNSAVE_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></r';
 v_vc(200) := 'form><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[PGGBTNSAVE_NAME]]></value><valuecode><![CDATA[="PGGBTNSAVE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value><![CDATA[ ACTION.value=this.value; submit();]]></value><valuecode><![CDATA[=" ACTION.value=this.value; submit();"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[button]]></value><valuecode><![CDATA[="button"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[PGGBTNSAVE_VALUE]]></value><valuecode><![CDATA[="''||PGGBTNSAVE(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[/><% end if; %>]]></value><valuecode><![CDATA[/>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attri';
 v_vc(201) := 'bute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGGBTNSAVE#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGGBTNSAVE#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).error is not null then htp.p('' title="''||PGGBTNSAVE#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).error is not null then htp.p('' title="''||PGGBTNSAVE#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></f';
 v_vc(202) := 'orloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).info is not null then htp.p('' title="''||PGGBTNSAVE#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).info is not null then htp.p('' title="''||PGGBTNSAVE#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGGBTNSAVE#SET(1).visible then %><input]]></value><valuecode><![CDATA['');  if PGGBTNSAVE#SET(1).visible then  
htp.prn(''<input]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>143</elementid><pelementid>53</pelementid><orderby>520</orderby><element>TX_</element><type>E</type><id></id><nameid>PGUPLOAD_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGUPLOAD rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGUPLOAD rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDA';
 v_vc(203) := 'TA[rasdTxPGUPLOAD_NAME]]></value><valuecode><![CDATA[="rasdTxPGUPLOAD_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>144</elementid><pelementid>143</pelementid><orderby>1</orderby><element>IMG_</element><type>D</type><id>PGUPLOAD</id><nameid>PGUPLOAD</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>11</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[rasdImg<% if PGUPLOAD#SET(1).error is not null then htp.p('' errorField''); end if; %><% if PGUPLOAD#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(PGUPLOAD#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGUPLOAD#SET(1).custom , instr(upper(PGUPLOAD#SET(1).custom),''CLASS="'')+7 , instr(upper(PGUPLOAD#SET(1).custom),''"'',instr(upper(PGUPLOAD#SET(1).custom),''CLASS="'')+8)-instr(upper(PGUPLOAD#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdImg'');  if PGUPLOAD#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if PGUPLOAD#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(PGUPLOAD#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(PGUPLOAD#SET(1).custom , instr(upper(PGUPLOAD#SET(1).custom),''CLASS="'')+7 , instr(upper(PGUPLOAD#SET(1).custom),''"'',instr(upper(PGUPLOAD#SET(1).custom),';
 v_vc(204) := '''CLASS="'')+8)-instr(upper(PGUPLOAD#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>9</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[ID]]></name><value><![CDATA[PGUPLOAD_NAME_RASD]]></value><valuecode><![CDATA[="PGUPLOAD_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value><![CDATA[link$uploadgeneral]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$uploadgeneral(PGUPLOAD(1),''PGUPLOAD_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>A_SRC</attribute><type>A</type><text></text><name><![CDATA[SRC]]></nam';
 v_vc(205) := 'e><value><![CDATA[PGUPLOAD_VALUE]]></value><valuecode><![CDATA[="''||PGUPLOAD(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  PGUPLOAD_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  PGUPLOAD(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><';
 v_vc(206) := 'source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||PGUPLOAD#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||PGUPLOAD#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).error is not null then htp.p('' title="''||PGUPLOAD#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).error is not null then htp.p('' title="''||PGUPLOAD#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).info is not null then htp.p('' title="''||PGUPLOAD#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).info is not null then htp.p('' title="''||PGUPLOAD#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</IMG><% end if; %>]]></value><valuecode><![CDATA[</IMG>''';
 v_vc(207) := ');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if PGUPLOAD#SET(1).visible then %><IMG]]></value><valuecode><![CDATA['');  if PGUPLOAD#SET(1).visible then  
htp.prn(''<IMG]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>145</elementid><pelementid>53</pelementid><orderby>620</orderby><element>TX_</element><type>E</type><id></id><nameid>PGHINT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxPGHINT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxPGHINT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxPGHINT_NAME]]></value><valuecode><![CDATA[="rasdTxPGHINT_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><sourc';
 v_vc(208) := 'e></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>146</elementid><pelementid>145</pelementid><orderby>1</orderby><element>PLSQL_</element><type>D</type><id>PGHINT</id><nameid>PGHINT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[PGHINT_NAME_RASD]]></value><valuecode><![CDATA[="PGHINT_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% PGHINT_VALUE %>]]></value><valuecode><![CDATA['');  rasdc_hints.link(replace(this_form,''2'',''''), lang);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>147</elementid><pelementid>70</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B10</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</ord';
 v_vc(209) := 'erby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>148</elementid><pelementid>147</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RS</id><nam';
 v_vc(210) := 'eid>B10RS</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RS_NAME_RASD]]></value><valuecode><![CDATA[="B10RS_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RS_NAME]]></value><valuecode><![CDATA[="B10RS_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderb';
 v_vc(211) := 'y>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RS_VALUE]]></value><valuecode><![CDATA[="''||B10RS(1)||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>149</elementid><pelementid>79</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B10G</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></';
 v_vc(212) := 'endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10G_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>150</elementid><pelementid>149</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10GRS</id><nameid>B10GRS</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby';
 v_vc(213) := '><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10GRS_NAME_RASD]]></value><valuecode><![CDATA[="B10GRS_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10GRS_NAME]]></value><valuecode><![CDATA[="B10GRS_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10GRS_VALUE]]></value><valuecode><![CDATA[="''||B10GRS(1)||''"]]></valuecode><forloo';
 v_vc(214) := 'p></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>151</elementid><pelementid>70</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B10</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_V';
 v_vc(215) := 'ALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>152</elementid><pelementid>151</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10RID</id><nameid>B10RID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10RID_NAME_RASD]]></value><valuecode><![CDATA[="B10RID_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddeny';
 v_vc(216) := 'n>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10RID_NAME]]></value><valuecode><![CDATA[="B10RID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10RID_VALUE]]></value><valuecode><![CDATA[="''||to_char(B10RID(1))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</ty';
 v_vc(217) := 'pe><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>153</elementid><pelementid>79</pelementid><orderby>1</orderby><element>HIDDFIELD_</element><type></type><id></id><nameid>B10G</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>4</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[hiddenRowItems]]></value><valuecode><![CDATA[="hiddenRowItems"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10G_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10G_ROW]]></value><valuecode><![CDATA[=""]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[1]]></value><valuecode><![CDATA[="1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddeny';
 v_vc(218) := 'n>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>154</elementid><pelementid>153</pelementid><orderby>1</orderby><element>INPUT_HIDDEN</element><type>D</type><id>B10GRID</id><nameid>B10GRID</nameid><endtagelementid></endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[CLASS]]></name><value><![CDATA[HIDDEN]]></value><valuecode><![CDATA[="HIDDEN"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10GRID_NAME_RASD]]></value><valuecode><![CDATA[="B10GRID_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10GRID_NAME]]></value><valuecode><![CDATA[="B10GRID_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLI';
 v_vc(219) := 'CK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value><![CDATA[hidden]]></value><valuecode><![CDATA[="hidden"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value><![CDATA[B10GRID_VALUE]]></value><valuecode><![CDATA[="''||to_char(B10GRID(1))||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<input]]></name><value><![CDATA[/>]]></value><valuecode><![CDATA[/>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>155</elementid><pelementid>79</pelementid><orderby>2006</orderby><element>TX_</element><type>E</type><id></id><nameid>B10GBLOBVC_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rl';
 v_vc(220) := 'obid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10GBLOBVC rasdTxTypeL]]></value><valuecode><![CDATA[="rasdTxB10GBLOBVC rasdTxTypeL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10GBLOBVC_NAME]]></value><valuecode><![CDATA[="rasdTxB10GBLOBVC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>156</elementid><pelementid>155</pelementid><orderby>1</orderby><element>TEXTAREA_</element><type>D</type><id>B10GBLOBVC</id><nameid>B10GBLOBVC</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextarea <% if B10GBLOBVC#SET(1).error is not null then htp.p('' errorField''); end if; %><% if B10GBLOBVC#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10GBLOBVC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10GBLOBVC#SET(1).custom ';
 v_vc(221) := ', instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')+7 , instr(upper(B10GBLOBVC#SET(1).custom),''"'',instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')+8)-instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdTextarea '');  if B10GBLOBVC#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if B10GBLOBVC#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(B10GBLOBVC#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10GBLOBVC#SET(1).custom , instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')+7 , instr(upper(B10GBLOBVC#SET(1).custom),''"'',instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')+8)-instr(upper(B10GBLOBVC#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10GBLOBVC_NAME_RASD]]></value><valuecode><![CDATA[="B10GBLOBVC_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10GBLOBVC_NAME]]></value><valuecode><![CDATA[="B10GBLOBVC_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textco';
 v_vc(222) := 'de></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></va';
 v_vc(223) := 'lueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10GBLOBVC#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10GBLOBVC#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).error is not null then htp.p('' title="''||B10GBLOBVC#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).error is not null then htp.p('' title="''||B10GBLOBVC#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).info is not null then htp.p('' title="''||B10GBLOBVC#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).info is not null then htp.p('' title="''||B10GBLOBVC#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</textarea><% end if; %>]]></value><valuecode><![CDATA[</textarea>'');  end if;  
htp';
 v_vc(224) := '.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10GBLOBVC#SET(1).visible then %><textarea]]></value><valuecode><![CDATA['');  if B10GBLOBVC#SET(1).visible then  
htp.prn(''<textarea]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% htpClob( ''B10GBLOBVC_VALUE'' ); %>]]></value><valuecode><![CDATA['');  htpClob( ''''||B10GBLOBVC(1)||'''' );  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>157</elementid><pelementid>70</pelementid><orderby>200036</orderby><element>TX_</element><type>E</type><id></id><nameid>B10PLSQL_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB10PLSQL rasdTxTypeL]]></value><valuecode><![CDATA[="rasdTxB10PLSQL rasdTxTypeL"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB10PLSQL_NAME]]></value><valuecode><![CDATA[="rasdTxB10PLSQL_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value>';
 v_vc(225) := '<valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>158</elementid><pelementid>157</pelementid><orderby>1</orderby><element>TEXTAREA_</element><type>D</type><id>B10PLSQL</id><nameid>B10PLSQL</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis>Y</includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTextarea <% if B10PLSQL#SET(1).error is not null then htp.p('' errorField''); end if; %><% if B10PLSQL#SET(1).info is not null then htp.p('' infoField''); end if; %><% if instr(upper(B10PLSQL#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PLSQL#SET(1).custom , instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')+7 , instr(upper(B10PLSQL#SET(1).custom),''"'',instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')+8)-instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')-7) ); end if; %>]]></value><valuecode><![CDATA[="rasdTextarea '');  if B10PLSQL#SET(1).error is not null then htp.p('' errorField''); end if;  
htp.prn('''');  if B10PLSQL#SET(1).info is not null then htp.p('' infoField''); end if;  
htp.prn('''');  if instr(upper(B10PLSQL#SET(1).custom) ,''CLASS="'') > 0 then  htp.p('' ''||substr(B10PLSQL#SET(1).custom , instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')+7 , instr(upper(B10PLSQL#SET(1).custom),''"'',instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')+8)-instr(upper(B10PLSQL#SET(1).custom),''CLASS="'')-7) ); end if;  
htp.prn(''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]>';
 v_vc(226) := '</name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B10PLSQL_NAME_RASD]]></value><valuecode><![CDATA[="B10PLSQL_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B10PLSQL_NAME]]></value><valuecode><![CDATA[="B10PLSQL_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid>';
 v_vc(227) := '</rid></attribute><attribute><orderby>999</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>980</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).readonly then htp.p('' readonly="readonly"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).readonly then htp.p('' readonly="readonly"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>981</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).disabled then htp.p('' disabled="disabled"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).disabled then htp.p('' disabled="disabled"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>982</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).required then htp.p('' required="required"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).required then htp.p('' required="required"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>983</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% htp.p('' ''||B10PLSQL#SET(1).custom); %>]]></value><valuecode><![CDATA['');  htp.p('' ''||B10PLSQL#SET(1).custom);  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>984</orderby><attribute>C_<';
 v_vc(228) := '/attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).error is not null then htp.p('' title="''||B10PLSQL#SET(1).error||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).error is not null then htp.p('' title="''||B10PLSQL#SET(1).error||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>985</orderby><attribute>C_</attribute><type>C</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).info is not null then htp.p('' title="''||B10PLSQL#SET(1).info||''"''); end if; %>]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).info is not null then htp.p('' title="''||B10PLSQL#SET(1).info||''"''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name></name><value><![CDATA[</textarea><% end if; %>]]></value><valuecode><![CDATA[</textarea>'');  end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name></name><value><![CDATA[<% if B10PLSQL#SET(1).visible then %><textarea]]></value><valuecode><![CDATA['');  if B10PLSQL#SET(1).visible then  
htp.prn(''<textarea]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[<% htpClob( ''B10PLSQL_VALUE'' ); %>]]></value><valuecode><![CDATA['');  htpClob( ''''||B10PLSQL(1)||'''' );  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><ele';
 v_vc(229) := 'mentid>159</elementid><pelementid>88</pelementid><orderby>1999</orderby><element>TX_</element><type>E</type><id></id><nameid>B20TEMPLATE_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20TEMPLATE]]></value><valuecode><![CDATA[="rasdTxLabB20TEMPLATE"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>160</elementid><pelementid>159</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B20TEMPLATE_LAB</id><nameid>B20TEMPLATE_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]';
 v_vc(230) := ']></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20TEMPLATE_LAB]]></value><valuecode><![CDATA[="B20TEMPLATE_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>161</elementid><pelementid>87</pelementid><orderby>2002</orderby><element>TR_</element><type>B</type><id></id><nameid>B20_BLOCK2002</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S';
 v_vc(231) := '_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>162</elementid><pelementid>88</pelementid><orderby>2000</orderby><element>TX_</element><type>E</type><id></id><nameid>B20TEMPLATE_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20TEMPLATE rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20TEMPLATE rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20TEMPLATE_NAME]]></value><valuecode><![CDATA[="rasdTxB20TEMPLATE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>163</elementid><pelementid>162</pelementid><orderby>1</orderby><element>A_</element><type>D</type><id>B20TEMPLATE</id><nameid>B20TEM';
 v_vc(232) := 'PLATE</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[href]]></name><value><![CDATA[link$template]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$template(B20TEMPLATE(1),''B20TEMPLATE_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20TEMPLATE_NAME_RASD]]></value><valuecode><![CDATA[="B20TEMPLATE_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B20TEMPLATE_NAME]]></value><valuecode><![CDATA[="B20TEMPLATE_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B20TEMPLATE_VALUE is not null and '''' is not null  then';
 v_vc(233) := ' htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  B20TEMPLATE(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20TEMPLATE_VALUE]]></value><valuecode><![CDATA[''||B20TEMPLATE(1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>164</elementid><pelementid>161</pelementid><orderby>2019</orderby><element>TX_</element><type>E</type><id></id><nameid>B20EDITOR_TX_LAB</nameid><endtagelementid>0</endtagelementid><sourc';
 v_vc(234) := 'e></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20EDITOR]]></value><valuecode><![CDATA[="rasdTxLabB20EDITOR"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>165</elementid><pelementid>164</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B20EDITOR_LAB</id><nameid>B20EDITOR_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute';
 v_vc(235) := '><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20EDITOR_LAB]]></value><valuecode><![CDATA[="B20EDITOR_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>166</elementid><pelementid>87</pelementid><orderby>2022</orderby><element>TR_</element><type>B</type><id></id><nameid>B20_BLOCK2022</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hidden';
 v_vc(236) := 'yn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>167</elementid><pelementid>161</pelementid><orderby>2020</orderby><element>TX_</element><type>E</type><id></id><nameid>B20EDITOR_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20EDITOR rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20EDITOR rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20EDITOR_NAME]]></value><valuecode><![CDATA[="rasdTxB20EDITOR_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>168</elementid><pelementid>167</pelementid><orderby>1</orderby><element>A_</element><type>D</type><id>B20EDITOR</id><nameid>B20EDITOR</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_HREF</a';
 v_vc(237) := 'ttribute><type>A</type><text></text><name><![CDATA[href]]></name><value><![CDATA[link$editor]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$editor(B20EDITOR(1),''B20EDITOR_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,width=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20EDITOR_NAME_RASD]]></value><valuecode><![CDATA[="B20EDITOR_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B20EDITOR_NAME]]></value><valuecode><![CDATA[="B20EDITOR_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B20EDITOR_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  B20EDITOR(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hidd';
 v_vc(238) := 'enyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20EDITOR_VALUE]]></value><valuecode><![CDATA[''||B20EDITOR(1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>169</elementid><pelementid>166</pelementid><orderby>2039</orderby><element>TX_</element><type>E</type><id></id><nameid>B20REGEX_TX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDAT';
 v_vc(239) := 'A[rasdTxLab rasdTxLabBlockB20]]></value><valuecode><![CDATA[="rasdTxLab rasdTxLabBlockB20"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxLabB20REGEX]]></value><valuecode><![CDATA[="rasdTxLabB20REGEX"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>170</elementid><pelementid>169</pelementid><orderby>1</orderby><element>FONT_</element><type>L</type><id>B20REGEX_LAB</id><nameid>B20REGEX_LAB</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>3</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[label]]></value><valuecode><![CDATA[="label"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20REGEX_LAB]]></value><valuecode><![CDATA[="B20REGEX_LAB"]]></valuecode><forloop></forloop><endloop></endloop><source>';
 v_vc(240) := '</source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<span]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>171</elementid><pelementid>87</pelementid><orderby>2042</orderby><element>TR_</element><type>B</type><id></id><nameid>B20_BLOCK2042</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<tr]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>172</elementid><pelementid>166</pelementid><orderby>2040</orderby><element>TX_</element><typ';
 v_vc(241) := 'e>E</type><id></id><nameid>B20REGEX_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB20REGEX rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB20REGEX rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB20REGEX_NAME]]></value><valuecode><![CDATA[="rasdTxB20REGEX_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>173</elementid><pelementid>172</pelementid><orderby>1</orderby><element>A_</element><type>D</type><id>B20REGEX</id><nameid>B20REGEX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[href]]></name><value><![CDATA[link$regex]]></value><valuecode><![CDATA[="javascript: var link=window.open(encodeURI(''); js_link$regex(B20REGEX(1),''B20REGEX_1''); 
htp.prn(''),''''x1'''',''''resizable,scrollbars,wid';
 v_vc(242) := 'th=680,height=550'''');"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B20REGEX_NAME_RASD]]></value><valuecode><![CDATA[="B20REGEX_1_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[name]]></name><value><![CDATA[B20REGEX_NAME]]></value><valuecode><![CDATA[="B20REGEX_1"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[onclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ondblclick]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_TITLE</attribute><type>M</type><text></text><name></name><value><![CDATA[<% if  B20REGEX_VALUE is not null and '''' is not null  then htp.prn(''title=""''); end if; %>]]></value><valuecode><![CDATA['');  if  B20REGEX(1) is not null and '''' is not null  then htp.prn(''title=""''); end if;  
htp.prn('']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[type]]></name><value></value><value';
 v_vc(243) := 'code></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[value]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>13</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<a]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>12</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B20REGEX_VALUE]]></value><valuecode><![CDATA[''||B20REGEX(1)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>174</elementid><pelementid>93</pelementid><orderby>20000</orderby><element>TX_</element><type>E</type><id></id><nameid>B30TEXT_TX</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>2</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdTxB30TEXT rasdTxTypeC]]></value><valuecode><![CDATA[="rasdTxB30TEXT rasdTxTypeC"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></';
 v_vc(244) := 'rid></attribute><attribute><orderby>3</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[rasdTxB30TEXT_NAME]]></value><valuecode><![CDATA[="rasdTxB30TEXT_''||iB30||''"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type><text></text><name><![CDATA[<td]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element><element><elementid>175</elementid><pelementid>174</pelementid><orderby>1</orderby><element>FONT_</element><type>D</type><id>B30TEXT</id><nameid>B30TEXT</nameid><endtagelementid>0</endtagelementid><source></source><hiddenyn></hiddenyn><rlobid></rlobid><rform></rform><rid></rid><includevis></includevis><attributes><attribute><orderby>9</orderby><attribute>A_CLASS</attribute><type>A</type><text></text><name><![CDATA[class]]></name><value><![CDATA[rasdFont]]></value><valuecode><![CDATA[="rasdFont"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>2</orderby><attribute>A_HREF</attribute><type>A</type><text></text><name><![CDATA[HREF]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>6</orderby><attribute>A_ID</attribute><type>A</type><text></text><name><![CDATA[id]]></name><value><![CDATA[B30TEXT_NAME_RASD]]></value><valuecode><![CDATA[="';
 v_vc(245) := 'B30TEXT_''||iB30||''_RASD"]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>5</orderby><attribute>A_NAME</attribute><type>A</type><text></text><name><![CDATA[NAME]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>3</orderby><attribute>A_ONCLICK</attribute><type>A</type><text></text><name><![CDATA[ONCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>4</orderby><attribute>A_ONDBLCLICK</attribute><type>A</type><text></text><name><![CDATA[ONDBLCLICK]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>7</orderby><attribute>A_TYPE</attribute><type>A</type><text></text><name><![CDATA[TYPE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>8</orderby><attribute>A_VALUE</attribute><type>A</type><text></text><name><![CDATA[VALUE]]></name><value></value><valuecode></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>Y</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>11</orderby><attribute>E_</attribute><type>E</type><text></text><name><![CDATA[</font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>1</orderby><attribute>S_</attribute><type>S</type';
 v_vc(246) := '><text></text><name><![CDATA[<font]]></name><value><![CDATA[>]]></value><valuecode><![CDATA[>]]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute><attribute><orderby>10</orderby><attribute>V_</attribute><type>V</type><text></text><name></name><value><![CDATA[B30TEXT_VALUE]]></value><valuecode><![CDATA[''||B30TEXT(iB30)||'']]></valuecode><forloop></forloop><endloop></endloop><source></source><hiddenyn>N</hiddenyn><valueid></valueid><textid></textid><textcode></textcode><rlobid></rlobid><rform></rform><rid></rid></attribute></attributes></element></elements></form>';
     return v_vc;
  end;
function metadata return clob is
  v_clob clob := '';
  v_vc cctab;
  begin
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       v_clob := v_clob || v_vc(i);
     end loop;
     return v_clob;
  end;
procedure metadata is
  v_clob clob := '';
  v_vc cctab;
  begin
  owa_util.mime_header('text/xml', FALSE);
  HTP.p('Content-Disposition: filename="Export_RASDC2_CSSJS_v.1.1.20231101005325.xml"');
  owa_util.http_header_close;
  htp.p('<?xml version="1.0" encoding="UTF-8" ?>');
     v_vc := metadata_xml;
     for i in 1..v_vc.count loop
       htp.prn(v_vc(i));
     end loop;
  end;
     begin
       null;
  -- initialization part

end RASDC2_CSSJS;
/
