create or replace package rasdi_client IS
  /*
  // +----------------------------------------------------------------------+
  // | RASD - Rapid Application Service Development                         |
  // +----------------------------------------------------------------------+
  // | Copyright (C) 2014       http://rasd.sourceforge.net                 |
  // +----------------------------------------------------------------------+
  // | This program is free software; you can redistribute it and/or modify |
  // | it under the terms of the GNU General Public License as published by |
  // | the Free Software Foundation; either version 2 of the License, or    |
  // | (at your option) any later version.                                  |
  // |                                                                      |
  // | This program is distributed in the hope that it will be useful       |
  // | but WITHOUT ANY WARRANTY; without even the implied warranty of       |
  // | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         |
  // | GNU General Public License for more details.                         |
  // +----------------------------------------------------------------------+
  // | Author: Domen Dolar       <domendolar@users.sourceforge.net>         |
  // +----------------------------------------------------------------------+
  */
  function version(p_log out varchar2) return varchar2;

  type rtab is table of rowid index by binary_integer;
  type ntab is table of number index by binary_integer;
  type dtab is table of date index by binary_integer;
  type ctab is table of varchar2(4000) index by binary_integer;
  type cctab is table of varchar2(32000) index by binary_integer;
  type itab is table of pls_integer index by binary_integer;

  e_finished exception;

  
  /* do not change*/ C_DATE_FORMAT    constant varchar2(10) := 'mm/dd/yyyy';
  /* do not change*/ C_DATE_TIME_FORMAT    constant varchar2(30) := 'mm/dd/yyyy hh24:mi:ss';
  /* do not change*/ C_TIMESTAMP_FORMAT constant varchar2(30) := 'mm/dd/yyyy hh24:mi:ss.ff';
  /* do not change*/ C_NUMBER_DECIMAL constant varchar2(1) := ',';
  /* do not change*/ C_NUMBER_THOUSAND constant varchar2(1) := '.';
  
  type dt is varray(2) of varchar2(30);
  type dttab is table of dt;
  c_date_transform constant dttab := dttab(
      -- PL/SQL type , jQuery type : format is case sensitive
      dt('yyyy-mm-dd','yy-mm-dd') ,
      dt('yyyy/mm/dd','yy/mm/dd') ,
      dt('dd.mm.yyyy','dd.mm.yy') ,
      dt('dd/mm/yyyy', 'dd/mm/yy') ,
      dt('dd-mm-yyyy', 'dd-mm-yy') ,
      dt('mm/dd/yyyy', 'mm/dd/yy') ,
      -- default type NULL must always be the last element
      dt('NULL','mm/dd/yy')
  );
  type dtt is varray(2) of varchar2(30);
  type dtttab is table of dtt;
  c_time_transform constant dtttab := dtttab(
      -- PL/SQL type , jQuery type : ime format is case sensitive
      dtt('hh24:mi','hh:mm') ,
      dtt('hh24:mi:ss','hh:mm:ss'),
      -- default type NULL must always be the last element
      dtt('NULL','hh:mm')
  );
    
  c_registerTo varchar2(100) := 'place your enviorment in RASDI_CLIENT - c_registerTo'; 
  c_defaultLanguage      varchar2(30) := 'EN'; -- translates are in table rasd_translates
  c_WalletLocation       varchar2(50) := 'file:/u01/../wallet';
  c_WalletPwd            varchar2(50) := 'pwd';
  c_GITLocation          varchar2(100) := '';


  c_HtmlJSLibraryFile varchar2(30) := 'rasd/rasd_jslib.html';
  c_HtmlDataTableFile varchar2(30) := 'rasd/rasd_datatable.html';
  c_HtmlDataTable2File varchar2(30) := 'rasd/rasd_datatable2.html';
  c_HtmlHeaderFile  varchar2(30) := 'rasd/rasd_header.html';
  c_HtmlHtmlDatePickerFile varchar2(30) := 'rasd/rasd_datepicker.html';
  c_HtmlFooterFile varchar2(30) := 'rasd/rasd_footer.html';
  
  function varchr2number(p_value varchar2) return number;


  /*Security functions*/
  function secSuperUsers return varchar2;
  function secGetUsername return varchar2;
  function secGetLOB return varchar2;
  procedure secCheckPermission(p_form varchar2, p_action varchar2);
  procedure secCheckCredentials(p_username varchar2,
                                p_password varchar2,
                                p_other    varchar2 default null);

  procedure sessionStart;
  procedure sessionSetValue(pname varchar2, pvalue varchar2);
  function  sessionGetValue(pname varchar2) return varchar2;
  procedure sessionClose;

  --New RASD
  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2;
  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlDataTable (name varchar2, value varchar2 default '') return varchar2;
  function getHtmlMenuList(p_formname varchar2) return varchar2;
  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2;
  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2;

  /*Security functions new RASD*/
  function secGet_HTTP_ACCEPT return varchar2;
  function secGet_HTTP_ACCEPT_ENCODING return varchar2;
  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2;
  function secGet_HTTP_ACCEPT_CHARSET return varchar2;
  function secGet_HTTP_HOST return varchar2;
  function secGet_HTTP_PORT return varchar2;
  function secGet_HTTP_USER_AGENT return varchar2;
  function secGet_PATH_INFO return varchar2;
  function secGet_PATH_ALIAS return varchar2;
  function secGet_REMOTE_ADDR return varchar2;
  function secGet_REQUEST_CHARSET return varchar2;
  function secGet_SCRIPT_NAME return varchar2;  
  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr);
    

  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 );

end;
/
create or replace package body rasdi_client is
  /*
  // +----------------------------------------------------------------------+
  // | RASD - Rapid Application Service Development                         |
  // +----------------------------------------------------------------------+
  // | Copyright (C) 2014       http://rasd.sourceforge.net                 |
  // +----------------------------------------------------------------------+
  // | This program is free software; you can redistribute it and/or modify |
  // | it under the terms of the GNU General Public License as published by |
  // | the Free Software Foundation; either version 2 of the License, or    |
  // | (at your option) any later version.                                  |
  // |                                                                      |
  // | This program is distributed in the hope that it will be useful       |
  // | but WITHOUT ANY WARRANTY; without even the implied warranty of       |
  // | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         |
  // | GNU General Public License for more details.                         |
  // +----------------------------------------------------------------------+
  // | Author: Domen Dolar       <domendolar@users.sourceforge.net>         |
  // +----------------------------------------------------------------------+
  */

  function version(p_log out varchar2) return varchar2 is
  begin
    p_log := '/* Change LOG:
20180321 - Added default language    
20171201 - Added C_DATE_TIME_FORMAT        
20171019 - Added suport for ORDS - reading DAD values    
20151202 - Added session functions    
20150814 - Added superuser
*/';
    return 'v.1.20180321225530';

  end;

  function getDAD return varchar2 is
    vdad varchar2(1000);
  begin
    vdad := OWA_UTIL.get_cgi_env('DAD_NAME');
    if vdad is null then
      vdad := upper(OWA_UTIL.get_cgi_env('SCRIPT_NAME'));
      vdad := substr( replace (upper(vdad) , '/ORDS/' , '') , 1 , instr(replace (upper(vdad) , '/ORDS/' , ''),'/')-1);
    end if;
    
    return vdad;
  end;  


 function varchr2number(p_value varchar2) return number is
    v_dot   number;
    v_comma number;
    v_value varchar2(30) := p_value;
    error exception;
  begin
    if p_value is not null then
      v_dot := instr(v_value, C_NUMBER_THOUSAND);
      if v_dot = 0 then
        return to_number(v_value);
      else
        while v_dot <> 0 Loop
          v_value := substr(v_value, v_dot + 1);
          v_dot   := instr(v_value, C_NUMBER_THOUSAND);
          v_comma := instr(v_value, C_NUMBER_DECIMAL);
          if v_dot <> 0 then
            if v_dot <> 4 then
              raise error;
            end if;
          else
            
            if v_comma <> 0 then
              if v_comma <> 4 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '')));
              end if;
            else
              if length(v_value) <> 3 then
                raise error;
              else
                return(to_number(replace(p_value, C_NUMBER_THOUSAND, '')));
              end if;
            end if;
          end if;
        end loop;
      end if;
    else
      return(to_number(null));
    end if;
  end;


  function getHtmlMenuList(p_formname varchar2) return varchar2 is
    v_menu varchar2(1000);
  begin

    v_menu := '<ul class="menu">
      <li><a href=""><span></span></a></li>
              </ul>';

    return v_menu;
  end;

  /*Security functions*/
  function secSuperUsers return varchar2 is
  begin
    /*Your code. Usernamemes of super users separated with ,. They can only view the code.*/
    return '';
    /*---------*/
  end;  

  function secGetUsername return varchar2 is
    vc OWA_COOKIE.cookie;
  begin
    /*Your code. Username is from cgi enviorment and it is based on you DADs configutration.*/
    vc := owa_cookie.get('LOGON');
    return vc.vals(1);
    /*---------*/
  exception when others then  
      raise_application_error('-20000', 'Login COOKIE does not exists!');     
  end;

  function secGetLOB return varchar2 is
    vc   OWA_COOKIE.cookie;
    vdad varchar2(1000);
    vlob varchar2(1000);
  begin
    /*Your code. LineOfBusiness is from cgi enviorment and it is based on you DADs configutration.*/
    /*Here you can return custom input lob or you can always return DADs*/
    vdad := getDAD;
    vc   := owa_cookie.get('LOGONLOB');
    vlob := vc.vals(1);

    return vlob;
    /*---------*/
  end;

  procedure secCheckPermission(p_form varchar2, p_action varchar2) is
         vcv varchar2(100);
         vuser varchar2(100);
         vlob varchar2(100);    
  begin
    /*Your code.*/
    /*
    procedure preveri_privilegije(p_id number) is
       n number;
       --p_id -> ID FORME KI KONTROLIRAMO ?E IMA editor PRAVICE ZA NJO!!!
       venota RASD_PRVS_LOB.lobid%type;
       vuname  RASD_FORMS_COMPILED.editor%type;
      begin
        venota := rasd_client.secGetLOB;
        vuname  := rasd_client.secGetUsername;

        if venota is not null and vuname is not null then
        select count(*) into n --id od forme
        from RASD_FORMS_COMPILED fg
        where fg.formid = p_id
          and fg.editor = vuname
          and fg.lobid = venota;
        else
         n := 0;
        end if;

        if n > 0 then return; end if;

        owa_util.redirect_url('!rasdc_security.logon');
      end;
    */

    declare
      vc OWA_COOKIE.cookie;      
    begin
      vc := owa_cookie.get('LOGON');
      vuser := vc.vals(1);
      if vc.name is null then
          raise_application_error('-20000', 'No privileges');
      end if;
    exception when others then
      vuser := ''; 
    end;    

    declare
      vc OWA_COOKIE.cookie;      
    begin
      vc := owa_cookie.get('LOGONLOB');
      vlob := vc.vals(1);
    exception when others then
      vlob := ''; 
    end;     

  --  if nvl(vuser,'.') <> nvl(vcv,',') then
  --    OWA_COOKIE.SEND('LOGON', vcv, null, '/' , '');
  --    OWA_COOKIE.SEND('LOGONLOB', upper(getDAD) , null, '/' , '');
  --    OWA_UTIL.REDIRECT_URL(rasdc_security.C_starturl);
  --  end if;
  ----  OWA_UTIL.REDIRECT_URL(rasdc_security.C_login );      
      
      /*---------*/
  end;

  procedure secCheckCredentials(p_username varchar2,
                                p_password varchar2,
                                p_other    varchar2 default null) is
  begin
    /*Your code.*/
    if false is null then
      raise_application_error('-20000', 'Invalid user logon credentials!');
    end if;
    /*---------*/
  end;


  procedure sessionStart is
  begin
    owa_util.mime_header('text/html', FALSE);
  end;

  procedure sessionSetValue(pname varchar2, pvalue varchar2) is
  begin
    OWA_COOKIE.SEND('rasdi$'||pname, pvalue , null,null);
  end;

  function sessionGetValue(pname varchar2) return varchar2 is
     vc OWA_COOKIE.cookie;
  begin
     vc := owa_cookie.get('rasdi$'||pname);
     return vc.vals(1);
  exception when others then
    return '';
  end;

  procedure sessionclose is
  begin
     owa_util.http_header_close;
  end;

--- New version of RASD
  function Blob2Clob(B BLOB) 
return clob is 
c clob;
n number;
v varchar2(32767 CHAR); 
begin 
if (b is null) then 
return null;
end if;
if (length(b)=0) then
return empty_clob(); 
end if;
dbms_lob.createtemporary(c,true);
n:=1;

while (n+32767<=length(b)) loop
dbms_lob.writeappend(c,32767,utl_raw.cast_to_varchar2(dbms_lob.substr(b,32767,n)));
n:=n+32767;
end loop;
v := utl_raw.cast_to_varchar2(dbms_lob.substr(b,length(b)-n+1,n));

dbms_lob.writeappend(c,length(v),v);
return c;
end;

  function getHtmlJSLibrary (name varchar2, value varchar2) return varchar2 is
    v_t varchar(32000);
    v_root varchar2(200);
    x blob;
  begin
    v_root := OWA_UTIL.get_cgi_env('DOC_ACCESS_PATH');
  
    select blob_content into x
    from rasd_files where name = c_HtmlJSLibraryFile;
    v_t := Blob2Clob(x);
    
   -- if v_root is null then
   --    v_t := replace(v_t,'docs/',c_DOC_ACCESS_PATH||'/');
   -- end if;    
    
    return replace(replace(v_t, ':NAME', name),':VALUE',value);
    exception when others then return '';
  end;

  function getHtmlHeaderDataTable (name varchar2, value varchar2 default '') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from rasd_files where name = c_HtmlHeaderFile;
    v_t := Blob2Clob(x);
    select blob_content into x
    from rasd_files where name = c_HtmlDataTable2File;
    v_t := v_t || Blob2Clob(x);

    v_t := replace(v_t , ':LOGOUT' , '(<a href="!rasdc_security.logout">logout</a>)');
    v_t := replace(v_t , ':USER' , rasdi_client.secGetUsername);
    v_t := replace(v_t , ':LOB' , rasdi_client.secGetLOB);
    v_t := replace(v_t , ':VALUE' , value);

    return replace(v_t, ':NAME', name);
    exception when others then return '';
  end;

  function getHtmlDataTable (name varchar2, value varchar2 default '') return varchar2 is
    v_t varchar(32000);
    x blob;
 begin
    select blob_content into x
    from rasd_files where name = c_HtmlDataTableFile;
    v_t := Blob2Clob(x);

    v_t := replace(v_t , ':LOGOUT' , '(<a href="!rasdc_security.logout">logout</a>)');
    v_t := replace(v_t , ':USER' , rasdi_client.secGetUsername);
    v_t := replace(v_t , ':LOB' , rasdi_client.secGetLOB);
    v_t := replace(v_t , ':VALUE' , value);

    return replace(v_t, ':NAME', name);
    exception when others then return '';
  end;

  function getHtmlDatePicker (name varchar2, format varchar2) return varchar2 is
     v_t varchar(32000);
    v_fd varchar2(100);
    v_ft varchar2(100);
    v_p varchar2(1000);
    v_format varchar2(1000) := format;
    x blob;
  begin
    if v_format is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';
    else
      FOR i IN c_date_transform.FIRST .. c_date_transform.LAST
      LOOP
        if instr( nvl(v_format,'NULL') , c_date_transform(i)(1)) > 0 then
           v_fd := c_date_transform(i)(2);
           v_format := replace(v_format, c_date_transform(i)(1) , '');
           exit;
        end if;   
      END LOOP;
      if v_fd is null then
        v_fd := c_date_transform(c_date_transform.LAST)(2);
      end if;
      if v_format is not null then
      FOR i IN c_time_transform.FIRST .. c_time_transform.LAST
      LOOP
        if instr( nvl(v_format,'NULL') , c_time_transform(i)(1)) > 0 then
           v_ft := c_time_transform(i)(2);
           v_format := replace(v_format, c_time_transform(i)(1) , '');
           exit;
        end if;   
      END LOOP;
      if v_ft is null then
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';        
      end if;
      if trim(v_format) is not null then 
        v_fd := c_date_transform(c_date_transform.LAST)(2);
        v_ft := c_time_transform(c_time_transform.LAST)(2);
        v_p := ', showTimepicker: false';           
      end if;  
      else
        v_p := ', showTimepicker: false';                   
      end if;    
    end if;
       
    select blob_content into x 
    from rasd_files where name = c_HtmlHtmlDatePickerFile;
        v_t := Blob2Clob(x);
    return replace(replace(replace(replace(v_t, ':NAME', name), ':DFORMAT', v_fd),':TFORMAT', v_ft),':PROPERTIES', v_p);
  exception when others then return '';
  end;  

  function getHtmlFooter (version varchar2, program varchar2 , user varchar2) return varchar2 is
    v_t varchar(32000);
    x blob;
  begin
    select blob_content into x 
    from rasd_files where name = c_HtmlFooterFile;
        v_t := Blob2Clob(x);

    return replace(replace(replace(v_t, ':VERSION', version), ':PROGRAM', program),':LICENCE', c_registerTo);
    exception when others then return '';
  end; 


function secGet_HTTP_ACCEPT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT'); --HTTP_ACCEPT = text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8
  end;

  function secGet_HTTP_ACCEPT_ENCODING return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_ENCODING'); --HTTP_ACCEPT_ENCODING = gzip, deflate
  end;

  function secGet_HTTP_ACCEPT_LANGUAGE return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_LANGUAGE'); --HTTP_ACCEPT_LANGUAGE = en,sl;q=0.9,en-GB;q=0.8
  end;

  function secGet_HTTP_ACCEPT_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_ACCEPT_CHARSET'); --HTTP_ACCEPT_CHARSET =
  end;

  function secGet_HTTP_HOST return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_HOST'); --HTTP_HOST = was-test.zpiz.si:9081
  end;

  function secGet_HTTP_PORT return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('HTTP_PORT'); --HTTP_PORT = 9081
  end;

  function secGet_HTTP_USER_AGENT return varchar2 is
  begin
     return  OWA_UTIL.get_cgi_env('HTTP_USER_AGENT'); --HTTP_USER_AGENT = Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36
  end;

  function secGet_PATH_INFO return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('PATH_INFO'); --
  end;

  function secGet_PATH_ALIAS return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('PATH_ALIAS'); --
  end;
  
  function secGet_REMOTE_ADDR return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('REMOTE_ADDR'); --
  end;
  
  function secGet_REQUEST_CHARSET return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('REQUEST_CHARSET'); --
  end;
  
  function secGet_SCRIPT_NAME return varchar2 is
  begin
     return OWA_UTIL.get_cgi_env('SCRIPT_NAME'); --
  end; 

  procedure secCheckCredentials(  name_array  in owa.vc_arr, value_array in owa.vc_arr ) is
    /*Your code.*/
    vu varchar2(111);
    vp varchar2(222);
    vo varchar2(333);
    vl varchar2(32000);
    vl1 varchar2(32000);
    vcv varchar2(444);
  begin
    declare
      vc OWA_COOKIE.cookie;
    begin
      vc := owa_cookie.get('SECCLIENTUSER');
      vcv := vc.vals(1);
    exception when others then
      vcv := '';
    end;

    if vcv is null then

    for i__ in 1 .. nvl(name_array.count, 0) loop
      if    upper(name_array(i__)) = upper('SECCLIENTUSER') then
        vu := value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTPWD') then
        vp := value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTOTHER') then
        vo:= value_array(i__);
      elsif upper(name_array(i__)) = upper('SECCLIENTURL') then
        vl:= value_array(i__);
      end if;
      vl1 := vl1||'&'||name_array(i__)||'='||value_array(i__);
    end loop;

    vl1:= substr(owa_util.get_cgi_env('PATH_INFO'),2)||'?rasd=cool'||vl1;


    secCheckCredentials(vu, vp, vo);
    begin
    owa_util.mime_header('text/html', FALSE);
    OWA_COOKIE.SEND('SECCLIENTUSER', nvl(vu,'set values in RASD_CLIENT package'), null);
    OWA_COOKIE.SEND('SECCLIENTOTHER', nvl(vo,'set values in RASD_CLIENT package'), null);


    if vl is not null then
       OWA_UTIL.REDIRECT_URL(vl);
       owa_util.http_header_close;
    else
       OWA_UTIL.REDIRECT_URL(vl1);
       owa_util.http_header_close;
    end if;

    exception when others then
      null;
    end ;

    end if;
  exception when others then
       OWA_UTIL.REDIRECT_URL('your redirection to login page');
       owa_util.http_header_close;
  /*---------*/
  end;
     

  procedure callLog(p_form varchar2, p_content varchar2, p_date timestamp, p_token varchar2 ) is
   begin
    /*Your code for remote logging.*/

    /*---------*/
    null;
   end;

end;
/
